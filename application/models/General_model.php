<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General_model extends MY_Model {
	
    public $before_create = array( 'created_at', 'updated_at' );
    public $before_update = array( 'updated_at' );

	public function __construct()
	{
		parent::__construct();
	}

	public function set_table($table)
	{
		$this->_table = $table;

		return $this;
	}

	public function get_table()
	{
		return $this->_table;
	}

	public function get_all()
   	{
   		if($this->db->field_exists('priority', $this->_table))
   			$this->db->order_by($this->_table.'.priority asc');
   		
   		// $this->db->order_by($this->_table.'.updated_at desc');
   		return parent::get_all();
   	}
	
	public function get_contact_emails()
	{
		$result = array();
		$this->db->where('item','contact_email_from');
		$q = $this->db->get('constants');
		$result['contact_email_from'] = $q->row()->value;
		$this->db->where('item','contact_email_to');
		$q = $this->db->get('constants');
		$result['contact_email_to'] = $q->row()->value;
		return $result;
	}
}

/* End of file general_model.php */
/* Location: ./application/models/general_model.php */