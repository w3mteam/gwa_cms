<?php 
/**
 * statistics 
 */
class Statistics_model extends CI_Model {
	
	
	
	public function add_new_visitor($ip='')
	{
		$this->db->insert('visitors',array('ip'=>$ip));
		if($this->db->affected_rows() > 0){
			return TRUE;
		}
		return FALSE;
	}
	
	
	
	public function is_new_visitor($ip)
	{
		$q = $this->db->get_where('visitors',array('ip'=>$ip));
		if($q->num_rows() > 0){
			return FALSE;
		}
		return TRUE;
	}
	
	public function count_x($x='')
	{
		$this->db->from($x);
		return $this->db->count_all_results();
	}
	
	public function count_articles_x($x='')
	{
		$this->db->from('articles');
		$this->db->where('parent_id !=',-1 );
		return $this->db->count_all_results();
	}
	
	public function get_visitors_count_per_country()
	{
		$q = $this->db->get_where('statistics',array('Name !='=>'Visitors'));
		return $q->result();
	}
	public function get_visitors_count()
	{
		$q = $this->db->get_where('statistics',array('Name'=>'Visitors'));
		$res = $q->result();
		return $res[0]->Count;
	}
	
	public function increase_visitors()
	{
		$this->db->where('Name','Visitors');
		$this->db->set('Count', 'Count+1', FALSE);
		$this->db->update('statistics');
	}
	
	public function increase_products($prod_id)
	{
		$this->db->where('prod_id',$prod_id);
		$this->db->set('prod_views', 'prod_views+1', FALSE);
		$this->db->update('products');
	}
	public function get_most_products_visiting()
	{
		$this->db->select_max('prod_views');
		$q = $this->db->get('products');
		if($q->num_rows() > 0){
			return $q->row();
		}
		return FALSE;
	}
	
	public function set_visitor_country($country='')
	{
		$q = $this->db->get_where('statistics',array('Name'=>$country));
		if($q->num_rows() > 0){
			
			$this->db->where('Name',$country);
			$this->db->set('Count', 'Count+1', FALSE);
			$this->db->update('statistics');
		}
		else {
			$this->db->insert('statistics',array('Name'=>$country,'Count'=>'1'));
		}
	}
	public function set_visitor_browser($browser='')
	{
		$q = $this->db->get_where('browser_statistics',array('browser'=>$browser));
		if($q->num_rows() > 0){
			
			$this->db->where('browser',$browser);
			$this->db->set('count', 'count+1', FALSE);
			$this->db->update('browser_statistics');
		}
		else {
			$this->db->insert('browser_statistics',array('browser'=>$browser));
		}
	}
	
	public function get_browser_statistics()
	{
		$q = $this->db->get('browser_statistics');
		if($q->num_rows() > 0){
			return $q->result();
		}
		return FALSE;
	}
}


?>