<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Advertisement_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function edit($img,$data)
	{
		$this->db->where('Image',$img);
		$this->db->update('advertisement',$data);		
		if ($this->db->affected_rows() > 0)
				return TRUE;
			else	
				return FALSE;
	}
	
	public function get($img)
	{
		$this->db->where('Image',$img);
		$q = $this->db->get('advertisement');
		$data = $q->row();
		
		if ($q->num_rows() > 0)
			{
				return $data->Name;
			}
			else
				return FALSE;
	}
}
