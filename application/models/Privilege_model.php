<?php
/**
 * 
 */
class Privilege_model extends MY_Model {
	
    public $before_create = array( 'created_at', 'updated_at' );
    public $before_update = array( 'updated_at' );

	public function __construct()
	{
		parent::__construct();
	}
	
	
	
	public function get_group_users($g_id='')
	{
		$this->db->where('group_id',$g_id);
		$this->db->join('users','users_groups.user_id = users.id');
		$q = $this->db->get('users_groups');
		if($q->num_rows() > 0){
			$result=array();
			foreach ($q->result() as $key => $user) {
				$result[$user->id] = $user->username;
			}
			return $result;
		}
		return FALSE;
	}
	public function get_profile_id($user_id='')
	{
		$this->db->select('profile_id');
		$q = $this->db->get_where('users',array('id'=>$user_id));
		if($q->num_rows() > 0){
			
			return $q->row()->profile_id;
		}
		return FALSE;
	}
	
	public function get_roles()
	{
		$records = $this->db->get('groups');
		if($records->num_rows() > 0){
			return $records->result();
		}
		else {
			return FALSE;
		}
	}
	public function get_users()
	{
		$records = $this->db->get('users');
		if($records->num_rows() > 0){
			return $records->result();
		}
		else {
			return FALSE;
		}
	}
	public function get_privilage($role_id)
	{
		$query = $this->db->get('priv_cat');
		if($query->num_rows() >0){
			$final_res = array();
			foreach ($query->result() as $key => $cat) {
				if(is_array($role_id)){
					foreach ($role_id as $key => $role) {
						$this->db->or_where('id_role',$role['id']);
					}
				}
				else
					$this->db->where('id_role',$role_id);
				
				$this->db->join('role_privilage','privilage.id=role_privilage.id_privilage');
				$q = $this->db->get_where('privilage',array('id_cat'=>$cat->id));
				if($q->num_rows() > 0 ){
					$final_res[] = array('cat'=>$cat,'priv'=>$q->result());
				}else{
					$final_res[] = FALSE;
				}
			}
			return $final_res;
		}
		return FALSE;
		 
		
	}
	public function get_cat($id_cat)
	{
		$res = $this->db->get_where('priv_cat',array('id'=>$id_cat));
		if($res->num_rows() > 0){
			return $res->first_row();
		}else{
			return FALSE;
		}
	}
	public function get_priv_cat()
	{
		$res = $this->db->get('priv_cat');
		if($res->num_rows() > 0){
			return $res->result();
		}else{
			return FALSE;
		}
	}
	public function get_privilage_by_role_id($role_id)
	{
		$q = $this->db->query("SELECT privilage.privilage_en,privilage.privilage_ar,role_privilage.id_role,role_privilage.id_privilage,role_privilage.id
							  FROM groups INNER JOIN role_privilage ON groups.id = '".$role_id."'AND groups.id=role_privilage.id_role
							  INNER JOIN privilage ON role_privilage.id_privilage = privilage.id ");
		if($q->num_rows() > 0 ){
			
			return $q->result_array();
		}else{
			return FALSE;
		} 
	}
	public function get_role_by_user_id($user_id)
	{
		$q = $this->db->query("SELECT groups.name,users_groups.id
							  FROM users INNER JOIN users_groups ON users.id = '".$user_id."'AND users.id=users_groups.user_id
							  INNER JOIN groups ON users_groups.group_id = groups.id ");
		if($q->num_rows() > 0 ){
			
			return $q->result_array();
		}else{
			return FALSE;
		}  
	}
	public function delete_role_privilage($priv_id,$role_id)
	{
		$res = $this->db->delete('role_privilage', array('id_privilage' => $priv_id,'id_role'=>$role_id));
		if($this->db->affected_rows() > 0){
			return TRUE;
		} else{
			return FALSE;
		}
	}
	public function delete_users_groups($id)
	{
		$res = $this->db->delete('users_groups', array('id' => $id));
		if($this->db->affected_rows() > 0){
			return TRUE;
		} else{
			return FALSE;
		}
	}
	public function get_non_privilage_by_role_id($role_id)
	{
		$q = $this->db->query("SELECT privilage.privilage_en,privilage.privilage_ar,privilage.id
							  FROM groups INNER JOIN role_privilage ON groups.id = '".$role_id."'AND groups.id=role_privilage.id_role
							  RIGHT JOIN privilage ON role_privilage.id_privilage = privilage.id WHERE role_privilage.id_privilage IS NULL");
		
		if($q->num_rows() > 0 ){
			
			return $q->result_array();
		}else{
			return FALSE;
		} 
	}
	public function get_non_role_by_user_id($user_id)
	{
		$q = $this->db->query("SELECT groups.id,groups.name
							  FROM users INNER JOIN users_groups ON users.id = '".$user_id."'AND users.id=users_groups.user_id
							  RIGHT JOIN groups ON users_groups.group_id = groups.id WHERE users_groups.group_id IS NULL");
		if($q->num_rows() > 0 ){
			
			return $q->result_array();
		}else{
			return FALSE;
		} 
	}
	public function add_role_privilage($privs,$id_role)
	{
		$data['id_role'] = $id_role;
		$data['id_privilage'] = $privs;
		$data = array();
		foreach ($privs as $key => $priv) {
			$data[] = array('id_role' => $id_role,'id_privilage'=>$priv );
		}
		 $this->db->insert_batch('role_privilage', $data);
		//$res = $this->db->insert('role_privilage', $data);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function add_users_role($group_id,$user_id)
	{
		$data['user_id'] = $user_id;
		$data['group_id'] = $group_id;
		$res = $this->db->insert('users_groups', $data);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function insert_role($name,$desc)
	{
		$data['name'] = $name;
		$data['description'] = $desc;
		$res = $this->db->insert('groups', $data);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function remove_role($id)
	{
		$res = $this->db->delete('groups', array('id' => $id));
		if($this->db->affected_rows() > 0){
			return TRUE;
		} else{
			return FALSE;
		}
	}
	public function exist_role_privilage($privs,$id_role)
	{
		$q = $this->db->get_where('role_privilage',array('id_role'=>$id_role,'id_privilage'=>$id_privilage));
		if ($q->num_rows()>0) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
    
    
    public function user_has_preivilege($role_id,$privilege_name='')
    {
    	if(is_array($role_id))
			return false;
    	$this->db->where(array('id_role'=>$role_id,'name'=>$privilege_name));
		$this->db->join('role_privilage','role_privilage.id_privilage = privilage.id');
        $q = $this->db->get('privilage');
		if ($q->num_rows()>0) {
			return TRUE;
		}
		else {
			return FALSE;
		}
    }
	public function set_profile_to_user($p_id,$user_id)
	{
		$this->db->where('id',$user_id);
		$this->db->update('users',array('profile_id'=>$p_id));
		if($this->db->affected_rows() > 0){
			return TRUE;
		} else{
			return FALSE;
		}
	}
	public function get_user_group($u_id='')
	{
		$this->db->select('groups.id');
		$this->db->where('user_id',$u_id);
		$this->db->join('users_groups','users_groups.group_id= groups.id');
		$q = $this->db->get_where('groups');
		$num_rows = $q->num_rows();
		if($num_rows > 0 ){
			if($num_rows > 1)
				return $q->result_array();
			else
				return $q->row()->id;
		}else{
			return FALSE;
		}
	}
	public function get_user_id($profile_id='')
	{
		$q = $this->db->get_where('users',array('profile_id'=>$profile_id));
		if ($q->num_rows()>0) {
			return $q->row()->id;
		}
		else {
			return FALSE;
		}
	}
	public function get_privileges_functions($role_id)
	{
		$this->db->where(array('id_role'=>$role_id));
		$this->db->join('role_privilage','role_privilage.id_privilage = privilage.id');
		
		$q = $this->db->get_where('privilage',array('id_cat'=>3));
		if ($q->num_rows()>0) {
			return $q->result();
		}
		else {
			return FALSE;
		}
	}
}
