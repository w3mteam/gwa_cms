<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Newsletter_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function exist($email='')
	{
		$this->db->where( 'Email',$email);
		$q = $this->db->get('newsletter');
		if($q->num_rows() > 0){
			$email  = $q->row();
			if ($email->State == 1){
				return TRUE;
			}
			return FALSE;
		}
		return FALSE;
	}
	public function subscribe($email)
	{
		$this->db->where( 'Email',$email);
		$q = $this->db->get('newsletter');

		if ( !($q->num_rows() > 0))
			{
				$this->db->insert('newsletter', array('Email' => $email , 'State'=>1));
				
			}
		else{
				$this->db->where( 'Email',$email);
				$this->db->update('newsletter', array('Email'=>$email , 'State'=>1));
		}
		if($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	
	public function unsubscribe($email)
	{
		//var_dump($email);die();
		$this->db->where('Email',$email);
		$q = $this->db->get('newsletter');
		if ( $q->num_rows() > 0)
			{
				$this->db->where('Email',$email);
				$this->db->update('newsletter', array('Email'=>$email , 'State'=>FALSE));
				if($this->db->affected_rows() > 0){
					return TRUE;
				}
				else {
					return FALSE;
				}
			}
		else
			return FALSE;
	}
	
	public function get_emails()
	{
		$this->db->where('State',TRUE);
		$this->db->select('Email');
		$q = $this->db->get('newsletter');
		$data = $q->result_array();

		$emails = array();
		foreach ($data as $key => $value)
			$emails[] = $value['Email'];
		
		return $emails;
	}
	
}
