<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_ByID($id)
	{
		$this->db->where('Cat_ID', $id);
		$q = $this->db->get('categories');
		$row = $q->row();

		if ($q->num_rows() > 0)
			return $row;
		else
			return FALSE;
	}
	
	
	public function get_All()
	{
		$q = $this->db->get('categories');
		$resulte = $q->result();

		if ($q->num_rows() > 0)
			return $resulte;
		else
			return FALSE;
	}
	
	
	
	public function insert($data)
	{
		$this->db->insert('categories', $data);
		
		if ($this->db->affected_rows() > 0)
			return  $this->db->insert_id() ;
		else
			return FALSE;
	}
	
	public function edit($id,$data)
	{
		$this->db->where('Cat_ID', $id); 
        $this->db->update('categories', $data);
		
		if ($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}

	public function delete($id)
	{
		$this->db->where('Cat_ID', $id);
		return $this->db->delete('categories');
	}

}
