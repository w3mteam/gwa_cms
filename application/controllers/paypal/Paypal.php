<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Paypal extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('general_model', 'order_m');
   		$this->order_m->set_table('order');

		// Load helpers
		$this->load->helper('url');
		
		// Load PayPal library
		$this->config->load('paypal_config');
		//$this->load->library('paypal/paypal_adaptive');
		
		$config = array(
			'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
			'APIUsername' => $this->config->item('APIUsername'), 	// PayPal API username of the API caller
			'APIPassword' => $this->config->item('APIPassword'), 	// PayPal API password of the API caller
			'APISignature' => $this->config->item('APISignature'), 	// PayPal API signature of the API caller
			'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
			'APIVersion' => $this->config->item('APIVersion'), 		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
			'DeviceID' => $this->config->item('DeviceID'), 
			'ApplicationID' => $this->config->item('ApplicationID'), 
			'DeveloperEmailAccount' => $this->config->item('DeveloperEmailAccount')
		);
		
		if($config['Sandbox'])
		{
			// Show Errors
			error_reporting(E_ALL);
			ini_set('display_errors', '1');	
		}
		
		$this->load->library('paypal/Paypal_adaptive', $config);	
	}
	
	
	function index()
	{
		$this->pay_view();
	}
	
	//view form to add email for pay payment
	public function pay_view($order_id='')
	{
		$data['order_id']=$order_id;
		$this->load->view('paypal/pay_view',$data);
	}

	// function to process payment with paypal account
	function pay($order_id='')
	{

		if ( $order_id == '' || !is_numeric($order_id))
			redirect('main');

		$this->form_validation->set_rules('email','email','valid_email|required');
		if ($this->form_validation->run() === FALSE){

			redirect('paypal/paypal/pay_view/'.$order_id);
		}

		$CancelURL = site_url('paypal/paypal/canceled/'.$order_id);
		$ReturnURL=site_url('paypal/paypal/approved/'.$order_id);
		$amount='500';
		$CurrencyCode='USD';
		//$SenderEmail='shalabi.eng-buyer@gmail.com';
		$SenderEmail=$this->input->post('email');
		//$ReciverEmail='shalabi.eng-seller@gmail.com';
		$ReciverEmail= $this->config->item('ReciverEmail');


		// Prepare request arrays
		$PayRequestFields = array(
								'ActionType' => 'PAY', 								// Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
								'CancelURL' => $CancelURL, 									// Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
								'CurrencyCode' => $CurrencyCode, 								// Required.  3 character currency code.
								'FeesPayer' => '', 									// The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
								'IPNNotificationURL' => '', 						// The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
								'Memo' => '', 										// A note associated with the payment (text, not HTML).  1000 char max
								'Pin' => '', 										// The sener's personal id number, which was specified when the sender signed up for the preapproval
								'PreapprovalKey' => '', 							// The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.  
								'ReturnURL' => $ReturnURL, 									// Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
								'ReverseAllParallelPaymentsOnError' => '', 			// Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
								'SenderEmail' => $SenderEmail, 								// Sender's email address.  127 char max.
								'TrackingID' => ''									// Unique ID that you specify to track the payment.  127 char max.
								);
								
		$ClientDetailsFields = array(
								'CustomerID' => '', 								// Your ID for the sender  127 char max.
								'CustomerType' => '', 								// Your ID of the type of customer.  127 char max.
								'GeoLocation' => '', 								// Sender's geographic location
								'Model' => '', 										// A sub-identification of the application.  127 char max.
								'PartnerName' => ''									// Your organization's name or ID
								);
								
		$FundingTypes = array('ECHECK', 'BALANCE', 'CREDITCARD');
		
		$Receivers = array();
		$Receiver = array(
						'Amount' => $amount, 											// Required.  Amount to be paid to the receiver.
						'Email' => $ReciverEmail, 												// Receiver's email address. 127 char max.
						'InvoiceID' => '', 											// The invoice number for the payment.  127 char max.
						'PaymentType' => 'SERVICE', 										// Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
						'PaymentSubType' => '', 									// The transaction subtype for the payment.
						'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
						'Primary' => ''												// Whether this receiver is the primary receiver.  Values are:  TRUE, FALSE
						);
		array_push($Receivers,$Receiver);
		
		$SenderIdentifierFields = array(
										'UseCredentials' => ''						// If TRUE, use credentials to identify the sender.  Default is false.
										);
										
		$AccountIdentifierFields = array(
										'Email' => '', 								// Sender's email address.  127 char max.
										'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '')								// Sender's phone number.  Numbers only.
										);
										
		$PayPalRequestData = array(
							'PayRequestFields' => $PayRequestFields, 
							'ClientDetailsFields' => $ClientDetailsFields, 
							'FundingTypes' => $FundingTypes, 
							'Receivers' => $Receivers, 
							'SenderIdentifierFields' => $SenderIdentifierFields, 
							'AccountIdentifierFields' => $AccountIdentifierFields
							);	
							
		$PayPalResult = $this->paypal_adaptive->Pay($PayPalRequestData);
		
		if(!$this->paypal_adaptive->APICallSuccessful($PayPalResult['Ack']))
		{
			$errors = array('Errors'=>$PayPalResult['Errors']);
			//$this->load->view('paypal_error',$errors);
		}
		else
		{
			// Successful call.  Load view or whatever you need to do here.	
       		$this->order_m->update_by(array('order_id'=>$order_id),array('order_pay_key' =>$PayPalResult['PayKey'] ,'order_payment_status'=>$PayPalResult['PaymentExecStatus']));

			
			redirect($PayPalResult['RedirectURL']);
			//echo '<pre />';
			//print_r($PayPalResult);
		}
	}


	//called when user cancel payment in paypal account
	public function canceled($order_id='')
	{

		if ( $order_id == '' || !is_numeric($order_id))
			redirect('main');

		$order=$this->order_m->get_by(array('order_id' => $order_id));
		if ( !$order)
			redirect('main');
		
		$PayKey=$order->order_pay_key;
		$PayPalResult=$this->Payment_details($PayKey);

		$this->order_m->update_by(array('order_id'=>$order_id,'order_pay_key' =>$PayPalResult['PayKey']),array('order_payment_status'=>$PayPalResult['Status']));

		echo '<pre />';
		print_r($PayPalResult);

	}

	//called when user approve payment in paypal account
	public function approved($order_id='')
	{		
		 // get paykey from database by order id 
		if ( $order_id == '' || !is_numeric($order_id))
			redirect('main');

		$order=$this->order_m->get_by(array('order_id' => $order_id));
		if ( !$order)
			redirect('main');

		$PayKey=$order->order_pay_key;
		$PayPalResult=$this->Payment_details($PayKey);

		$this->order_m->update_by(array('order_id'=>$order_id,'order_pay_key' =>$PayPalResult['PayKey']),array('order_payment_status'=>$PayPalResult['Status']));

		echo '<pre />';
		print_r($PayPalResult);

	}

	//get payment details
	function Payment_details($PayKey='')
	{
		//$PayKey='AP-3BJ02656FF421202C';

		// Prepare request arrays
		$PaymentDetailsFields = array(
									'PayKey' => $PayKey, 							// The pay key that identifies the payment for which you want to retrieve details.  
									'TransactionID' => '', 						// The PayPal transaction ID associated with the payment.  
									'TrackingID' => ''							// The tracking ID that was specified for this payment in the PayRequest message.  127 char max.
									);
									
		$PayPalRequestData = array('PaymentDetailsFields' => $PaymentDetailsFields);
		$PayPalResult = $this->paypal_adaptive->PaymentDetails($PayPalRequestData);
		
		if(!$this->paypal_adaptive->APICallSuccessful($PayPalResult['Ack']))
		{
			$errors = array('Errors'=>$PayPalResult['Errors']);
			$this->load->view('paypal/paypal_error',$errors);
		}
		else
		{
			// Successful call.  Load view or whatever you need to do here.
			//echo '<pre />';
			//print_r($PayPalResult);
			return $PayPalResult;
		}

	}

	// function Convert_currency()
	// {
	// 	// Prepare request arrays
	// 	$BaseAmountList = array();
	// 	$BaseAmountData = array(
	// 							'Code' => 'USD', 						// Currency code.
	// 							'Amount' => '100.00'						// Amount to be converted.
	// 							);
	// 	array_push($BaseAmountList, $BaseAmountData);
		
	// 	$ConvertToCurrencyList = array('BRL', 'AUD', 'CAD');			// Currency Codes
		
	// 	$PayPalRequestData = array(
	// 							'BaseAmountList' => $BaseAmountList, 
	// 							'ConvertToCurrencyList' => $ConvertToCurrencyList
	// 							);	
								
	// 	$PayPalResult = $this->paypal_adaptive->ConvertCurrency($PayPalRequestData);
		
	// 	if(!$this->paypal_adaptive->APICallSuccessful($PayPalResult['Ack']))
	// 	{
	// 		$errors = array('Errors'=>$PayPalResult['Errors']);
	// 		$this->load->view('paypal_error',$errors);
	// 	}
	// 	else
	// 	{
	// 		// Successful call.  Load view or whatever you need to do here.	
	// 		$data = array('PayPalResult'=>$PayPalResult);
	// 		$this->load->view('convert_currency',$data);
	// 	}	
	// }
	
	
	
	
}
