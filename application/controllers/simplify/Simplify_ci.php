<?php
/**
*  
*/
class Simplify_ci extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();


		$this->load->model('general_model', 'order_m');
   		$this->order_m->set_table('order');

		//$this->load->helper('url');
		$this->config->load('simplify_config');
	}

	//view form to add email for pay payment
	public function pay_view($order_id='')
	{
		$data['publicKey'] = $this->config->item('publicKey');
		$this->load->view('simplify/pay_view',$data);
	}


	//function to process payment with paypal account for master and visa card 
	public function pay($order_id='')
	{

		//die($order_id);
		if ( $order_id == '' || !is_numeric($order_id))
			redirect('main');
		

		$order=$this->order_m->get_by(array('order_id' => $order_id));
		//var_dump($order);die();
		if ( !$order)
			redirect('main');
		

		require_once(APPPATH."third_party/simplify/lib/Simplify.php");
		 
		Simplify::$publicKey = $this->config->item('publicKey');
		Simplify::$privateKey =  $this->config->item('privateKey');

		$amount=1000; // amount 
		$description='payment description'; //description 
		$reference='1'; //user id optional 
		$currency='USD'; // currency 
		$payment = Simplify_Payment::createPayment(array(
		        'amount' => $amount,
		        'token' => $this->input->post('simplifyToken'),
		        'description' => $description,
		        'reference' => $reference,
		        'currency' => $currency
		));
		 
		if ($payment->paymentStatus == 'APPROVED') { //approve payment 
			$this->order_m->update_by(array('order_id'=>$order_id),array('order_payment_status'=>'COMPLETED'));
		    echo "Payment approved\n";
		    //echo $payment;
		}else{	
			$this->order_m->update_by(array('order_id'=>$order_id),array('order_payment_status'=>'REJECTED'));									// reject payment
		 	echo "Payment rejected\n";              
		   // echo $payment;
		}
	}
}