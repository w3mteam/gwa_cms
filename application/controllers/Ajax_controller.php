<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_controller  extends CI_Controller {
	
	public function __construct()
	{
parent::__construct();
		
		$this->load->model('advertisement_model');
			
		$this->load->model('general_model', 'articles_m');
		$this->articles_m->set_table('articles');
		
		$this->load->model('general_model', 'galleries_m');
		$this->galleries_m->set_table('galleries');
		
		$this->load->model('general_model', 'constants_m');
		$this->constants_m->set_table('constants');
	
		$this->load->model('general_model', 'category_m');
        $this->category_m->set_table('categories');
		
		$this->load->model('general_model', 'products_m');
        $this->products_m->set_table('products');
		
		$this->load->model('general_model', 'album_m');
        $this->album_m->set_table('albums');
		
		$this->load->model('general_model','style_m');
		$this->style_m->set_table('product_style');
		
		$this->load->model('general_model', 'album_galleries_m');
        $this->album_galleries_m->set_table('album_gallery');
		
		$this->load->model('general_model', 'cart_temp_m');
        $this->cart_temp_m->set_table('cart_temp');
		
		$this->load->model('general_model', 'order_m');
        $this->order_m->set_table('order');
		
		$this->load->model('general_model', 'order_product_m');
        $this->order_product_m->set_table('order_product');
		
		$this->load->model('general_model', 'brands_m');
        $this->brands_m->set_table('brands');
		
		$this->load->helper('youtube');

		// Language
		//die($this->uri->segment(2));
		if(!$this->session->userdata('lang'))
			$this->session->set_userdata('lang', 'arabic');
		//$this->session->set_userdata('lang', 'english');
	
        if($this->uri->segment(2) == 'lang'){
            switch ($this->uri->segment(3)) {
                case 'en': $this->session->set_userdata('lang', 'english'); break;
                case 'ar': $this->session->set_userdata('lang', 'arabic'); break;
            }
            redirect('main');
        }
	
	
		$this->config->set_item('language', $this->session->userdata('lang'));

		$this->load->language('site', $this->config->item('language'));
		//$this->load->language('dashboard', $this->config->item('language'));
		$this->load->language('ion_auth', $this->config->item('language'));
		$this->load->language('auth', $this->config->item('language'));
		
		$this->_lang = substr($this->session->userdata('lang'), 0, 2);
		//$this->_lang = 'ar';
		$this-> data['lang'] = $this->_lang;
		$this->data['dir'] = $this->_lang=='ar'?'rtl':'ltr';
		
		$main_categories = $this->category_m->get_many_by('parent_id', 0);
		foreach ($main_categories as $item) {
			$item->sub_categories = $this->category_m->get_many_by('parent_id', $item->Cat_ID);
		}
		$this->data['main_categories'] = $main_categories;
		$this->data['styles']=$this->style_m->get_all();
		
		//static pages like news and culture
		$main_articles = $this->articles_m->get_many_by('parent_id', 0);
		foreach ($main_articles as $item) {
			$item->sub_articles = $this->articles_m->get_many_by('parent_id', $item->id);
		}
		$this->data['main_articles'] = $main_articles;
		

		$this->data['left_advertisement']=$this->advertisement_model->get('Left');		
		$this->data['right_advertisement']=$this->advertisement_model->get('Right');
		
		//images and videos albums
		 $this->data['image_albums']=$this->album_m->get_many_by('alb_type','image');
		 $this->data['video_albums']=$this->album_m->get_many_by('alb_type','video'); 
		 
		 
		 //social network
        $fields = array(
                'contact_email_from',
                'contact_email_to',
                'sm_facebook',
                'sm_twitter',
                'sm_youtube',
                'sm_google_plus',
                'sm_linked_in',
                'sm_instagram'
            );
        foreach ($fields as $item) {
            $this->data[$item] = $this->constants_m->get_by('item', $item)->value;
        }
		
		$this->data['about_us']=$this->articles_m->get_by('id',22); 
	}
	
	public function get_sub_categories($id='')
	{
		//var_dump($id);die();
		if(!$id || !is_numeric($id))
			redirect('main');
		else{
			$cats=$this->category_m->get_many_by('parent_id',$id);
			foreach ($cats as $key => $cat) {
				$cat->cat_name=$cat->{"Cat_Name_{$this->_lang}"};
			}
			echo json_encode($cats);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */