<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('privileges');
		$this->load->model('privilege_model');	
			
		$this->load->model('advertisement_model');
			
		$this->load->model('general_model', 'articles_m');
		$this->articles_m->set_table('articles');
		
		$this->load->model('general_model', 'galleries_m');
		$this->galleries_m->set_table('galleries');
		
		$this->load->model('general_model', 'constants_m');
		$this->constants_m->set_table('constants');
	
		$this->load->model('general_model', 'category_m');
        $this->category_m->set_table('categories');
		
		$this->load->model('general_model', 'products_m');
        $this->products_m->set_table('products');
		
		$this->load->model('general_model', 'product_galleries_m');
        $this->product_galleries_m->set_table('product_gallery');
		
		$this->load->model('general_model', 'album_m');
        $this->album_m->set_table('albums');
		
		$this->load->model('general_model','style_m');
		$this->style_m->set_table('product_style');
		
		$this->load->model('general_model', 'album_galleries_m');
        $this->album_galleries_m->set_table('album_gallery');
		
		$this->load->model('general_model', 'cart_temp_m');
        $this->cart_temp_m->set_table('cart_temp');
		
		$this->load->model('general_model', 'order_m');
        $this->order_m->set_table('order');
		
		$this->load->model('general_model', 'order_product_m');
        $this->order_product_m->set_table('order_product');
		
		$this->load->model('general_model', 'brands_m');
        $this->brands_m->set_table('brands');
		
		$this->load->model('general_model', 'towns_m');
        $this->towns_m->set_table('town');
		
		$this->load->model('general_model', 'group_m');
        $this->group_m->set_table('groups');
		
		$this->load->model('general_model','educations_m');
		$this->educations_m->set_table('education');
		$this->load->helper('youtube');
		$this->load->helper('visitors');
		// Language
		//die($this->uri->segment(2));
		if(!$this->session->userdata('lang'))
			$this->session->set_userdata('lang', 'arabic');
		//$this->session->set_userdata('lang', 'english');
	
        if($this->uri->segment(2) == 'lang'){
            switch ($this->uri->segment(3)) {
                case 'en': $this->session->set_userdata('lang', 'english'); break;
                case 'ar': $this->session->set_userdata('lang', 'arabic'); break;
            }
            redirect($this->input->get('url'));
        }
	
	
		$this->config->set_item('language', $this->session->userdata('lang'));

		$this->load->language('site', $this->config->item('language'));
		//$this->load->language('dashboard', $this->config->item('language'));
		$this->load->language('ion_auth', $this->config->item('language'));
		$this->load->language('auth', $this->config->item('language'));
		
		$this->_lang = substr($this->session->userdata('lang'), 0, 2);
		//$this->_lang = 'ar';
		$this-> data['lang'] = $this->_lang;
		$this->data['dir'] = $this->_lang=='ar'?'rtl':'ltr';
		
		$main_categories = $this->category_m->get_many_by('parent_id', 0);
		foreach ($main_categories as $item) {
			$item->sub_categories = $this->category_m->get_many_by('parent_id', $item->Cat_ID);
		}
		$this->data['main_categories'] = $main_categories;
		$this->data['styles']=$this->style_m->get_all();
		
		//static pages like news and culture
		$main_articles = $this->articles_m->get_many_by('parent_id', 0);
		foreach ($main_articles as $item) {
			$item->sub_articles = $this->articles_m->get_many_by('parent_id', $item->id);
		}
		$this->data['main_articles'] = $main_articles;
		

		$this->data['left_advertisement']=$this->advertisement_model->get('Left');		
		$this->data['right_advertisement']=$this->advertisement_model->get('Right');
		
		//images and videos albums
		 $this->data['image_albums']=$this->album_m->get_many_by('alb_type','image');
		 $this->data['video_albums']=$this->album_m->get_many_by('alb_type','video'); 
		 
		 
		 //social network
        $fields = array(
                'contact_email_from',
                'contact_email_to',
                'sm_facebook',
                'sm_twitter',
                'sm_youtube',
                'sm_google_plus',
                'sm_linked_in',
                'sm_instagram',  
                'shop_now' ,
                'shop_now_slider' ,
                'about_us_url' , 
                'about_us_title',
                'is_active_menu_images_albume',  
                'is_active_menu_video_albume',  
                'is_active_menu_products',  
                'is_active_menu_contact_us',  
                'category_levels',
                'article_levels',
            );
        foreach ($fields as $item) {
            $this->data[$item] = $this->constants_m->get_by('item', $item)->value;
        }

		$this->data['about_us_text'] = $this->constants_m->get_by('item', 'about_us_text_'.$this->_lang)->value;

		$this->data['brands_header'] = $this->constants_m->get_by('item', 'brands_header_'.$this->_lang)->value;
		$this->data['unit'] = $this->constants_m->get_by('item', 'unit_'.$this->_lang)->value;
		$this->data['min_price'] = $this->constants_m->get_by('item', 'min_price_'.$this->_lang)->value;
		$this->data['max_price'] = $this->constants_m->get_by('item', 'max_price_'.$this->_lang)->value;
		$this->data['avarge_price'] = ($this->data['min_price'] + $this->data['max_price'])/2;
		$this->data['about_us']=$this->articles_m->get_by('id',1); 
				
		$this->data['brands']=$this->brands_m->get_all();
		
		$this->data['towns']=$this->towns_m->get_all();
		$this->data['educations']=$this->educations_m->get_all();
		$this->data['contact_us_id']=1;
		$this->load->library('recaptcha');
		$this->data['recaptcha_html'] = $this->recaptcha->recaptcha_get_html();
		
		$this->cart->product_name_rules = '^.';

	}
	
	public function destroy()
	{
		$this->session->unset_userdata('visited');
		$this->view = false;
	}
	public function index()
	{
		$this->data['slideshow'] = $this->articles_m->get_many_by('parent_id', -1);
		
		$filter_categories = $this->category_m->get_many_by('parent_id', 0);
		foreach ($filter_categories as $item) {
			$this->db->limit(8);
			$item->products = $this->products_m->get_many_by('prod_main_cat_id', $item->Cat_ID);
		}
		$this->data['filter_categories'] = $filter_categories;
		
		// $this->db->limit(6);
  //  		$this->db->order_by('updated_at desc');
		// $this->data['man_products']=$this->products_m->get_many_by('prod_main_cat_id' , 13);
		
		// $this->db->limit(6);
  //  		$this->db->order_by('updated_at desc');
		// $this->data['women_products']=$this->products_m->get_many_by('prod_main_cat_id' , 14);
		
		$this->db->limit(8);
   		$this->db->order_by('updated_at desc');
		//$where='prod_price_discount_'.$this->_lang.' <> "" OR prod_price_discount <> ""';
		$this->data['news_products']=$this->products_m->get_all();
	}

	// view products by category id 
	public function products($id='')
	{
		if( $id=='' || !is_numeric($id))
			redirect();
		else{
			
			$category=$this->category_m->get_by('Cat_ID',$id);
			
			if(!$category)
				redirect();
			// Update Views
			$this->category_m->update_by(array('Cat_ID' => $category->Cat_ID), array('views' => $category->views+1));
			
			
			$this->data['products']=$this->products_m->get_many_by('prod_cat_id',$id);
			$this->data['category']=$this->category_m->get_by('Cat_ID',$id);
		}	
	}
	
	// view product details  by  product id
	public function product($id='')
	{
		if( $id=='' || !is_numeric($id))
			redirect();
		else{
			$product=$this->products_m->get_by('prod_id',$id);
			if (!$product)
				redirect();
			$this->data['product']=$product;
			$this->data['category']=$this->category_m->get_by('Cat_ID',$this->data['product']->prod_cat_id);
			$this->data['product_gallery']=$this->product_galleries_m->get_many_by(array('product_id' => $id));
			

		}	
	}
	
	// view album details  by  album id
	public function view_album($id='')
	{
		if( $id=='' || !is_numeric($id))
			redirect();
		else{
			$this->data['gallery']=$this->album_galleries_m->get_many_by('album_id',$id);
			$this->data['album']=$this->album_m->get_by('id',$id);
			$this->data['page'] = 'album';
		}	
	}
	
	// public function get_sub_categories_by_category($id='')
	// {
		// if( $id=='' || !is_numeric($id))
			// redirect();
		// else{
			// $products=$this->category_m->get_many_by('parent_id',$id);
		// }	
	// }
	
	
	// ajax
	public function get_sub_categories($id='')
	{
		if(!$id || !is_numeric($id))
			redirect();
		else{
			$cats=$this->category_m->get_many_by('parent_id',$id);
			foreach ($cats as $key => $cat) {
				$cat->cat_name=$cat->{"Cat_Name_{$this->_lang}"};
			}
			echo json_encode($cats);
		}
	}
	
	// view aricle details by article id 
	public function article(){
		$item_id = $this->uri->segment(3);
		if (!$article = $this->articles_m->get($item_id))
			redirect();

		if ($article->parent_id < 0)
			redirect();
		
		$this->data['parent_article'] = $this->articles_m->get($article->parent_id);
		
		$this -> load -> library('pagination');
		$config['base_url'] = site_url('site/article/'.$item_id);
		$config['total_rows'] = $this->articles_m->count_by('parent_id', $article->id);
		$config['per_page'] = 10;
		$config['uri_segment'] = 4;

		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li class="page">';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li class="page">';
		$config['next_tag_close'] = '</li>';

		$this -> pagination -> initialize($config);

		$this->data['links'] = $this -> pagination -> create_links();

		$this->db->limit($config['per_page'], $this -> uri -> segment(4));

		$article->sub_articles = $this->articles_m->get_many_by('parent_id', $article->id);

		$this->data['article'] = $article;

		$this->data['article_gallery'] = $this->galleries_m->get_many_by('article_id', $article->id);


		// Recent Articles
		$this->db->limit(5);
		$this->db->where('id !=', $article->id);
		$this->data['related_articles'] =  $this->articles_m->get_many_by('parent_id', $article->parent_id);

		// Update Views
		$this->articles_m->update($article->id, array('views' => $article->views+1));
		
	}

	// subscribe email  in mail list 
	public function subscribe()
	{
		$this->form_validation->set_rules('email', '', 'required|valid_email');
		
	 	if (!$this->form_validation->run() == true)
		{
			echo lang('unvalide_email');
			
		}
		else 
		{
			$email= $this->input->post('email',TRUE);
			$email = strtolower($email);
			$this->load->model('newsletter_model');
			if($this->newsletter_model->exist($email)){
				$this->view = FALSE;
				echo lang('email_exist');
				return;
			}
			if($this->newsletter_model->subscribe($email)){
				echo lang('subscrib_succcess');
			}
			else {
				echo lang('subscrib_failed');
			}
		}
		$this->view = FALSE;
	}


	// recieve message from contact us page  
	public function contact_us(){	
		$this->data['contact_us_mail']=$this->constants_m->get_by(array('item' =>'contact_us_mail'))->value;
		$this->data['contact_us_phone']=$this->constants_m->get_by(array('item' =>'contact_us_phone'))->value;;
		
		$this->form_validation->set_rules('name','Name','required|trim|xss_clean');
		$this->form_validation->set_rules('email','email','required|trim|xss_clean');
		$this->form_validation->set_rules('message', 'message', 'required|xss_clean');
		if($this->form_validation->run()=== True){
			//die('valid');
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$message = 'Name : '.$name."\n".$this->input->post('message');
			$contact_emails['contact_email_from'] = $this->constants_m->get_by('item','contact_email_from')->value;
			$contact_emails['contact_email_to'] = $this->constants_m->get_by('item','contact_email_to')->value;

            if(@$this->send_email($email,$contact_emails['contact_email_to'],'Contact-From:'.$email,$message)){
				$this->data['message'] = lang('message_sent_success');
            }
            else 
            {
                $this->data['message'] = lang('message_sent_faild');
            }
			
		}
		else {
			$this->data['message'] =  validation_errors();
			
		}
	}

	// view cart  for user
	public function view_cart($product_id='')
	{

		if(!$this->ion_auth->logged_in()) redirect('main');
		
		if($this->has_privilage($this->session->userdata('user_id'),"view_cart")){
				
			//var_dump($_POST);
			if ($product_id != '' && is_numeric($product_id) ){
					$this->update_item($product_id,1);
					$this->session->set_flashdata('messages_success', lang('add_to_cart_success'));
					redirect('main/view_cart');

			}elseif ( isset ($_POST['update'])  && $_POST['update']=='submit'){
					$in_cart=$this->input->post('in_cart');
					$in_cart=json_decode($in_cart);
					var_dump($in_cart);die();
					$this->update_cart($in_cart);
					redirect('main');
			}

		}else{
			redirect('main');
		}
	
	}
	//cart 
	
	// test if product in cart or not in cart 
	private function in_cart($id='')
	{
		if(!$this->ion_auth->logged_in()) redirect('main');
		
		foreach ($this->cart->contents() as $items)
			if ($id == $items['id'])
					return $items;
			
		return FALSE;
	}
	
	
	// update item in cart 
	private function update_item($id='',$qty='1')
	{

		if(!$this->ion_auth->logged_in()) redirect('main');
		
		if ($id=='' || !is_numeric($id) )
			redirect('main');
		else{
			//check if the product in cart	
			//$lang=get_current_lang_letter();		
			$item=$this->in_cart($id);
			if (!$item){

				
				//$product=$this->bill_model->get_product($id);
				$product=$this->products_m->get_by('prod_id',$id);
				
				 $cart_price='';
				 $cart_discount_price=0;
				 $prod_price=$product->prod_price;
				 $prod_price_discount=$product->prod_price_discount;
				 $prod_price_lang=$product->{"prod_price_{$this->_lang}"} ;
				 $prod_price_discount_lang=$product->{"prod_price_discount_{$this->_lang}"} ;
				 
				 // if ( $prod_price){ 
     //        				 if ($prod_price_discount){
     //        				 	$price=$prod_price_discount;
     //    					 }else{
     //    					 	$price=$prod_price;
     //    					 }
            				
     //        	}else{
     //        			if ($prod_price_discount_lang){
     //        				$price=$prod_price_discount_lang;
     //    				 }else{
     //    				 	$price=$prod_price_lang;
     //        			 }
					// }
					
				if ( $prod_price){ 
						$cart_price=$prod_price;
        				$cart_discount_price=$prod_price_discount;		
            	}else{
            			$cart_price=$prod_price_discount_lang;
        				$cart_discount_price=$prod_price_discount_lang;	
					}
				$data = array(
		               'id'      => $id,
		               'qty'     => 1,
		               'price'   => $cart_price,
		               'price_discount' => $cart_discount_price,
		             //  'unit' => $this->data['unit'] ,
		               'name'    =>$product->{"prod_name_{$this->_lang}"}, 
		               'description'    =>$product->{"prod_description_{$this->_lang}"}, 
		               'image' 	 => base_url().'assets/uploads/products/'.$product->prod_image,             
		           	 );	
				
				if ($this->cart->insert($data)){
					
					$info = array(
		               'product_id'      => $id,
		               'user_id'		=>$this->session->userdata('user_id'),
		               'qty'     => 1,
		               'price'   => $cart_price,  
		               'price_discount' => $cart_discount_price,
		               //'unit' => $this->data['unit'] ,          
		           	 );	
					$this->cart_temp_m->insert($info);
					
					//$this->session->set_flashdata('messages_success', lang('update_cart_success'));
				
					//echo $this->lang->line('update_success');
				}else{
					//$this->session->set_flashdata('messages_warning', lang('update_cart_failed'));
					//echo $this->lang->line('update_failed');
				}
					
			}
			else{

				$item['qty']=$qty;
				$this->cart->update($item);
				
				//update database
				if ( $qty == '0' ){
					$this->cart_temp_m->delete_by(array('user_id'=> $this->session->userdata('user_id') ,'product_id' => $id));
				}else{
					$info = array('qty'=>$qty);	
					$where=array('user_id' => $this->session->userdata('user_id'));
					$this->cart_temp_m->update_by($where,$info);
				}

			

					
				//$this->session->set_flashdata('messages_success', lang('update_cart_success'));
			}
			
		}
	}


	// update many items  in cart  by call update item function
	public function update_cart()
	{
		if(!$this->ion_auth->logged_in()) redirect('main');
		
		 if($this->has_privilage($this->session->userdata('user_id'),"update_cart")){
		
				$in_cart=$this->input->post('in_cart');
				// var_dump($in_cart);
				// $in_cart=json_decode($in_cart);
				// var_dump($in_cart);die();

				foreach ($in_cart as $key => $value) {
					$this->update_item($value[0],$value[1]);
				}
				echo lang('update_cart_success');
				//$this->session->set_flashdata('messages_success', lang('update_cart_success'));
				$this->view=false;
			//redirect('main/view_cart');
		}else{
			redirect('main');	
		}
	}

	// delete cart 
	public function delete_cart()
	{
		if(!$this->ion_auth->logged_in()) redirect('main');
		
		 if($this->has_privilage($this->session->userdata('user_id'),"delete_cart")){
		
		$this->cart_temp_m->delete_by(array('user_id'=> $this->session->userdata('user_id')));
		$this->cart->destroy();
		$this->session->set_flashdata('messages_success', lang('delete_cart_success'));
		redirect('main/view_cart');
		
		}else{
			redirect('main');	
		}
	}

	// delete_item in cart
	public function delete_item_in_cart($id='')
	{
		if(!$this->ion_auth->logged_in()) redirect('main');
		
		if ($id=='' || !is_numeric($id) )
			redirect('main');
		
		//check if the product in cart		
		$item=$this->in_cart($id);
		if ($item){

			$this->update_item($id,'0');
			$this->session->set_flashdata('messages_success', lang('delete_item_success'));
		}


		redirect('main/view_cart');

	}


	// checout cart by user  ( insert buy order )
	public function checkout_cart()
	{

		if(!$this->ion_auth->logged_in()) redirect('main');
		
		if($this->has_privilage($this->session->userdata('user_id'),"checkout_cart")){
			 	
		if ($this->cart->contents()){
			$info=array('total' => $this->cart->total(),
					'user_id' => $this->session->userdata('user_id'),
					'status'=> "pending"
					);

			$order_id=$this->order_m->insert($info);
			foreach ($this->cart->contents() as $key => $items) {
				$data=array('product_id' => $items['id'],
							'qty' =>  $items['qty'],
							'price' => $items['price'],
							'price_discount' => $items['price_discount'],
							'order_id' => $order_id,
							);
				$this->order_product_m->insert($data);
				$product=$this->products_m->get_by( array('prod_id' => $items['id']));
				$this->products_m->update_by(array('prod_id' => $items['id']), array('prod_order_count' => $product->prod_order_count+1));
			}


			

			//send email 

			//$from = "info@richtime.tic-push.com";
			//$to =$this->session->userdata('email');
			$from= $this->constants_m->get_by('item', 'order_email_from')->value;
			$to =$this->constants_m->get_by('item', 'order_email_to')->value;

			$msg='';
			foreach ($this->cart->contents() as $key => $items) {
				$msg = "".lang('product_name').": ".$items['name']." </ br>
	                    ".lang('qty').": ".$items['qty']."<br>
	                    ".lang('price').":".$items['price']."
	                    	<hr>";
			}
			$msg.=lang('user_email').' : '.$this->session->userdata('email');


			$subject= lang('product_order');	
			$this->session->set_flashdata('messages_success', lang('checkout_cart_success'));
			
			@$this->send_email($from,$to,$subject,$msg);

			
		} 
			
		$this->cart_temp_m->delete_by(array('user_id'=> $this->session->userdata('user_id')));
		$this->cart->destroy();
		
		redirect('main/view_cart');
				
		}else{
			redirect('main');	
		}
	}
	
	
	// helper function for send email 
	public function send_email($from,$to,$subject,$msg)
	{
		$this->config->set_item('language', 'english');

		 $config['protocol'] = 'sendmail';
		 $config['mailpath'] = '/usr/sbin/sendmail';
		// $config['charset'] = 'utf-8';
		 $config['wordwrap'] = TRUE;
		 $config['mailtype'] = 'html';


		$this -> load -> library('email');

		$this -> email -> from($from);
		$this -> email -> to($to);
		$this -> email -> subject($subject);
		$this -> email -> message($msg);

		$result=  @$this -> email -> send();
		
		$this->config->set_item('language', $this->session->userdata('lang'));
		return $result;
	}

	//  fillter products and search for product based by parameter 
	public function shop($type='')
	{
		$this->form_validation->set_rules('gender_id', lang('select_gender'), 'numeric');
		$this->form_validation->set_rules('category_id',lang('select_category'), 'numeric');
		$this->form_validation->set_rules('style_id', lang('select_style'), 'numeric');
		$this->form_validation->set_rules('min_price', lang('min_price'), 'numeric');
		$this->form_validation->set_rules('max_price', lang('max_price'), 'numeric');
		
		if ($this->form_validation->run() == true) // fillter product 
		{
			$gender_id = $this->input->post('gender_id');
			$category_id = $this->input->post('category_id');
			$style_id = $this->input->post('style_id');
			$min_price = $this->input->post('min_price');
			$max_price = $this->input->post('max_price');
			

			$where='';
			$cod='';
			if ($gender_id && $gender_id !=0){
				$where.=$cod.'prod_main_cat_id = '.$gender_id;
				$cod=' and ';
			}
			if ($category_id && $category_id !=0){
				$where.=$cod.'prod_cat_id = '.$category_id;
				$cod=' and ';
			}
			if ($style_id && $style_id !=0 ){
				$where.=$cod.'prod_style_id = '.$style_id;
				$cod=' and ';
			}
			
			if ($min_price){
				$where.=$cod.'prod_price_'.$this->_lang.' >= '.$min_price;
				$cod=' and ';
			}
			if ($max_price){
				$where.=$cod.'prod_price_'.$this->_lang.' <= '.$max_price;
				$cod=' and ';
			}

			$products=$this->products_m->get_many_by($where);
			$this->data['products']=$products;
		}elseif($type==1){  						// latest products
			//$this->db->limit(25);
	   		$this->db->order_by('updated_at desc');
			$this->data['products']=$this->products_m->get_all();
			
		}elseif($type==2){ 							// offer products
			//$this->db->limit(25);
	   		$this->db->order_by('updated_at desc');
			$where='prod_price_discount_'.$this->_lang.' <> "" OR prod_price_discount <> ""';
			$this->data['products']=$this->products_m->get_many_by($where);
			
		}elseif($type==3){ 							// search for products 
			$this->data['search_term'] = $search_term = $this->input->get('search');

			$terms = explode(' ', $search_term);
	
			
			foreach($terms as $term){
				$this->db->or_like('prod_name_en', $term);
				$this->db->or_like('prod_name_ar', $term);
				//$this->db->or_like('prod_name_rd', $term);
				
				$this->db->or_like('prod_description_en', $term);
				$this->db->or_like('prod_description_ar', $term);
				//$this->db->or_like('prod_description_en', $term);
				
			}
			$this->db->limit(25);
			$this->db->order_by('updated_at desc');
			$this->data['products']=$this->products_m->get_all();
		}else{
			redirect('main');
		}
	}
	
	// login function 
	public function login()
	{
		if($this->ion_auth->logged_in()) redirect('main/logout');

		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				$this->session->set_flashdata('messages_success', $this->ion_auth->messages());

				// set cart

				$temp_cart=$this->cart_temp_m->get_many_by(array('user_id' => $this->session->userdata('user_id')));
				if($temp_cart)	{
					foreach ($temp_cart as $key => $order_product) {
						$product=$this->products_m->get_by('prod_id',$order_product->product_id);
						if ($product){
							$data = array(
				               'id'      => $product->prod_id,
				               'qty'     => $order_product->qty,
				               'price'   => $order_product->price,
				               'name'    =>$product->{"prod_name_{$this->_lang}"}, 
				               'description'    =>$product->{"prod_description_{$this->_lang}"}, 
				               'image' 	 => base_url().'assets/uploads/products/'.$product->prod_image,             
				           	 );	

							$this->cart->insert($data);
						}
					}
				}
			
				// !set cart
				redirect('main', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('messages_warning', $this->ion_auth->errors());
				redirect('main', 'refresh');
			}
		}else{
			
			$error= preg_replace('/[ \t]+/', ' ', preg_replace('/[\r\n]+/', "\n", validation_errors()));
			$error=preg_replace("/[\r\n]{2,}/", "\n\n", $error);
			//var_dump($error);die();
			$this->session->set_flashdata('login_error',$error);
			$this->session->set_flashdata('messages_warning',lang('login_failed'));
			redirect('main', 'refresh');
		}
	}

	// logout  function 
	public function logout()
	{
		$this->ion_auth->logout();

		$this->session->set_flashdata('messages_success', $this->ion_auth->messages());

		redirect('main', 'refresh');
	}
	
	//create a new user
	function create_user()
	{

		if ($this->ion_auth->logged_in() || $this->ion_auth->is_admin())
		{
			redirect('main', 'refresh');
		}

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'required|xss_clean');
		$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'xss_clean');
		$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');
		$this->form_validation->set_rules('recaptcha_response_field', $this->lang->line('verify_code'), 'required|xss_clean|callback_recaptcha_check');

		//var_dump($_POST);die();

			$valid=false;
			if ($this->form_validation->run() == true)
			{
				
			//die('valid success ');
			$valid=true;
			$username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
			$email    = strtolower($this->input->post('email'));
			$password = $this->input->post('password');

			
			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name'),
				'phone'      => $this->input->post('phone'),
				'age'        => $this->input->post('age'),
				'education_id'      => $this->input->post('education'),
			);
			
			if ($this->input->post('town_id'))
				$additional_data['town_id']=$this->input->post('town_id');
			elseif($this->input->post('town_name'))
				$additional_data['town_name']=$this->input->post('town_name');
				
				// else{
					// $this->session->set_flashdata('messages_warning', lang('town_required'));
					// redirect('main');
				// }

				
		}else{
			$this->session->set_flashdata('register_error',validation_errors());
			$this->session->set_flashdata('messages_warning',lang('register_failed'));

			redirect('main', 'refresh');
		}
		
		$user_id=$this->ion_auth->register($username, $password, $email, $additional_data);
		if ($valid && $user_id)
		{
			//check to see if we are creating the user
			//redirect them back to the admin page
			$grp=$this->group_m->get_by(array('name' => 'memebers'));
			$this->ion_auth->add_to_group($grp->id, $user_id);
			
			//$this->session->set_flashdata('messages_success', $this->ion_auth->messages());
			$this->session->set_flashdata('messages_success', lang('register_success'));
			$this->ion_auth->login($email, $password);
			redirect("main");
		}
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			//var_dump($this->data['message'] );die();
			// $this->session->set_flashdata('messages_warning', trim(preg_replace('/\s\s+/', ' ', $this->data['message'])));
			
			$this->data['first_name'] = array(
				'name'  => 'first_name',
				'id'    => 'first_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array(
				'name'  => 'last_name',
				'id'    => 'last_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$this->data['email'] = array(
				'name'  => 'email',
				'id'    => 'email',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('email'),
			);
			// $this->data['company'] = array(
				// 'name'  => 'company',
				// 'id'    => 'company',
				// 'type'  => 'text',
				// 'value' => $this->form_validation->set_value('company'),
			// );
			$this->data['phone'] = array(
				'name'  => 'phone',
				'id'    => 'phone',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('phone'),
			);
			$this->data['password'] = array(
				'name'  => 'password',
				'id'    => 'password',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password'),
			);
			$this->data['password_confirm'] = array(
				'name'  => 'password_confirm',
				'id'    => 'password_confirm',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
			);

			redirect('main');
			//$this->_render_page('auth/create_user', $this->data);
		}
	}


	// call back function for check verify code is correct 
	public function recaptcha_check()
	{
		$this->load->library('recaptcha');
		$this->recaptcha->recaptcha_check_answer(  $_SERVER['REMOTE_ADDR'],
	    $this->input->post('recaptcha_challenge_field'),
	    $this->input->post('recaptcha_response_field'));

		if(!$this->recaptcha->getIsValid()) {
		//$this->session->set_flashdata('messages_warning', $this->recaptcha->getError());
		$this->session->set_flashdata('messages_warning',lang('register_failed'));
		$this->session->set_flashdata('register_error',$this->recaptcha->getError());

		$this->form_validation->set_message('recaptcha_response_field', lang('verify_code_required'));
		//die( ' recaptach failed ');
		return FALSE;
		
		}
		else
		{
			//die( ' recaptach success ');
			return TRUE;
		}
	}

	// edit user 
	function edit_user($id='')
	{		

		$id=$this->ion_auth->get_user_id();
			
		if (!$this->ion_auth->logged_in() )
		{
			redirect('account/login', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required|xss_clean');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required|xss_clean');
		$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'valid_email');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			$data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name'),
				'phone'      => $this->input->post('phone'),
				'email'      => $this->input->post('email'),
			);

			
			if ($this->input->post('town_id'))
				$additional_data['town_id']=$this->input->post('town_id');
			elseif($this->input->post('town_name'))
				$additional_data['town_name']=$this->input->post('town_name');
				
				
			//Update the groups user belongs to
			$groupData = $this->input->post('groups');

			if (isset($groupData) && !empty($groupData)) {

				$this->ion_auth->remove_from_group('', $id);

				foreach ($groupData as $grp) {
					$this->ion_auth->add_to_group($grp, $id);
				}

			}
			
			//update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');

				$data['password'] = $this->input->post('password');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$this->ion_auth->update($user->id, $data);

				//check to see if we are creating the user
				//redirect them back to the admin page
				$this->session->set_flashdata('messages_success', lang('edit_successfully'));
				redirect("main", 'refresh');
			}
		}

		//display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		$this->data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
		);
		$this->data['email'] = array(
			'name'  => 'email',
			'id'    => 'email',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('email', $user->email),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);
		
		$this->data['town_id'] = $user->town_id;
		
		$this->data['town_name'] = array(
			'name' => 'town_name',
			'id'   => 'town_name',
			'type' => 'text',
			'value' => $this->form_validation->set_value('email', $user->town_name),
		);
	}


	//activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("main", 'refresh');
		}
		else
		{
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("main/forgot_password", 'refresh');
		}
	}


	//forgot password
	function forgot_password()
	{
		$this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required');
		if ($this->form_validation->run() == false)
		{
			//setup the input
			
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);

			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_username_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			//set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//$this->_render_page('auth/forgot_password', $this->data);
		}
		else
		{
			// get identity for that email
            $identity = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
            if(empty($identity)) {
                $this->ion_auth->set_message('forgot_password_email_not_found');
                $this->session->set_flashdata('messages_warning', $this->ion_auth->messages());
                redirect("main/forgot_password", 'refresh');
            }
            
			
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});
			
			if ($forgotten)
			{
				//if there were no errors
				$this->session->set_flashdata('messages_success', $this->ion_auth->messages());
				redirect("main", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('messages_warning', $this->ion_auth->errors());
				redirect("main/forgot_password", 'refresh');
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				//display the form

				//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
				'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				//render
				//$this->_render_page('auth/reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						//if the password was successfully changed
						$this->session->set_flashdata('messages_success', $this->ion_auth->messages());
						$this->logout();
					}
					else
					{
						$this->session->set_flashdata('messages_warning', $this->ion_auth->errors());
						redirect('main/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('messages_warning', $this->ion_auth->errors());
			redirect("main/forgot_password", 'refresh');
		}
	}

	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	// show order for  user ( history or pending for admin accept )
	 public function orders($type='')
	 {
	 	if(!$this->ion_auth->logged_in()) redirect('main');
		
		if($this->has_privilage($this->session->userdata('user_id'),"checkout_cart")){
			
	    if($type =='' || !is_numeric($type))
		 	redirect('main');
		elseif( $type == 1 || $type==2) {
			if ($type== 1 ){ // order history
				$this->db->join('users','users.id = order.user_id');
				$orders=$this->order_m->get_many_by(array('status <> ' => "pending" , 'order.user_id' => $this->session->userdata('user_id')));
			}elseif($type== 2){ // order pending
				$this->db->join('users','users.id = order.user_id');
				$orders=$this->order_m->get_many_by(array('status = ' => "pending" , 'order.user_id' => $this->session->userdata('user_id')));
			}
			foreach ($orders as $key => $order) {
				$this->db->join('products','order_product.product_id = products.prod_id');
				$order->products=$this->order_product_m->get_many_by('order_id',$order->order_id);
			}
			$this->data['orders']=$orders;

		}else
			redirect('main');
			
		}else{
			redirect('main');
		}
	}
	 
	
		
	//  test if user have privilage 
	public function has_privilage($user_id,$priv_name)
	{
		return $this->privilege_model->user_has_preivilege($this->ion_auth->get_user_role($user_id),$priv_name);
	
		
	}
	
	 
	// public function search()
	// {
		// $this->data['search_term'] = $search_term = $this->input->post('search');
// 
		// $terms = explode(' ', $search_term);
// 
		// foreach($terms as $term){
			// $this->db->or_like('prod_name_en', $term);
			// $this->db->or_like('prod_name_ar', $term);
			// $this->db->or_like('prod_name_rd', $term);
// 			
			// $this->db->or_like('prod_description_en', $term);
			// $this->db->or_like('prod_description_ar', $term);
			// $this->db->or_like('prod_description_en', $term);
// 			
		// }
		// $this->db->limit(25);
		// $this->data['search_articles'] =  $this->articles_m->get_all();
// 
	// }
	 
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */