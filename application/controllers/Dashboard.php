<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	private $_create;

	private $_update;

	private $_delete;

	private $_sort;

	public function __construct()
	{
		parent::__construct();
		//load models
		$this->load->model('categories_model');
		
		$this->load->helper('privileges');
		// login..
		//if($this->router->method != 'login' && !$this->ion_auth->logged_in())
		if($this->uri->segment(2)!= 'login' && !$this->ion_auth->logged_in())
			redirect('dashboard/login');
		
		
		// if( $this->ion_auth->logged_in() && !$this->ion_auth->is_admin() )
			// redirect('main');
		
		$this->data['page'] = $this->router->method;

		$this->load->model('general_model', 'articles_m');
		$this->articles_m->set_table('articles');
        
        $this->load->model('general_model', 'constants_m');
        $this->constants_m->set_table('constants');

	    $this->load->model('general_model', 'category_m');
        $this->category_m->set_table('categories');

        $this->load->model('general_model', 'styles_m');
        $this->styles_m->set_table('product_style');
		
		$this->load->model('general_model', 'products_m');
        $this->products_m->set_table('products');
		
		$this->load->model('general_model', 'album_m');
        $this->album_m->set_table('albums');
		
		$this->load->model('general_model','style_m');
		$this->style_m->set_table('product_style');
		
		
		$this->load->model('general_model', 'album_galleries_m');
        $this->album_galleries_m->set_table('album_gallery');
		
		$this->load->model('general_model', 'cart_temp_m');
        $this->cart_temp_m->set_table('cart_temp');
		
		$this->load->model('general_model', 'order_m');
        $this->order_m->set_table('order');
		
		$this->load->model('general_model', 'order_product_m');
        $this->order_product_m->set_table('order_product');
		
		$this->load->model('general_model', 'newsletter_m');
        $this->newsletter_m->set_table('newsletter');
		
        $this->load->model('general_model','users_m');
        $this->users_m->set_table('users');
		
		$this->load->model('privilege_model');
        // $this -> load -> library('image_CRUD');

		$this->load->model('general_model','product_offers_m');
        $this->product_offers_m->set_table('product_offers');

        $this->data['articles'] = $this->articles_m->get_many_by('parent_id', 0);

		$this->_create = $this->input->post('create');
		$this->_update = $this->input->post('update');
		$this->_delete = $this->input->post('delete');
		$this->_sort = $this->input->post('sort');

        // language
        if(!$this->session->userdata('lang'))
            //$this->session->set_userdata('lang', 'arabic');
            $this->session->set_userdata('lang', 'english');
			
        if($this->uri->segment(2) == 'lang'){
        	
            switch ($this->uri->segment(3)) {
            	
                case 'en': $this->session->set_userdata('lang', 'english'); break;
                case 'ar': $this->session->set_userdata('lang', 'arabic'); break;
				case 'rd': $this->session->set_userdata('lang', 'rd'); break;
            }
            redirect('dashboard');
        }
		$this->lang->load('auth',$this->session->userdata('lang'));
        $this->config->set_item('language', $this->session->userdata('lang'));
        $this->load->language('dashboard', $this->config->item('language'));
		//$this->load->language('site', $this->config->item('language'));
        $this->_lang = substr($this->session->userdata('lang'), 0, 2);
        $this->data['lang'] = $this->_lang;
        $this->data['dir'] = $this->_lang=='ar'?'rtl':'ltr';


        $fields=array(
            'category_levels',
            'article_levels'
            );
         foreach ($fields as $item) {
            $this->data[$item] = $this->constants_m->get_by('item', $item)->value;
        }

	}

    public function index()
    {
        redirect('dashboard/home');
    }


	/*
	 * manage slider in home page 
	 */
    public function home()
    {
        // Slideshow items..
        if($this->has_privilage($this->session->userdata('user_id'),"home")){
        	
        if ($this->_create || $this->_delete) {

            $item_id = $this->input->post('item_id');

            $old_item = $this->articles_m->get($item_id);

            $this->form_validation->set_rules('title_ar', false, 'required|trim');
			//$this->form_validation->set_rules('title_en', false, 'required|trim');

            if ($this->form_validation->run()) {
                $item = array(
                        'parent_id' => -1,
                        'url' => $this->input->post('url'),
                        'title_en' => $this->input->post('title_en'),
                        'title_ar' => $this->input->post('title_ar'),
                        'title_rd' => $this->input->post('title_rd'),
                        'text_en' => $this->input->post('text_en'),
                        'text_ar' => $this->input->post('text_ar'),
                        'text_rd' => $this->input->post('text_rd'),
                    );

                // image file upload
                $old_file = $old_item?$old_item->image:false;
                $res = $this->_update_image('image', './assets/uploads/slideshow/', $this->input->post('image'), $old_file,1140,500);
                if($res['success'] && isset($res['filename'])){
                    $item['image'] = $res['filename'];
                    
					// image resize 
					$r=$this->image_resize('./assets/uploads/slideshow/'.$res['filename'],'./assets/uploads/slideshow/',$res['filename'],1140,500);
                    
                    if($r){
                         // @unlink('./assets/uploads/slideshow/' . $res['filename']);
                    }
                }else
                    $item['image'] = '';

                if($this->_create && $this->articles_m->insert($item)){
                    $this->session->set_flashdata('messages_success', lang('insert_success'));
                    redirect('dashboard/home');
                }
            }

            if($this->_delete && $this->articles_m->delete($item_id)){
                $this->session->set_flashdata('messages_success', lang('delete_success'));
                @unlink('./assets/uploads/slideshow/' . $old_item->image);
                redirect('dashboard/home');
            }
        }

        if($this->_sort){
            $priority = 1;
            $items = $this->input->post('item');
            $this->view = FALSE;
            foreach ($items as $item_id) {
                $this->articles_m->update($item_id, array('priority' => $priority++));
            }
        }

        $this->data['slideshow'] = $this->articles_m->get_many_by('parent_id', -1);

        $fields = array(
                'shop_now',
                'shop_now_slider',
                'about_us_url',
                'about_us_text_en',
                'about_us_text_ar',
                'about_us_title',
                'is_active_menu_images_albume',
                'is_active_menu_video_albume',
                'is_active_menu_products',
                'is_active_menu_contact_us',
                // 'about_us_text_rd',

            );
        
        // $fields_img = array(
            // );

        // Home Content
        if($this->_update) {

            foreach ($fields as $item) {
                $this->constants_m->update_by(array('item' => $item), array('value' => $this->input->post($item)));
            }

            // image file
            // foreach ($fields_img as $item) {
            //     $res = $this->_update_image($item, './assets/uploads/home/', $this->input->post("delete_image_$item"), $this->constants_m->get_by('item', $item)->value);
            //     $this->constants_m->update_by(array('item' => $item), array('value' => ($res['success'] && isset($res['filename']))?$res['filename']:''));
            // }

            $this->session->set_flashdata('messages_success', lang('update_success'));
            
        }

        // foreach ($fields as $item) {
        //         $this->constants_m->update_by(array('item' => $item), array('value' => $this->input->post($item)));
        //     }

         foreach ($fields as $item) {
            $this->data[$item] = $this->constants_m->get_by('item', $item)->value;
        }

        $this->data['page'] = lang('home');
		
		}else {
			redirect('main');
		}
	 }


    //private_offer
     public function product_offers()
     {
        $item_id = $this->input->post('item_id');

        $old_item = $this->product_offers_m->get_by(array('pof_id' => $item_id));

        if($this->_delete && $this->product_offers_m->delete_by(array('pof_id' => $item_id))){
            $this->session->set_flashdata('messages_success', lang('delete_success'));

            if($old_item->pof_image && is_dir(base_url().'assets/uploads/product_offers_m/'.$old_item->pof_image))
                @unlink('.upload/'.$img1);
            redirect('dashboard/product_offers');
        }

        if($this->_sort){
            $priority = 1;
            $items = $this->input->post('item');
            $this->view = FALSE;
            foreach ($items as $item_id) {
                $this->product_offers_m->update(array('pof_id'=> $item_id ), array('priority' => $priority++));
            }
        }

        $this->db->join('products','prod_id = pof_product_id');
        $this->data['offers'] = $this->product_offers_m->get_all();
        

        $this->data['page'] = lang('private_offers');
        
     }


     public function add_product_offer($category='',$product='')
     {
        if ( !is_numeric($category) || !is_numeric($product) )
            redirect('dashboard');

        $this->form_validation->set_rules('price', lang('price'), 'required|trim|numeric');
        $this->form_validation->set_rules('price_discount', lang('price_discount'), 'required|trim|numeric');

        if ($this->form_validation->run()) {
            $item = array(
                    'pof_url' => $this->input->post('url'),
                    'pof_price' => $this->input->post('price'),
                    'pof_price_discount' => $this->input->post('price_discount'),
                    'pof_start_date' => $this->input->post('start_date'),
                    'pof_end_date' => $this->input->post('end_date'),
                    'pof_product_id' => $product,
                );

            // image file upload
            $old_file = false;
            $res = $this->_update_image('image', './assets/uploads/product_offers/', $this->input->post('image'), $old_file,1140,500);
            if($res['success'] && isset($res['filename'])){
                $item['pof_image'] = $res['filename'];
                
                // image resize 
                $r=$this->image_resize('./assets/uploads/product_offers/'.$res['filename'],'./assets/uploads/product_offers/',$res['filename'],1140,500);

            }else
                $item['pof_image'] = '';

            if( $this->product_offers_m->insert($item)){
                $this->session->set_flashdata('messages_success', lang('insert_success'));
                redirect('dashboard/product_offers');
            }
         }else{
                $this->data['url'] = array('name' => 'url',
                    'value' => $this->form_validation->set_value('url'),
                );
                $this->data['price'] = array('name' => 'price',
                    'value' => $this->form_validation->set_value('price'),
                );
                $this->data['price_discount'] = array('name' => 'price_discount',
                    'value' => $this->form_validation->set_value('price_discount'),
                );
                $this->data['start_date'] = array('name' => 'start_date',
                    'value' => $this->form_validation->set_value('start_date'),
                );
                $this->data['end_date'] = array('name' => 'end_date',
                    'value' => $this->form_validation->set_value('end_date'),
                );


                $this->data['page']=lang('add_private_offer');
                $this->data['product']= $this->products_m->get_by('prod_id',$product);

                $parent_id = $category;
               // var_dump($parent_id);die();
                $breadcrumbs = array();
                while($parent_id) {
                    $category= $this->category_m->get_by('Cat_ID',$parent_id);
                    $parent_id = $category->parent_id;
                    $breadcrumbs[] = $category;
                }
                $this->data['breadcrumbs'] = array_reverse($breadcrumbs);
         }
    }



    public function edit_product_offer($id='')
     {
        if ( !is_numeric($id) || !$offer=$this->product_offers_m->get_by('pof_id',$id))
            redirect('dashboard');

        $this->form_validation->set_rules('price', lang('price'), 'required|trim|numeric');
        $this->form_validation->set_rules('price_discount', lang('price_discount'), 'required|trim|numeric');

        if ($this->form_validation->run()) {
            $item = array(
                    'pof_url' => $this->input->post('url'),
                    'pof_price' => $this->input->post('price'),
                    'pof_price_discount' => $this->input->post('price_discount'),
                    'pof_start_date' => $this->input->post('start_date'),
                    'pof_end_date' => $this->input->post('end_date'),
                );

            // image file upload
            $old_file = $offer->pof_image?$offer->pof_image:false;

            //var_dump($old_file);die();
            $res = $this->_update_image('image', './assets/uploads/product_offers/', $this->input->post('delete_image_image'), $old_file,1140,500);
            if($res['success'] && isset($res['filename'])){
                $item['pof_image'] = $res['filename'];
                
                // image resize 
                $r=$this->image_resize('./assets/uploads/product_offers/'.$res['filename'],'./assets/uploads/product_offers/',$res['filename'],1140,500);

            }else
                $item['pof_image'] = '';

            if( $this->product_offers_m->update_by(array('pof_id' => $id),$item)){
                $this->session->set_flashdata('messages_success', lang('update_success'));
                redirect('dashboard/product_offers');
            }
         }else{
                $this->data['url'] = array('name' => 'url',
                    'value' => $offer->pof_url,
                );
                $this->data['price'] = array('name' => 'price',
                    'value' => $offer->pof_price,
                );
                $this->data['price_discount'] = array('name' => 'price_discount',
                    'value' => $offer->pof_price_discount,
                );
                $this->data['start_date'] = array('name' => 'start_date',
                    'value' => $offer->pof_start_date,
                );
                $this->data['end_date'] = array('name' => 'end_date',
                    'value' =>$offer->pof_end_date,
                );


                $this->data['page']=lang('edit_product_offer');
                $this->data['product']= $this->products_m->get_by('prod_id',$offer->pof_product_id);
                $this->data['pof_image']=$offer->pof_image;

                $parent_id = $this->data['product']->prod_cat_id;
                $breadcrumbs = array();
                while($parent_id) {
                    $category= $this->category_m->get_by('Cat_ID',$parent_id);
                    $parent_id = $category->parent_id;
                    $breadcrumbs[] = $category;
                }
                $this->data['breadcrumbs'] = array_reverse($breadcrumbs);
         }
    }
    // !private_offer



	// edit slider in home page 
	public function edit_slider($id='')
	{
		if ($id == ''  || !is_numeric($id))	
			redirect('');
		else{
			
			$old_item=$this->articles_m->get_by(array('id' => $id));
			$this->data['slider']=$old_item;
			
            $this->form_validation->set_rules('url', lang('url'), '');

			$this->form_validation->set_rules('title_en', lang('title_en'), '');
			$this->form_validation->set_rules('text_en', lang('text_en'), '');
						
			$this->form_validation->set_rules('title_ar', lang('title_ar'), '');
			$this->form_validation->set_rules('text_ar', lang('text_ar'), '');
			
			// $this->form_validation->set_rules('title_rd', lang('title_rd'), '');
			// $this->form_validation->set_rules('text_rd', lang('text_rd'), '');
			if ($this->form_validation->run() == true)
			{
				$info=array('url'=>$this->input->post('url'),
                            'title_en'=>$this->input->post('title_en'),
							'text_en'=>$this->input->post('text_en'),
							'title_ar'=>$this->input->post('title_ar'),
							'text_ar'=>$this->input->post('text_ar'),
							'title_rd'=>$this->input->post('title_rd'),
							'text_rd'=>$this->input->post('text_rd'),
							);	
				

               // var_dump($_FILES);die();
				 // image file
                $old_file = $old_item?$old_item->image:false;
                $res = $this->_update_image('image', './assets/uploads/slideshow/', $this->input->post('image'), $old_file);
                if($res['success'] && isset($res['filename'])){
                    $info['image'] = $res['filename'];
                    $r=$this->image_resize('./assets/uploads/slideshow/'.$res['filename'],'./assets/uploads/slideshow/',$res['filename'],1140,500);
                    
                    if($r){
                         // @unlink('./assets/uploads/slideshow/' . $res['filename']);
                    }
                    
                }else
                    $info['image'] = '';
				
				if ($this->articles_m->update_by(array('id' => $id),$info)	){
                	$this->session->set_flashdata('messages_success', lang('update_success'));
					redirect('dashboard/home');
				}else{
					$this->session->set_flashdata('messages_warning', lang('save_failed'));
					redirect('dashboard/edit_slider/'.$id);
				}	
			}else{
				
                 $this->data['url'] = array('name' => 'url',
                    'value' => $this->form_validation->set_value('url')?$this->form_validation->set_value('url'):$old_item->url,
                );

				$this->data['title_en'] = array('name' => 'title_en',
					'value' => $this->form_validation->set_value('title_en')?$this->form_validation->set_value('title_en'):$old_item->title_en,
				);
				$this->data['text_en'] = array('name' => 'text_en',
					'value' => $this->form_validation->set_value('text_en')?$this->form_validation->set_value('text_en'):$old_item->text_en,
				);
				
				$this->data['title_ar'] = array('name' => 'title_ar',
					'value' => $this->form_validation->set_value('title_ar')?$this->form_validation->set_value('title_ar'):$old_item->title_ar,
				);
				$this->data['text_ar'] = array('name' => 'text_ar',
					'value' => $this->form_validation->set_value('text_ar')?$this->form_validation->set_value('text_ar'):$old_item->text_ar,
				);
				
				$this->data['title_rd'] = array('name' => 'title_rd',
					'value' => $this->form_validation->set_value('title_rd')?$this->form_validation->set_value('title_rd'):$old_item->title_rd,
				);
				$this->data['text_rd'] = array('name' => 'text_rd',
					'value' => $this->form_validation->set_value('text_rd')?$this->form_validation->set_value('text_rd'):$old_item->text_rd,
				);
			}		
			
		}
	}
	
	
	// manage article  insert , update , delete  
    public function articles()
    {
    	if($this->has_privilage($this->session->userdata('user_id'),"articles")){

		
		
        // multimedia
        $this -> load -> library('multimedia_crud');

        $this -> multimedia_crud -> set_mode(true, true, true, true, true);
        $this -> multimedia_crud -> set_primary_key_field('id');
        $this -> multimedia_crud -> set_url_field('url');
        $this -> multimedia_crud -> set_title_field('title');
        $this -> multimedia_crud -> set_table('galleries') -> set_ordering_field('priority') -> set_image_path("assets/uploads/galleries");
        $this -> multimedia_crud -> set_relation_field('article_id');
        // $this -> multimedia_crud -> set_thumb_field('thumb_url');
        // $this -> multimedia_crud -> set_title_field('title');

        $this->data['multimedia_crud'] = (array) $this -> multimedia_crud -> render();
        

        $article_id = $this->uri->segment(3) or 0;

        if($article_id && !$article = $this->articles_m->get($article_id))
            redirect();

        // Breadcrumbs

        $parent_id = $article_id;
        $breadcrumbs = array();
        while($parent_id) {
            $article = $this->articles_m->get($parent_id);
            $parent_id = $article->parent_id;
            $breadcrumbs[] = $article;
        }
        $this->data['breadcrumbs'] = array_reverse($breadcrumbs);

        // sub items..
        if ($this->_create || $this->_delete) {

            $item_id = $this->input->post('item_id');

            $this->form_validation->set_rules('title_ar', '', 'required|trim');
			$this->form_validation->set_rules('title_en', '', 'required|trim');

            if ($this->form_validation->run()) {
                $item = array(
                        'parent_id' => $article_id,
                        'title_en' => $this->input->post('title_en'),
                        'title_ar' => $this->input->post('title_ar'),
                        'title_rd' => $this->input->post('title_rd'),
                    );

                if($this->_create && $this->articles_m->insert($item)){
                    $this->session->set_flashdata('messages_success', lang('insert_success'));
                    redirect('dashboard/articles/'.$article_id);
                }
            }

            if($this->_delete && $this->articles_m->get_many_by('parent_id', $item_id)){
                $this->session->set_flashdata('messages_danger', lang('messages_danger'));
                redirect('dashboard/articles/'.$article_id);
            }elseif($this->_delete && $this->articles_m->delete($item_id)){
                $this->session->set_flashdata('messages_success', lang('delete_success'));
                redirect('dashboard/articles/'.$article_id);
            }
        }

        if($this->_sort){
            $priority = 1;
            $items = $this->input->post('item');
            $this->view = FALSE;
            foreach ($items as $item_id) {
                $this->articles_m->update($item_id, array('priority' => $priority++));
            }
        }

        $this->data['sub_articles'] = $this->articles_m->get_many_by('parent_id', $article_id);

        // edit article..

        if($this->_update) {

            $item_id = $this->input->post('item_id');

            $old_item = $this->articles_m->get($item_id);

            $this->form_validation->set_rules('title_ar', '', 'required|trim');

            if ($this->form_validation->run()) {
                $item = array(
                        'title_en' => $this->input->post('title_en'),
                        'title_ar' => $this->input->post('title_ar'),
                        'title_rd' => $this->input->post('title_rd'),
                        'description_en' => $this->input->post('description_en'),
                        'description_ar' => $this->input->post('description_ar'),
                        'description_rd' => $this->input->post('description_rd'),
                        'youtube' => $this->input->post('youtube'),
                        'text_en' => $this->input->post('text_en'),
                        'text_ar' => $this->input->post('text_ar'),
                        'text_rd' => $this->input->post('text_rd'),
                    );
                
                // thumbnail
                $old_file = $old_item?$old_item->thumbnail:false;
                $res = $this->_update_image('thumbnail', './assets/uploads/thumbnails/', $this->input->post('delete_image_thumbnail'), $old_file);
                if($res['success'] && isset($res['filename'])){
                    $item['thumbnail'] = $res['filename'];
                }else
                    $item['thumbnail'] = '';

                // image file
                $old_file = $old_item?$old_item->image:false;
                $res = $this->_update_image('image', './assets/uploads/images/', $this->input->post('delete_image_image'), $old_file);
                if($res['success'] && isset($res['filename'])){
                    $item['image'] = $res['filename'];
                }else
                    $item['image'] = '';

                if($this->_update && $this->articles_m->update($item_id, $item)){
                    $this->session->set_flashdata('messages_success', lang('update_success'));
                    redirect('dashboard/articles/'.$article_id);
                }
            }
            
            if($this->_delete && $this->articles_m->delete($item_id)){
                @unlink('./assets/uploads/thumbnails/' . $old_item->thumbnail);
                @unlink('./assets/uploads/images/' . $old_item->image);

                $this->session->set_flashdata('messages_success', lang('delete_success'));
                redirect('dashboard/articles/'.$article_id);
            }
        }

        $this->data['article_id'] = $article_id;
        $this->data['article'] = $article = $article_id?$this->articles_m->get($article_id):false;
        // $this->data['page'] = $article_id?$article->{"title_{$this->_lang}"}:false;
        $this->data['page'] = $article_id?$article->{"title_ar"}:false;


		}else{
		redirect('main');
		}
    }

	// login function 
	public function login()
	{
		
		
		if($this->ion_auth->logged_in()) redirect('dashboard/logout');

		$this->form_validation->set_rules('identity', 'Identity', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{
			
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				$this->session->set_flashdata('messages_success', $this->ion_auth->messages());
				redirect('dashboard', 'refresh');
			}
			else
			{
				$this->session->set_flashdata('messages_warning', $this->ion_auth->errors());
				redirect('dashboard/login', 'refresh');
			}
		}
	}

	// logout  function 
	public function logout()
	{
		$this->ion_auth->logout();

		$this->session->set_flashdata('messages_success', $this->ion_auth->messages());

		redirect('dashboard/login', 'refresh');
	}

	// manage constant in website 
    public function constants()
    {
	if($this->has_privilage($this->session->userdata('user_id'),"constants")){
		
        $fields = array(
                // 'title',
                // 'description',
                // 'keywords',
                //'shop_now',
                'contact_email_from',
                'contact_email_to',
                'order_email_from',
                'order_email_to',
                'sm_facebook',
                'sm_twitter',
                'sm_youtube',
                'sm_google_plus',
                'sm_instagram',
                'contact_us_mail',
                'contact_us_phone',
                'unit_',
                'unit_en',
                'unit_ar',
               // 'unit_rd',
                'min_price',
                'min_price_en',
                'min_price_ar',
               // 'min_price_rd',
                'max_price',
                'max_price_en',
                'max_price_ar',
                //'max_price_rd',
                'brands_header_en',
                'brands_header_ar',
               // 'brands_header_rd'
                'category_levels',
                'article_levels', 
            );

        if($this->_update) {

            foreach ($fields as $item) {
                $this->constants_m->update_by(array('item' => $item), array('value' => $this->input->post($item)));
            }

            $this->session->set_flashdata('messages_success', 'Items was updated successfully..');
            
        }

        foreach ($fields as $item) {
            $this->data[$item] = $this->constants_m->get_by('item', $item)->value;
        }

        $this->data['page'] = lang('constants');
		
		}else{
			redirect('main');
			}
		
    }

	// helper functions..

	//  update image  file
	private function _update_image($input, $upload_path, $new_image = false, $old_name = '', $width = null, $height = null) {
        if ($new_image != 'on') { // 'on' for checkbox..
            if (isset($_FILES[$input]) && is_uploaded_file($_FILES[$input]['tmp_name'])) {
                $data = $this->_upload_image($input, $upload_path);

                if($data['success'])
                    @unlink($upload_path . $old_name);

            }else{
                //$error = 'No image file selected';
                $data['success'] = 1;
                $data['filename'] = $old_name;
            }
            
        }else{// if (!empty($old_name)){
            @unlink($upload_path . $old_name);
            $data['success'] = 1;
        }
        return $data;
    }
	
	//  upload image  file
    private function _upload_image($input, $upload_path, $width = false, $height = false)
	{
		$error = false;

        $_FILES[$input]['name'] = strtolower($_FILES[$input]['name']);
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = 'gif|jpeg|jpg|png';
        // $config['max_size'] = '100';
        // $config['max_width']  = '1024';
        // $config['max_height']  = '768';

        $this -> load -> library('upload', $config);
		$this -> upload -> initialize($config);
        if (!$this -> upload -> do_upload($input)) {
            $error = $this -> upload -> display_errors();
        } else {
            $d = $this -> upload -> data();

            $d['file_name'] = strtolower($d['file_name']);
            $file = $d['file_name'];
            if($width && $height){
                $config['image_library'] = 'gd2';
                $config['source_image'] = $config['upload_path'] . $file;
                //$config['create_thumb'] = TRUE;
                //$config['maintain_ratio'] = TRUE;
                $config['width'] = $width;
                $config['height'] = $height;

                if ($d['image_width'] < $width || $d['image_height'] < $height) {
                    @unlink($config['source_image']);
                    $error = 'Logo size is too small.';
                }

                $this -> load -> library('image_lib', $config);

                if (!$this -> image_lib -> resize()) {
                    @unlink($config['source_image']);
                    $error = $this -> upload -> display_errors();
                }
            }
        }

        if($error){
            $data['success'] = 0;
            $data['error'] = $error;
        }else{
            $data['success'] = 1;
            $data['filename'] = $file;
        }

        return $data;
	}

	//  upload file by extension your pass as paramenter 
	private function _upload_file($input, $upload_path, $allowed_types = 'gif|jpeg|jpg|png')
	{
		$error = false;

        $_FILES[$input]['name'] = strtolower($_FILES[$input]['name']);
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = $allowed_types;

        $this -> load -> library('upload', $config);
        $this -> upload -> initialize($config);
        if (!$this -> upload -> do_upload($input)) {
            $error = $this -> upload -> display_errors();
        } else {
            $d = $this -> upload -> data();

            $d['file_name'] = strtolower($d['file_name']);
            $file = $d['file_name'];
        }

        if($error){
            $data['success'] = 0;
            $data['error'] = $error;
        }else{
            $data['success'] = 1;
            $data['filename'] = $file;
        }

        return $data;
	}

	//  update file by extension your pass as paramenter 
	private function _update_file($input, $upload_path, $new_file = false, $old_name = '', $allowed_types = false) {
        if ($new_file != 'on') { // 'on' for checkbox..
            if (isset($_FILES[$input]) && is_uploaded_file($_FILES[$input]['tmp_name'])) {
                $data = $this->_upload_file($input, $upload_path, $allowed_types);

                // if($data['success'])
                    @unlink($upload_path . $old_name);

            }else{
                //$error = 'No image file selected';
                $data['success'] = 1;
                $data['filename'] = $old_name;
            }
            
        }else{// if (!empty($old_name)){
            @unlink($upload_path . $old_name);
            $data['success'] = 1;
        }
        return $data;
    }
	
	//  manage advertisements ( insert , update )
	public function advertisement()
	{
		
		if($this->has_privilage($this->session->userdata('user_id'),"advertisement")){
		
		
		$this->load->model('advertisement_model');
		if($this->input->post('submit')){
		
			$this->config_upload('./upload/',3000,3000,5000);
			if ( $this->upload->do_upload('Left'))
			{
				$img1=$this->advertisement_model->get('Left');
				if($img1 && is_dir(base_url().'upload/'.$img1))
					unlink('.upload/'.$img1);
				$file_data = $this->upload->data();
				$img_name = md5($file_data['raw_name']).md5(strtotime("now")).$file_data['file_ext'];
				$this->image_resize($file_data['full_path'],'./upload/',$img_name,190,190);
				$info['Name'] = $img_name;
				unlink($file_data['full_path']);
				$this->advertisement_model->edit('Left',$info);
				$this->session->set_flashdata('messages_success', lang('save_advertising_successfully'));			
			}else
				{
					//$this->data['message']= 'لم يتم اختيار الصورة الأولى للتحميل'.'<br>';
					$this->session->set_flashdata('messages_warning',lang('no_image_choice'));
				}
			if ( $this->upload->do_upload('Right'))
			{
				$img1=$this->advertisement_model->get('Right');
				if($img1 && is_dir(base_url().'upload/'.$img1))
					unlink('upload/'.$img1);
				$file_data = $this->upload->data();
				$img_name = md5($file_data['raw_name']).md5(strtotime("now")).$file_data['file_ext'];
				$this->image_resize($file_data['full_path'],'./upload/',$img_name,190,190);
				$info['Name'] = $img_name;
				unlink($file_data['full_path']);
				$this->advertisement_model->edit('Right',$info);
				$this->session->set_flashdata('messages_success', lang('save_advertising_successfully'));				
			}else
				$this->session->set_flashdata('messages_warning',lang('no_image_choice'));
			

			$img1=$this->advertisement_model->get('Left');		
			$img2=$this->advertisement_model->get('Right');


			$this->data['Left'] = array(
				'name'  => 'Left',
				'id'    => 'Left',
				'value' => $img1,
			);
			$this->data['Right'] = array(
				'name'  => 'Right',
				'id'    => 'Right',
				'value' => $img2,
			);
			//$this->data['view_page']='advertisement';
			//$this->load->view('cpanel/cpanel_controller', $this->data);
		}
		else 
		{
			$img1=$this->advertisement_model->get('Left');		
			$img2=$this->advertisement_model->get('Right');
			
			$this->data['Left'] = array(
				'name'  => 'Left',
				'id'    => 'Left',
				'value' => $img1,
			);
			$this->data['Right'] = array(
				'name'  => 'Right',
				'id'    => 'Right',
				'value' => $img2,
			);

			//$this->data['view_page']='advertisement';
			//$this->load->view('cpanel/cpanel_controller', $this->data);
			
		}


		}else{
			redirect('main');
		}
	}

	//  add category for products 
	public function add_category()
	{
		
		if($this->has_privilage($this->session->userdata('user_id'),"add_category")){
		
			$this->form_validation->set_rules('cat_name_en', lang('cat_name_en'), 'required');
			$this->form_validation->set_rules('cat_name_ar', lang('cat_name_ar'), 'required');
			$this->form_validation->set_rules('cat_name_rd',lang('cat_name_rd'), '');
			//$this->form_validation->set_rules('parent_id', lang('parent_category'), 'required');
			
			if ($this->form_validation->run() == true)
			{
				//$parent_id=$this->input->post('parent_id');
				$parent_id=$this->uri->segment(3)?$this->uri->segment(3):0;
				$info=array('Cat_Name_en'=>$this->input->post('cat_name_en'),
							'Cat_Name_ar'=>$this->input->post('cat_name_ar'),
							'Cat_Name_rd'=>$this->input->post('cat_name_rd'),
							'parent_id' => $parent_id);
							
							
				 // image file
                //$old_file = $old_item?$old_item->image:false;
                $old_file= false;
				
                $res = $this->_update_image('image', './assets/uploads/category/', $this->input->post('delete_image_image'), $old_file);
                if($res['success'] && isset($res['filename'])){
                    $info['image'] = $res['filename'];
					//$this->image_resize($res['full_path'],$res['full_path'],$res['filename'],1140,500);
                }else
                    $info['image'] = '';
					
				$id= $this->category_m->insert($info);
				
				
				// uplaod images 
				
				$this->session->set_flashdata('messages_success', lang('save_successfully'));	
				
				if($parent_id)
					redirect('dashboard/show_all_category/'.$parent_id);
				else {
					redirect('dashboard/show_all_category');
				}
			}
			else
			{ 
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->set_flashdata('messages_warning', lang('save_failed'));
				$this->data['cat_name_en'] = array('name' => 'cat_name_en',
					'id' => 'cat_name_en',
					'type' => 'text',
					'value' => $this->form_validation->set_value('cat_name_en'),
				);
				
				$this->data['cat_name_ar'] = array('name' => 'cat_name_ar',
					'id' => 'cat_name_ar',
					'type' => 'text',
					'value' => $this->form_validation->set_value('cat_name_ar'),
				);
				
				$this->data['cat_name_rd'] = array('name' => 'cat_name_rd',
					'id' => 'cat_name_rd',
					'type' => 'text',
					'value' => $this->form_validation->set_value('cat_name_rd'),
				);
				$this->data['mode']='error';
				$this->data['main_categories']=$this->category_m->get_many_by('parent_id',0);
				
				$parent_id = $this->uri->segment(3);
		        $breadcrumbs = array();
		        while($parent_id) {
		            $category= $this->category_m->get_by('Cat_ID',$parent_id);
		            $parent_id = $category->parent_id;
		            $breadcrumbs[] = $category;
		        }
		        $this->data['breadcrumbs'] = array_reverse($breadcrumbs);
				
				// $this->data['view_page'] = 'categories/add';
				// $this->load->view('cpanel/cpanel_controller',$this->data);
			}
			
		}else{
			redirect('main');
			}	
	}

	// edit  category info  for products 
	public function edit_category()
	{
		
		if($this->has_privilage($this->session->userdata('user_id'),"edit_category")){
		
			
		if ($this->uri->segment(3) == false)
		{
		   redirect($this->config->item('base_url'));		// here 
		}
		else
		{
			if(!is_numeric($this->uri->segment(3))){
				redirect('dashboard/show_all_category');
			}
			
			if(!$this->categories_model->get_ByID($this->uri->segment(3)))
				redirect($this->config->item('base_url'), 'refresh');
			
			$this->form_validation->set_rules('cat_name_en', lang('cat_name_en'), 'required');
			$this->form_validation->set_rules('cat_name_ar', lang('cat_name_ar'), 'required');
			$this->form_validation->set_rules('cat_name_rd',lang('cat_name_rd'), '');
			$this->form_validation->set_rules('parent_id', lang('parent_category'), 'required');
			
			if ($this->form_validation->run() == true)
			{
				$id = $this->uri->segment(3);	
				$parent_id=$this->input->post('parent_id');
				$info=array('Cat_Name_en'=>$this->input->post('cat_name_en'),
							'Cat_Name_ar'=>$this->input->post('cat_name_ar'),
							'Cat_Name_rd'=>$this->input->post('cat_name_rd'),
							'parent_id' => $parent_id);
				
					
				$this->category_m->update_by(array('Cat_ID'=>$id),$info);
                //$this->category_m->update_by(array('Cat_ID' => $id),$info);
				$this->session->set_flashdata('messages_success', lang('save_successfully'));	
				
				if($parent_id)
					redirect('dashboard/show_all_category/'.$parent_id);
				else {
					redirect('dashboard/show_all_category');
				}
			}
			else
			{ 
				$this->data['message']= '';				
				$this->data['mode']='error';
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->set_flashdata('messages_warning', lang('save_failed'));;
				
				$cat = $this->categories_model->get_ByID($this->uri->segment(3));
				$this->data['cat_name_en'] = array('name' => 'cat_name_en',
					'id' => 'cat_name_en',
					'type' => 'text',
					'value' => $cat->Cat_Name_en,
				);
				
				$this->data['cat_name_ar'] = array('name' => 'cat_name_ar',
					'id' => 'cat_name_ar',
					'type' => 'text',
					'value' => $cat->Cat_Name_ar,
				);
				
				$this->data['cat_name_rd'] = array('name' => 'cat_name_rd',
					'id' => 'cat_name_rd',
					'type' => 'text',
					'value' => $cat->Cat_Name_rd,
				);
				
				
				$this->data['parent_id']=$cat->parent_id;
				
				
				$this->data['main_categories']=$this->category_m->get_many_by('parent_id',0);
								
				$this->data['cat_id']=$cat->Cat_ID;
				$this->data['category']= $this->category_m->get_by('Cat_ID',$this->uri->segment(3));
				
				$parent_id = $this->uri->segment(3);
		        $breadcrumbs = array();
		        while($parent_id) {
		            $category= $this->category_m->get_by('Cat_ID',$parent_id);
		            $parent_id = $category->parent_id;
		            $breadcrumbs[] = $category;
		        }
		        $this->data['breadcrumbs'] = array_reverse($breadcrumbs);
		
		
				$this->data['mode']='error';
			}
		}

		}else{
		redirect('main');
		}
	}

	// delete  category info  for products 
	public function delete_category()
	{
		if($this->has_privilage($this->session->userdata('user_id'),"delete_category")){
		
		
		if ($this->uri->segment(3) == false)
		{
		  redirect('');		
		}
		else
		{
			if(!is_numeric($this->uri->segment(3))){
				redirect('dashboard/show_all_category');
			}
			if(!$this->categories_model->get_ByID($this->uri->segment(3)))
				 redirect('');		

				$this->categories_model->delete($this->uri->segment(3));
		
				
				$cat=$this->categories_model->get_ByID($this->uri->segment(3));
				if($cat && $cat->parent_id)
					redirect('dashboard/show_all_category/'.$parent_id);
				else {
					redirect('dashboard/show_all_category');
				}
		}
		}else{
			redirect('main');
			}
		
	}
	
	// show all categories 
	public function show_all_category($id='')
	{
		
		if($this->has_privilage($this->session->userdata('user_id'),"show_all_category")){
		
		
		if ($id=='' || !is_numeric($id)){
			// test
			$this->data['categories']=$this->category_m->get_many_by('parent_id',0);
		}else{
			$this->data['categories']=$this->category_m->get_many_by('parent_id',$id);
		}
	
		$parent_id = $id;
        $breadcrumbs = array();
        while($parent_id) {
            $category= $this->category_m->get_by('Cat_ID',$parent_id);
            $parent_id = $category->parent_id;
            $breadcrumbs[] = $category;
        }
        $this->data['breadcrumbs'] = array_reverse($breadcrumbs);
		
		$this->data['page']=lang('categories');
		// $data['view_page']='categories/show_all';
		// $this->load->view('cpanel/cpanel_controller',$data);
		
		}else{
		redirect('main');
		}

		
	}
	 


//styles
    // show all styles 
    public function show_all_styles()
    {
        $this->data['styles']=$this->styles_m->get_all();   
        $this->data['page']=lang('styles'); 
    }

    //  add style 
    public function add_style()
    {
        
            $this->form_validation->set_rules('ps_title_en', lang('cat_name_en'), 'required');
            $this->form_validation->set_rules('ps_title_ar', lang('cat_name_ar'), 'required');
            //$this->form_validation->set_rules('ps_title_rd',lang('cat_name_rd'), '');
            //$this->form_validation->set_rules('parent_id', lang('parent_category'), 'required');
            
            if ($this->form_validation->run() == true)
            {
                $info=array('ps_title_en'=>$this->input->post('ps_title_en'),
                            'ps_title_ar'=>$this->input->post('ps_title_ar'),
                            //'ps_title_rd'=>$this->input->post('ps_title_rd'),
                            );
            
                $id= $this->styles_m->insert($info);
                
                
                $this->session->set_flashdata('messages_success', lang('save_successfully'));   
                redirect('dashboard/show_all_styles');
            }
            else
            { 
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->set_flashdata('messages_warning', lang('save_failed'));
                $this->data['ps_title_en'] = array('name' => 'ps_title_en',
                    'value' => $this->form_validation->set_value('ps_title_en'),
                );
                
                $this->data['ps_title_ar'] = array('name' => 'ps_title_ar',
                    'value' => $this->form_validation->set_value('ps_title_ar'),
                );
                
                // $this->data['ps_title_rd'] = array('name' => 'ps_title_rd',
                //     'value' => $this->form_validation->set_value('ps_title_rd'),
                // );
                $this->data['mode']='error';
            }
    }


     //  edit style 
    public function edit_style($id='')
    {
            if (!$id || !is_numeric($id) || !$style=$this->styles_m->get_by('ps_id',$id))
                redirect('dashboard/show_all_styles');
        
            $this->form_validation->set_rules('ps_title_en', lang('cat_name_en'), 'required');
            $this->form_validation->set_rules('ps_title_ar', lang('cat_name_ar'), 'required');
            //$this->form_validation->set_rules('ps_title_rd',lang('cat_name_rd'), '');
            //$this->form_validation->set_rules('parent_id', lang('parent_category'), 'required');
            
            if ($this->form_validation->run() == true)
            {
                $info=array('ps_title_en'=>$this->input->post('ps_title_en'),
                            'ps_title_ar'=>$this->input->post('ps_title_ar'),
                            //'ps_title_rd'=>$this->input->post('ps_title_rd'),
                            );
            
            
                $this->styles_m->update_by(array('ps_id' => $id),$info);
                
                
                $this->session->set_flashdata('messages_success', lang('save_successfully'));   
                redirect('dashboard/show_all_styles');
            }
            else
            { 
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->set_flashdata('messages_warning', lang('save_failed'));
                $this->data['ps_title_en'] = array('name' => 'ps_title_en',
                    'value' => $style->ps_title_en,
                );
                
                $this->data['ps_title_ar'] = array('name' => 'ps_title_ar',
                    'value' => $style->ps_title_ar,
                );
                
                // $this->data['ps_title_rd'] = array('name' => 'ps_title_rd',
                //     'value' => $tyle->ps_title_rd,
                // );
                $this->data['mode']='error';
            }
    }

    // delete  style 
    public function delete_style()
    {
        
        if ($this->uri->segment(3) == false)
        {
          redirect('');     
        }
        else
        {
            if(!is_numeric($this->uri->segment(3))){
                redirect('dashboard/show_all_styles');
            }
            if(!$this->styles_m->get_by('ps_id',$this->uri->segment(3)))
                 redirect('');      

                $this->styles_m->delete_by('ps_id',$this->uri->segment(3));
                redirect('dashboard/show_all_styles');
        }
    }
    

//products

	// show all categories  for select category to view products related with selected category 
 	public function show_all_products_category($id='')
	{
		if($this->has_privilage($this->session->userdata('user_id'),"show_all_products_category")){
			
		if (!$id){
			$this->data['categories']=$this->category_m->get_many_by('parent_id',0);
			$this->data['products']=false;
		}else{
			$this->data['categories']=$this->category_m->get_many_by('parent_id',$id);
			$this->data['products']=$this->products_m->get_many_by('prod_cat_id',$id);
		}

		$this->data['page']=lang('products');
		$parent_id = $id;
        $breadcrumbs = array();
        while($parent_id) {
            $category= $this->category_m->get_by('Cat_ID',$parent_id);
            $parent_id = $category->parent_id;
            $breadcrumbs[] = $category;
        }
        $this->data['breadcrumbs'] = array_reverse($breadcrumbs);
		
		// $data['view_page']='categories/show_all';
		// $this->load->view('cpanel/cpanel_controller',$data);
		}else{
		redirect('main');
		}
	}
	 
	 
	 // add new product 
	public function add_product()
	{
		
		
		$this->data['page']=lang('add_product');
		if($this->has_privilage($this->session->userdata('user_id'),"add_product")){
		
			$this->form_validation->set_rules('prod_name_en', lang('prod_name_en'), 'required');
			$this->form_validation->set_rules('prod_description_en', lang('prod_des_en'), '');
			$this->form_validation->set_rules('prod_price_en', lang('prod_price_en'), 'numeric');
			$this->form_validation->set_rules('prod_price_discount_en', lang('prod_price_discount_en'), 'numeric');
						
			$this->form_validation->set_rules('prod_name_ar', lang('prod_name_ar'), 'required');
			$this->form_validation->set_rules('prod_description_ar', lang('prod_des_ar'), '');
			$this->form_validation->set_rules('prod_price_ar',lang('prod_price_ar'), 'numeric');
			$this->form_validation->set_rules('prod_price_discount_ar', lang('prod_price_discount_ar'), 'numeric');
			
			// $this->form_validation->set_rules('prod_name_rd', lang('prod_name_rd'), 'required');
			// $this->form_validation->set_rules('prod_description_rd',  lang('prod_des_rd'), '');
			// $this->form_validation->set_rules('prod_price_rd', lang('prod_price_rd'), '');
			// $this->form_validation->set_rules('prod_price_discount_rd', lang('prod_price_discount_rd'), '');
			
			$this->form_validation->set_rules('prod_price', lang('prod_price_other'), 'numeric');
			$this->form_validation->set_rules('prod_price_discount', lang('prod_price_discount'), 'numeric');
			
			//$this->form_validation->set_rules('gender', lang('gender'), 'required');
			$this->form_validation->set_rules('prod_number', lang('prod_number'), 'required|callback_is_unique_product_number_check');
            $this->form_validation->set_rules('prod_quantity', lang('prod_quantity'), 'required');
			
			$this->form_validation->set_rules('style_id',lang('style'), 'required');
			
			//$this->form_validation->set_rules('prod_cat_id',lang('category'), 'required');
			//$this->form_validation->set_rules('prod_main_cat_id',lang('category'), 'required');

									
			if ($this->form_validation->run() == true)
			{
				$prod_cat_id=$this->uri->segment(3)?$this->uri->segment(3):0;
				$category=$this->category_m->get_by('Cat_ID',$this->uri->segment(3));
				$prod_main_cat_id=$category?$category->parent_id:0;
				
				$info=array('prod_name_en'=>$this->input->post('prod_name_en'),
							'prod_name_ar'=>$this->input->post('prod_name_ar'),
							'prod_name_rd'=>$this->input->post('prod_name_rd'),
							'prod_description_en'=>$this->input->post('prod_description_en'),
							'prod_description_ar'=>$this->input->post('prod_description_ar'),
							'prod_description_rd'=>$this->input->post('prod_description_rd'),
							'prod_price_en'=>$this->input->post('prod_price_en'),
							'prod_price_ar'=>$this->input->post('prod_price_ar'),
							'prod_price_rd'=>$this->input->post('prod_price_rd'),
							'prod_price'=>$this->input->post('prod_price'),
							'prod_price_discount_en'=>$this->input->post('prod_price_discount_en'),
							'prod_price_discount_ar'=>$this->input->post('prod_price_discount_ar'),
							'prod_price_discount_rd'=>$this->input->post('prod_price_discount_rd'),
							'prod_price_discount'=>$this->input->post('prod_price_discount'),
							'prod_style_id'=>$this->input->post('style_id'),
							//'prod_gender'=>$this->input->post('gender'),
							// 'prod_cat_id'=>$this->input->post('prod_cat_id'),
							// 'prod_main_cat_id' =>$this->input->post('prod_main_cat_id'),
							'prod_cat_id'=>$prod_cat_id,
							'prod_main_cat_id' =>$prod_main_cat_id,
							'prod_number'		=> $this->input->post('prod_number'),
                            'prod_quantity'       => $this->input->post('prod_quantity'),
							);
							
			
			$this->config_upload('./assets/uploads/products/',3000,3000,5000);
			
			//var_dump($_FILES);die('files');
			if ( $this->upload->do_upload('image'))
			{
				$file_data = $this->upload->data();
				$img_name = md5($file_data['raw_name']).md5(strtotime("now")).$file_data['file_ext'];
				
				//var_dump($file_data['full_path']);die('full path');
				
				$this->image_resize($file_data['full_path'],'./assets/uploads/products/',$img_name,458,458);
				$info['prod_image'] = $img_name;
				unlink($file_data['full_path']);			
			}else
				{
					$this->data['error']= $this->upload->display_errors();
					$this->session->set_flashdata('messages_warning',lang('no_image_choice'));
				}

				$id= $this->products_m->insert($info);
				
				//send email for news letter 
				
				$from = $this->constants_m->get_by('item', 'newsletter_email_from')->value;
				$subject= lang('new_product');
				$mails=$this->newsletter_m->get_all();
				$msg=lang('visit_our_website').': <br>';
				$msg.=lang('new_product').'<br>'.lang('this_link').' : '.site_url('main/product/'.$id);
				foreach ($mails as $key => $mail) {
					@$this->send_email($from,$mail->Email,$subject,$msg);

					
				}

				// !send email
				$this->session->set_flashdata('messages_success', lang('save_successfully'));	
				//redirect('dashboard/edit_product/'.$id.'/'.$this->uri->segment(3));	
							
				redirect('dashboard/show_all_products_category/'.$category->Cat_ID);
				
			}
			else
			{
				//var_dump($_POST);die('error');
				 
				if (validation_errors()){
					$this->session->set_flashdata('messages_success', validation_errors());
					$this->data['error']=(validation_errors());
					$this->data['mode']='error';
				}
				$this->data['prod_name_en'] = array('name' => 'prod_name_en',
					'value' => $this->form_validation->set_value('prod_name_en'),
				);
				
				$this->data['prod_name_ar'] = array('name' => 'prod_name_ar',
					'value' => $this->form_validation->set_value('prod_name_ar'),
				);
				
				$this->data['prod_name_rd'] = array('name' => 'prod_name_rd',
					'value' => $this->form_validation->set_value('prod_name_rd'),
				);
				
				$this->data['prod_description_en'] = array('name' => 'prod_description_en',
					'value' => $this->form_validation->set_value('prod_description_en'),
					'id' => 'prod_description_en'
				);
				
				$this->data['prod_description_ar'] = array('name' => 'prod_description_ar',
					'value' => $this->form_validation->set_value('prod_description_ar'),
					'id' => 'prod_description_ar'
				);
				$this->data['prod_description_rd'] = array('name' => 'prod_description_rd',
					'value' => $this->form_validation->set_value('prod_description_rd'),
					'id' => 'prod_description_rd'
				);
				
				$this->data['prod_price_en'] = array('name' => 'prod_price_en',
					'value' => $this->form_validation->set_value('prod_price_en'),
				);
				$this->data['prod_price_ar'] = array('name' => 'prod_price_ar',
					'value' => $this->form_validation->set_value('prod_price_ar'),
				);
				$this->data['prod_price_rd'] = array('name' => 'prod_price_rd',
					'value' => $this->form_validation->set_value('prod_price_rd'),
				);
				$this->data['prod_price'] = array('name' => 'prod_price',
					'value' => $this->form_validation->set_value('prod_price'),
				);
				$this->data['prod_price_discount'] = array('name' => 'prod_price_discount',
					'value' => $this->form_validation->set_value('prod_price_discount'),
				);
				$this->data['prod_price_discount_en'] = array('name' => 'prod_price_discount_en',
					'value' => $this->form_validation->set_value('prod_price_discount_en'),
				);
				$this->data['prod_price_discount_ar'] = array('name' => 'prod_price_discount_ar',
					'value' => $this->form_validation->set_value('prod_price_discount_ar'),
				);
				$this->data['prod_price_discount_rd'] = array('name' => 'prod_price_discount_rd',
					'value' => $this->form_validation->set_value('prod_price_discount_rd'),
				);
				$this->data['prod_number'] = array('name' => 'prod_number',
					'value' => $this->form_validation->set_value('prod_number'),
				);
                $this->data['prod_quantity'] = array('name' => 'prod_quantity',
                    'value' => $this->form_validation->set_value('prod_quantity'),
                );


				$this->data['main_categories']=$this->category_m->get_many_by('parent_id',0);
				$this->data['styles']=$this->style_m->get_all();
				$this->data['page']=lang('products');
				$parent_id = $this->uri->segment(3);
		        $breadcrumbs = array();
		        while($parent_id) {
		            $category= $this->category_m->get_by('Cat_ID',$parent_id);
		            $parent_id = $category->parent_id;
		            $breadcrumbs[] = $category;
		        }
		        $this->data['breadcrumbs'] = array_reverse($breadcrumbs);
		
				// $this->data['view_page'] = 'categories/add';
				// $this->load->view('cpanel/cpanel_controller',$this->data);
			}
		}else{
		redirect('main');
		}


	}

	
    //callback function  in for validation
    public function is_unique_product_number_check($number,$product_id=false)
    {

        
        $product=$this->products_m->get_by('prod_number',$number);
        if($product &&  $product->prod_id != $product_id) {
            $this->form_validation->set_message('is_unique_product_number_check', lang('prod_number_must_unique'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

	// edit product info  
	public function edit_product($id='')
	{
		

		
		if($this->has_privilage($this->session->userdata('user_id'),"edit_product")){
			
		if ( $id==='' || !is_numeric($id))
			redirect('dashboard/show_all_products_category');
		else{
			$this->form_validation->set_rules('prod_name_en', lang('prod_name_en'), 'required');
			$this->form_validation->set_rules('prod_description_en', lang('prod_des_en'), '');
			$this->form_validation->set_rules('prod_price_en', lang('prod_price_en'), 'numeric');
			$this->form_validation->set_rules('prod_price_discount_en', lang('prod_price_discount_en'), 'numeric');
						
			$this->form_validation->set_rules('prod_name_ar', lang('prod_name_ar'), 'required');
			$this->form_validation->set_rules('prod_description_ar', lang('prod_des_ar'), '');
			$this->form_validation->set_rules('prod_price_ar',lang('prod_price_ar'), 'numeric');
			$this->form_validation->set_rules('prod_price_discount_ar', lang('prod_price_discount_ar'), 'numeric');
			
			// $this->form_validation->set_rules('prod_name_rd', lang('prod_name_rd'), 'required');
			// $this->form_validation->set_rules('prod_description_rd',  lang('prod_des_rd'), '');
			// $this->form_validation->set_rules('prod_price_rd', lang('prod_price_rd'), '');
			// $this->form_validation->set_rules('prod_price_discount_rd', lang('prod_price_discount_rd'), '');
			
			$this->form_validation->set_rules('prod_price', lang('prod_price_other'), 'numeric');
			$this->form_validation->set_rules('prod_price_discount', lang('prod_price_discount'), 'numeric');
			
			
			//$this->form_validation->set_rules('gender', lang('gender'), 'required');
			$this->form_validation->set_rules('prod_number', lang('prod_number'), 'required|callback_is_unique_product_number_check['.$this->uri->segment(3).']');
			$this->form_validation->set_rules('prod_quantity', lang('prod_quantity'), 'required');

			$this->form_validation->set_rules('style_id',lang('style'), 'required');
			
			$this->form_validation->set_rules('prod_cat_id',lang('category'), 'required');
			$this->form_validation->set_rules('prod_main_cat_id',lang('category'), 'required');

							
							
			if ($this->form_validation->run() == true)
			{
				
               

				$info=array('prod_name_en'=>$this->input->post('prod_name_en'),
					'prod_name_ar'=>$this->input->post('prod_name_ar'),
					'prod_name_rd'=>$this->input->post('prod_name_rd'),
					'prod_description_en'=>$this->input->post('prod_description_en'),
					'prod_description_ar'=>$this->input->post('prod_description_ar'),
					'prod_description_rd'=>$this->input->post('prod_description_rd'),
					'prod_price_en'=>$this->input->post('prod_price_en'),
					'prod_price_ar'=>$this->input->post('prod_price_ar'),
					'prod_price_rd'=>$this->input->post('prod_price_rd'),
					'prod_price'=>$this->input->post('prod_price'),
					'prod_price_discount_en'=>$this->input->post('prod_price_discount_en'),
					'prod_price_discount_ar'=>$this->input->post('prod_price_discount_ar'),
					'prod_price_discount_rd'=>$this->input->post('prod_price_discount_rd'),
					'prod_price_discount'=>$this->input->post('prod_price_discount'),
					'prod_style_id'=>$this->input->post('style_id'),
					
					//'prod_gender'=>$this->input->post('gender'),
					'prod_cat_id'=>$this->input->post('prod_cat_id'),
					'prod_main_cat_id' =>$this->input->post('prod_main_cat_id'),
					'prod_number'		=> $this->input->post('prod_number'),
                    'prod_quantity'       => $this->input->post('prod_quantity'),
					'prod_id' => $id,
					);

							

           
                $old_item=$this->products_m->get_by('prod_id',$id);
                // image file
                $old_file = $old_item?$old_item->prod_image:false;
                $res = $this->_update_image('image', './assets/uploads/products/', $this->input->post('delete_image_image'), $old_file);
                if($res['success'] && isset($res['filename'])){
                    $info['prod_image'] = $res['filename'];
                }else
                    $info['prod_image'] = '';

			// $this->config_upload('./assets/uploads/products/',3000,3000,5000);
			// if ( $this->upload->do_upload('image'))
			// {
				
				
			// 	$file_data = $this->upload->data();
			// 	$img_name = md5($file_data['raw_name']).md5(strtotime("now")).$file_data['file_ext'];
			// 	$this->image_resize($file_data['full_path'],'./assets/uploads/products/',$img_name,458,458);
			// 	$info['prod_image'] = $img_name;
			// 	unlink($file_data['full_path']);
			// 	$prod=$this->products_m->get_by('prod_id',$id);
   //              if(is_dir(base_url().'assets/uploads/products/'.$prod->prod_image))
			// 	    unlink('./assets/uploads/products/'.$prod->prod_image);			
			// }else
			// 	{
					
			// 		// $this->data['error']= $this->upload->display_errors(); // errors
			// 		// $this->session->set_flashdata('messages_warning',lang('no_image_choice'));
			// 	}

			   if ( $this->products_m->update_by(array('prod_id' => $id),$info)){

    				$this->session->set_flashdata('messages_success', lang('save_successfully'));					
    				redirect('dashboard/show_all_products_category/'.$this->input->post('prod_cat_id'));
                }
                else{
                    $this->session->set_flashdata('messages_success', lang('save_failed'));   
                }
				
			}
			else
			{
				
					// multimedia
	        $this -> load -> library('multimedia_crud');
	
	        $this -> multimedia_crud -> set_mode(true, true, true, true, true);
	        $this -> multimedia_crud -> set_primary_key_field('id');
	        $this -> multimedia_crud -> set_url_field('url');
	        $this -> multimedia_crud -> set_title_field('title');
	        $this -> multimedia_crud -> set_table('product_gallery') -> set_ordering_field('priority') -> set_image_path("assets/uploads/products/galleries");
	        $this -> multimedia_crud -> set_relation_field('product_id');
	        // $this -> multimedia_crud -> set_thumb_field('thumb_url');
	        // $this -> multimedia_crud -> set_title_field('title');
	
	        $this->data['multimedia_crud'] = (array) $this -> multimedia_crud -> render();
			 
				if (validation_errors()){
					$this->session->set_flashdata('messages_warning', validation_errors());
				}
				$prod=$this->products_m->get_by('prod_id',$id);
				
				$this->data['prod_name_en'] = array('name' => 'prod_name_en',
					'value' => $this->form_validation->set_value('prod_name_en')?$this->form_validation->set_value('prod_name_en'):$prod->prod_name_en,
				);
				
				$this->data['prod_name_ar'] = array('name' => 'prod_name_ar',
					'value' => $this->form_validation->set_value('prod_name_ar')?$this->form_validation->set_value('prod_name_ar'):$prod->prod_name_ar,
				);
				$this->data['prod_name_rd'] = array('name' => 'prod_name_rd',
					'value' => $this->form_validation->set_value('prod_name_rd')?$this->form_validation->set_value('prod_name_rd'):$prod->prod_name_rd,
				);
				
				$this->data['prod_description_en'] = array('name' => 'prod_description_en',
					'value' => $this->form_validation->set_value('prod_description_en')?$this->form_validation->set_value('prod_description_en'):$prod->prod_description_en,
				);
				
				$this->data['prod_description_ar'] = array('name' => 'prod_description_ar',
					'value' => $this->form_validation->set_value('prod_description_ar')?$this->form_validation->set_value('prod_description_ar'):$prod->prod_description_ar,
				);
				$this->data['prod_description_rd'] = array('name' => 'prod_description_rd',
					'value' => $this->form_validation->set_value('prod_description_rd')?$this->form_validation->set_value('prod_description_rd'):$prod->prod_description_rd,
				);
				
				$this->data['prod_price_en'] = array('name' => 'prod_price_en',
					'value' => $this->form_validation->set_value('prod_price_en')?$this->form_validation->set_value('prod_price_en'):$prod->prod_price_en,
				);
				$this->data['prod_price_ar'] = array('name' => 'prod_price_ar',
					'value' => $this->form_validation->set_value('prod_price_ar')?$this->form_validation->set_value('prod_price_ar'):$prod->prod_price_ar,
				);
				$this->data['prod_price_rd'] = array('name' => 'prod_price_rd',
					'value' => $this->form_validation->set_value('prod_price_rd')?$this->form_validation->set_value('prod_price_rd'):$prod->prod_price_rd,
				);
				$this->data['prod_price'] = array('name' => 'prod_price',
					'value' => $this->form_validation->set_value('prod_price')?$this->form_validation->set_value('prod_price'):$prod->prod_price,
				);
				
				$this->data['prod_price_discount_en'] = array('name' => 'prod_price_discount_en',
					'value' => $this->form_validation->set_value('prod_price_discount_en')?$this->form_validation->set_value('prod_price_discount_en'):$prod->prod_price_discount_en,
				);
				$this->data['prod_price_discount_ar'] = array('name' => 'prod_price_discount_ar',
					'value' => $this->form_validation->set_value('prod_price_discount_ar')?$this->form_validation->set_value('prod_price_discount_ar'):$prod->prod_price_discount_ar,
				);
				$this->data['prod_price_discount_rd'] = array('name' => 'prod_price_discount_rd',
					'value' => $this->form_validation->set_value('prod_price_discount_rd')?$this->form_validation->set_value('prod_price_discount_rd'):$prod->prod_price_discount_rd,
				);
				$this->data['prod_price_discount'] = array('name' => 'prod_price_discount',
					'value' => $this->form_validation->set_value('prod_price_discount')?$this->form_validation->set_value('prod_price_discount'):$prod->prod_price_discount,
				);
				
				$this->data['prod_number'] = array('name' => 'prod_number',
					'value' => $this->form_validation->set_value('prod_number')?$this->form_validation->set_value('prod_number'):$prod->prod_number,
				);
				
				$this->data['prod_quantity'] = array('name' => 'prod_quantity',
                    'value' => $this->form_validation->set_value('prod_quantity')?$this->form_validation->set_value('prod_quantity'):$prod->prod_quantity,
                );

                
				
				$this->data['product']=$prod;
				$this->data['main_categories']=$this->category_m->get_many_by('parent_id',0);
				$this->data['sub_categories']=$this->category_m->get_many_by('parent_id',$prod->prod_main_cat_id);
				$this->data['styles']=$this->style_m->get_all();
				$this->data['page']=lang('products');
				
                $parent_id = $this->input->get('category');
		        $breadcrumbs = array();
		        while($parent_id) {
		            $category= $this->category_m->get_by('Cat_ID',$parent_id);
		            $parent_id = $category->parent_id;
		            $breadcrumbs[] = $category;
		        }
		        $this->data['breadcrumbs'] = array_reverse($breadcrumbs);
		
				// $this->data['view_page'] = 'categories/add';
				// $this->load->view('cpanel/cpanel_controller',$this->data);
			}
		}
		
		}else{
		redirect('main');
		}
			
	}

	 // delete product info  
	public function delete_product()
	{
		if($this->has_privilage($this->session->userdata('user_id'),"delete_product")){
		
		if ($this->uri->segment(3) == false)
		{
		  redirect($this->config->item('base_url'), 'refresh');		// here 
		}
		else
		{
			if(!is_numeric($this->uri->segment(3))){
				redirect('dashboard/show_all_products_category');
			}
			$this->products_m->delete_by('prod_id',$this->uri->segment(3));
            $this->session->set_flashdata('messages_success', lang('delete_success'));
            if ($this->uri->segment(4))
			     redirect('dashboard/show_all_products_category/'.$this->uri->segment(4));
            else
                redirect('dashboard/show_all_products_category');
                
		}
		}else{
		redirect('main');
		}
				
	}


//albums
	
	//  view all albums ( image album and video album )
	public function albums()
	{
		if($this->has_privilage($this->session->userdata('user_id'),"albums")){
			
		$this->data['video_albums']=$this->album_m->get_many_by('alb_type','video');
		$this->data['image_albums']=$this->album_m->get_many_by('alb_type','image');
		}else{
			redirect('main');
		}
	}
	
	//  add new album( image album or  video album ) by parameter 
	public function add_album($type)
	{
		
		if($this->has_privilage($this->session->userdata('user_id'),"add_album")){
			
		$this->form_validation->set_rules('alb_title_en', lang('alb_title_en'), 'required');
		$this->form_validation->set_rules('alb_title_ar', lang('alb_title_ar'), 'required');
		$this->form_validation->set_rules('alb_title_rd',lang('alb_title_rd'), '');
		//$this->form_validation->set_rules('type', lang('type'), 'required');
		
		if ($type == 'image' || $type=='video'){

		if ($this->form_validation->run() == true)
		{
			$info=array('alb_title_en'=>$this->input->post('alb_title_en'),
						'alb_title_ar'=>$this->input->post('alb_title_ar'),
						'alb_title_rd'=>$this->input->post('alb_title_rd'),
						'alb_type' => $type);
			$id= $this->album_m->insert($info);
			$this->session->set_flashdata('messages_success', lang('save_successfully'));	

			redirect('dashboard/edit_album/'.$id);
		}
		else
		{
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->set_flashdata('messages_warning', lang('save_failed'));
			$this->data['alb_title_en'] = array('name' => 'alb_title_en',
				'id' => 'alb_title_en',
				'type' => 'text',
				'value' => $this->form_validation->set_value('alb_title_en'),
			);
			
			$this->data['alb_title_ar'] = array('name' => 'alb_title_ar',
				'id' => 'alb_title_ar',
				'type' => 'text',
				'value' => $this->form_validation->set_value('catalb_title_ar_name_ar'),
			);
			
			$this->data['alb_title_rd'] = array('name' => 'alb_title_rd',
				'id' => 'alb_title_rd',
				'type' => 'text',
				'value' => $this->form_validation->set_value('alb_title_rd'),
			);

		}
		}else{
			redirect('main');
		}

		}else{
		redirect('main');
		}

	}
	// public function show_album($value='')
	// {
// 		
	// }
	
	//  edit album info and gallery ( image album or  video album ) 
	public function edit_album($id)
	{
		if($this->has_privilage($this->session->userdata('user_id'),"edit_album")){
			
			//var_dump($_POST);die();
		if (!$id || !is_numeric($id) )
			redirect();
		$album=$this->album_m->get_by('id',$id);
		
		$this->form_validation->set_rules('alb_title_en', lang('alb_title_en'), 'required');
		$this->form_validation->set_rules('alb_title_ar', lang('alb_title_ar'), 'required');
		$this->form_validation->set_rules('alb_title_rd',lang('alb_title_rd'), '');
		//$this->form_validation->set_rules('type', lang('type'), 'required');
		
		if ($this->form_validation->run() == true)
		{
			$data=array('alb_title_en'=>$this->input->post('alb_title_en'),
						'alb_title_ar'=>$this->input->post('alb_title_ar'),
						'alb_title_rd'=>$this->input->post('alb_title_rd'),
						//'alb_type' => $this->input->post('type'),
						);
			$this->album_m->update($id,$data);
			$this->session->set_flashdata('messages_success', lang('save_successfully'));	

			redirect('dashboard/albums');
		}
		else
		{
			// multimedia
	        $this -> load -> library('multimedia_crud');
	
	        $this -> multimedia_crud -> set_mode(true, true, true, true, true);
	        $this -> multimedia_crud -> set_primary_key_field('id');
	        $this -> multimedia_crud -> set_url_field('url');
	        $this -> multimedia_crud -> set_title_field('title');
	        $this -> multimedia_crud -> set_table('album_gallery') -> set_ordering_field('priority') -> set_image_path("assets/uploads/album_galleries");
	        $this -> multimedia_crud -> set_relation_field('album_id');
	        // $this -> multimedia_crud -> set_thumb_field('thumb_url');
	        // $this -> multimedia_crud -> set_title_field('title');
	
	        $this->data['multimedia_crud'] = (array) $this -> multimedia_crud -> render();
		 
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->set_flashdata('messages_warning', lang('save_failed'));
			$this->data['alb_title_en'] = array('name' => 'alb_title_en',
				'id' => 'alb_title_en',
				'type' => 'text',
				'value' => $this->form_validation->set_value('alb_title_en')?$this->form_validation->set_value('alb_title_en') :$album->alb_title_en,
			);
			
			$this->data['alb_title_ar'] = array('name' => 'alb_title_ar',
				'id' => 'alb_title_ar',
				'type' => 'text',
				'value' => $this->form_validation->set_value('catalb_title_ar_name_ar')? $this->form_validation->set_value('catalb_title_ar_name_ar'):$album->alb_title_ar,
			);
			
			$this->data['alb_title_rd'] = array('name' => 'alb_title_rd',
				'id' => 'alb_title_rd',
				'type' => 'text',
				'value' => $this->form_validation->set_value('alb_title_rd')?$this->form_validation->set_value('alb_title_rd'):$album->alb_title_rd,
			);
			$this->data['album']=$album;

		}
		}else{
		redirect('main');
		}

	}

	//  delete album info and gallery ( image album or  video album ) 
	public function delete_album()
	{
		if($this->has_privilage($this->session->userdata('user_id'),"delete_album")){
			
		if ($this->uri->segment(3) == false)
		{
		  redirect($this->config->item('base_url'), 'refresh');		// here 
		}
		else
		{
			if(!is_numeric($this->uri->segment(3))){
				redirect('dashboard/albums');
			}
			$this->album_m->delete_by('id',$this->uri->segment(3));
			redirect('dashboard/albums');
		}
		}else{
		redirect('main');
		}
	}


// excel files management 
 
 	//  update products price by file excel 
	 public function update_products_price()
	 {
	 	if($this->has_privilage($this->session->userdata('user_id'),"update_products_price")){
	 			
		$res = $this->_upload_file('excel', './assets/uploads/excel_files/','*');
		//var_dump($res);die();
        if($res['success'] && isset($res['filename'])){
            $filename= $res['filename'];
			
		    $path=APPPATH."../assets/uploads/excel_files/".$filename;
			//var_dump($path);
			require_once APPPATH."third_party/PHPExcel-1.8/example/my_script.php";	 		
			//read_excel_file($path);
			$data=read_excel_file($path);
            // echo '</pre>';
            // var_dump($data);die();
			for ($i=2; $i <= sizeof($data); $i++) {
				$prod=$data[$i];
				// $item=array('prod_price_en' => $prod[1],
				// 			'prod_price_ar' => $prod[2],
				// 			'prod_price_rd' => $prod[3],
				// 			'prod_price' => $prod[4],
				// 			); 
                $item=array('prod_price'            => $prod[1],
                            'prod_price_discount'   => $prod[2],
                            'prod_price_ar'         => $prod[3],
                            'prod_price_discount_ar' => $prod[4],
                            'prod_price_en'         => $prod[5],
                            'prod_price_discount_en' => $prod[6],
                            'prod_price_rd'         => $prod[7],
                            'prod_price_discount_rd' => $prod[8],
                            ); 
				$where=array('prod_number'  => ''.$prod[0]);
				
               // var_dump($where);die();
				$this->products_m->update_by($where,$item);
                // var_dump($item);
                // var_dump($where);die();
			}
            
			$this->session->set_flashdata('messages_success', lang('update_products_successfully'));
			redirect('dashboard/show_all_products_category');
        }else{
        	$this->session->set_flashdata('messages_warning', lang('failed_in_update_products'));
        	redirect('dashboard/show_all_products_category');
        }	
		}else{
			redirect('main');
			}
	 }
 

     public function export_users_to_excel()
    {
        require_once APPPATH."third_party/PHPExcel-1.8/example/my_script.php"; 

        $this->db->join('town','town.id = users.town_id','left');
        $this->db->join('education','education.ed_id = users.education_id','left');
        $users=$this->users_m->get_all();         
        export_file_excel($users,$this->_lang);
        exit;
    }

 	//  show buying order based by parameter ( history order ot pending order )
	 public function orders($type='')
	 {
	 	if($this->has_privilage($this->session->userdata('user_id'),"orders")){
	 		
	    if($type =='' || !is_numeric($type))
		 	redirect('main');
		elseif( $type == 1 || $type==2) {
			if ($type== 1 ){                      // order history

				$this->db->join('users','users.id = order.user_id');
				$orders=$this->order_m->get_many_by(array('status <> ' => "pending"));
			}elseif($type== 2){                  // order pending

				$this->db->join('users','users.id = order.user_id');
				$orders=$this->order_m->get_many_by(array('status ' => "pending"));
			}
			foreach ($orders as $key => $order) {
				$this->db->join('products','order_product.product_id = products.prod_id');
				$order->products=$this->order_product_m->get_many_by('order_id',$order->order_id);
			}
			$this->data['orders']=$orders;

		}else
			redirect('main');
			
	}else{
	redirect('main');
	}		
			
	}
	 
	//  show reports based by parameter ( report buying order count )
	 public function reports($type='')
	 {
	 	if($this->has_privilage($this->session->userdata('user_id'),"reports")){
	 		
	    if($type =='' || !is_numeric($type))
		 	redirect('main');
		elseif( $type == 1 || $type==2) {
			if ($type== 1 ){ // order count report
				$this->db->order_by('prod_order_count desc');
                $this->db->where('prod_order_count !=' ,0);
				$this->data['products']=$this->products_m->get_all();
                //var_dump($this->data['products']);die();
			}
		}else
			redirect('main');
	}else{
	redirect('main');
	}
	}

	//  accept buying  order 
	public function accept_order($id_order)
	{
		if($this->has_privilage($this->session->userdata('user_id'),"accept_order")){
			
		 if($id_order =='' || !is_numeric($id_order))
		 	redirect('main');
		else{
			$this->order_m->update_by(array('order_id' => $id_order),array('status' => "accept"));
			
            //update product quantity 
           // $this->order_m->get_by
            //$this->db->join('products','order_product.product_id = products.prod_id');
            $products=$this->order_product_m->get_many_by('order_id',$id_order);
            foreach ($products as $key => $product) {
                $prod=$this->products_m->get_by(array('prod_id' => $product->product_id));
                $this->products_m->update_by(array('prod_id' => $product->product_id) , array('prod_quantity' => $prod->prod_quantity - $product->qty));
            }

			//send email 
			$order_email_from = $this->constants_m->get_by('item', 'order_email_from')->value;
			$odrer_email_to = $this->session->userdata('email');
			$msg=lang('accept_your_order');
			$subject= lang('product_order');	
			$this->send_email($order_email_from,$odrer_email_to,$subject,$msg);
			
			redirect('dashboard/orders/2');
		}
		}else{
		redirect('main');
		}
		
	}
	
	//  reject  buying  order 
	public function reject_order($id_order)
	{
		if($this->has_privilage($this->session->userdata('user_id'),"reject_order")){
			
		 if($id_order =='' || !is_numeric($id_order))
		 	redirect('main');
		else{
			$this->order_m->update_by(array('order_id' => $id_order),array('status' => "reject"));
			
			//send email 
			$order_email_from = $this->constants_m->get_by('item', 'order_email_from')->value;
			$odrer_email_to = $this->constants_m->get_by('item', 'order_email_to')->value;
			$msg=lang('reject_your_order');
			$subject= lang('product_order');	
			$this->send_email($order_email_from,$odrer_email_to,$subject,$msg);
			//$this->session->set_flashdata('messages_success', lang('reject_success'));
			
			redirect('dashboard/orders/2');
		}
		}else{
		redirect('main');
		}
	}
	 

//edit brands images
public function brands()
{
	if($this->has_privilage($this->session->userdata('user_id'),"brands")){
	// multimedia
    $this -> load -> library('multimedia_crud');

    $this -> multimedia_crud -> set_mode(true, 0, 0, 0, 0);
    $this -> multimedia_crud -> set_primary_key_field('id');
    $this -> multimedia_crud -> set_url_field('url');
    $this -> multimedia_crud -> set_title_field('title');
    $this -> multimedia_crud -> set_table('brands') -> set_ordering_field('priority') -> set_image_path("assets/uploads/brands");
    $this -> multimedia_crud -> set_relation_field('relation_id');
    // $this -> multimedia_crud -> set_thumb_field('thumb_url');
    // $this -> multimedia_crud -> set_title_field('title');

    $this->data['multimedia_crud'] = (array) $this -> multimedia_crud -> render();
  }else{
	redirect('main');
	}    	
}

// images functions 

	// config upload library 
	private function config_upload($path,$max_width,$max_height,$max_size)
	{
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_width']  = $max_width;
		$config['max_height']  = $max_height;
		
		$config['max_size']	= $max_size;

		$this->load->library('upload', $config);
	}
	
	// image resize by  upload library 
	private function image_resize($path,$save_path,$image_name,$width,$height)
    {
    	$this->load->library('image_lib'); 
        $config['image_library'] = 'gd2'; 
        $config['source_image'] = $path;
        $config['new_image'] = $save_path.$image_name; 
        $config['create_thumb'] = FALSE; 
        $config['maintain_ratio'] = FALSE; 
        $config['width'] = $width; 
        $config['height'] = $height; 
        
        $this->image_lib->initialize($config); 
        
       
         
        // handle if there is any problem 
        if ( ! $this->image_lib->resize())
        {
             return $this->image_lib->display_errors(); 
        }
        $this->image_lib->clear();
        return 1;
    }
	
	
	// privileges 
	function assig_priv()
	{
		if($this->has_privilage($this->session->userdata('user_id'),"assigning_privilage")){
			$this->data['mode'] = "success";
			$this->data['message'] = '';
			$this->data['roles'] = $this->privilege_model->get_roles();
		}else{
			$this->data['result'] = "faild";
		}
		
	}
	 // function assign_role()
	// {
		// if($this->has_privilage($this->session->userdata('user_id'),"assigning_role")){
			// $this->form_validation->set_rules('group_id', 'ID_Group', 'required|xss_clean|numeric');
			// $this->form_validation->set_rules('user_id', 'ID_User', 'required|xss_clean|numeric');
			// if($this->form_validation->run() == TRUE){
				// $res = $this->privilege_model->add_users_role($group_id,$user_id);
				// $this->data['users'] = $this->privilege_model->get_users();
			// }else{
// 				
			// }
// 			
		// }
	// }
	
	// get privilages
    public function get_privilages()
    {
    	
		if($this->has_privilage($this->session->userdata('user_id'),"assigning_privilage")){
			$this->form_validation->set_rules('role_id', 'Role', 'required|xss_clean|numeric');
			if($this->form_validation->run() === TRUE) {
				$role_id   = $this->input->post('role_id');
			    $privilage = $this->privilege_model->get_privilage_by_role_id($role_id);
				$this->view = FALSE;
			    echo json_encode($privilage);
			}
			else {
				return 'No Content';
			}
		}
		
        
    }
	
	// get roles
	 public function get_roles()
    {
		if($this->has_privilage($this->session->userdata('user_id'),"assigning_role")){
			$this->form_validation->set_rules('user_id', 'User', 'required|xss_clean|numeric');
			if($this->form_validation->run() === TRUE) {
				$user_id   = $this->input->post('user_id');
			    $roles = $this->privilege_model->get_role_by_user_id($user_id);
				$this->view = FALSE;
			    echo json_encode($roles);
			}
			else {
				return 'No Content';
			}
		}
		
        
    }
	
	// delete role privilage
	public function delete_role_privilage()
	{
		if($this->has_privilage($this->session->userdata('user_id'),"remove_assigned_privilage")){
			$this->form_validation->set_rules('priv_id', 'ID_role_privilage', 'required|xss_clean|numeric');
			$this->form_validation->set_rules('role_id', 'ID_role', 'required|xss_clean|numeric');
			$priv_id = $this->input->post('priv_id');
			$role_id = $this->input->post('role_id');
			if($this->form_validation->run() === TRUE) {
			    $res = $this->privilege_model->delete_role_privilage($priv_id,$role_id);
				if($res){
					$this->view = FALSE;
					echo lang('priv_delete_succss');
					
				}else{
					$this->view = FALSE;
					echo lang('priv_delete_failed');
					
				}
			}
			else {
				$this->view = FALSE;
				echo validation_errors();
			}
		}
		else{
			$this->view = FALSE;
			echo '403 Forbidden';
		}
		
	}
	// public function delete_users_roles($id)
	// {
// 		
		// if($this->has_privilage($this->session->userdata('user_id'),"assigning_role")){
			// $this->form_validation->set_rules($id, 'ID_users_groups', 'required|xss_clean|numeric');
			// if(is_numeric($id)) {
			    // $res = $this->privilege_model->delete_users_groups($id);
				// if($res){
					// $this->data['result'] = "sucess";
// 					
				// }else{
					// $this->data['result'] = "faild";
// 					
				// }
			// }
			// else {
				// return 'No Content';
			// }
		// }
// 		
	// }
	
	// get non privilages for role by id 
	public function get_non_privilages()
    {
    	
		if($this->has_privilage($this->session->userdata('user_id'),"assigning_privilage")){
			$this->form_validation->set_rules('role_id', 'Role', 'required|xss_clean|numeric');
			if($this->form_validation->run() === TRUE) {
				$role_id   = $this->input->post('role_id');
			    $privilage = $this->privilege_model->get_non_privilage_by_role_id($role_id);
				$this->view = FALSE;
			    echo json_encode($privilage);
			}
			else {
				return 'No Content';
			}
		}
		
        
    }
	
   // get non roles  for user by user_id 
	public function get_non_roles()
    {
		if($this->has_privilage($this->session->userdata('user_id'),"assigning_role")){
			$this->form_validation->set_rules('user_id', 'User', 'required|xss_clean|numeric');
			if($this->form_validation->run() === TRUE) {
				$user_id   = $this->input->post('user_id');
			    $roles = $this->privilege_model->get_non_role_by_user_id($user_id);
			    $this->view = FALSE;
			    echo json_encode($roles);
			}
			else {
				return 'No Content';
			}
		}
		
        
    }
    public function add_role_priv()
    {
        if($this->has_privilage($this->session->userdata('user_id'),"assigning_privilage")){
			
			$this->form_validation->set_rules('privs', 'ID_role_privilage', 'required|xss_clean');
			$this->form_validation->set_rules('role_id', 'ID_role', 'required|xss_clean|numeric');
			if($this->form_validation->run() === TRUE) {
				$privs = $this->input->post('privs');
				$role_id = $this->input->post('role_id');
				$res = $this->privilege_model->add_role_privilage($privs,$role_id);
				if($res){
					$this->view = FALSE;
					echo lang('priv_added_success');
					
				}else{
					$this->view = FALSE;
					echo lang('priv_added_failed');
					
				}
				
			}
			else {
				$this->view = FALSE;
				echo validation_errors();
			}
		}
		else{
			$this->view = FALSE;
			echo '403 Forbidden';
		}
    }
    // add role privilage  for role  by role_id 
	public function add_role_privilage($id_privilage,$id_role)
	{
		
		if($this->has_privilage($this->session->userdata('user_id'),"assigning_privilage")){
			$this->data['result'] = "success";
			$this->form_validation->set_rules($id_privilage, 'ID_role_privilage', 'required|xss_clean|numeric');
			$this->form_validation->set_rules($id_role, 'ID_role', 'required|xss_clean|numeric');
			if(is_numeric($id_privilage)&&is_numeric($id_role)) {
				if(!$this->privilege_model->exist_role_privilage($id_privilage,$id_role)){
					$res = $this->privilege_model->add_role_privilage($id_privilage,$id_role);
					if($res){
						// toFix must taken from lang ..
						$this->data['mode'] = 'success';
						$this->data['message'] = lang('priv_added_success');
						$this->data['roles'] = $this->privilege_model->get_roles();
						$this->view = 'dashboard/assig_priv';
					}else{
						$this->data['result'] = "faild";
						$this->view = 'dashboard/assig_priv';
					}
				}
			}
			else {
				return 'No Content';
			}
		}
		
	}

	// add role to user 
	public function add_users_role($group_id,$user_id)
	{
		if($this->has_privilage($this->session->userdata('user_id'),"assigning_role")){
			
			if(is_numeric($group_id)&&is_numeric($user_id)) {
			    $res = $this->ion_auth_model->add_users_role($group_id,$user_id);
				if($res){
					$this->data['result'] = "sucess";
				}else{
					$this->data['result'] = "faild";
				}
			}
			else {
				return 'No Content';
			}
		}
	}

	

	// delete role 
	public function delete_role()
	{
		if($this->has_privilage($this->session->userdata('user_id'),"delete_role")){
			$this->data['mode'] = 'Not Ready';
			$this->data['roles'] = $this->privilege_model->get_roles();
			
		}
		else{
			$this->data['mode'] = 'no_priv';
			$this->data['message'] = '403 Forbidden';
		}
		
	}
	
	// remove role from user 
	public function remove_role($id)
	{
		
		if($this->has_privilage($this->session->userdata('user_id'),"delete_role")){
			if(is_numeric($id)) {
				if($this->privilege_model->remove_role($id)){
					$this->data['mode'] = 'success';
					$this->data['roles'] = $this->privilege_model->get_roles();
					$this->view = 'dashboard/delete_role';
				}else{
					$this->data['mode'] = 'error';
					$this->data['error'] = $this->form_validation->display_errors();
					$this->data['roles'] = $this->privilege_model->get_roles();
					$this->view = 'dashboard/delete_role';
				}
			}
			
		}
	}
	
	
	//  test if user have privilage 
	public function has_privilage($user_id,$priv_name)
	{
		return $this->privilege_model->user_has_preivilege($this->ion_auth->get_user_role($user_id),$priv_name);
	
		
	}
	
	// get user role 
	public function get_user_role($user_id)
	{
		$this->load->model('privilege_model');
		return $this->privilege_model->get_user_group($user_id);
	}
	
	//manage users ( edit active )
	public function manage_users()
	{
		if($this->has_privilage($this->session->userdata('user_id'),"show_users")){
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
		}
		
	}
	
	//create a new user
	function create_user()
	{
		$this->data['title'] = "Create User";

		if (!$this->ion_auth->logged_in()||!$this->ion_auth->is_admin())
		{
			redirect("dashboard", 'refresh');
		}
		
		$tables = $this->config->item('tables','ion_auth');

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique['.$tables['users'].'.email]');
		$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'required');
		$this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), '');
		$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('first_name')) . strtolower($this->input->post('last_name'));
			$email    = strtolower($this->input->post('email'));
			$password = $this->input->post('password');

			$additional_data = array(
				'first_name' => $this->input->post('first_name'),
				'last_name'  => $this->input->post('last_name'),
				'company'    => $this->input->post('company'),
				'phone'      => $this->input->post('phone'),
			);
		}
		if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data))
		{
			//check to see if we are creating the user
			//redirect them back to the admin page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("dashboard/manage_users", 'refresh');
		}
		else
		{
			//display the create user form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array(
				'name'  => 'first_name',
				'id'    => 'first_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('first_name'),
				'class' => 'form-control',
			);
			$this->data['last_name'] = array(
				'name'  => 'last_name',
				'id'    => 'last_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('last_name'),
				'class' => 'form-control',
			);
			$this->data['email'] = array(
				'name'  => 'email',
				'id'    => 'email',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('email'),
				'class' => 'form-control',
			);
			$this->data['company'] = array(
				'name'  => 'company',
				'id'    => 'company',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('company'),
				'class' => 'form-control',
			);
			$this->data['phone'] = array(
				'name'  => 'phone',
				'id'    => 'phone',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('phone'),
				'class' => 'form-control',
			);
			$this->data['password'] = array(
				'name'  => 'password',
				'id'    => 'password',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password'),
				'class' => 'form-control',
			);
			$this->data['password_confirm'] = array(
				'name'  => 'password_confirm',
				'id'    => 'password_confirm',
				'type'  => 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
				'class' => 'form-control',
			);
			
		}
	}

	//edit a user
	function edit_user($id)
	{
		$this->data['title'] = "Edit User";

		if (!$this->ion_auth->logged_in() || (!$this->has_privilage($this->session->userdata('user_id'),"edit_user") && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect("dashboard", 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		//validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');
		$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), '');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			//update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
					'company'    => $this->input->post('company'),
					'phone'      => $this->input->post('phone'),
				);

				//update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}



				// Only allow updating groups if user is admin
				if ($this->has_privilage($this->session->userdata('user_id'),"edit_user_role"))
				{
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

			//check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->has_privilage($this->session->userdata('user_id'),"show_users"))
					{
						redirect("dashboard/manage_users", 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }
			    else
			    {
			    	//redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->has_privilage($this->session->userdata('user_id'),"show_users"))
					{
						redirect("dashboard/manage_users", 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }

			}
		}

		//display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
			'class' => 'form-control',
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
			'class' => 'form-control',
		);
		$this->data['company'] = array(
			'name'  => 'company',
			'id'    => 'company',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('company', $user->company),
			'class' => 'form-control',
		);
		$this->data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
			'class' => 'form-control',
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password',
			'class' => 'form-control',
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password',
			'class' => 'form-control',
		);
		
		
	}

	// create a new group
	function create_group()
	{
		$this->data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('main', 'refresh');
		}

		//validate form input
		//$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');
		$this->form_validation->set_rules('group_name_ar', $this->lang->line('create_group_validation_name_label_ar'), 'required|xss_clean');
		$this->form_validation->set_rules('group_name_en', $this->lang->line('create_group_validation_name_label_en'), 'required|xss_clean');
		$this->form_validation->set_rules('group_name_rd', $this->lang->line('create_group_validation_name_label_rd'), 'xss_clean');

		if ($this->form_validation->run() == TRUE)
		{
			$group_names = array('name' => $this->input->post('group_name_en'),
								 'name_en'=> $this->input->post('group_name_en'),
								 'name_ar'=> $this->input->post('group_name_ar'),
								 'name_rd'=> $this->input->post('group_name_rd') );
			$new_group_id = $this->ion_auth->create_group($group_names, $this->input->post('description'));
			if($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("dashboard/manage_users", 'refresh');
			}
		}
		else
		{
			//display the create group form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$this->data['group_name_ar'] = array(
				'name'  => 'group_name_ar',
				'id'    => 'group_name_ar',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name_ar'),
			);
			$this->data['group_name_en'] = array(
				'name'  => 'group_name_en',
				'id'    => 'group_name_en',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name_en'),
			);
			$this->data['group_name_rd'] = array(
				'name'  => 'group_name_rd',
				'id'    => 'group_name_rd',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name_rd'),
			);
			$this->data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);
		
		}
	}

	//edit a group
	function edit_group($id)
	{
		// bail if no group id given
		if(!$id || empty($id))
		{
			redirect("dashboard", 'refresh');
		}

		$this->data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect("dashboard", 'refresh');
		}

		$group = $this->ion_auth->group($id)->row();

		//validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');
		$this->form_validation->set_rules('group_name_ar', $this->lang->line('create_group_validation_name_label_ar'), 'required|xss_clean');
		$this->form_validation->set_rules('group_name_en', $this->lang->line('create_group_validation_name_label_en'), 'required|xss_clean');
		$this->form_validation->set_rules('group_name_rd', $this->lang->line('create_group_validation_name_label_rd'), 'required|xss_clean');
		
		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_names = array('name' => $this->input->post('group_name'),
								 'name_en'=> $this->input->post('group_name_en'),
								 'name_ar'=> $this->input->post('group_name_ar'),
								 'name_rd'=> $this->input->post('group_name_rd') );
								 
				$group_update = $this->ion_auth->update_group($id, $group_names, $_POST['group_description']);

				if($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("dashboard/manage_users", 'refresh');
			}
		}

		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		//pass the user to the view
		$this->data['group'] = $group;

		$readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

		$this->data['group_name'] = array(
			'name'  => 'group_name',
			'id'    => 'group_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_name', $group->name),
			$readonly => $readonly,
		);
		$this->data['group_name_ar'] = array(
				'name'  => 'group_name_ar',
				'id'    => 'group_name_ar',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name_ar',$group->name_ar),
			);
			$this->data['group_name_en'] = array(
				'name'  => 'group_name_en',
				'id'    => 'group_name_en',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name_en',$group->name_en),
			);
			$this->data['group_name_rd'] = array(
				'name'  => 'group_name_rd',
				'id'    => 'group_name_rd',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name_rd',$group->name_rd),
			);
		$this->data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);

	}


	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function _render_page($view, $data=null, $render=false)
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $render);

		if (!$render) return $view_html;
	}
	//activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("dashboard/manage_users", 'refresh');
		}
		else
		{
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("dashboard/forgot_password", 'refresh');
		}
	}

	//deactivate the user
	function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			//redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();
			$this->view = "dashboard/deactivate_user";
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			//redirect them back to the auth page
			redirect("dashboard/manage_users", 'refresh');
		}
	}

	public function send_email($from,$to,$subject,$msg)
	{

        $this->config->set_item('language', 'english');

		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		// $config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;

		$this -> load -> library('email', $config);

		$this -> email -> from($from);
		$this -> email -> to($to);
		$this -> email -> subject($subject);
		$this -> email -> message($msg);

	    $result=  @$this -> email -> send();
        
        $this->config->set_item('language', $this->session->userdata('lang'));
        return $result;
	}
	
	public function statistics()
	{
        if($this->has_privilage($this->session->userdata('user_id'),"statistics")){

		$this->load->model('statistics_model');
		$this->data['users'] = $this->statistics_model->count_x('users');
		$this->data['categories'] = $this->statistics_model->count_x('categories');
		$this->data['products'] = $this->statistics_model->count_x('products');
		$this->data['articles']=$this->statistics_model->count_articles_x();
		
		$this->data['browser_statistics'] = $this->statistics_model->get_browser_statistics();
		$this->data['country_statistics'] = $this->statistics_model->get_visitors_count_per_country();
		//$this->data['most_product_views'] = $this->statistics_model->get_most_products_visiting();
		$this->db->order_by('prod_views', 'desc');
		$this->data['most_product_views'] = $this->products_m->get_all();
		
		$this->db->order_by('views', 'desc');
		$this->data['most_article_views'] = $this->articles_m->get_many_by('parent_id !=',-1);

		$main_categories = $this->category_m->get_many_by('parent_id', 0);
		foreach ($main_categories as $item) {
			$this->db->order_by('views', 'desc');
			$item->sub_categories = $this->category_m->get_many_by('parent_id', $item->Cat_ID);
		}
		
		$this->data['most_category_views'] =$main_categories;
		
		
		// visite rate 
		$this->load->model('general_model','statistics_date_m');
		$this->statistics_date_m->set_table('statistics_date');
		$dates=$this->statistics_date_m->get_all();
		$visiting_rate=array();
		$my_months=array(
			1=>0,
			2=>0,
			3=>0,
			4=>0,
			5=>0,
			6=>0,
			7=>0,
			8=>0,
			9=>0,
			10=>0,
			11=>0,
			12=>0,
		);
		foreach ($dates as $key => $date) {
			$month = date("m",strtotime($date->date));
			$year = date("Y",strtotime($date->date));
			if (isset($visiting_rate[$year][$month]))
				$visiting_rate[$year][$month]+=$date->count;
			else
				$visiting_rate[$year][$month]=$date->count;
			
		}

       

		$final=array();
		foreach ($visiting_rate as $year => $months) {
			$final_months=$my_months;
			$final_year=array();
			foreach ($my_months as $month => $count) {
				if (isset($months[$month]))
					$final_months[(int)$month] += (int)$months[$month];
				else
					$final_months[(int)$month] += (int)$count;
			}
			$final_year=array((int)$year, $final_months);
			$final[]=$final_year;
		}

		$this->data['visiting_rate']=$final;
		 // echo '</pre>';
		 // var_dump($this->data['visiting_rate']);die();

        }else{
            redirect();
        }
	}



}



/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
?>