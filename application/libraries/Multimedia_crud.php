<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MultiMedia_CRUD..
 * 
 */
class multimedia_CRUD {

	protected $table_name = null;
	protected $priority_field = null;
	protected $url_field = 'url';
	protected $title_field = null;
	protected $relation_field = null;
	protected $thumb_field = null;
	protected $subject = 'Record';
	protected $file_path = '';
	protected $primary_key = 'id';
	protected $ci = null;
	protected $thumbnail_prefix = 'thumb__';
	protected $views_as_string = '';
	protected $css_files = array();
	protected $js_files = array();
	
	/* Unsetters */
	protected $unset_delete = false;
	protected $unset_upload = false;

	protected $language = null;
	protected $lang_strings = array();
	protected $default_language_path = 'assets/multimedia_crud/languages';
	
	protected $controler_segment = 0;
	protected $method_segment = 1;
	protected $relation_segment = 2;
	protected $ajax_segment = 3;
		
	protected $mod_audio = false ;
	protected $mod_video = false ;
	protected $mod_image = true ;
	protected $mod_flash = false ;
	protected $mod_docs = false ;
	
	protected $max_file_size = 0;
	
	protected $default_audio_thumb_dir = 'assets/multimedia_crud/images/thumb-audio.jpg';
	protected $default_video_thumb_dir = 'assets/multimedia_crud/images/thumb-video.jpg';
	protected $default_flash_thumb_dir = 'assets/multimedia_crud/images/thumb-flash.jpg';
	protected $default_flash_url_thumb_dir ='assets/multimedia_crud/images/thumb-flash-url.jpg';
	protected $default_docs_thumb_dir = 'assets/multimedia_crud/images/thumb-docs.jpg';

	/**
	 *
	 * @var Image_moo
	 */
	private $image_moo = null;

	function __construct() {
		$this->ci = &get_instance();
	}
	
	function set_mode($image,$audio,$video,$flash,$docs)
	{
		$this->mod_image = $image;
		$this->mod_audio = $audio;
		$this->mod_video = $video;
		$this->mod_flash = $flash;
		$this->mod_docs = $docs;
	}
	
	function set_max_file_size($size)
	{
		if (!empty($size) && is_numeric($size) && $size >= 0)
			$this -> max_file_size = $size;
	}
	
	function get_url_cond()
	{
		$cond = '';
		if ($this -> mod_video) 
			$cond = 'ytVidId(data_video_url)';	
		if ($this -> mod_flash)
		{
			if($cond != '')
				$cond .= ' || ';
			$cond .= 'validFlash(data_video_url)';	
		}
		return $cond;
	}
	
	function is_url_form_needed()
	{
		return $this -> mod_video || $this -> mod_flash ;
	}
	
	function set_table($table_name)
	{
		$this->table_name = $table_name;

		return $this;
	}

	function set_relation_field($field_name)
	{
		$this->relation_field = $field_name;

		return $this;
	}

	function set_thumb_field($field_name)
	{
		$this-> thumb_field = $field_name;

		return $this;
	}

	function set_ordering_field($field_name)
	{
		$this->priority_field = $field_name;

		return $this;
	}

	function set_primary_key_field($field_name)
	{
		$this->primary_key = $field_name;
	}

	function set_subject($subject)
	{
		$this->subject = $subject;

		return $this;
	}

	function set_url_field($url_field)
	{
		$this->url_field = $url_field;

		return $this;
	}

	function set_title_field($title_field)
	{
		$this->title_field = $title_field;

		return $this;
	}

	function set_image_path($file_path,$forc_create_dir = true)
	{
		$this->image_path = $file_path;
		if (!file_exists($this->image_path) && !is_dir($this->image_path) && $forc_create_dir) {
    		mkdir($this -> image_path,0777, true);
		}  
		return $this;
	}

	function set_thumbnail_prefix($prefix)
	{
		$this->thumbnail_prefix = $prefix;

		return $this;
	}
	
	/**
	 * Unsets the delete operation from the gallery
	 *
	 * @return	void
	 */
	public function unset_delete()
	{
		$this->unset_delete = true;
	
		return $this;
	}	
	
	/**
	 * Unsets the upload functionality from the gallery
	 *
	 * @return	void
	 */
	public function unset_upload()
	{
		$this->unset_upload = true;
	
		return $this;
	}	
	
	public function set_css($css_file)
	{
		$this->css_files[sha1($css_file)] = base_url().$css_file;
	}

	public function set_js($js_file)
	{
		$this->js_files[sha1($js_file)] = base_url().$js_file;
	}
	
	protected function _library_view($view, $vars = array(), $return = FALSE)
	{
		$vars = (is_object($vars)) ? get_object_vars($vars) : $vars;

		$file_exists = FALSE;

		$ext = pathinfo($view, PATHINFO_EXTENSION);
		$file = ($ext == '') ? $view.'.php' : $view;

		$view_file = 'assets/multimedia_crud/views/';

		if (file_exists($view_file.$file))
		{
			$path = $view_file.$file;
			$file_exists = TRUE;
		}

		if ( ! $file_exists)
		{
			throw new Exception('Unable to load the requested file: '.$file, 16);
		}

		extract($vars);

		#region buffering...
		ob_start();

		include($path);

		$buffer = ob_get_contents();
		@ob_end_clean();
		#endregion

		if ($return === TRUE)
		{
		return $buffer;
		}

		$this->views_as_string .= $buffer;
	}

	public function get_css_files()
	{
		return $this->css_files;
	}

	public function get_js_files()
	{
		return $this->js_files;
	}
	
	/**
	 *
	 * Load the language strings array from the language file
	 */
	private function _load_language()
	{
		$ci = &get_instance();
		$ci->config->load('multimedia_crud');
		if($this->language === null)
		{
			$this->language = strtolower($ci->config->item('image_crud_default_language'));
		}
		include($this->default_language_path.'/'.$this->language.'.php');

		foreach($lang as $handle => $lang_string)
			if(!isset($this->lang_strings[$handle]))
				$this->lang_strings[$handle] = $lang_string;
	}

	/**
	 *
	 * Just an alias to get_lang_string method
	 * @param string $handle
	 */
	public function l($handle)
	{
		return $this->get_lang_string($handle);
	}

	/**
	 *
	 * Get the language string of the inserted string handle
	 * @param string $handle
	 */
	public function get_lang_string($handle)
	{
		return $this->lang_strings[$handle];
	}

	/**
	 *
	 * Simply set the language
	 * @example english
	 * @param string $language
	 */
	public function set_language($language)
	{
		$this->language = $language;

		return $this;
	}

	protected function get_layout()
	{
		$js_files = $this->get_js_files();
		$css_files =  $this->get_css_files();

		return (object)array('output' => $this->views_as_string, 'js_files' => $js_files, 'css_files' => $css_files);

	}
	
	function _get_file_type($file_path) {
        if(!($videoId = $this->_is_youtube_valid_url($file_path)))
		{
			include('assets/multimedia_crud/config/allowed_extensions.php');
	        $ext = strtolower(pathinfo($file_path, PATHINFO_EXTENSION));
			
			if(in_array($ext, $allowed_extensions['ext_image']))
				return 'image';
			else if(in_array($ext, $allowed_extensions['ext_video']))
				return 'video';
			else if(in_array($ext, $allowed_extensions['ext_flash']))
				return 'flash';
			else if (filter_var($file_path, FILTER_VALIDATE_URL))
				return 'flash-url';
			else if(in_array($ext, $allowed_extensions['ext_audio']))
				return 'audio';
			else if(in_array($ext, $allowed_extensions['ext_docs']))
				return 'docs';
		}
        else
            return $videoId;	
    }
	
	function _is_youtube_valid_url($video_url)
    {
        $rx = '/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/';
   
        if(!preg_match($rx, $video_url, $matches))
            return FALSE;
        else
            return $matches[1];
    } 
	
	function _is_flash($flash_url){
		include('assets/multimedia_crud/config/allowed_extensions.php');
	    $ext = strtolower(pathinfo($flash_url, PATHINFO_EXTENSION));
		
		if(in_array($ext, $allowed_extensions['ext_flash']))
			return true;
		else if (filter_var($flash_url, FILTER_VALIDATE_URL))
			return true;
		else
			return false;	
	}
	

	protected function _upload_thumb($id)
	{
		$result = $this -> ci->db->get_where($this->table_name, array($this->primary_key => $id , $this -> relation_field => get_current_char_id()));
		if($result->num_rows == 1){
			$upload_dir = $this->image_path;
			$options = array(
					'upload_dir' 		=> $upload_dir.'/',
					'param_name'		=> 'qqfile',
					'upload_url'		=> base_url().$upload_dir.'/',
					'accept_file_types' => '/(\\.|\\/)(jpeg|jpg|png|gif)$/i'
			);

			$this->ci->load->library('UploadHandler',$options);
			$uploader_response = $this->ci->uploadhandler->post(false);
			if(is_array($uploader_response['qqfile']))
			{
				foreach($uploader_response['qqfile'] as &$response)
				{
					unset($response->delete_url);
					unset($response->delete_type);
				}
				
				$upload_response = $uploader_response['qqfile'][0];
			} else {
				$upload_response = false;
			}

			if (!empty($upload_response) && !isset($upload_response -> error)) {
				$this->ci->load->library('image_moo');
				
				$filename = $upload_response->name;
				$path = $upload_dir.'/'.$filename;
				
				//Resizing to 270 x 180
				$this->ci->image_moo->load($path)->resize(270,180)->save($path,true);

				//delete old thumb..
				$current = $result -> result();
				if(is_file($this->image_path.'/'.$current[0]->{$this->thumb_field}))
    				@unlink( $this->image_path.'/'.$current[0]->{$this->thumb_field});

				//update thumb..
				if($this->ci->db->update($this->table_name, array($this->thumb_field => $filename), array($this->primary_key => $id)))
					return true;
			}
		}
			return false;
	}

	protected function _upload_file($upload_dir) {
		$reg_exp = $this->getValidExtForReg();
		$options = array(
				'upload_dir' 		=> $upload_dir.'/',
				'param_name'		=> 'qqfile',
				'upload_url'		=> base_url().$upload_dir.'/',
				'accept_file_types' => $reg_exp
		);
		
		if(is_numeric($this -> max_file_size) && $this -> max_file_size > 0)
			$options['max_file_size'] = $this -> max_file_size;
		
		$this->ci->load->library('UploadHandler',$options);
		$uploader_response = $this->ci->uploadhandler->post(false);
		
		if(is_array($uploader_response['qqfile']))
		{
			foreach($uploader_response['qqfile'] as &$response)
			{
				unset($response->delete_url);
				unset($response->delete_type);
			}
			
			$upload_response = $uploader_response['qqfile'][0];
		} else {
			$upload_response = false;
		}
		
		if (!empty($upload_response) && !isset($upload_response -> error)) {
			$ci = &get_instance();
			$ci->load->library('image_moo');
			
			$filename = $upload_response->name;
			$path = $upload_dir.'/'.$filename;
			
			if($this->_get_file_type($path) == 'image')
			{
				/* Resizing to 1024 x 768 if its required */
				list($width, $height) = getimagesize($path);
				if($width > 1024 || $height > 768)
				{
					$ci->image_moo->load($path)->resize(1024,768)->save($path,true);
				}
				/* ------------------------------------- */		
			}
			return $filename;
		} else {
			return false;
		}
    }

	public function getValidExt()
	{
		include('assets/multimedia_crud/config/allowed_extensions.php');
		$result = array();
		if (isset($allowed_extensions))
		{
			if($this->mod_image)
				$result = array_merge($result,$allowed_extensions['ext_image']);
			if($this->mod_audio)
				$result = array_merge($result,$allowed_extensions['ext_audio']);
			if($this->mod_video)
				$result = array_merge($result,$allowed_extensions['ext_video']);
			if($this->mod_flash)
				$result = array_merge($result,$allowed_extensions['ext_flash']);
			if($this->mod_docs)
				$result = array_merge($result,$allowed_extensions['ext_docs']);
		}
		return $result;
	}
	
	public function getValidExtForJavaScript()
	{
		$result = "";	
		$ext = $this->getValidExt();
		for ($i=0; $i < count($ext); $i++) 
			if($i == 0)
				$result .= "'".$ext[$i]."'";
			else
				$result .= ", '".$ext[$i]."'";	
		return $result;
	}
	
	public function getValidExtForReg()
	{
		$reg_exp = '/(\\.|\\/)(';
		$ext = $this->getValidExt();
		for ($i=0; $i < count($ext); $i++) 
			if($i == count($ext) - 1)
				$reg_exp .= $ext[$i];
			else
				$reg_exp .= $ext[$i]."|";
		$reg_exp .= ')$/i';
		return $reg_exp;
	}
	
    protected function _changing_priority($post_array)
    {
    	$counter = 1;
		foreach($post_array as $photo_id)
		{
			$this->ci->db->update($this->table_name, array($this->priority_field => $counter), array($this->primary_key => $photo_id));
			$counter++;
		}
    }

    protected function _insert_title($primary_key, $value)
    {
		$this->ci->db->update($this->table_name, array($this->title_field => $value), array($this->primary_key => $primary_key));
    }

    protected function _insert_table($file_name, $relation_id = null)
    {
    	$insert = array($this->url_field => $file_name);
    	if(!empty($relation_id))
    		$insert[$this->relation_field] = $relation_id;
    	$this->ci->db->insert($this->table_name, $insert);
    }

    protected function _delete_file($id)
    {
    	$this->ci->db->where($this->primary_key,$id);
    	$result = $this->ci->db->get($this->table_name)->row();

		if(is_file($this->image_path.'/'.$result->{$this->url_field}))
    		@unlink( $this->image_path.'/'.$result->{$this->url_field} );
    	if(is_file($this->image_path.'/'.$this->thumbnail_prefix.$result->{$this->url_field}))
    		@unlink( $this->image_path.'/'.$this->thumbnail_prefix.$result->{$this->url_field} );
    	if($this -> thumb_field != null && is_file($this->image_path.'/'.$result->{$this->thumb_field}))
    		@unlink( $this->image_path.'/'.$result->{$this->thumb_field} );

    	$this->ci->db->delete($this->table_name, array($this->primary_key => $id));
    }

    protected function _get_delete_url($value)
    {
    	//$rsegments_array = $this->ci->uri->rsegment_array();
		$rsegments_array[] = $this->ci->uri->segment(1);
		$rsegments_array[] = $this->ci->uri->segment(2);
		$rsegments_array[] = $this->ci->uri->segment(3);
		if($rsegments_array[2])
			return site_url($rsegments_array[0].'/'.$rsegments_array[1].'/'.$rsegments_array[2].'/delete_file/'.$value);
		else
			return site_url($rsegments_array[0].'/'.$rsegments_array[1].'/delete_file/'.$value);
    }

    protected function _get_thumb_upload_url($value)
    {
    	//$rsegments_array = $this->ci->uri->rsegment_array();
		$rsegments_array[] = $this->ci->uri->segment(1);
		$rsegments_array[] = $this->ci->uri->segment(2);
		
		return site_url($rsegments_array[0].'/'.$rsegments_array[1].'/upload_thumb/'.$value);
    }
	
	protected function _get_flash_url($url)
    {
    	if (filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED))
    		return $url;
		else
			return base_url().$this->image_path.'/'.$url; 
    }
	
    protected function _get_multimedia_files($relation_value = null)
    {
    	if(!empty($this->priority_field))
    	{
    		$this->ci->db->order_by($this->priority_field);
    	}
    	if(!empty($relation_value))
    	{
    		$this->ci->db->where($this->relation_field, $relation_value);
    	}
    	$results = $this->ci->db->get($this->table_name)->result();

    	$thumbnail_url = !empty($this->thumbnail_path) ? $this->thumbnail_path : $this->image_path;

    	foreach($results as $num => $row)
    	{
			$type = $this->_get_file_type($row->{$this->url_field});
			$results[$num]->delete_url = $this->_get_delete_url($row->{$this->primary_key});
			$results[$num]->thumb_upload_url = $this->_get_thumb_upload_url($row->{$this->primary_key});
			
			switch ($type) {
				case 'image' :
					if (!file_exists($this->image_path.'/'.$this->thumbnail_prefix.$row->{$this->url_field})) {
						$this->_create_thumbnail($this->image_path.'/'.$row->{$this->url_field}, $this->image_path.'/'.$this->thumbnail_prefix.$row->{$this->url_field});
					}
		    		$results[$num]->image_url = base_url().$this->image_path.'/'.$row->{$this->url_field};
		    		$results[$num]->thumbnail_url = base_url().$this->image_path.'/'.$this->thumbnail_prefix.$row->{$this->url_field};
		    		$results[$num]->file_type = $type;
					break;
				case 'video' :
					$results[$num]->image_url = base_url().$this->image_path.'/'.$row->{$this->url_field};
		    		$results[$num]->thumbnail_url = ($this ->thumb_field != null && $row -> {$this -> thumb_field} != '') ? base_url().$this->image_path.'/'.$row -> {$this -> thumb_field} : base_url().$this->default_video_thumb_dir;
		    		$results[$num]->file_type = $type;					
					break;
				case 'flash':
					$results[$num]->image_url = $this -> _get_flash_url($row->{$this->url_field});
		    		$results[$num]->thumbnail_url = ($this ->thumb_field != null && $row -> {$this -> thumb_field} != '') ? base_url().$this->image_path.'/'.$row -> {$this -> thumb_field} : base_url().$this->default_flash_thumb_dir;
		 			$results[$num]->file_type = $type;					
					break;
				case 'flash-url':
		    		$results[$num]->image_url = $this -> _get_flash_url($row->{$this->url_field});
		    		$results[$num]->thumbnail_url = ($this ->thumb_field != null && $row -> {$this -> thumb_field} != '') ? base_url().$this->image_path.'/'.$row -> {$this -> thumb_field} : base_url().$this->default_flash_url_thumb_dir;
		 			$results[$num]->file_type = $type;					
					break;
				case 'audio':
		    		$results[$num]->image_url = base_url().$this->image_path.'/'.$row->{$this->url_field};
		    		$results[$num]->thumbnail_url = ($this -> thumb_field != null && $row -> {$this -> thumb_field} != '') ? base_url().$this->image_path.'/'.$row -> {$this -> thumb_field} : base_url().$this->default_audio_thumb_dir;
		    		$results[$num]->file_type = $type;					
					break;
				case 'docs':
		    		$results[$num]->image_url = base_url().$this->image_path.'/'.$row->{$this->url_field};
		    		$results[$num]->thumbnail_url = ($this -> thumb_field != null && $row -> {$this -> thumb_field} != '') ? base_url().$this->image_path.'/'.$row -> {$this -> thumb_field} : base_url().$this->default_docs_thumb_dir;
		    		$results[$num]->file_type = $type;					
					break;
				default: //this mean that youtube url
					$results[$num]->image_url = "https://www.youtube.com/watch?v=$type";
	                $results[$num]->thumbnail_url = "https://img.youtube.com/vi/$type/default.jpg";
	                $results[$num]->file_type = "embaded_url";
					break;
			}
    	}
    	return $results;
    }

	protected function _convert_foreign_characters($str_i)
	{
		include('assets/multimedia_crud/config/translit_chars.php');
		if ( ! isset($translit_characters))
		{
			return $str_i;
		}
		return preg_replace(array_keys($translit_characters), array_values($translit_characters), $str_i);
	}

	protected function _create_thumbnail($file_path, $thumbnail_path, $ScreenShortSecond = 10)
	{
		$type = $this->_get_file_type($file_path); 
		if($type == 'video')
		{
			$thumbnail_path = str_replace(".".pathinfo($thumbnail_path, PATHINFO_EXTENSION), ".jpg", $thumbnail_path);
			//echo $file_path."<br/>".$thumbnail_path;
		    /*
		    if (!file_exists($thumbnail_path)) {
		        $FFMPEG_Command = sprintf(
		            "ffmpeg -i \"%s\" -y -ss \"00:00:%02d\" -f image2 \"%s\" > /dev/null 2>&1",
		            $file_path, 0 + $ScreenShortSecond, $thumbnail_path
		        );
		        system($FFMPEG_Command);
		    }	
			*/
		    if (file_exists($thumbnail_path))
		    	$this->image_moo
					->load($thumbnail_path)
					->resize_crop(270,180)
					->save($thumbnail_path,true);
		}
		else if($type == 'image')
			$this->image_moo
				->load($file_path)
				->resize_crop(270,180)
				->save($thumbnail_path,true);
	}

	protected function getState()
	{
		//$rsegments_array = $this->ci->uri->rsegment_array();
		$rsegments_array[] = $this->ci->uri->segment(1);
		$rsegments_array[] = $this->ci->uri->segment(2);

		if($this->ci->uri->segment(3))
			$rsegments_array[] = $this->ci->uri->segment(3);
		if($this->ci->uri->segment(4))
			$rsegments_array[] = $this->ci->uri->segment(4);
		if($this->ci->uri->segment(5))
			$rsegments_array[] = $this->ci->uri->segment(5);
		/*
		var_dump($rsegments_array);
		var_dump($this->ci->uri->rsegment_array());
		die();*/
		if(isset($rsegments_array[$this->ajax_segment]) && $rsegments_array[$this->ajax_segment] == 'upload_file')
		{
			$results = array( 'name' => 'upload_file');
			#region Just rename my file
			if(isset($_GET['qqfile']))
			{
				$new_file_name = '';
				//$old_file_name = $this->_to_greeklish($_GET['qqfile']);
				$old_file_name = $this->_convert_foreign_characters($_GET['qqfile']);
				$max = strlen($old_file_name);
				for($i=0; $i< $max;$i++)
				{
					$numMatches = preg_match('/^[A-Za-z0-9.-_]+$/', $old_file_name[$i], $matches);
					if($numMatches >0)
					{
						$new_file_name .= strtolower($old_file_name[$i]);
					}
					else
					{
						$new_file_name .= '-';
					}
				}
				$file_name = substr( substr( uniqid(), 9,13).'-'.$new_file_name , 0, 100) ;
				$results['file_name'] = $file_name;
			}
			#endregion
			if(isset($rsegments_array[$this->relation_segment]) && is_numeric($rsegments_array[$this->relation_segment]))
			{
				$results['relation_value'] = $rsegments_array[$this->relation_segment];
			}
			return (object)$results;
		}
		elseif(isset($rsegments_array[$this->ajax_segment]) && $rsegments_array[$this->ajax_segment] == 'upload_file_url')
        {
            $results = array( 'name' => 'upload_file_url');
            if(isset($rsegments_array[$this->relation_segment]) && is_numeric($rsegments_array[$this->relation_segment]))
			{
				$results['relation_value'] = $rsegments_array[$this->relation_segment];
			}
            return (object)$results;
        }
		elseif(isset($rsegments_array[$this->ajax_segment]) && isset($rsegments_array[4]) && $rsegments_array[$this->ajax_segment] == 'delete_file' && is_numeric($rsegments_array[4]))
		{
			$state = array( 'name' => 'delete_file', 'id' => $rsegments_array[4]);
			return (object)$state;
		}
		elseif(isset($rsegments_array[$this->ajax_segment]) && $rsegments_array[$this->ajax_segment] == 'ordering')
		{
			$state = array( 'name' => 'ordering');
			return (object)$state;
		}
		elseif(isset($rsegments_array[$this->ajax_segment]) && $rsegments_array[$this->ajax_segment] == 'insert_title')
		{
			$state = array( 'name' => 'insert_title');
			return (object)$state;
		}
		elseif(isset($rsegments_array[$this->ajax_segment]) && $rsegments_array[$this->ajax_segment] == 'upload_thumb')
		{
			$state = array( 'name' => 'upload_thumb','id' => $rsegments_array[4]);
			return (object)$state;
		}
		elseif(isset($rsegments_array[$this->relation_segment]) && is_numeric($rsegments_array[$this->relation_segment]))
		{
			$upload_url = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/'.$rsegments_array[$this->relation_segment].'/upload_file');
			$ajax_list_url  = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/'.$rsegments_array[$this->relation_segment].'/ajax_list');
			$ordering_url  = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/'.$rsegments_array[$this->relation_segment].'/ordering');
			$insert_title_url  = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/'.$rsegments_array[$this->relation_segment].'/insert_title');
			$upload_file_url = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/'.$rsegments_array[$this->relation_segment].'/upload_file_url');
			$thumb_upload_url = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/'.$rsegments_array[$this->relation_segment].'/upload_thumb');
						
			$state = array( 'name' => 'list', 'upload_url' => $upload_url, 'upload_file_url' => $upload_file_url, 'relation_value' => $rsegments_array[$this->relation_segment]);
			$state['ajax'] = isset($rsegments_array[$this->ajax_segment]) && $rsegments_array[$this->ajax_segment] == 'ajax_list'  ? true : false;
			$state['ajax_list_url'] = $ajax_list_url;
			$state['ordering_url'] = $ordering_url;
			$state['insert_title_url'] = $insert_title_url;
			$state['thumb_upload_url'] = $thumb_upload_url;

			return (object)$state;
		}
		elseif( (empty($rsegments_array[$this->relation_segment]) && empty($this->relation_field)) || (!empty($rsegments_array[$this->relation_segment]) &&  $rsegments_array[$this->relation_segment] == 'ajax_list') 
				|| ( isset($rsegments_array[$this->relation_segment]) && is_numeric($rsegments_array[$this->relation_segment]) && empty($this->relation_field)))
		{
			$upload_url = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/upload_file');
			$ajax_list_url  = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/ajax_list');
			$ordering_url  = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/ordering');
			$insert_title_url  = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/insert_title');
			$upload_file_url = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/upload_file_url');
			$thumb_upload_url = site_url($rsegments_array[$this->controler_segment].'/'.$rsegments_array[$this->method_segment].'/upload_thumb');
			
			$state = array( 'name' => 'list', 'upload_url' => $upload_url, 'upload_file_url' => $upload_file_url);
			$state['ajax'] = isset($rsegments_array[$this->ajax_segment]) && $rsegments_array[$this->ajax_segment] == 'ajax_list'  ? true : false;
			$state['ajax_list_url'] = $ajax_list_url;
			$state['ordering_url'] = $ordering_url;
			$state['insert_title_url'] = $insert_title_url;
			$state['thumb_upload_url'] = $thumb_upload_url;

			return (object)$state;
		}
	}

	function render()
	{
		$ci = &get_instance();
		$this->_load_language();
		$ci->load->helper('url');
		$ci->load->library('Image_moo');
		$this->image_moo = new Image_moo();
		$state_info = $this->getState();
		
		if(!empty($state_info))
		{
			switch ($state_info->name) {
				case 'list':
					$photos = isset($state_info->relation_value) ? $this->_get_multimedia_files($state_info->relation_value) : $this->_get_multimedia_files();
					$this->_library_view('list.php',array(
						'upload_url' => $state_info->upload_url,
						'upload_file_url' => $state_info->upload_file_url,
						'insert_title_url' => $state_info->insert_title_url,
						'thumb_upload_url' => $state_info->thumb_upload_url,
						
						'photos' => $photos,
						'ajax_list_url' => $state_info->ajax_list_url,
						'ordering_url' => $state_info->ordering_url,
						'primary_key' => $this->primary_key,
						'title_field' => $this->title_field,
						'thumb_field' => $this->thumb_field,
						'unset_delete' => $this->unset_delete,
						'unset_upload' => $this->unset_upload,
						'has_priority_field' => $this->priority_field !== null ? true : false,
						'IsNeedButton' => $this->mod_audio || $this->mod_docs || $this -> mod_flash,
						'IsAjax' => $state_info -> ajax
					));

					if($state_info->ajax === true)
					{
						@ob_end_clean();
						echo $this->get_layout()->output;
						die();
					}
					return $this->get_layout();
				break;

				case 'upload_file':
					if($this->unset_upload)
					{
						throw new Exception('This user is not allowed to do this operation', 1);
						die();
					}					
					
					$file_name = $this->_upload_file($this->image_path);
					
					if ($file_name !== false) {
						$this->_create_thumbnail( $this->image_path.'/'.$file_name , $this->image_path.'/'.$this->thumbnail_prefix.$file_name );
						$this->_insert_table($file_name, $state_info->relation_value);
						
						$result = true;
					} else {
						
						$result = false;
					} 

					@ob_end_clean();
					echo json_encode((object)array('success' => $result));					
					
					die();
				break;
				
				case 'upload_file_url':
                    if($this->unset_upload)
                    {
                        throw new Exception('This user is not allowed to do this operation', 1);
                        die();
                    }                   
                    
                    $file_url = FALSE;
                    if(isset($_POST['file_url']))
                        $file_url = $_POST['file_url'];
                    
					
					//var_dump($file_url);
					
                    if ($this -> mod_video && $this->_is_youtube_valid_url($file_url)) {
		                $this->_insert_table($file_url, $state_info->relation_value);
                        $result = true;
					}
					elseif ($this -> mod_flash && $this->_is_flash($file_url)) {
		                $this->_insert_table($file_url, $state_info->relation_value);
                        $result = true;
					} else
                        $result = false;

                    @ob_end_clean();
                    echo json_encode((object)array('success' => $result));                  
                    
                    die();
                break;

                case 'upload_thumb':
                	if($this -> thumb_field == null)
                	{
                        throw new Exception('This user is not allowed to do this operation', 1);
                        die();
                    } 

                	$result = $this->_upload_thumb($state_info->id);
					
					@ob_end_clean();
					echo json_encode((object)array('success' => $result));
					die();
                	break;

				case 'delete_file':
					@ob_end_clean();
					if($this->unset_delete)
					{
						throw new Exception('This user is not allowed to do this operation', 1);
						die();
					}
					$id = $state_info->id;

					$this->_delete_file($id);

					redirect($_SERVER['HTTP_REFERER']);
				break;

				case 'ordering':
					@ob_end_clean();
					$this->_changing_priority($_POST['photos']);
					die();
				break;

				case 'insert_title':
					@ob_end_clean();
					$this->_insert_title($_POST['primary_key'],$_POST['value']);
					die();
				break;
			}
		}
	}
}