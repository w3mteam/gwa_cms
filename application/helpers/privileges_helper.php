<?php
function get_user_privileges()
{
	$ci = &get_instance();
	$ci->load->library('ion_auth');
	return $ci->ion_auth->get_priv_and_cat();
}

function get_privileges_functions()
{
	$ci = &get_instance();
	$ci->load->library('ion_auth');
	return $ci->ion_auth->get_privileges_functions();
}

function get_user_priv_cat()
{
	$ci = &get_instance();
	$ci->load->library('ion_auth');
	return $ci->ion_auth->get_priv_cat();
}
