<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('bs3_input')) {
    function bs3_input($name, $value = '', $extra = false) {
        
        ?>

        <div class="form-group">
            <label for="input<?php echo ucfirst($name);?>" class="col-sm-2 control-label"><?php echo lang($name);?>:</label>
            <div class="col-sm-10">
                <input type="text" name="<?php echo $name;?>" id="input<?php echo ucfirst($name);?>" class="form-control" value="<?php echo $value ?>" <?php echo $extra ?>>
            </div>
        </div>

        <?php
    }

}

if (!function_exists('bs3_password')) {
    function bs3_password($name, $value = '', $extra = false) {
        
        ?>

        <div class="form-group">
            <label for="input<?php echo ucfirst($name);?>" class="col-sm-2 control-label"><?php echo lang($name);?>:</label>
            <div class="col-sm-10">
                <input type="password" name="<?php echo $name;?>" id="input<?php echo ucfirst($name);?>" class="form-control" value="<?php echo $value ?>" <?php echo $extra ?>>
            </div>
        </div>

        <?php
    }

}

if (!function_exists('bs3_date')) {
    function bs3_date($name, $value = '', $extra = false) {
        
        ?>

        <div class="form-group">
            <label for="input<?php echo ucfirst($name);?>" class="col-sm-2 control-label"><?php echo lang($name);?>:</label>
            <div class="col-sm-10">
                <div class="input-group date">
                  <input type="date" name="<?php echo $name;?>" id="input<?php echo ucfirst($name);?>" class="form-control"  value="<?php echo $value ?>" <?php echo $extra ?>><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                </div>
            </div>
        </div>

        <?php
    }

}

if (!function_exists('bs3_file')) {
    function bs3_file($name, $old_file = false, $id = false) {
        
        ?>

        <div class="form-group">
            <label for="input" class="col-sm-2 control-label"><?php echo ucfirst($name);?>:</label>
            <div class="col-sm-10"> 
                <div class="checkbox">
                    <p>
                        <input type="checkbox" id="delete_file_<?php echo $id ?>" name="delete_file_<?php echo $name ?>" <?= $old_file?'':'checked'?> data-label="No File">
                    </p>
                </div>
                <a class="col-sm-4 btn-download_<?php echo $id ?> btn btn-default" target="_blank" href="<?php echo base_url($old_file) ?>"  style="<?= $old_file?'':'display: none'?>">Download Old File</a>
                <div class="clearfix"></div>
                <div class="fileinput fileinput-new input-group" data-provides="fileinput" id="file_uploader_<?php echo $id ?>" style="<?= $old_file?'':'display: none'?>">
                    <div class="form-control" data-trigger="fileinput">
                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                        <span class="fileinput-filename"></span>
                    </div>
                  <span class="input-group-addon btn btn-default btn-file">
                  <span class="fileinput-new">Select file</span>
                  <span class="fileinput-exists">Change</span>
                  <input type="file" name="<?php echo $name;?>"></span>
                  <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
                <script type="text/javascript" charset="utf-8">
                    $(document).ready(function(){
                        $("#delete_file_<?php echo $id ?>").change(function() {
                            if(this.checked) {
                                // $('.btn-download_<?php echo $id ?>').hide();
                                $('#file_uploader_<?php echo $id ?>').stop().slideUp();
                            }else{
                                // $('.btn-download_<?php echo $id ?>').show();
                                $('#file_uploader_<?php echo $id ?>').stop().slideDown();
                            }
                        });
                    });
                </script>
            </div>
        </div>

        <?php
    }

}

if (!function_exists('bs3_image')) {
    function bs3_image($name, $old_img = false, $preview_img = false, $id = false) {
        
        ?>

        <div class="form-group">
            <label for="input" class="col-sm-2 control-label"><?php echo lang($name);?>:</label>
            <div class="col-sm-10"> 
                <?php $img = $old_img; $image_url = $old_img?base_url("assets/uploads/$old_img"):base_url('assets/img/no_image.png'); ?>
                <div class="checkbox">
                    <p>
                        <input type="checkbox" id="delete_image_<?php echo $id ?>" name="delete_image_<?php echo $name ?>" <?= $img?'':'checked'?> data-label="No Image">
                    </p>
                </div>
                <div class="fileinput fileinput-new" data-provides="fileinput" id="image_uploader_<?php echo $id ?>" style="<?= $img?'':'display: none'?>">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="<?php echo $image_url ?>" alt="<?php echo ucfirst($name);?>">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                    <div>
                        <span class="btn btn-default btn-file">
                        <span class="fileinput-new">Select image</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="<?php echo $name;?>"></span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
                <script type="text/javascript" charset="utf-8">
                    $(document).ready(function(){
                        $("#delete_image_<?php echo $id ?>").change(function() {
                            if(this.checked) {
                                $('#image_uploader_<?php echo $id ?>').stop().slideUp();
                            }else{
                                $('#image_uploader_<?php echo $id ?>').stop().slideDown();
                            }
                        });
                    });
                </script>
            </div>
        </div>

        <?php
    }

}

if (!function_exists('bs3_hidden')) {
    function bs3_hidden($name, $value = '', $extra = false) {
        
        ?>

        <input type="hidden" name="<?php echo $name;?>" id="input<?php echo ucfirst($name);?>" class="form-control" value="<?php echo $value ?>" <?php echo $extra ?>>

        <?php
    }

}

if (!function_exists('bs3_dropdown')) {
    function bs3_dropdown($name, $options, $selected = false, $extra = false, $options_extra = false) {
        
        ?>

        <div class="form-group">
            <label for="input<?php echo ucfirst($name);?>" class="col-sm-2 control-label"><?php echo lang($name);?>:</label>
            <div class="col-sm-10">
                <select name="<?php echo $name;?>" id="input<?php echo ucfirst($name);?>" class="form-control" <?php echo $extra ?>>
                    <?php
                        foreach ($options as $key => $val)
                        {
                            $sel = ($key == $selected) ? ' selected="selected"' : '';

                            if($options_extra && isset($options_extra[$key])){
                                echo '<option value="'.$key.'"'.$sel.' '.$options_extra[$key]['key'].'="'.$options_extra[$key]['value'].'">'.$val."</option>\n";
                            }
                            else
                                echo '<option value="'.$key.'"'.$sel.'>'.$val."</option>\n";
                        }
                    ?>
                </select>
            </div>
        </div>

        <?php
    }

}

if (!function_exists('bs3_checkbox')) {
    function bs3_checkbox($name, $selected = false, $extra = false) {
        
        ?>

        <div class="form-group">
            <label for="input<?php echo ucfirst($name);?>" class="col-sm-2 control-label"><?php echo lang($name);?>:</label>
            <div class="col-sm-10">
                <input type="checkbox" name="<?php echo $name;?>" data-label="" value="1" <?php echo ($selected)?'checked="checked"':'' ?> <?php echo $extra ?>>
            </div>
        </div>

        <?php
    }

}

if (!function_exists('bs3_radio')) {
    function bs3_radio($name, $label,$value ,$selected = false, $extra = false) {
        ?>
        <div class="form-group">
            <label for="input<?php echo $name;?>" class="col-sm-2 control-label"><?php echo humanize(lang($label));?>:</label>
            <div class="col-sm-10">
                <input type="radio" name="<?php echo $name;?>" data-label="" value="<?php  echo $value ?>" <?php echo ($selected)?'checked="checked"':'' ?> <?php echo $extra ?>>
            </div>
        </div>

        <?php
    }

}

if (!function_exists('bs3_textarea')) {
    function bs3_textarea($name, $value = '', $extra = false) {
        
        ?>

        <div class="form-group">
            <label for="input<?php echo ucfirst($name);?>" class="col-sm-2 control-label"><?php echo lang($name);?>:</label>
            <div class="col-sm-10">
                <textarea name="<?php echo $name;?>" id="input<?php echo ucfirst($name);?>" class="form-control" <?php echo $extra ?>><?php echo $value ?></textarea>
            </div>
        </div>

        <?php
    }

}

if (!function_exists('bs3_submit')) {
    function bs3_submit($label = false, $extra = false) {
        if(empty($label)) $label = lang('save');
        ?>

        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <input type="submit" class="btn btn-primary" value="<?php echo $label ?>" <?php echo $extra ?> />
            </div>
        </div>

        <?php
    }




}