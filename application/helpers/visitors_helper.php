<?php 
function set_visitor()
{
	$this_ptr = &get_instance();
	$visitor_ip = $this_ptr->input->ip_address();
	if(!$this_ptr->session->userdata('visited'))
	{
		$this_ptr->load->model('statistics_model');
		$this_ptr->statistics_model->increase_visitors();
		if($this_ptr->statistics_model->is_new_visitor($visitor_ip)){
			$this_ptr->statistics_model->add_new_visitor($visitor_ip);
		}
		
		$this_ptr->load->library('ip2country');
		$country = $this_ptr->ip2country->get_country_name($visitor_ip);
		if($country == null){
			$country = 'localhost';
		}
		$this_ptr->statistics_model->set_visitor_country($country);
		
		$this_ptr->load->library('user_agent');
		$browser = $this_ptr->agent->browser();
		$this_ptr->statistics_model->set_visitor_browser($browser);
		
		$this_ptr->session->set_userdata('visited',true);
	}
	else{
		if(is_new_visitor()){
			$this_ptr->statistics_model->add_new_visitor($visitor_ip);
		}
	}
	
}
function increase_product_views($prod_id='')
{
	$this_ptr = &get_instance();
	$this_ptr->load->model('statistics_model');
	$this_ptr->statistics_model->increase_products($prod_id);
}

function is_new_visitor()
{
	$this_ptr = &get_instance();
	$visitor_ip = $this_ptr->input->ip_address();
	$this_ptr->load->model('statistics_model');
	return $this_ptr->statistics_model->is_new_visitor($visitor_ip);
}

 function set_date_statistic()
{
	$ci=& get_instance();
	$ci->load->model('general_model','statistics_date_m');
	$ci->statistics_date_m->set_table('statistics_date');
	$current_date=date('Y-m-d');
	$date=$ci->statistics_date_m->get_by('date',$current_date);
	if($date)
		$ci->statistics_date_m->update_by(array('id'=>$date->id) , array('count' => $date->count+1));
	else
		$ci->statistics_date_m->insert(array('date' => $current_date, 'count' => 1));
	
	
}
?>