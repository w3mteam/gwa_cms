<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('get_youtube_video_id')) {

    function get_youtube_video_id($video_url) {
        $rx = '/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/';

        if (!preg_match($rx, $video_url, $matches)) {
            return FALSE;
        } else {
            return $matches[1];
        }
    }

}
