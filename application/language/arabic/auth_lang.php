<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Auth Lang - English
*
* Author: Ben Edmunds
* 		  ben.edmunds@gmail.com
*         @benedmunds
*
* Author: Daniel Davis
*         @ourmaninjapan
*
* Location: http://github.com/benedmunds/ion_auth/
*
* Created:  03.09.2013
*
* Description:  English language file for Ion Auth example views
*
*/

// Errors
$lang['error_csrf'] = 'This form post did not pass our security checks.';

// Login
$lang['login_heading']         = 'تسجيل الدخول';
$lang['login_subheading']      = 'من فضلك سجل دخول عن طريق البريد الالكتروني وكلمة المرور';
$lang['login_identity_label']  = 'البريد الالكتروني:';
$lang['login_password_label']  = 'كلمة المرور:';
$lang['login_remember_label']  = 'تذكرني:';
$lang['login_submit_btn']      = 'تسجيل الدخول';
$lang['login_forgot_password'] = 'هل نسيت كلمة المرور؟';

// Index
$lang['index_heading']           = 'المستخدمون';
$lang['index_subheading']        = 'قائمة المستخدمين المسجلين في الموقع';
$lang['index_fname_th']          = 'الاسم الأول';
$lang['index_lname_th']          = 'الاسم الأخير';
$lang['index_email_th']          = 'البريد الالكتروني';
$lang['index_phone_th']          = 'التلفون';
$lang['index_groups_th']         = 'الدور';
$lang['index_status_th']         = 'الحالة';
$lang['index_action_th']         = 'حدث';
$lang['index_active_link']       = 'تفعيل';
$lang['index_inactive_link']     = 'إلغاء التفعيل';
$lang['index_create_user_link']  = 'إنشاء مستخدم جديد';
$lang['index_create_group_link'] = 'إنشاء دور جديد';

// Deactivate User
$lang['deactivate_heading']                  = 'إلغاء تفعيل مستخدم';
$lang['deactivate_subheading']               = 'هل أنت متأكد من إلغاء تفعيل المستخدم: \'%s\'';
$lang['deactivate_confirm_y_label']          = 'نعم:';
$lang['deactivate_confirm_n_label']          = 'لا:';
$lang['deactivate_submit_btn']               = 'إرسال';
$lang['deactivate_validation_confirm_label'] = 'تأكيد';
$lang['deactivate_validation_user_id_label'] = 'رقم المستخدم';

// Create User
$lang['create_user_heading']                           = 'إنشاء مستخدم جديد';
$lang['create_user_subheading']                        = 'من فضلك أدخل المعلومات التالية';
$lang['create_user_fname_label']                       = 'الاسم الأول:';
$lang['create_user_lname_label']                       = 'الاسم الأخير:';
$lang['create_user_company_label']                     = 'اسم الشركة:';
$lang['create_user_email_label']                       = 'البريد الالكتروني:';
$lang['create_user_phone_label']                       = 'الهاتف:';
$lang['create_user_password_label']                    = 'كلمة المرور:';
$lang['create_user_password_confirm_label']            = 'تأكيد كلمة المرور:';
$lang['create_user_submit_btn']                        = 'إنشاء المستخدم';
$lang['create_user_validation_fname_label']            = 'الاسم الأول';
$lang['create_user_validation_lname_label']            = 'الاسم الأخير';
$lang['create_user_validation_email_label']            = 'البريد الالكتروني';
$lang['create_user_validation_phone1_label']           = 'الجزء الأول من الهاتف';
$lang['create_user_validation_phone2_label']           = 'الجزء الثاني من الهاتف';
$lang['create_user_validation_phone3_label']           = 'الجزء الثالث من الهاتف';
$lang['create_user_validation_company_label']          = 'اسم الشركة';
$lang['create_user_validation_password_label']         = 'كلمة المرور';
$lang['create_user_validation_password_confirm_label'] = 'تأكيد كلمة المرور';

// Edit User
$lang['edit_user_heading']                           = 'تعديل مستخدم';
$lang['edit_user_subheading']                        = 'من فضلك أدخل المعلومات التالية';
$lang['edit_user_fname_label']                       = 'الاسم الأول:';
$lang['edit_user_lname_label']                       = 'الاسم الأخير:';
$lang['edit_user_company_label']                     = 'اسم الشركة:';
$lang['edit_user_email_label']                       = 'البريد الالكتروني:';
$lang['edit_user_phone_label']                       = 'الهاتف:';
$lang['edit_user_password_label']                    = ' كلمة المرور: (إذا أردت تغييرها)';
$lang['edit_user_password_confirm_label']            = 'تأكيد كلمة المرور: (إذا أردت تغييرها)';
$lang['edit_user_groups_heading']                    = 'دور المستخدم الحالي';
$lang['edit_user_submit_btn']                        = 'حفظ المستخدم';
$lang['edit_user_validation_fname_label']            = 'الاسم الأول';
$lang['edit_user_validation_lname_label']            = 'الاسم الأخير';
$lang['edit_user_validation_email_label']            = 'البريد الالكتروني';
$lang['edit_user_validation_phone1_label']           = 'الجزء الأول من الهاتف';
$lang['edit_user_validation_phone2_label']           = 'الجزء الثاني من الهاتف';
$lang['edit_user_validation_phone3_label']           = 'الجزء الثالث من الهاتف';
$lang['edit_user_validation_company_label']          = 'اسم الشركة';
$lang['edit_user_validation_groups_label']           = 'الأدوار';
$lang['edit_user_validation_password_label']         = 'كلمة المرور';
$lang['edit_user_validation_password_confirm_label'] = 'تأكيد كلمة المرور';

// Create Group
$lang['create_group_title']                  = 'إنشاء دور جديد';
$lang['create_group_heading']                = 'إنشاء دور';
$lang['create_group_subheading']             = 'من فضلك أدخل المعلومات التالية.';
$lang['create_group_name_label']             = 'اسم الدور:';
$lang['create_group_name_label_en']          = 'اسم الدور بالانكليزية:';
$lang['create_group_name_label_ar']          = 'اسم الدور بالعربية:';
$lang['create_group_name_label_rd']          = 'اسم الدور باللغة الثالثة:';

$lang['create_group_desc_label']             = 'الوصف:';
$lang['create_group_submit_btn']             = 'إنشاء دور';
$lang['create_group_validation_name_label']  = 'اسم الدور';
$lang['create_group_validation_desc_label']  = 'الوصف';

// Edit Group
$lang['edit_group_title']                  = 'تعديل الدور';
$lang['edit_group_saved']                  = 'حفظ الدور';
$lang['edit_group_heading']                = 'تعديل الدور';
$lang['edit_group_subheading']             = 'من فضلك أدخل المعلومات التالية.';
$lang['edit_group_name_label']             = 'اسم الدور:';
$lang['edit_group_desc_label']             = 'الوصف:';
$lang['edit_group_submit_btn']             = 'حفظ الدور';
$lang['edit_group_validation_name_label']  = 'اسم الدور';
$lang['edit_group_validation_desc_label']  = 'الوصف';

// Change Password
$lang['change_password_heading']                               = 'تغيير كلمة المرور';
$lang['change_password_old_password_label']                    = 'كلمة المرور القديمة:';
$lang['change_password_new_password_label']                    = 'كلمة المرور الجديدة (على الأقل يجب أن يكون طولها %s أحرف):';
$lang['change_password_new_password_confirm_label']            = 'تأكيد كلمة المرور الجديدة:';
$lang['change_password_submit_btn']                            = 'تغيير';
$lang['change_password_validation_old_password_label']         = 'كلمة المرور القديمة';
$lang['change_password_validation_new_password_label']         = 'كلمة المرور الجديدة';
$lang['change_password_validation_new_password_confirm_label'] = 'تأكيد كلمة المرور الجديدة';

// Forgot Password
$lang['forgot_password_heading']                 = 'نسيت كلمة المرور؟';
$lang['forgot_password_subheading']              = 'من فضلك أدخل %s لنستطيع إرسال بريد الكتروني لتغيير كلمة مرورك';
$lang['forgot_password_email_label']             = '%s:';
$lang['forgot_password_submit_btn']              = 'إرسال';
$lang['forgot_password_validation_email_label']  = 'البريد الالكتروني:';
$lang['forgot_password_username_identity_label'] = 'اسم المستخدم';
$lang['forgot_password_email_identity_label']    = 'البريد الالكتروني';
$lang['forgot_password_email_not_found']         = 'البريد الالكتروني المدخل غير موجود.';

// Reset Password
$lang['reset_password_heading']                               = 'تغيير كلمة المرور';
$lang['reset_password_new_password_label']                    = 'كلمة المرور الجديدة (على الأقل يجب أن يكون طولها %s أحرف):';
$lang['reset_password_new_password_confirm_label']            = 'تأكيد كلمة المرور الجديدة:';
$lang['reset_password_submit_btn']                            = 'تغيير';
$lang['reset_password_validation_new_password_label']         = 'كلمة المرور الجديدة';
$lang['reset_password_validation_new_password_confirm_label'] = 'تأكيد كلمة المرور الجديدة';

// Activation Email
$lang['email_activate_heading']    = 'تفعيل حساب لـ، %s';
$lang['email_activate_subheading'] = 'من فضلك اضغط على الرابط التالي لـ %s.';
$lang['email_activate_link']       = 'قم بتفعيل حسابك';

// Forgot Password Email
$lang['email_forgot_password_heading']    = 'تغيير كلمة المرور لـ %s';
$lang['email_forgot_password_subheading'] = 'من فضلك اضغط على الرابط التالي لـ %s.';
$lang['email_forgot_password_link']       = 'تغيير كلمة المرور';

// New Password Email
$lang['email_new_password_heading']    = 'كلمة المرور الجديدة لـ %s';
$lang['email_new_password_subheading'] = 'تم تعيين كلمة مرور جديدة لـ: %s';

$lang['priv_added_success'] = 'تم إسناد الصلاحية بنجاح';
$lang['priv_added_failed'] = 'لم يتم إسناد الصلاحية حدث خطأ';
$lang['priv_delete_succss'] = "تم حذف الصلاحية بنجاح";
$lang['priv_delete_failed'] = 'حدث خطأ لم يتم حذف الصلاحية';
$lang['edit_user_profile_header'] = 'تعديل بيانات المستخدم';