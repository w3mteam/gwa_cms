<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Language
$lang['lang']										= 'arabic';
$lang['recent_posts']								= 'البوستات الجديدة';
$lang['phone']										= 'تلفون';
$lang['email']										= 'إيميل';
$lang['address']									= 'عناوين';
$lang['language']='لغة';
$lang['english']='إنكليزي';
$lang['arabic']='عربي';
$lang['rd']='rd';
$lang['search']='بحث';
$lang['sign_in']='تسجيل دخول';
$lang['sign_up']='تسجيل';
$lang['home']										= 'الرئيسية';
$lang['contact']									= 'اتصل بنا';
$lang['views_count']								= 'المشاهدات';
$lang['follow_us_on']								= 'تابعونا على';
$lang['related_articles']							= 'مقالات ذات صلة';
$lang['contact_form']								= 'أرسل رسالة';
$lang['name']										= 'الاسم';
$lang['email']										= 'الإيميل';
$lang['comment']									= 'الرسالة';
$lang['submit']										= 'إرسال';
$lang['reset']										= 'تراجع';
$lang['address']									= 'العنوان';
$lang['contact_success']							= 'تم إرسال الرسالة بنجاح.';
$lang['contact_fail']								= 'عذراً، لم يتم إرسال الرسالة. يرجى المحاولة لاحقاً.';
$lang['read_more']									= 'اقرأ المزيد';
$lang['recent_posts']								= 'أحدث الأخبار';
$lang['phone']										= 'الهاتف';
$lang['email']										= 'البريد الإلكتروني';
$lang['address']									= 'العنوان';
$lang['copyrights']									= '<a href="http://gwa-group.com/">الحقوق محفوظة &copy; GWA</a>';


$lang['location']									= 'موقعنا';
$lang['send']										= 'إرسال';
$lang['name_placeholder']							= 'الاسم مطلوب';
$lang['email_placeholder']							= 'البريد الالكتروني مطلوب';
$lang['msg_placeholder']							= 'اترك رسالتك ...';
$lang['msg_form']									= 'راسلنا';
$lang['message_sent_success']						= 'تم إرسال الرسالة بنجاح شكرا لك';
$lang['message_sent_faild']							= 'لم يتم إرسال الرسالة بسبب حدوث خطأ يرجى المحاولة من جديد';
$lang['view_map']									= 'مشاهدة الخريطة';
$lang['our_location']								= 'موقعنا';
$lang['our_map_location']							= 'موقعنا على الخريطة';


$lang['cultrue']									= 'ثقافتنا';
$lang['album']='ألبومات';
$lang['product']='منتجات';	
$lang['shop_now']='تسوق الآن';	
$lang['men']='رجال';
$lang['women']='نساء';	
$lang['news']='جديد الموقع';	
$lang['reviews']='مشاهدات';		
$lang['reviews']='مشاهدات';	
$lang['brand']='جميع العلامات التجارية لبريميوم';
$lang['price_filter']='فلترة السعر';
$lang['min_price']='السعر الأدنى';
$lang['max_price']='السعر الأعلى';		
$lang['product_details']='تفاصيل منتج';	
$lang['our_location']='موقعنا';
$lang['message']='الرسالة' ;
$lang['your_name']='اسمك-مطلوب' ;
$lang['your_email']='إيميلك-مطلوب' ;
$lang['your_message']='رسالتك' ;
$lang['send']='إرسال' ;	
$lang['remember_me']='تذكرني' ;
$lang['password']='كلمة السر' ;
$lang['repet_pass']='تأكيد كلمة السر' ;
$lang['accept']='موافق على الشروط وبنود الإتفاقية' ;			
$lang['not_found_men']='لا يوجد اي منتجات خاصة بالرجال' ;
$lang['not_found_women']='لا يوجد اي منتجات خاصة بالنساء' ;	
$lang['not_found_news']='لا يوجد اي منتجات جديدة' ;
$lang['not_found_pro']='لا يوجد اي منتجات ' ;

//mhdprogr

//cart

$lang['image']='الصورة';
$lang['view_cart']='عرض السلة ';
$lang['checkout_cart']='شراء';
$lang['delete_cart']='إفراغ السلة';
$lang['update_cart']='تعديل السلة';
$lang['description']='التوصيف';
$lang['qty']='الكمية';
$lang['price']='السعر';
$lang['price_discount']='السعر بعد الخصم';
$lang['total']='لإجمالي';
$lang['add_to_cart']='إضافة إلى سلة الشراء';
$lang['no_products']='لا يوجد منتجات';
// $lang['men']='men';
// $lang['women']='women';
// $lang['news']='news';
$lang['product_name']='اسم المنتج';
$lang['product_order']='طلب المنتج';
$lang['shop']='تسوق';
$lang['select_gender']='اختر الجنس';
$lang['select_category']='اختر تصنيف';
$lang['select_style']='اختر الستايل';
$lang['products_filter']='فلترة المنتجات';
$lang['edit_profile']='تعديل الحساب';

$lang['newsletter_mail_list']='القائمة البريدية';
$lang['subscribe']='اشترك';
$lang['enter_your_email']='أدخل إيميلك';
$lang['follow_us']='تابعنا على شبكات التواصل الاجتماعي';
$lang['daily_deals']='العروض الخاصة يوميا';

$lang['unvalide_email']='البريد الالكتروني خاطئ';
$lang['subscrib_succcess']= 'تم تسجيل بريدك في القائمة البريدية';
$lang['subscrib_failed']='حدث خطأ أثناء تسجيل بريدك يرجى المحاولة من جديد';
$lang['email_exist'] = 'بريدك الالكتروني مسجل لدينا مسبقاً شكرا لك.';

$lang['image_albums']='ألبومات الصور';
$lang['video_albums']='ألبومات الفيديو';
$lang['logout']										= 'تسجيل الخروج';



$lang['order_email_from']='إيميل الطلبات من';
$lang['order_email_to']='إيميل الطلبات ل';
$lang['orders']='الطلبات';
$lang['accept']='مقبول';
$lang['reject']='مرفوض';
$lang['pending']='في الانتظار';
$lang['order_from']='الطلب من : ';
$lang['description']='التوصيف';
$lang['qty']='الكمية';
$lang['price']='السعر';
$lang['total']='لإجمالي';
$lang['no_roders']='لا يوجد طلبات';
$lang['orders_history'] = 'أرشيف الطلبات';
$lang['orders_pending'] = 'الطلبات التي في الانتظار';

$lang['edit']='تعديل';
$lang['show']='عرض';

$lang['latest_products']='آخر المنتجات';
$lang['offer_products']='العروض الخاصة';



$lang['info']='معلومات';
$lang['success']='نجاح';
$lang['warning']='خطأ';
$lang['danger']='خطأ';

$lang['first_name']='الاسم الأول ';
$lang['last_name']='الاسم الأخير';	

$lang['select_town']='اختر المحافظة';
$lang['town_name']='أدخل اسم المدينة';
$lang['town_required']='المدينة مطلوبة';

$lang['checkout_cart_success']='تم استلام طلب الشراء الخاص بك سيتم التواصل معك لتسليم الطلب';
$lang['update_cart_success']='تم تعديل سلة الشراء بنجاح';
$lang['add_to_cart_success']='تمت إضافة المنتج إلى سلة الشراء الخاصة بك يمكنك الدخول إلى سلة المشتريات أعلى الموقع وإرسال طلبات الشراء';
$lang['delete_cart_success']						='تم حذف السلة بنجاح';
$lang['delete_item_success']						='تم حذف المنتج من السلة بنجاح';

$lang['login_forgot_password']='نسيت كلمة المرور';

$lang['login_befor_add_to_cart']='يجب تسجيل الدخول قبل استخدام سلة الشراء ';
$lang['date']='التاريخ';
$lang['login_failed'] ='  يوجد معلومات ناقصة . الرجاء الاطلاع على قسم تسجيل الدخول';
$lang['register_failed'] ='  يوجد معلومات ناقصة , الرجاء الاطلاع على قسم التسجيل';
$lang['verify_code_required']='رمز التحقق مطلوب';
$lang['verify_code']='رمز النحقق';
$lang['other']='غير ذلك';
$lang['login_forgot_password_']='هل نسيت كلمة المرور';

$lang['user_email']='إيميل الزبون';
$lang['register_success']='لقد تم التسجيل بنجاح أهلاً وسهلاً بك في موقع Rich time';

$lang['age']='العمر';
$lang['education']='المرحلة الدراسية';
$lang['select_education']='اختر المرحلة الدراسية';

$lang['select_age']='اختر العمر ';
