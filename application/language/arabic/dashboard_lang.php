<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Language
$lang['lang']										= 'arabic';

$lang['home']										= 'الرئيسية';
$lang['articles']									= 'المقالات';
$lang['constants']									= 'الثوابت';
$lang['edit_articles']								= 'تعديل المقالات';
$lang['logout']										= 'تسجيل الخروج';
$lang['sub_articles']								= 'المقالات الفرعية';
$lang['title_en']									= 'العنوان بالإنكليزي ';
$lang['title_ar']									= 'العنوان بالعربي';
$lang['title_rd']									= 'Title Rd';
$lang['updated_at']									= ' تاريخ التعديل';
$lang['created_at']									= ' تاريخ الإضافة';
$lang['details']									= 'التفاصيل';
$lang['delete']										= 'حذف';
$lang['delete_confirmation']						= 'حذف التأكيد';
$lang['delete_confirmation_text']					= 'هل أنت متأكد من أنك تريد حذف العنصر?..';
$lang['close']										= 'إغلاق';
$lang['description']								= 'الوصف';
$lang['description_en']								= 'الوصف بالإنكليزي';
$lang['description_ar']								= 'الوصف';
$lang['description_rd']								= 'الوصف بال rd';
$lang['thumbnail']									= 'الصورة المصغرة';
$lang['image']										= 'الصورة';
$lang['text_en']									= 'المحتوى بالإنكليزي';
$lang['text_ar']									= 'المحتوى بالعربي ';
$lang['text_rd']									= 'Text Rd';
$lang['save']										= 'حفظ';
$lang['gallery']									= 'المعرض ';
$lang['language']									= 'اللغة';
$lang['insert_success']								= 'تم الإدخال بنجاح..';
$lang['update_success']								= 'تم التعديل بنجاح..';
$lang['delete_success']								= 'تم الحذف بنجاح..';
$lang['messages_danger']							= 'عذراً، يرجى حذف الأقسام الفرعية قبل الحذف.';
$lang['read_more']									= 'اقرأ المزيد';
$lang['youtube']									= 'Youtube Video';
$lang['best_size']									= 'مقاس الصورة الأفضل';
$lang['please_signin']								= 'الرجاء تسجيل الدخول';
$lang['username']									= 'البريد الالكتروني';
$lang['password']									= 'كلمة السر';
$lang['remember_me']								= 'تذكرني';
$lang['signin']										= 'تسجيل الدخول';
$lang['gallery11']									= 'المعرض';
$lang['gallery11']									= 'المعرض';

$lang['slideshow']									= 'شريط الصور';
$lang['sort']										= 'فرز';
$lang['preview']									= 'معاينة';
$lang['add_new_item']								= 'إضافة عنصر جديد';


$lang['manage_users']								= 'إدارة المستخدمين';

// home and constants..
$lang['home_phone'] = 'الهاتف';
$lang['home_email'] = 'البريد الإلكتروني';
$lang['home_address'] = 'العنوان';
$lang['home_block_1_url'] = 'رابط الكتلة 1';
$lang['home_block_2_url'] = 'رابط الكتلة 2';
$lang['home_block_3_url'] = 'رابط الكتلة 3';
$lang['home_block_4_url'] = 'رابط الكتلة 4';
$lang['home_title_ar'] = 'العنوان';
$lang['home_intro_ar'] = 'المقدمة';
$lang['home_block_1_title_ar'] = 'عنوان الكتلة 1';
$lang['home_block_1_content_ar'] = 'محتوى الكتلة 1';
$lang['home_block_2_title_ar'] = 'عنوان الكتلة 2';
$lang['home_block_2_content_ar'] = 'محتوى الكتلة 2';
$lang['home_block_3_title_ar'] = 'عنوان الكتلة 3';
$lang['home_block_3_content_ar'] = 'محتوى الكتلة 3';
$lang['home_block_4_title_ar'] = 'عنوان الكتلة 4';
$lang['home_block_4_content_ar'] = 'محتوى الكتلة 4';
$lang['home_box_title_ar'] = 'عنوان المربع المميز';
$lang['home_box_content_ar'] = 'محتوى المربع المميز';
$lang['home_quote_1_ar'] = 'الاقتباس الأول';
$lang['home_quote_2_ar'] = 'الاقتباس الثاني';
$lang['home_quote_1_url'] = 'رابط الاقتباس الأول';
$lang['home_quote_2_url'] = 'رابط الاقتباس الثاني';

$lang['title'] = 'العنوان';
$lang['keywords'] = 'الكلمات المفتاحية';
$lang['author'] = 'المحرر';
$lang['contact_email_from'] = 'البريد المرسل منه في صفحة اتصل بنا';
$lang['contact_email_to'] = 'البريد المرسل إليه في صفحة اتصل بنا';
$lang['sm_facebook'] = 'فيسبوك';
$lang['sm_twitter'] = 'تويتر';
$lang['sm_youtube'] = 'يوتيوب';
$lang['sm_google_plus'] = 'غوغل بلس';
$lang['sm_linked_in'] = 'لينكد أن';
$lang['sm_instagram']='أنستغرام';

$lang['advertisement']='الإعلانات';
$lang['categories'] = 'التصنيفات';
$lang['products'] = 'المنتجات';
$lang['albums'] = 'الألبومات';
$lang['orders'] = 'الطلبات';
$lang['privileges'] = 'الصلاحيات';
$lang['privileges_usermanagement']					= 'الصلاحيات والمستخدمين';

$lang['current_priv']								= 'الصلاحيات الحالية';
$lang['add_priv']									= 'الصلاحيات التي يمكن إضافتها';
$lang['delete_priv']								= 'حذف صلاحية';
$lang['assigning_priv']								= 'الصلاحيات المسندة';

$lang['orders_history'] = 'أرشيف الطلبات';
$lang['orders_pending'] = 'الطلبات التي في الانتظار';



////// MHDProgr


$lang['save_advertising_successfully']='تم الحفظ بنجاح';
$lang['advertisement']='الإعلانات';
$lang['left_advertisement']='الإعلان اليساري';
$lang['right_advertisement']='الإعلان اليميني';
$lang['currnet_left_advertisement']='الإعلان اليساري الحالي';
$lang['currnet_right_advertisement']='الإعلان اليميني الحالي';

//categories
$lang['categories'] = 'التصنيفات';
$lang['no_categories']='لا يوجد تصنيفات';
$lang['cat_name']='اسم التصنيف';
$lang['show_sub_categories']='عرض التصنيفات الفرعية';
$lang['cat_name_en']='اسم التصنيف بالانكليزية';
$lang['cat_name_ar']='اسم التصنيف بالعربية';
$lang['cat_name_rd']='اسم التصنيف باللغة الثالثة';
$lang['category']= 'تصنيف';
$lang['add']='إضافة';
$lang['parent_category']='التصنيف الرئيسي';
$lang['add_category']='إضافة تصنيف';
$lang['no_parent']='ليس لها تصنيف  أب';

//product
$lang['product_name']='اسم المنتج';
$lang['no_products']='لا يوجد منتجات';
$lang['prod_name_en']='اسم المنتج بالانكليزية';
$lang['prod_name_ar']='اسم المنتج بالعربية';
$lang['prod_name_rd']='اسم المنتج باللغة الثالثة';
$lang['prod_des_en']='وصف المنتج بالانكليزية';
$lang['prod_des_ar']='وصف المنتج بالعربية';
$lang['prod_des_rd']='وصف المنتج باللغة الثالثة';
$lang['prod_price_en']='سعر المنتج بالانكليزية';
$lang['prod_price_ar']='سعر المنتج بالعربية';
$lang['prod_price_rd']='سعر المنتج باللغة الثالثة';
$lang['prod_price']='سعر المنتج';
$lang['prod_price_discount_en']=' السعر بعد الحسم  بالانكليزية';
$lang['prod_price_discount_ar']=' السعر  بعد الحسم بالعربية';
$lang['prod_price_discount_rd']=' السعر  بعد الحسم باللغة الثالثة';
$lang['prod_price_discount']='السعر بعد الحسم ';
$lang['price_discount']='السعر بعد الخصم';

$lang['edit_product']='تعديل منتج ';
$lang['prod_number']='رقم المنتج';
$lang['prod_number_must_unique'] ='رقم المنتج يجب أن يكون فريد';
$lang['products']='المنتجات';
$lang['add_product']='إضافة منتج';
$lang['gender']='الجنس';
$lang['man']='رجل';
$lang['women']='امرأة';

//$lang['unit']='ل.س';


$lang['update_products_price']='تعديل سعر المنتج';
$lang['update_products_successfully']='تم تعديل المنتج بنجاح';
$lang['failed_in_update_products']='فشل في تعديل المنتج';

//album
$lang['image_albums']='ألبوم الصور';
$lang['video_albums']='ألبوم الفيديو';
$lang['albums']='الألبومات';
$lang['album_name']='اسم الألبوم';
$lang['no_albums']='لا يوجد ألبومات';
$lang['add_album']='إضافة ألبوم';
$lang['edit_album']='تعديل ألبوم';
$lang['alb_title_en']='عنوان الألبوم بالانكليزية';
$lang['alb_title_ar']='عنوان الألبوم بالعربية';
$lang['alb_title_rd']='عنوان الألبوم باللغة الثالثة';

//public

$lang['save_successfully']='تم الحفظ بنجاح';
$lang['save_failed']='فشل الحفظ';
$lang['delete']='حذف';
$lang['close']='إغلاق';
$lang['edit']='تعديل';
$lang['show']='عرض';
$lang['type']='نوع';
$lang['video']='فيديو';
$lang['no_image_choice'] ='لا يوجد صورة';
$lang['style']='نمط';

$lang['language']='لغة';
$lang['english']='الانكليزية';
$lang['arabic']='العربية';
$lang['rd']='اللغة الثالثة';
$lang['update']='تحديث';

$lang['order_email_from']='إرسال طلبات الشراء عبر هذا الإيميل ';
$lang['order_email_to']='استقبال طلبات الشراء على هذا الإيميل';
$lang['orders']='الطلبات';
$lang['accept']='موافقة';
$lang['reject']='رفض';
$lang['pending']='في الانتظار';
$lang['order_from']='طلب من: ';
$lang['qty']='الكمية';
$lang['price']='السعر';
$lang['total']='الإجمالي';
$lang['orders_history']='أرشيف الطلبات';
$lang['orders_pending']='الطلبات التي في الانتظار';
$lang['no_roders']='لا يوجد طلبات';
$lang['reject_your_order']='طلبك مرفوض';
$lang['accept_your_order']='تم قبول طلبك';

$lang['reports']='التقارير';
$lang['report_order_count']='تقرير المنتجات الآكثر طلباً';
$lang['order_count']='عدد الطلبات';
$lang['brands']='الماركات';

$lang['edit']='تعديل';
$lang['show']='عرض';
$lang['new_product']='منتج جديد';
$lang['visit_our_website']='زوروا موقعنا لمشاهدة المنتجات الجديدة';

$lang['contact_us_mail']='الإيميل في صفحة تواصل معنا ';
$lang['contact_us_phone']='الموبايل في صفحة تواصل معنا';
$lang['unit_']='وحدة السعر التي تظهر عند إدخال السعر غير المحدد وفق لغة';
$lang['unit_en']='وحدة السعر عند عرض الموقع باللغة الإنكليزية';
$lang['unit_ar']='وحدة السعر عند عرض الموقع باللغة العربية';
$lang['unit_rd']='وحدة السعر عند عرض الموقع باللغة  rd ';


$lang['min_price']='الحد الأدنى للسعر عند إدخال منتج وفق سعر بدون لغة';
$lang['min_price_en']='الحد الأدنى للسعر  عند تصفح الموقع باللغة الإنكليزية';
$lang['min_price_ar']='الحد الأدنى للسعر  عند تصفح الموقع باللغة العربية';
$lang['min_price_rd']='الحد الأدنى للسعر  عند تصفح الموقع باللغة rd';

$lang['max_price']='الحد الأعلى للسعر عند إدخال منتج وفق سعر بدون لغة';
$lang['max_price_en']='الحد الأعلى للسعر  عند تصفح الموقع باللغة الإنكليزية';
$lang['max_price_ar']='الحد الأعلى للسعر  عند تصفح الموقع باللغة العربية';
$lang['max_price_rd']='الحد الأعلى للسعر  عند تصفح الموقع باللغة rd';

$lang['brands_header_en']='عنوان منطقة الماركات بالصفحة الرئيسية باللغة الإنكليزية ';
$lang['brands_header_ar']='عنوان منطقة الماركات بالصفحة الرئيسية باللغة العربية ';
$lang['brands_header_rd']='عنوان منطقة الماركات بالصفحة الرئيسية باللغة rd ';

$lang['info']='معلومات';
$lang['success']='نجاح';
$lang['warning']='خطأ';
$lang['danger']='خطأ';

$lang['manage_users'] = 'إدارة المستخدمين';

$lang['shop_now']='رابط تسوق الآن ';	

$lang['select_gender']='اختر الجنس';
$lang['select_category']='اختر تصنيف';
$lang['select_style']='اختر الستايل';
$lang['date']='التاريخ';
$lang['edit_slider']=' تعديل السلايدر';
$lang['edit_category']='تعديل التصنيف';

$lang['assigning_roles']='إشناد الأدوار';
$lang['delete_role']='حذف دور';
$lang['current_role']='الأدوار الحالية';

$lang['stat_content']='المحتوى';
$lang['stat_number']='العدد';
$lang['stat_count']='عدد الزيارات';
$lang['users']='مستخدمين';
$lang['statistics']='الإحصائيات';
$lang['browsers']='المتصفحات';
$lang['countries'] ='البلدان';
$lang['visite_rate'] ='معدل الزيادرة';

$lang['prod_quantity'] ='كمية المنتج';
$lang['prod_qty_avaliabel'] ='الكمية المتوفرة من المنتج';

//styles
$lang['styles']		='ستايلات المنتجات';
$lang['add_style']='إضافة ستايل';
$lang['edit_style']='تعديل ستايل';
$lang['no_styles']='لا يوجد ستايلات';
$lang['style_title']=' العنوان ';
$lang['ps_title_en']='العنوان بالإنكليزي';
$lang['ps_title_ar']='العنوان بالعربي';
$lang['ps_title_rd']=' العنوان بال rd';


$lang['shop_now_slider']='رابط تسوق الآن للسلايدر';
$lang['about_us_url']='رابط من نحن الموجود بالفوتر';
$lang['about_us_title']='عنوان من نحن الموجود بالفوتر';
$lang['about_us_text_en']='النص بالإنكليزي';
$lang['about_us_text_ar']='النص بالعربي';
$lang['about_us_text_rd']='النص باللغة الثالثة';

$lang['url']='الرابط';
$lang['private_offers']='العروض الخاصة';
$lang['add_private_offer']='إضافة عرض خاص';
$lang['start_date'] = 'تاريخ البداية';
$lang['end_date']='تاريخ النهاية';
$lang['edit_product_offer']='تعديل العرض الخاص';
$lang['price_discount']='السعر بعد الحسم ';


$lang['product']='المنتج';
$lang['export_users_to_excel']='تصدير المستخدمين إلى ملف أكسل';
$lang['outer_image']='الصورة الخارجية';

$lang['active']='فعال';
$lang['not_active']='غير فعال';
$lang['contact_us']='تواصل معنا';

$lang['category_levels']='عدد مستويات تصنيف المنتجات';
$lang['article_levels']='عدد مستويات المقالة';
$lang['priv_name'] = 'اسم الصلاحية';
$lang['update_info'] = 'تعديل المعلومات';

?>