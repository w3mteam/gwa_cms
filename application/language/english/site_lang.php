<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Language
$lang['lang']										= 'english';
$lang['recent_posts']								= 'Recent Posts';
$lang['phone']										= 'Phone';
$lang['email']										= 'Email';
$lang['address']									= 'Address';
$lang['language']='Language';
$lang['english']='english';
$lang['arabic']='arabic';
$lang['rd']='rd';
$lang['search']='SEARCH';
$lang['sign_in']='sign in';
$lang['sign_up']='sign up';
$lang['home']										= 'Home';
$lang['contact']									= 'contact us';
$lang['views_count']								= 'views';
$lang['follow_us_on']								= 'follow us ';
$lang['related_articles']							= 'related';
$lang['contact_form']								= 'contact form';
$lang['name']										= 'name';
$lang['email']										= 'email';
$lang['comment']									= 'comment';
$lang['submit']										= 'submit';
$lang['reset']										= 'reset';
$lang['address']									= 'address';
$lang['contact_success']							= 'contact_success';
$lang['contact_fail']								= 'contact_fail';
$lang['read_more']									= 'read_more';
$lang['recent_posts']								= 'recent posts';
$lang['phone']										= 'phone';
$lang['email']										= ' email';
$lang['address']									= 'address';
$lang['copyrights']									= '<a href="http://gwa-group.com/">ALL RIGHTS RESERVED &copy; GWA</a>';


$lang['update_cart_success']						='update cart successfully';
$lang['update_cart_failed']							='failed in update cart ';
$lang['add_to_cart_success']						='product added to your cart,please checkout your order';
$lang['delete_cart_success']						='delete cart successfully';
$lang['delete_item_success']						='delete item form cart successfully';


//cart

$lang['image']='Image';
$lang['view_cart']='view cart';
$lang['checkout_cart']='Checkout';
$lang['delete_cart']='delete cart';
$lang['update_cart']='update cart';
$lang['description']='Description';
$lang['qty']='Qty';
$lang['price']='Price';
$lang['price_discount']='price discount';
$lang['total']='Total';
$lang['add_to_cart']='add to cart';
$lang['no_products']='Not found any products..';
$lang['men']='men';
$lang['women']='women';
$lang['news']='website news';
$lang['product_name']='product name';
$lang['product_order']='product order';
$lang['shop']='shop';
$lang['select_gender']='Select Gender';
$lang['select_category']='Select Category';
$lang['select_style']='Select Style';
$lang['min_price']='Min Price';
$lang['max_price']='Max Price';
$lang['products_filter']='Product filter';





/* contact lang */
$lang['location']									= 'Our location';
$lang['send']										= 'Send';
$lang['name_placeholder']							= 'Your Name - Required';
$lang['email_placeholder']							= 'Your Email - Required';
$lang['msg_placeholder']							= 'You Message...';
$lang['msg_form']									= 'Message form';
$lang['message_sent_success']						= 'Message Sent successfully.. Thank You';
$lang['message_sent_faild']							= 'Message Sent Faild .. Please try again';
$lang['view_map']									= 'VIEW MAP';
$lang['our_location']								= 'Our location';
$lang['our_map_location']							= 'Our Map Location';



$lang['cultrue']									= 'Cultrue';
$lang['album']='Album';
$lang['product']='Products';
$lang['shop_now']='SHOP NOW';
$lang['men']='MEN';
$lang['women']='WOMEN';	
$lang['news']='NEW';	
$lang['reviews']='review(s)';
$lang['brand']='All PREMIUM BRANDS';
$lang['price_filter']='PRICE FILTER';
$lang['min_price']='Min Price';
$lang['max_price']='Max Price';
$lang['product_details']='Product Details';	
$lang['our_location']='OUR LOCATION';
$lang['message']='Message' ;
$lang['your_name']='Your Name-Required' ;
$lang['your_email']='Your Email-Required' ;
$lang['your_message']='Your Message' ;
$lang['send']='SEND' ;
$lang['remember_me']='Remember Me' ;
$lang['password']='Password' ;	
$lang['repet_pass']='Repeat Password' ;
$lang['accept']='I Accept the terms and conditions' ;
$lang['not_found_men']='Not found any MEN Products' ;
$lang['not_found_women']='Not found any WOMEN Products' ;	
$lang['not_found_news']='لNot found any NEWS Products' ;
$lang['not_found_pro']='Not found any Products' ;
$lang['first_name']='first name';
$lang['last_name']='last name';	
$lang['edit_successfully']='edit successfully';	
$lang['edit_profile']='edit profile';
$lang['orders_history']='orders history';
$lang['orders_pending']='orders pending';
$lang['edit_profile']='edit profile';

$lang['newsletter_mail_list']='MAILING LIST';
$lang['subscribe']='subscribe';
$lang['enter_your_email']='Enter your email addres';
$lang['follow_us']='Follow us on social media';
$lang['daily_deals']='daily exclusive deals';

$lang['unvalide_email']='unvalide email';
$lang['subscrib_succcess']='subscrib succcessfully';
$lang['subscrib_failed']='subscrib failed';
$lang['email_exist'] = 'Your Email already Exist.';

$lang['image_albums']='image albums';
$lang['video_albums']='video albums';

$lang['logout']										= 'Logout';


$lang['order_email_from']='order_email_from';
$lang['order_email_to']='order_email_to';
$lang['orders']='orders';
$lang['accept']='accept';
$lang['reject']='reject';
$lang['pending']='pending';
$lang['order_from']='order from: ';
$lang['qty']='Qty';
$lang['price']='Price';
$lang['total']='Total';
$lang['orders_history']='orders history';
$lang['orders_pending']='orders pending';
$lang['no_roders']='no roders';
$lang['reject_your_order']='reject your order';
$lang['accept_your_order']='accept your order';

$lang['edit']='edit';
$lang['show']='show';

$lang['latest_products']='latest products';
$lang['offer_products']='offer products';


$lang['info']='info';
$lang['success']='success';
$lang['warning']='warning';
$lang['danger']='danger';

$lang['select_town']='select town';
$lang['town_name']='town name';
$lang['town_required']='town required';

$lang['checkout_cart_success']='checkout cart successfully, we are contact with you to deliver your order';
$lang['update_cart_success']='update cart successfully';

$lang['login_befor_add_to_cart']='login befor add to cart';
$lang['date']='date';

$lang['login_failed'] =' login failed show error in sign in section';
$lang['register_failed'] =' register failed show error in sign up section';
$lang['verify_code_required']='verify code required';
$lang['verify_code']='verify code';
$lang['other']='other';
$lang['login_forgot_password_']='forgot  your password';

$lang['user_email']='user email';
$lang['register_success']='register successfully , welcome to website Richtime';

$lang['age']='age';
$lang['education']='education';
$lang['select_education']='select education';

$lang['select_age']='select age';


