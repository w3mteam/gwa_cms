<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Language
$lang['lang']										= 'english';

$lang['home']										= 'Home';
$lang['articles']									= 'Articles';
$lang['constants']									= 'Constants';
$lang['edit_articles']								= 'Edit Articles';
$lang['logout']										= 'Logout';
$lang['sub_articles']								= 'Sub Articles';
$lang['title_en']									= 'Title En';
$lang['title_ar']									= 'Title Ar';
$lang['title_rd']									= 'Title Rd';
$lang['updated_at']									= 'Updated At';
$lang['created_at']									= 'Created At';
$lang['details']									= 'Details';
$lang['delete']										= 'Delete';
$lang['delete_confirmation']						= 'Delete Confirmation';
$lang['delete_confirmation_text']					= 'Are you sure you want to delete this item?..';
$lang['close']										= 'Close';
$lang['description']								= 'Description';
$lang['description_en']								= 'description en';
$lang['description_ar']								= 'description ar';
$lang['description_rd']								= 'description rd';
$lang['thumbnail']									= 'Thumbnail';
$lang['image']										= 'Image';
$lang['text_en']									= 'Text En';
$lang['text_ar']									= 'Text Ar';
$lang['text_rd']									= 'Text Rd';
$lang['save']										= 'Save';
$lang['gallery']									= 'Gallery';
$lang['language']									= 'Language';
$lang['insert_success']								= 'New item was inserted successfully..';
$lang['update_success']								= 'item was updated successfully..';
$lang['delete_success']								= 'item was deleted successfully..';
$lang['messages_danger']							= 'Oops!, Please remove sub items first.';
$lang['read_more']									= 'Read more';
$lang['youtube']									= 'Youtube Video';
$lang['best_size']									= 'Best Size';
$lang['please_signin']								= 'Please sign in';
$lang['username']									= 'Username';
$lang['password']									= 'Password';
$lang['remember_me']								= 'Remember me';
$lang['signin']										= 'Sign in';
$lang['gallery11']									= 'gallery11';
$lang['gallery11']									= 'gallery11';

$lang['slideshow']									= 'Slideshow';
$lang['sort']										= 'Sort';
$lang['preview']									= 'Preview';
$lang['add_new_item']								= 'Add new item';

$lang['privileges']									= 'Privileges';
$lang['privileges_usermanagement']					= 'Privileges and Users';
$lang['current_priv']								= 'Current Privileges';
$lang['add_priv']									= 'Add Privileges';
$lang['delete_priv']								= 'Delete Privileges';
$lang['assigning_priv']								= 'Assigning Privilages';

$lang['manage_users']								= 'Users management';
// home and constants..
$lang['home_phone'] = humanize('phone');
$lang['home_email'] = humanize('email');
$lang['home_address'] = humanize('address');
$lang['home_block_1_url'] = humanize('block_1_url');
$lang['home_block_2_url'] = humanize('block_2_url');
$lang['home_block_3_url'] = humanize('block_3_url');
$lang['home_block_4_url'] = humanize('block_4_url');
$lang['home_title_ar'] = humanize('title_ar');
$lang['home_intro_ar'] = humanize('intro_ar');
$lang['home_block_1_title_ar'] = humanize('block_1_title_ar');
$lang['home_block_1_content_ar'] = humanize('block_1_content_ar');
$lang['home_block_2_title_ar'] = humanize('block_2_title_ar');
$lang['home_block_2_content_ar'] = humanize('block_2_content_ar');
$lang['home_block_3_title_ar'] = humanize('block_3_title_ar');
$lang['home_block_3_content_ar'] = humanize('block_3_content_ar');
$lang['home_block_4_title_ar'] = humanize('block_4_title_ar');
$lang['home_block_4_content_ar'] = humanize('block_4_content_ar');
$lang['home_box_title_ar'] = humanize('box_title_ar');
$lang['home_box_content_ar'] = humanize('box_content_ar');
$lang['home_quote_1_ar'] = humanize('quote_1_ar');
$lang['home_quote_2_ar'] = humanize('quote_2_ar');
$lang['home_quote_1_url'] = humanize('quote_1_url');
$lang['home_quote_2_url'] = humanize('quote_2_url');

$lang['title'] = 'Title';
$lang['keywords'] = 'Keywords';
$lang['author'] = 'Author';
$lang['contact_email_from'] = 'Contact Email From';
$lang['contact_email_to'] = 'Contact Email To';
$lang['sm_facebook'] = 'Facebook';
$lang['sm_twitter'] = 'Twitter';
$lang['sm_youtube'] = 'Youtube';
$lang['sm_google_plus'] = 'Google Plus';
$lang['sm_linked_in'] = 'Linked In';
$lang['sm_instagram']='Instagram';


////// MHDProgr

$lang['save_advertising_successfully']='save successfully';
$lang['advertisement']='advertisement';
$lang['left_advertisement']='left advertisement';
$lang['right_advertisement']='right advertisement';
$lang['currnet_left_advertisement']='current left advertisement';
$lang['currnet_right_advertisement']='current right advertisement';

//categories
$lang['categories'] = 'categories';
$lang['no_categories']='no_categories';
$lang['cat_name']='category name';
$lang['show_sub_categories']='show sub categories';
$lang['cat_name_en']='category name english';
$lang['cat_name_ar']='category name arabic';
$lang['cat_name_rd']='cat_name_rd';
$lang['category']='category';
$lang['add']='save';
$lang['parent_category']='parent category';
$lang['add_category']='add category';
$lang['no_parent']='has no parent';

//product
$lang['product_name']='product name';
$lang['no_products']='no products';
$lang['prod_name_en']='product name english';
$lang['prod_name_ar']='product name arabic';
$lang['prod_name_rd']='product name rd';
$lang['prod_des_en']='prod_des_en';
$lang['prod_des_ar']='prod_des_ar';
$lang['prod_des_rd']='prod_des_rd';
$lang['prod_price_en']='prod_price_en';
$lang['prod_price_ar']='prod_price_ar';
$lang['prod_price_rd']='prod_price_rd';
$lang['prod_price']='prod_price';
$lang['prod_price_discount_en']='price after discount in english ';
$lang['prod_price_discount_ar']='price after discount in arabic';
$lang['prod_price_discount_rd']='price after discount in rd';
$lang['prod_price_discount']='price after discount';
$lang['price_discount']='price discount';

$lang['prod_number']='product number';
$lang['prod_number_must_unique'] ='product number must unique';
$lang['prod_quantity'] ='product quantity';
$lang['prod_qty_avaliabel'] ='product quantity avaliabel';
$lang['products']='products';
$lang['add_product']='add product';
$lang['edit_product']='edit product';
$lang['gender']='gender';
$lang['man']='man';
$lang['women']='women';

$lang['unit']='$';


$lang['update_products_price']='update products price';
$lang['update_products_successfully']='update products successfully';
$lang['failed_in_update_products']='failed in update products';

//album
$lang['image_albums']='image albums';
$lang['video_albums']='video albums';
$lang['albums']='albums';
$lang['album_name']='album name';
$lang['no_albums']='no albums';
$lang['add_album']='add album';
$lang['edit_album']='edit album';
$lang['alb_title_en']='album title en';
$lang['alb_title_ar']='album title ar';
$lang['alb_title_rd']='album title rd';

//public

$lang['save_successfully']='save successfully';
$lang['save_failed']='save failed';
$lang['delete']='delete';
$lang['close']='colse';
$lang['edit']='edit';
$lang['show']='show';
$lang['type']='type';
$lang['video']='video';
$lang['no_image_choice'] ='there is no image';
$lang['style']='style';

$lang['language']='Language';
$lang['english']='english';
$lang['arabic']='arabic';
$lang['rd']='rd';
$lang['update']='update';

$lang['order_email_from']='email for send order buying -  from';
$lang['order_email_to']='email for  recive order buying - to';
$lang['orders']='orders';
$lang['accept']='accept';
$lang['reject']='reject';
$lang['pending']='pending';
$lang['order_from']='order from: ';
$lang['qty']='Qty';
$lang['price']='Price';
$lang['total']='Total';
$lang['orders_history']='orders history';
$lang['orders_pending']='orders pending';
$lang['no_roders']='no roders';
$lang['reject_your_order']='reject your order';
$lang['accept_your_order']='accept your order';

$lang['reports']='reports';
$lang['report_order_count']='report for most  products order';
$lang['order_count']='order count';
$lang['brands']='brands';

$lang['new_product']='new product';
$lang['visit_our_website']='visit our website for new products';

$lang['contact_us_mail']='contact us mail';
$lang['contact_us_phone']='contact us phone';
$lang['unit_en']='unit english';
$lang['unit_ar']='unit arabic';
$lang['unit_rd']='unit_rd';
$lang['unit_']='other unit';

$lang['min_price']='min price when enter price without language';
$lang['min_price_en']=' min price in english language';
$lang['min_price_ar']=' min price in ararbic language';
$lang['min_price_rd']=' min price in rd language';

$lang['max_price']='max price when enter price without language';
$lang['max_price_en']='max price in english language';
$lang['max_price_ar']='max price in arabic language';
$lang['max_price_rd']='max price in rd language';


$lang['brands_header_en']='brands section header english ';
$lang['brands_header_ar']='brands section header arabic ';
$lang['brands_header_rd']='brands section header rd ';
$lang['info']='info';
$lang['success']='success';
$lang['warning']='warning';
$lang['danger']='danger';

$lang['manage_users'] = 'Users management';

$lang['shop_now']='SHOP NOW link';

$lang['select_gender']='Select Gender';
$lang['select_category']='Select Category';
$lang['select_style']='Select Style';

$lang['date']='date';
$lang['edit_slider']='edit slider';

$lang['edit_category']='edit category';

$lang['assigning_roles']='Assigning Roles';
$lang['delete_role']='Delete Role';
$lang['current_role']=' Current Role';

$lang['stat_content']='content';
$lang['stat_number']='count';
$lang['stat_count']='visit count';
$lang['users']='users';
$lang['statistics']='statistics';
$lang['browsers']='browsers';
$lang['countries'] ='countries';
$lang['visite_rate'] ='visite rate';

//styles
$lang['styles']='styles';
$lang['add_style']='add style';
$lang['edit_style']='edit style';
$lang['no_styles']='no styles';
$lang['style_title']='style title';
$lang['ps_title_en']='title en';
$lang['ps_title_ar']='title ar';
$lang['ps_title_rd']='title rd';


//to do 
$lang['shop_now_slider']='shop now link in slider';
$lang['about_us_url']='about us url';
$lang['about_us_title']='about us title';
$lang['about_us_text_en']='about us text english';
$lang['about_us_text_ar']='about us text arablic';
$lang['about_us_text_rd']='about us text rd';


$lang['url']='url';
$lang['private_offers']='private offers';
$lang['add_private_offer']='add private offer';
$lang['start_date'] = 'start date';
$lang['end_date']='end date';
$lang['edit_product_offer']='edit product offer';
$lang['price_discount']='price after discount';

$lang['product']='product';
$lang['export_users_to_excel']='export users to excel';
$lang['outer_image']='outer image';

$lang['active']='active';
$lang['not_active']='not active';
$lang['contact_us']='contact us';
$lang['category_levels']='category levels';
$lang['article_levels']='article levels';
$lang['priv_name'] = 'Privilage name';
$lang['update_info'] = 'Update info';
