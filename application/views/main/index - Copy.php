      <?php $this->load->view('template/slider');
	  
      ?>
      
      <!-- Content Block
      ============================================ -->
      <div class="content-block">
      
        <!-- Container -->
        <div class="container no-pad-t">

          <!-- Product Tabs -->
          <div class="product-tabs">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-line-bottom line-pcolor nav-tabs-center case-u" role="tablist">

              <li class="active"><a href="#tab-men" data-toggle="tab"><?php echo lang('men');?></a></li>
              <li><a href="#tab-women" data-toggle="tab"><?php echo lang('women');?></a></li>
              <li><a href="#tab-news" data-toggle="tab"><?php echo lang('news');?></a></li>
            </ul>
            <!-- /Nav Tabs -->
            <!-- Tab panes -->
            <div class="tab-content tab-no-borders">
            
              <!-- Tab Latest -->
              <div class="tab-pane active" id="tab-men">
              

                 <div class="row">
                  <?php
                  if($man_products != null){
                  for ($i=0; $i <sizeof($man_products) ; $i++) { ?>
                      <div class="col-sm-6 col-md-3">
	                    <!-- product -->
	                    <div class="product clearfix">
	                    
	                      <!-- Image -->
	                      <div class="image"> 
	                        <a href="<?php echo base_url();?>index.php/main/product/<?php echo $man_products[$i]->prod_id ;?>" class="main"><img src="<?php echo base_url();?>assets/uploads/products/<?php echo $man_products[$i]->prod_image ;?>" alt=""></a>
	                       
	                      </div>
	                      <!-- Image -->
	                      
	                      <!-- Details -->
	                      <div class="details">
	                      
	                        <a class="title" href="#"><?php echo $man_products[$i]->{"prod_name_{$lang}"} ;?></a>
	                        
	                       
	                        <!-- Price Box -->
	                        <div class="price-box">
	                        	<?php
	                        	 $prod_price=$man_products[$i]->prod_price;
								 $prod_price_discount=$man_products[$i]->prod_price_discount;
								 $prod_price_lang=$man_products[$i]->{"prod_price_{$lang}"} ;
								 $prod_price_discount_lang=$man_products[$i]->{"prod_price_discount_{$lang}"} ;
								 
								 ?> 
	                        	<?php  if ( $prod_price){ ;?>
	                        				<?php if ($prod_price_discount){?>
    					                       		<span class="price price-old"><?php echo $prod_price .' '.$unit;?></span>
	                          						<span class="price"><?php echo $prod_price_discount.' '.$unit;?></span>
                        					<?php }else{?>
                        							<span class="price"><?php echo $prod_price.' '.$unit;?></span>
                        					<?php }
	                        				
	                        	}else{?>
	                        	
                        					<?php if ($prod_price_discount_lang){?>
    					                       		<span class="price price-old"><?php echo $prod_price_lang.' '.$unit;?></span>
	                          						<span class="price"><?php echo $prod_price_discount_lang.' '.$unit;?></span>
                        					<?php }else{?>
                        							<span class="price"><?php echo $prod_price_lang.' '.$unit;?></span>
                        					<?php }
	                        	
	                        	 }?>
	                       
	                        </div>
	                        <!-- /Price Box -->
	                        
	                        <!-- buttons -->
	                        <div class="btn-group">
	                        	<?php  if($this->ion_auth->logged_in()){?>
	                        	<a class="btn btn-outline btn-base-hover" href="<?php  echo site_url('main/view_cart/'.$man_products[$i]->prod_id);?>"><?php  echo lang('add_to_cart');?></a>
	                          	<?php } ?>
	                          <!-- <p ><?php echo $man_products[$i]->prod_views ;?> review(s)</p>	 -->
    
	                        </div> 
	                        <!-- /buttons -->
	                        
	                      </div>
	                      <!-- /Details -->
	                      
	                    </div>
	                    <!-- /product -->
	                  
	                  </div>
                  <?php } 
                  	} else { ?>

                  	<p class="not_found"><?php echo lang('not_found_men');?></p>
                  <?php }?>

                  <!-- Col -->
                  
                </div>
                <!-- /Row -->
              
              </div>
              <!-- /Tab Latest -->
              
              <!-- Tab Featured -->
              <div class="tab-pane" id="tab-women">
              
                <!-- Row -->
                <div class="row">
                  <?php
                  if($women_products != null){
                  	
                  for ($i=0; $i <sizeof($women_products) ; $i++) { ?>
                      <div class="col-sm-6 col-md-3">
	                    <!-- product -->
	                    <div class="product clearfix">
	                    
	                      <!-- Image -->
	                      <div class="image"> 
	                        <a href="<?php echo base_url();?>index.php/main/product/<?php echo $women_products[$i]->prod_id ;?>" class="main"><img src="<?php echo base_url();?>assets/uploads/products/<?php echo $women_products[$i]->prod_image ;?>" alt=""></a>
	                       
	                      </div>
	                      <!-- Image -->
	                      
	                      <!-- Details -->
	                      <div class="details">
	                      
	                        <a class="title" href="#"><?php echo $women_products[$i]->{"prod_name_{$lang}"} ;?></a>
	                        
	                       
	                       
	                        
	                         <!-- Price Box -->
	                        <div class="price-box">
	                        	<?php
	                        	 $prod_price=$women_products[$i]->prod_price;
								 $prod_price_discount=$women_products[$i]->prod_price_discount;
								 $prod_price_lang=$women_products[$i]->{"prod_price_{$lang}"} ;
								 $prod_price_discount_lang=$women_products[$i]->{"prod_price_discount_{$lang}"} ;
								 
								 ?> 
	                        	<?php  if ( $prod_price){ ;?>
	                        				<?php if ($prod_price_discount){?>
    					                       		<span class="price price-old"><?php echo $prod_price .' '.$unit;?></span>
	                          						<span class="price"><?php echo $prod_price_discount.' '.$unit;?></span>
                        					<?php }else{?>
                        							<span class="price"><?php echo $prod_price.' '.$unit;?></span>
                        					<?php }
	                        				
	                        	}else{?>
	                        	
                        					<?php if ($prod_price_discount_lang){?>
    					                       		<span class="price price-old"><?php echo $prod_price_lang.' '.$unit;?></span>
	                          						<span class="price"><?php echo $prod_price_discount_lang.' '.$unit;?></span>
                        					<?php }else{?>
                        							<span class="price"><?php echo $prod_price_lang.' '.$unit;?></span>
                        					<?php }
	                        	
	                        	 }?>
	                       
	                        </div>
	                        <!-- /Price Box -->
	                        
	                        <!-- buttons -->
	                        
	                        <div class="btn-group">
	                        	<?php  if($this->ion_auth->logged_in()){?>
	                        	<a class="btn btn-outline btn-base-hover" href="<?php  echo site_url('main/view_cart/'.$women_products[$i]->prod_id);?>"><?php  echo lang('add_to_cart');?></a>
	                          	<?php } ?>
	                          	
	                        	
	                          <!-- <p ><?php echo $women_products[$i]->prod_views ;?> review(s)</p>	 -->

	                          
	                        </div> 
	                        <!-- /buttons -->
	                        
	                      </div>
	                      <!-- /Details -->
	                      
	                    </div>
	                    <!-- /product -->
	                  
	                  </div>
                  <?php } 
                  }else{ ?>
        	<p class="not_found"><?php echo lang('not_found_women');?></p>

                  <?php }?>
                  <!-- Col -->
                  
                </div>
                <!-- /Row -->
              
              </div>
              <!-- /Tab Featured -->
            
              <!-- Tab Trending -->
              <div class="tab-pane" id="tab-news">
              
                <!-- Row -->
                <div class="row">
                	
                  <?php
                  if ($news_products != null){
                  for ($i=0; $i <sizeof($news_products) ; $i++) { ?>
                      <div class="col-sm-6 col-md-3">
	                    <!-- product -->
	                    <div class="product clearfix">
	                    
	                      <!-- Image -->
	                      <div class="image"> 
	                        <a href="<?php echo base_url();?>index.php/main/product/<?php echo $news_products[$i]->prod_id ;?>" class="main"><img src="<?php echo base_url();?>assets/uploads/products/<?php echo $news_products[$i]->prod_image ;?>" alt=""></a>
	                       
	                      </div>
	                      <!-- Image -->
	                      
	                      <!-- Details -->
	                      <div class="details">
	                      
	                        <a class="title" href="#"><?php echo $news_products[$i]->{"prod_name_{$lang}"} ;?></a>
	                        
	                       
	                       
	                        
	                        <!-- Price Box -->
	                        <div class="price-box">
	                        	<?php
	                        	 $prod_price=$news_products[$i]->prod_price;
								 $prod_price_discount=$news_products[$i]->prod_price_discount;
								 $prod_price_lang=$news_products[$i]->{"prod_price_{$lang}"} ;
								 $prod_price_discount_lang=$news_products[$i]->{"prod_price_discount_{$lang}"} ;
								 
								 ?> 
	                        	<?php  if ( $prod_price){ ;?>
	                        				<?php if ($prod_price_discount){?>
    					                       		<span class="price price-old"><?php echo $prod_price .' '.$unit;?></span>
	                          						<span class="price"><?php echo $prod_price_discount.' '.$unit;?></span>
                        					<?php }else{?>
                        							<span class="price"><?php echo $prod_price.' '.$unit;?></span>
                        					<?php }
	                        				
	                        	}else{?>
	                        	
                        					<?php if ($prod_price_discount_lang){?>
    					                       		<span class="price price-old"><?php echo $prod_price_lang.' '.$unit;?></span>
	                          						<span class="price"><?php echo $prod_price_discount_lang.' '.$unit;?></span>
                        					<?php }else{?>
                        							<span class="price"><?php echo $prod_price_lang.' '.$unit;?></span>
                        					<?php }
	                        	
	                        	 }?>
	                       
	                        </div>
	                        <!-- /Price Box -->
	                        
	                        <!-- buttons -->
	                        <div class="btn-group">
							
							<?php  if($this->ion_auth->logged_in()){?>
	                        	<a class="btn btn-outline btn-base-hover" href="<?php  echo site_url('main/view_cart/'.$news_products[$i]->prod_id);?>"><?php  echo lang('add_to_cart');?></a>
	                         <?php } ?>
	                          	
				              
	                          <!-- <p ><?php echo $news_products[$i]->prod_views ;?> review(s)</p>	 -->

	                        </div> 
	                        <!-- /buttons -->
	                        
	                      </div>
	                      <!-- /Details -->
	                      
	                    </div>
	                    <!-- /product -->
	                  
	                  </div>
                  <?php }
                  	
                  }else{ ?>

                  	<p class="not_found"><?php echo lang('not_found_news');?></p>
                  <?php } ?>
                  <!-- Col -->
                  
                </div>
                <!-- /Row -->
              
              </div>
              <!-- /Tab Trending -->
            </div>
            <!-- /Tab Panes -->
            
          </div>
          <!-- /Product Tabs -->
          
        </div>
        <!-- /Container -->
        
      </div>
      <!-- /Content Block
      ============================================ -->
      
      <div class="content-block boxed-section cover-3-bg overlay-dark-m" >
      
        <!-- Container -->
        <div class="container cont-lg">
        
          <!-- Promo Text -->
          <div class="promo-text">
            <h5>from 5th March - 26 March</h5>
            <h1 class="boxed">STORY RICHTIME COMPANY</h1>
            <br />
            <!-- <form action="<?php echo site_url('main/shop/1'); ?>"  method="post">
          		 <button class="btn btn-primary btn-borderless" type="submit"  ><?php echo lang('shop_now');?></button>
        	</form>  -->
        	
        	<a class="btn btn-primary btn-borderless"  href="<?php echo $shop_now; ?>" ><?php echo lang('shop_now');?></a>
        		
        	
          </div>
          <!-- Promo Text -->
        
        </div>
        <!-- /Container -->
        
      </div>  
      <!-- /Content Block -->
      	
