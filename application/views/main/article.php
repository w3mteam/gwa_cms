		<div class="content-block">
      
      		  <!-- Container -->
       		 <div class="container no-pad-t">
<?php if ($parent_article): ?>
<h2><?php echo $parent_article->{"title_{$lang}"} ?></h2>
<p class="big grey"><?php echo $parent_article->{"description_{$lang}"} ?></p>
<hr />
<?php endif ?>

<!-- Blog starts -->

<div class="blog">
	<div class="row">
		<div class="col-md-12">

			<!-- Blog Posts -->
			<div class="row">
				<div class="col-md-8 col-sm-8">
					<div class="posts">

						<!-- Each posts should be enclosed inside "entry" class" -->
						<div  class="entry printable">
							<h2 id="entry_printable"><?php echo $article->{"title_{$lang}"} ?></h2>

							<!-- Meta details -->
							<div class="meta">
								<i class="fa fa-calendar"></i><?php echo $article->updated_at ?>
								<span class="pull-right flip avoid-print">
									<?php echo lang('views_count') ?>: <?php echo $article->views ?>
								</span>
							</div>
							
							<?php if ($article->image): ?>
								<!-- Image -->
								<div class="bthumb article-image">
									<a href="#">
										<img src="<?php echo base_url("assets/uploads/images/{$article->image}") ?>" alt="" class="img-responsive"/>
									</a>
								</div>
							<?php endif ?>

							<div class="clearfix"></div>

							<?php if ($article->youtube): ?>
								<!-- Video -->
								<div class="videoWrapper avoid-print article-video">
								    <iframe width="100%" height="349" src="http://www.youtube.com/embed/<?php echo get_youtube_video_id($article->youtube) ?>" frameborder="0" allowfullscreen></iframe>
								</div>
							<?php endif ?>
							
							<div class="clearfix"></div>

							<?php if (0 && $article->thumbnail): ?>
								<!-- Thumbnail -->
								<div class="bthumb2 article-thumbnail">
									<a href="#">
										<img src="<?php echo base_url("assets/uploads/thumbnails/{$article->thumbnail}") ?>" alt="" class="img-responsive"/>
									</a>
								</div>
							<?php endif ?>
							
							<?php if ($article->{"text_{$lang}"}): ?>
								<div id="art_text" class="article-text">
									<p><?php echo $article->{"text_{$lang}"} ?></p>
								</div>
							<?php endif ?>

							<div class="clearfix"></div>

							<div  class="gallery article-gallery" id="gallery">
								<?php foreach ($article_gallery as $item): ?>
									<?php if (strpos(get_mime_by_extension($item->url), 'image') !== false): ?>
										<a href="<?php echo base_url("assets/uploads/galleries/{$item->url}") ?>" class="prettyphoto col-md-4 col-sm-6" rel="lightbox[group]" >
											<img src="<?php echo base_url("assets/uploads/galleries/thumb__{$item->url}") ?>" alt=""/>
											<span><?php echo $item->title ?></span>
										</a>
									<?php elseif(get_youtube_video_id($item->url)): ?>
										<a target="_blank" href="<?php echo $item->url; ?>" class="col-md-4 col-sm-6">
											<img src="<?php echo base_url("assets_site/img/video.png") ?>" alt="" class="no-border"/>
											<span><?php echo $item->title ?></span>
										</a>
									<?php elseif(strpos(get_mime_by_extension($item->url), 'video') !== false): ?>
										<a target="_blank" href="<?php echo base_url("assets/uploads/galleries/{$item->url}") ?>" class="prettyphoto col-md-4 col-sm-6" rel="prettyPhoto[gallery]">
											<img src="<?php echo base_url("assets_site/img/video.png") ?>" alt="" class="no-border"/>
											<span><?php echo $item->title ?></span>
										</a>
									<?php else: ?>
										<a target="_blank" href="<?php echo base_url("assets/uploads/galleries/{$item->url}") ?>" class="col-md-4 col-sm-6">
											<img src="<?php echo base_url("assets_site/img/docs.png") ?>" alt="" class="no-border"/>
											<span><?php echo $item->title ?></span>
										</a>
									<?php endif ?>
								<?php endforeach ?>
							</div>

							<?php  foreach ($article->sub_articles as $item):  ?>
								<!-- Sub Articles -->
								<div class="entry row " id="sub_article">

									<div class="col-sm-4 col-md-4">
										<!-- Thumbnail -->
										<div class="bthumb2 pull-left flip">
											<a href="<?php echo site_url("main/article/{$item->id}") ?>">
												<img src="<?php echo $item->thumbnail?base_url("assets/uploads/thumbnails/{$item->thumbnail}"):base_url("assets/img/no-thumbnail.png") ?>" alt="" class="img-responsive"/>
											</a>
										</div>
									</div>
									<div class="col-sm-8 col-md-8">
										<h5><?php echo $item->{"title_{$lang}"} ?></h5>
										<p><?php echo word_limiter($item->{"description_{$lang}"}, 15) ?></p>
										<div class="button"><a href="<?php echo site_url("main/article/{$item->id}") ?>"><?php echo lang('read_more') ?>...</a></div>
									</div>
									
									<div class="clearfix"></div>
								</div>
							<?php endforeach ?>
							<!--
							<nav class="text-center">
							  <ul class="pagination">
								<?php echo $links ?>
							  </ul>
							</nav>
							-->
						</div>

						<div class="clearfix"></div>

						<!-- <div class="post-foot well">
		
							<div class="social">
								<h6><?php echo lang('sharing') ?>: </h6>
								<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url() ?>"><i class="fa fa-facebook facebook"></i></a>
								<a target="_blank" href="https://twitter.com/home?status=<?php echo current_url() ?>"><i class="fa fa-twitter twitter"></i></a>
								<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo current_url() ?>&title=<?php echo $article->{"title_{$lang}"} ?>&summary=&source="><i class="fa fa-linkedin linkedin"></i></a>
								<a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo current_url() ?>&media=<?php echo current_url() ?>&description="><i class="fa fa-pinterest pinterest"></i></a>
								<a target="_blank" href="https://plus.google.com/share?url=<?php echo current_url() ?>"><i class="fa fa-google-plus google-plus"></i></a>
							</div>
						</div> -->

					</div>
				</div>                        
				<div class="col-md-4 col-sm-4">
					<div id="side_bar" class="sidebar">
						<div class="widget">
							<h4>
								<img src="<?php echo base_url("assets_site/img/icon_2.png") ?>"/>
								<?php echo lang('related_articles') ?>
							</h4>
							<ul>
								<?php foreach ($related_articles as $item): ?>
									<li><?php echo anchor("main/article/{$item->id}", $item->{"title_{$lang}"}); ?></li> 
								<?php endforeach ?>
							</ul>
						</div>
					</div>                                                
				</div>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>
<div class="border"></div>
</div></div>