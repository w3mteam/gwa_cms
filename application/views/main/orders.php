  
<!-- Slideshow Items -->

<div class="container">
	<div class="panel panel-default order_list " id="order_list">
		<div class="panel-heading">
			<h3 class="panel-title">
				<?php if ($this->uri->segment(3) == 2) 
						echo lang('orders_pending');
					 else 
					 	echo lang('orders_history');
				?>
			</h3>
		</div>
		<div class="panel-body">
			<!-- Accordion -->
		  <div class="panel-group" id="accordion">
		  <?php  if($orders) {?>
		  <?php foreach ($orders as $key => $order) : ?>
			  
		  	<?php  //var_dump($order);?>
		    <!-- Panel -->
		    <div class="panel panel-default">
		      <!-- Heading -->
		      <div class="panel-heading">
		        <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $order->order_id?>">
		          <?php echo lang('order_from').' '.$order->first_name.' '.$order->last_name ; ?>
		        </a>
		        </h4>
		      </div>
		      <!-- /Heading -->
		      
		      <!-- Collapse -->
		      <div id="<?php echo $order->order_id?>" class="panel-collapse collapse">
		        <div class="panel-body">
		        	
		     <table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover order_list" id="dataTables-example">
				<thead >
                    <tr >
	                  <th class=" hidden-xs"><?php echo  lang('image');?></th>
	                  <th><?php echo lang('description');?></th>
	                  <th><?php echo lang('qty');?> </th>
	                  <th class="hidden-xs"><?php echo lang('price');?></th>
	                   <th class="hidden-xs"><?php echo lang('date');?></th>
					</tr>
                </thead>
                <tbody>
		    	    <?php  foreach($order->products as $product){  ?>
		    	  	<tr class="gradeX">
		    	  		
		    	    <td><img  class="image hidden-xs" width="150"  height="150" src="<?php  echo base_url().'assets/uploads/products/'.$product->prod_image;?>" /></td>
					<td><div class="title"><?php echo $product->{"prod_name_{$lang}"};?></div></td>
			    	<td >  <?php echo $product->qty;?></td>
			    	<td><?php echo $product->price.' '.$unit; ?></td>
			    	<td> <?php echo $order->updated_at ; ?></td>
			    	
		 			</tr>
		 			<?php  }?>
	 			</tbody>
			</table>
			            <!-- Cart Summary -->
            <table class="cart-summary">
              <tr>
			
                <td class="totals"> 
                  <table class="cart-totals">
                    <tr>
                      <td class="cart-total"><?php echo lang('total');?></td>
                      <td class="cart-total price"><?php echo $order->total.' '.$unit; ?></td>
                    </tr>
                  </table>
                </td>

              </tr>
            </table>  
            <!-- /Cart Summary -->
            
		        <div  class="btn btn-info" > <?php echo lang($order->status); ?></div>	
		        	
		        </div>
		      </div>
		      <!-- /Collapse -->
		      
		    </div>
		    <!-- /Panel -->
		    <?php 	  endforeach;
					}else  echo lang('no_roders');
					?>
		  </div>
		  <!-- /Accordion -->
	  
		</div>
	</div>
</div>
  
  
  
  
  
  
