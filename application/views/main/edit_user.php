		<div class="content-block">
  
  		  <!-- Container -->
   		 <div  class="container no-pad-t">
       	<div class="row">
       		<div class="col-md-2 col-sm-2 col-xs-2"></div>
       		<div id="bg_edit_user" class="col-md-8 col-sm-8 col-xs-8">
       			
		<h1 style="text-align: center"><?php echo lang('edit_user_heading');?></h1>
		<hr />
		<div id="infoMessage"><?php echo $message;?></div>
		
			<?php echo form_open(uri_string());?>
			<?php if ($lang == 'ar'){ ?>
				<table class="edit_user">
					<tbody>
						<tr>
							
							<td><p><?php echo form_input($first_name,'','class="form-control"');?></p></td>
							<td><p> <?php echo lang('edit_user_fname_label', 'first_name');?></p></td>
						</tr>
						<tr>
							
							<td><p><?php echo form_input($last_name,'','class="form-control"');?></p></td>
							<td><p> <?php echo lang('edit_user_lname_label', 'last_name');?></p></td>
						</tr>
						<tr>
							
							<td><p> <?php echo form_input($phone,'','class="form-control"');?></p></td>
							<td><p><?php echo lang('edit_user_phone_label', 'phone');?></p></td>
						</tr>
						
						<tr>
							
							<td><p> <?php echo form_input($town_name,'','class="form-control"');?></p></td>
							<td><p><?php echo lang('edit_user_town_name_label', 'town_name');?></p></td>
						</tr>
						
						<tr>
						
						<td>
							<select  size="1" class="form-control"  name="town_id" >
	                      		<option value="0"><a href=""><?php  echo lang('select_town');?></a></option>
	                            <?php  foreach ($towns as $key => $town) { ?>
	                            	<?php  if($town_id ==  $town->id ){?>
	                            		 <option value="<?php echo $town->id ?>" selected="selected"><?php  echo $town->{"name_{$lang}"}?></option> 
	                            		<?php }else {?>
	                            			 <option value="<?php echo $town->id ?>"><?php  echo $town->{"name_{$lang}"}?></option> 
	                            			
	                               
	                         	<?php   }} ?> 
	                      </select >
	                      </td>

						</tr>
						

                                
                                
						<tr>
							
							<td><p> <?php echo form_input($password,'','class="form-control"');?></p></td>
							<td><p> <?php echo lang('edit_user_password_label', 'password');?></p></td>
						</tr>
						<tr>
							
							<td><p><?php echo form_input($password_confirm,'','class="form-control"');?></p></td>
							<td><p> <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?></p></td>
						</tr>
						<tr>
							<td colspan="2"> 
							      <?php echo form_hidden('id', $user->id);?>
							      <?php echo form_hidden($csrf); ?>
							      <p><?php echo form_submit('submit', lang('edit'),'class="btn btn-primary btn-block btn-default"');?></p>
							</td>
						</tr>
					</tbody>
				</table>
			<?php } else { ?>
				<table class="edit_user">
					<tbody>
						<tr>
							<td><p> <?php echo lang('edit_user_fname_label', 'first_name');?></p></td>
							<td><p><?php echo form_input($first_name,'','class="form-control"');?></p></td>
						</tr>
						<tr>
							<td><p> <?php echo lang('edit_user_lname_label', 'last_name');?></p></td>
							<td><p><?php echo form_input($last_name,'','class="form-control"');?></p></td>
						</tr>
						<tr>
							<td><p><?php echo lang('edit_user_phone_label', 'phone');?></p></td>
							<td><p> <?php echo form_input($phone,'','class="form-control"');?></p></td>
						</tr>
						<tr>
							<td><p> <?php echo lang('edit_user_password_label', 'password');?></p></td>
							<td><p> <?php echo form_input($password,'','class="form-control"');?></p></td>
						</tr>
						<tr>
							<td><p> <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?></p></td>
							<td><p><?php echo form_input($password_confirm,'','class="form-control"');?></p></td>
						</tr>
						<tr>
							<td colspan="2"> 
							      <?php echo form_hidden('id', $user->id);?>
							      <?php echo form_hidden($csrf); ?>
							      <p><?php echo form_submit('submit', lang('edit'),'class="btn btn-primary btn-block btn-default"');?></p>
							</td>
						</tr>
					</tbody>
				</table>
			<?php } ?>
			
			     
			
			<?php echo form_close();?>
		
       		</div>
       		<div class="col-md-2 col-sm-2 col-xs-2"></div>
       	</div>	 	
		
		</div>
	</div>
