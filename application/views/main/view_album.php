			<div class="content-block">
      
      		  <!-- Container -->
       		 <div class="container no-pad-t">

			<?php if ($album->{"alb_title_{$lang}"}): ?>
				<div class="article-text">
					<p class="album_title"><?php echo $album->{"alb_title_{$lang}"} ?></p>
				</div>
			<?php endif ?>
		   
			<div class="clearfix"></div>

			<!-- <div class="gallery article-gallery" id="gallery"> -->

			<div class="gallery article-gallery" >
				<?php foreach ($gallery as $item): ?>
					<?php if (strpos(get_mime_by_extension($item->url), 'image') !== false): ?>
						<div class="gallery">
            				<ul style="list-style-type: none;">
            					<li style="float: left;">
									<a rel="lightbox[group]" href="<?php echo base_url("assets/uploads/album_galleries/{$item->url}") ?>"  >
										<img class="group1 img-responsive img-thumbnail"  style="margin:10px;" src="<?php echo base_url("assets/uploads/album_galleries/thumb__{$item->url}") ?>" alt=""/>
										<span><?php echo $item->title ?></span>
									</a>
								</li>
							</ul>
						</div>
					<?php elseif(get_youtube_video_id($item->url)): ?>
						<a target="_blank" href="<?php echo $item->url; ?>" class="col-md-4 col-sm-6">
							<img src="<?php echo base_url("assets_site/img/video.png") ?>" alt="" class="no-border"/>
							<span><?php echo $item->title ?></span>
						</a>
					<?php elseif(strpos(get_mime_by_extension($item->url), 'video') !== false): ?>
					
						<video controls style="width:640px;height:360px;">
						  <source src="<?php echo base_url("assets/uploads/album_galleries/{$item->url}") ?>" 
						          type='video/webm;codecs="vp8, vorbis"'/>
						  <source src="<?php echo base_url("assets/uploads/album_galleries/{$item->url}") ?>"
						          type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"'/>
						</video>
						

					<?php else: ?>
						<a target="_blank" href="<?php echo base_url("assets/uploads/album_galleries/{$item->url}") ?>" class="col-md-4 col-sm-6">
							<img src="<?php echo base_url("assets_site/img/docs.png") ?>" alt="" class="no-border"/>
							<span><?php echo $item->title ?></span>
						</a>
					<?php endif ?>
				<?php endforeach ?>
			</div>
			</div>  </div>
	