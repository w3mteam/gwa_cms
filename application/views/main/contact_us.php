
      <!-- Content Block
      ============================================-->
      <section  class="content-block default-bg">
        <!-- Container -->
        <div  class="container no-pad-t">

          <!-- Row -->
          <div id="contatc_float" class="row">

            <!-- Main Col -->

            <div id="main-col" class="col-sm-8 col-md-8 mgb-30-xs">
 				<p class="center-block"><?php echo $message; ?></p>
                <h4 class="case-c"><?php echo lang('msg_form');?></h4>

      
                
                <!-- Comment Form -->
                <div class="contact-form">
                  <?php echo form_open('main/contact_us'); ?>
                    <!-- Row -->
                    <div class="row">
                      <!-- Col -->
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" placeholder="<?php echo lang('name_placeholder');?>" name="name" class="form-control">
                        </div>
                      </div>
                      <!-- /Col -->
                      <!-- Col -->
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="email" placeholder="<?php echo lang('email_placeholder');?>" name="email" class="form-control">
                        </div>
                      </div>
                      <!-- /Col -->
                    </div>
                    <!-- /Row -->
                    
                    <div class="form-group">
                     <textarea name="message" placeholder="<?php echo lang('msg_placeholder');?>" class="form-control" rows="8"></textarea>
                    </div>
                   
                   <button class="btn btn-default" type="submit"><?php echo lang('send');?></button>
                  </form>
                </div>
                <!-- /Contact Form -->
                
            </div>
            <!-- /Main Col -->
            
            <!-- Side Col -->
            <div id="contatc_float" class="col-sm-4 col-md-4">

              <!-- Side Widget -->
              <!-- <div id="contatc_float" class="side-widget"> -->
             <div class="side-widget">
              
                <h5 class="boxed-title"><?php echo lang('location');?></h5>
                
                <!-- fader -->
                <div class="fader mgb-20">
                  <img src="<?php echo base_url();?>assets_site/assets_cms/images/map.jpg" alt="">
                  <!-- hidden -->
                  <div class="fader-hidden pcolor-bg white">
                    <!-- stuff to be revealed here ! -->
                    <div class="vcenter align-center">
                      <div class="vcenter-this">
                        <a href="#" data-toggle="modal" data-target="#map-modal"><span class="white"><?php echo lang('view_map'); ?></span></a>
                      </div>
                    </div>
                    <!-- /stuff to be revealed here ! -->
                  </div>
                  <!-- /hidden-->
                </div>
                <!-- /fader -->
                
                <!-- vlinks -->
                <ul id="contatc_float" >
                  <!-- <li><img src="<?php echo base_url();?>assets_site/assets_cms/images/home.PNG" />795 Folsom Ave, Suite 600 Nairobi, Kenya</li> -->
                  <li><img src="<?php echo base_url();?>assets_site/assets_cms/images/email.PNG" /><?php  echo  $contact_us_mail;?></li>
                  <li><img src="<?php echo base_url();?>assets_site/assets_cms/images/phone.PNG" /><?php  echo  $contact_us_phone;?></li>
                </ul>
                <!-- /vlinks -->
                
              </div>
              <!-- /Side Widget -->
              
            </div>
            <!-- /Side Col -->

          </div>
          <!-- /Row -->
        
        </div>
        <!-- /Container -->
    </section>
    <!-- /Content Block
    ============================================-->
    
    <!-- Map Modal -->
    <div id="map-modal" class="modal fade map-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <!-- Modal Dialog -->
      <div class="modal-dialog">
      
        <div class="modal-content">
        
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('our_map_location'); ?></h4>
          </div>
          
          <div class="modal-body">
            <iframe style="width:100%; height: 350px" id="gmap" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Facebook+Inc.,+Earlham+Street,+London,+United+Kingdom&amp;aq=4&amp;oq=facebook&amp;sll=37.0625,-95.677068&amp;sspn=38.963048,56.513672&amp;ie=UTF8&amp;hq=Facebook+Inc.,+Earlham+Street,&amp;hnear=London,+United+Kingdom&amp;t=m&amp;cid=7509567643069030052&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
            <script type="text/javascript">
              // Delay loading iframe for better performance
              var iframe = document.getElementById('gmap');
              var src = iframe.src;
              iframe.src = '';
              window.onload =  function(){ iframe.src = src; }
            </script>
          </div>
        </div>
        
      </div>
      <!-- /Modal Dialog -->
    </div>
    <!-- /Map Modal -->
