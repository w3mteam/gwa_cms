 <section class="intro-block intro-page boxed-section page-bg overlay-dark-m">     
      </section>
      
      <!-- Content Block
      ============================================ -->
      <section class="content-block default-bg">
      
        <!-- Container -->
        <div class="container cont-main no-pad-t">
        
          <!-- Row -->
          <div class="row">

            <!-- Main Col -->
            <div class="main-col col-sm-9 col-md-9">
            
            
              <!-- Row -->
              <div class="product-grid row grid-20 mgb-20">
               <?php
               if($products != null){ 
               for ($i=0; $i <sizeof($products) ; $i++) { ?> 
                    <div class="col-sm-6 col-md-4">
	                  <!-- product -->
	                  <div class="product clearfix">
	                  
	                    <!-- Image -->
	                    <div class="image"> 
	                      <a href="<?php echo base_url();?>index.php/main/product/<?php echo $products[$i]->prod_id;?>" class="main"><img src="<?php echo base_url();?>assets/uploads/products/<?php echo $products[$i]->prod_image; ?>" alt=""></a>
	                      
	                    </div>
	                    <!-- Image -->
	                    <!-- Details -->
	                    <div class="details">
	                    
	                      <a class="title" href="#"><?php echo $products[$i]->{"prod_name_{$lang}"} ;?></a>
	                      
	                      <!-- Price Box -->
                        <div class="price-box">
	                     <?php
	                        	 $prod_price=$products[$i]->prod_price;
								 $prod_price_discount=$products[$i]->prod_price_discount;
								 $prod_price_lang=$products[$i]->{"prod_price_{$lang}"} ;
								 $prod_price_discount_lang=$products[$i]->{"prod_price_discount_{$lang}"} ;
								 
								 ?> 
	                        	<?php  if ( $prod_price){ ;?>
	                        				<?php if ($prod_price_discount){?>
    					                       		<span class="price price-old"><?php echo $prod_price .' '.$unit;?></span>
	                          						<span class="price"><?php echo $prod_price_discount.' '.$unit;?></span>
                        					<?php }else{?>
                        							<span class="price"><?php echo $prod_price.' '.$unit;?></span>
                        					<?php }
	                        				
	                        	}else{?>
	                        	
                        					<?php if ($prod_price_discount_lang){?>
    					                       		<span class="price price-old"><?php echo $prod_price_lang.' '.$unit;?></span>
	                          						<span class="price"><?php echo $prod_price_discount_lang.' '.$unit;?></span>
                        					<?php }else{?>
                        							<span class="price"><?php echo $prod_price_lang.' '.$unit;?></span>
                        					<?php }
	                        	
	                        	 }?>
	                       
	                        </div>
	                        <!-- /Price Box -->
	                      
	                      <!-- buttons -->
	                      <div class="btn-group">
	                      	 <?php  if($this->ion_auth->logged_in()){?>
	                        	<a class="btn btn-outline btn-base-hover" href="<?php  echo site_url('main/view_cart/'.$products[$i]->prod_id);?>"><?php  echo lang('add_to_cart');?></a>
	                       	<?php } ?>
	                         
	                         
	                        <!-- <p ><?php echo $products[$i]->prod_views ;?> review(s)</p>	 -->
	                      
	                      </div> 
	                      <!-- /buttons -->
	                      
	                    </div>
	                    <!-- /Details -->
	                    
	                  </div>
	                  <!-- /product -->
	                
	                </div>
	                <!-- /Col -->
               <?php } 
               } else{ ?>
               	<p class="not_found"><?php echo lang('no_products');?></p>
               <?php }?>
                <!-- Col -->
               
              </div>
              <!-- /Row -->
              
            </div>
            <!-- /Main Col -->
             <!-- Side Widget -->
              <div class="side-col col-sm-3 col-md-3">

                 <h5 class="boxed-title"><?php  echo lang('products_filter');?></h5>
              
					<?php  $this->load->view('template/products_fillter');?>
                    
            <!-- Side Col -->
            

          </div>
          <!-- /Row -->
        
        </div>
        <!-- /Container -->
        
      </section>
      <!-- /Content Block
      ============================================-->

      <!-- Newsletter Block
      ============================================ -->
      <div class="newsletter-block boxed-section overlay-dark-m cover-2-bg">
      
        <!-- Container -->
        <div class="container">
          <form>
            <!-- Row -->
            <div class="row grid-10">
              <!-- Col -->
              <div class="col-sm-3 col-md-2">
                <span class="case-c">get newsletter</span>
              </div>
              <!-- Col -->
              
              <!-- Col -->
              <div class="col-sm-6 col-md-8">
                  <input class="form-control" type="text" placeholder="Enter your email address" />
              </div>
              <!-- Col -->
              
              <!-- Col -->
              <div class="col-sm-3 col-md-2">
                <button class="btn btn-block btn-color yellow-bg"><i class="icon-left fa fa-envelope"></i>subscribe</button>
              </div>
              <!-- /Col -->
              
            </div>
            <!-- /Row -->
          </form>
        </div>
        <!-- /Container -->
      
      </div>
      <!-- /Newsletter Block
      =================================================== -->