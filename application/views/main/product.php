
      <!-- /Page Info Block
      ============================================-->

      <!-- Content Block
      ============================================-->
      <section class="content-block has-sidebar default-bg">
      	
        <!-- Container -->
        <div style="margin-left: 40px; margin-right: 40px;" class="no-pad-t">
			
          <!-- Product Row -->
          <div class="row product-details">
          
            <!-- Col -->
            <div id="prod_details" class="col-md-5 mgb-30-xs">
              
              <!-- Slider Wrapper -->
              <!-- <img class="fillw" id="zoom_01" src="<?php echo base_url();?>assets/uploads/products/<?php echo $product->prod_image; ?>" data-zoom-image="<?php echo base_url();?>assets/uploads/products/large/<?php echo $product->prod_image; ?>"/> -->
		      	<!-- <script>
				    $('#zoom_01').elevateZoom({
					  //  zoomType: "inner",
						//cursor: "crosshair",
						//zoomWindowFadeIn: 500,
						//zoomWindowFadeOut: 750
				   }); 
				</script>  -->
				
<!-- 				  <?php foreach ($product_gallery as $key => $image) { ?>
                 	
						<script>
							
							$('#my_zoom_product_<?php echo $key;?>').elevateZoom(); 
						</script>
				
					 <?php } ?> -->

             <div id="prod_details" class="main-slider">
                <ul class="bxslider" data-call="bxslider" data-options="{pagerCustom:'#thumb-pager', controls:false}">
                
                
                 <?php foreach ($product_gallery as $key => $image) { ?>
          				<!-- 	<li>
          						<img class="fillw" id="zoom_01<?php echo $image->id ;?>" src="<?php  echo base_url();?>assets/uploads/products/galleries/<?php echo $image->url;?>" data-zoom-image="<?php  echo base_url();?>assets/uploads/products/galleries/larg/<?php echo $image->url;?>"/> 
          	           <script>
          	              var primary_key = '<?php echo $image->id ;?>';
          						    $('#zoom_01'+primary_key).elevateZoom({
          							  zoomType: "window",
          								cursor: "crosshair",
          						   }); 
          						</script> 
          	         </li> -->

                      <li>
                      <img class="fillw" id="zoom_01<?php echo $image->id ;?>" src="<?php  echo base_url();?>assets/uploads/products/galleries/<?php echo $image->url;?>" data-zoom-image="<?php  echo base_url();?>assets/uploads/products/galleries/<?php echo $image->url;?>"/> 
                       <script>
                          var primary_key = '<?php echo $image->id ;?>';
                          $('#zoom_01'+primary_key).elevateZoom({
                          //zoomType: "window",
                         // cursor: "crosshair",
                         }); 
                      </script> 
                     </li>

					       <?php }
                 	?>
                 	
                </ul>
              </div>
            
			
              <!-- /Slider Wrapper -->
              
              <!-- Slider Wrapper -->
              
              <div  class="thumb-slider bx-controls-box">
                <ul id="thumb-pager" class="bxslider" data-call="bxslider" data-options="{pager:false, slides:4, slideMargin:10}">
                 	<?php foreach ($product_gallery as $key => $image) { ?>
					<li>
	                    <a  data-slide-index="<?php echo $key;?>" href="#"><img  id="my_zoom_product_thumb"  class="fillw" src="<?php  echo base_url();?>assets/uploads/products/galleries/<?php echo $image->url;?>" alt="" /></a>
	                 </li>
					 <?php }
                 	?>
                </ul>
              </div>
            
              <!-- /Slider Wrapper -->
              
            </div>
            <!-- /Col -->
            
            <!-- Col -->

            <div id="prod_details " class="col-md-7">
            	<!-- <span class="text"><?php echo $product->prod_views ;?> <?php echo lang('reviews') ;?></span> -->
              <h3 class="product-title"><?php echo $product->{"prod_name_{$lang}"} ;?></h3>
              
              <div class="price-box prod_details" >
	        	<?php
	        	 $prod_price=$product->prod_price;
				 $prod_price_discount=$product->prod_price_discount;
				 $prod_price_lang=$product->{"prod_price_{$lang}"} ;
				 $prod_price_discount_lang=$product->{"prod_price_discount_{$lang}"} ;
				 
				 ?> 
            	<?php  if ( $prod_price){ ;?>
            				<?php if ($prod_price_discount){?>
		                       		<span class="price price-old"><?php echo $prod_price .' '.$unit;?></span>
              						<span class="price"><?php echo $prod_price_discount.' '.$unit;?></span>
        					<?php }else{?>
        							<span class="price"><?php echo $prod_price.' '.$unit;?></span>
        					<?php }
            				
            	}else{?>
            	
        					<?php if ($prod_price_discount_lang){?>
		                       		<span class="price price-old"><?php echo $prod_price_lang.' '.$unit;?></span>
              						<span class="price"><?php echo $prod_price_discount_lang.' '.$unit;?></span>
        					<?php }else{?>
        							<span class="price"><?php echo $prod_price_lang.' '.$unit;?></span>
        					<?php }
            	
            	 }?>
	                        	 			 
                <!-- <h4 class="product-price"><?php echo $product->{"prod_price_{$lang}"}.' '.$unit;?></h4> -->
              </div>
              
              <!-- Accordion -->
              <div class="panel-group" id="accordion">
              
                <!-- Panel -->
                <div class="panel panel-default">
                  <!-- Heading -->
                  <div class="panel-heading">
                    <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                      <?php echo lang('product_details');?>
                    </a>
                    </h4>
                  </div>
                  <!-- /Heading -->
                  
                  <!-- Collapse -->
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                    <?php echo  $product->{"prod_description_{$lang}"} ;?>  
                    </div>
                  </div>
                  <!-- /Collapse -->
                  
                </div>
                <!-- /Panel -->
                
                <!-- Panel -->
               
                
                

              </div>
              <!-- /Accordion -->
              
               <a href="<?php  echo site_url('main/view_cart/'.$product->prod_id);?>" class="btn btn-default btn-bigger"><i class="icon-left ti ti-shopping-cart"></i><?php  echo lang('add_to_cart');?></a>
               
			</div>
            <!-- /Col -->
            
          </div>
        </div>
        <!-- /Container -->
      </section>
      <!-- /Content Block
      ============================================-->
      
      <!-- Newsletter Block
      ============================================ -->
      <hr class="y-200pc" />
      <?php
      	increase_product_views($product->prod_id);
      ?>
