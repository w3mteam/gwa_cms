
 <!-- Content Block
      ============================================-->
      <section class="content-block default-bg">
      
        <!-- Container -->
        <div class="container cont-pad-t-sm">
        
          <!-- Cart -->
          <div class="cart"   id="in_cart"> 
          
            <!-- Cart Contents -->
            <table class="cart-contents">
              <thead>
                <tr>
                  <th class="hidden-xs"><?php echo  lang('image');?></th>
                  <th><?php echo lang('description');?></th>
                  <th><?php echo lang('qty');?> </th>
                  <th class="hidden-xs"><?php echo lang('price');?></th>
                  <th class="hidden-xs"><?php echo lang('price_discount');?></th>
                  <th><?php echo lang('total');?></th>
                  <!-- <th><?php echo lang('delete');?></th> -->
                </tr>
              </thead>
              <tbody>
	<form action="<?php  echo site_url('main/view_cart');?>" method="post" id="in_cart">

	
		<?php foreach ($this->cart->contents() as $items): ?>
                <tr>
                  <td class="image hidden-xs"><img src="<?php  echo $items['image']?>" /></td>
                  <td class="details">
                    <div class="clearfix">
                      <div class="pull-left no-float-xs">
                        <a href="<?php  echo site_url('main/product/'.$items['id']);?>" class="title"><?php echo $items['name']?></a>
                      </div>
                      <div class="action pull-right no-float-xs">
                        <div class="clearfix">
                          <!-- <button class="edit"><i class="fa fa-pencil"></i></button>
                          <button class="refresh"><i class="fa fa-refresh"></i></button> -->
                          <a  class="btn btn-danger"  href="<?php echo site_url('main/delete_item_in_cart/'.$items['id']); ?>"> <i class="fa fa-trash-o"> </i>	</a>
                          
                         
                        </div>
                      </div>
                      
                    </div>
                  </td>
                  <td class="qty">
                      <input type="text"  value="<?php echo $items['qty'];?>" name="<?php echo $items['id'];?>">
                  </td>
                  <td class="unit-price hidden-xs"><span class="currency"></span><?php echo $this->cart->format_number($items['price']).' '.$unit; ?></td>
                  <td class="unit-price hidden-xs"><span class="currency"></span><?php echo $this->cart->format_number($items['price_discount']).' '.$unit; ?></td>
                  <td class="total-price"><span class="currency"></span><?php echo $this->cart->format_number($items['subtotal']).' '.$unit; ?></td>
                 
                </tr>
                
          <?php endforeach; ?>
              </tbody>
          
            </table>
            <!-- /Cart Contents -->
            
            <!-- Cart Summary -->
            <table class="cart-summary">
              <tr>

                <td class="totals"> 
                  <table class="cart-totals">
                    <tr>
                      <td class="cart-total"><?php echo lang('total');?></td>
                      <td class="cart-total price"><?php echo $this->cart->format_number($this->cart->total()).' '.$unit; ?></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>  
            <!-- /Cart Summary -->

          </div>
          <!-- /Cart -->
      
          <!-- Cart Buttons -->
          <div class="cart-buttons clearfix"> 
            <a class="btn btn-base checkout" href="<?php echo site_url('main/checkout_cart'); ?>"><i class="icon-left fa fa-shopping-cart"></i><?php echo  lang('checkout_cart'); ?></a>
            <a class="btn btn-primary checkout"  href="<?php echo site_url('main/delete_cart'); ?>"><i class="icon-left fa fa-arrow-left"></i><?php echo  lang('delete_cart'); ?></a>
            <a class="btn btn-primary checkout"  name="update" type="submit"  value="submit"   onclick="update_cart();"><i class="icon-left fa fa-arrow-left"></i><?php echo  lang('update_cart'); ?></a>
          </div>
          <!-- /Cart Buttons -->
          
        </div>
        <!-- /Container -->
        
      </section>
      <!-- /Content Block
      ============================================-->

<script>

// function update_cart () {
  	// data={};
    // var obj =$("#in_cart");
    // obj.find('[type]').each(function(index, value) {
	// // console.log(value);
    // var objj = $(this),
    // name = objj.attr('name'),
    // value = objj.val();
    // if ($(this).attr('type')=='text')
    	// data[index]=[name,value];
// 	
  // }); 
  	// //str=JSON.parse(data);
   	// $('#input_in_cart').val("sdsd");
   	// console.log($('#input_in_cart').val());
  	// // console.log();
  	// alert();
// 
// 
// }
// 
// $('#in_cart').submit(function(){
	// update_cart();
//     
// });

// $('#in_cart').submit(function(){
//   update_cart();
// });
function update_cart () {
 	data={};
    var obj =$("#in_cart");
    obj.find('[type]').each(function(index, value) {
	// console.log(value);
    var objj = $(this),
    name = objj.attr('name'),
    value = objj.val();
    if ($(this).attr('type')=='text')
    	data[index]=[name,value];
	
  }); 
    var formData={'in_cart' : data };
    console.log(formData);
     $.ajax({
    	url:"<?php  echo site_url('main/update_cart'); ?>",
    	method:'POST',
    	data:formData,
    	success:function(data){
    		show_message(data);
    		console.log(data); 
    		//location.replace("<?php echo site_url('main/view_cart');?>");
    	},
    	error :function(xhr,textStatus,errorThrown){
    		alert('request failed: '+ errorThrown);
    	}	
    });
}

	
	function show_message (msg) {
	     $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong><?php  echo lang('success');?></strong>: ",
          message: msg
        }, {type: 'success',mouse_over: "pause"});
	}
</script>