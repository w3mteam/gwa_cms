<div class="container" id="forget_pass_id">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">
				<h1><?php echo lang('forgot_password_heading');?></h1>
			</h3>
		</div>
		<div class="panel-body">
			<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
			<?php echo form_open("main/forgot_password");?>
	
	      <p>
	      	<label for="email"><?php echo sprintf(lang('forgot_password_email_label'), $identity_label);?></label> <br />
	      	
	      	<div id="infoMessage"><?php echo $message;?></div>
	      	<?php echo form_input($email);?>
	      </p>
	
	      <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'),'class="btn btn-default"');?></p>
	
		<?php echo form_close();?>
	
	  
		</div>
	</div>
</div>
