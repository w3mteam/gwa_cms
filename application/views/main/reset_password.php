<div class="container" id="forget_pass_id">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">
				<h1><?php echo lang('reset_password_heading');?></h1>
			</h3>
		</div>
		<div class="panel-body">
			
			<div id="infoMessage"><?php echo $message;?></div>
			
			
			<?php echo form_open('main/reset_password/' . $code);?>
			
				<p>
					<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
					<?php echo form_input($new_password);?>
				</p>
			
				<p>
					<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
					<?php echo form_input($new_password_confirm);?>
				</p>
			
				<?php echo form_input($user_id);?>
				<?php echo form_hidden($csrf); ?>
			
				<p><?php echo form_submit('submit', lang('reset_password_submit_btn'),'class="btn btn-default"');?></p>
			
			<?php echo form_close();?>
	
	  
		</div>
	</div>
</div>

