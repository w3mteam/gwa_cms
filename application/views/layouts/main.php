<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Richtime</title>
	<script>
		var base_url = '<?php echo base_url();?>';
	</script>
    <!-- Bootstrap core CSS
    ================================================== -->
    
        <link href="<?php echo base_url();?>assets_site/assets_cms/uikit/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        
    	<?php if ($lang == 'ar') { ?>
		 <!-- <link href="<?php echo base_url();?>assets_site/css/bootstrap-rtl.min.css" rel="stylesheet"> -->
		<?php }else { ?>
		<link href="<?php echo base_url();?>assets_site/assets_cms/uikit/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<?php } ?>
	

 	<link rel="stylesheet" href="<?php echo base_url();?>assets_site/assets_cms/uikit/css/gallery/prettyPhoto.css">
    <!-- Custom styles for this template
    ================================================== -->
    <link href="<?php echo base_url();?>assets_site/assets_cms/uikit/css/uikit.css" rel="stylesheet">
	<?php if ($lang == 'ar') { ?>
		 <link href="<?php echo base_url();?>assets_site/assets_cms/uikit/css/arabic.css" rel="stylesheet">
	<?php } ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>assets_site/assets_cms/uikit/js/html5shiv.js"></script>
    <script src="<?php echo base_url();?>assets_site/assets_cms/uikit/js/respond.min.js"></script>
    <![endif]-->
    
    
    <!-- from site controlelr   -->
    		<!-- <link href="<?php echo base_url('assets_site') ?>/css/style.css" rel="stylesheet"> -->
    	
    			<!-- Bootstrap CSS -->
		<!-- <link href="<?php echo base_url('assets_site') ?>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url('assets') ?>/css/bootstrap-rtl.min.css" rel="stylesheet"> -->
		<!-- Flex Slider CSS -->

		<!-- <link href="<?php echo base_url('assets_site') ?>/css/style.css" rel="stylesheet"> -->
		
	<link href="<?php echo base_url('assets') ?>/css/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets') ?>/css/prettyCheckable.css" rel="stylesheet">
    <link href="<?php echo base_url('assets') ?>/css/animate.min.css" rel="stylesheet">
    <script src="<?php echo base_url('assets') ?>/js/jquery-2.1.1.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/bootstrap-dialog.js"></script>

    
	<script type="text/javascript" src="<?php echo base_url('assets_site') ?>/js/jquery-1.8.3.min.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url('assets_site') ?>/js/jquery.elevatezoom.js"></script>

    <!-- from site controlelr   -->
	
   <style>

   #town_name{
    display: none;
   }
   	.sidebar {
	    background: #eee url('../img/header-back.png') repeat;
	    border: 1px solid #e1e1e1;
	    border-radius: 8px;
	    padding: 8px;
	    margin-top: 10px;
	}
   	
   	.sidebar .widget {
    background: #fff;
    padding: 8px;
    border-radius: 8px;
    border: 1px solid #e1e1e1;
    margin-bottom: 10px;
}


.blog .entry h2{
font-size:25px;
line-height:30px;
}

.blog .entry h2 a, .blog .entry h2 a:visited, .blog .entry h2 a:hover{
text-decoration:none;
color:#777;
}

.blog .meta{
border-top:1px solid #eee;
border-bottom:1px solid #eee;
margin:8px 0px;
padding:5px 8px;
color:#555;
text-transform:uppercase;
font-size:12px;
background:#eee url('../img/header-back.png') repeat;
}

.blog .meta a, .blog .meta a:visited{
color:#333;
}

.blog .meta i{
margin-left:3px;
}

.blog .bthumb{
margin:10px 0px 5px 0px;
padding:1px;
box-shadow:0px 0px 1px #777;
}

.blog .bthumb2{
float:left;
margin-top:8px;
margin-bottom:8px;
margin-right:8px;
padding:1px;
box-shadow:0px 0px 1px #777;
max-width: 256px;
}

.blog .bthumbleft{
	max-width:180px;
}

.blog .bthumb2 img{
margin:0px;
}

.blog .entry{
margin-bottom:20px;
padding-bottom:5px;
border-bottom:1px solid #eee;
}

#gallery img{
padding:4px;
border:1px solid #ddd;
border-radius:5px;
margin:10px;
width:200px;
max-width:200px;
height:150px;
}


.clearfix{
	clear: both;
}

.prod_details .price.price-old {
    color: rgba(0, 0, 0, 0.4);
    text-decoration: line-through;
   }
   
   .prod_details .price {
    display: inline-block;
    font: 400 15px/1 "Roboto Condensed";
    color: #E7214C;
    margin: 0 5px;
   }
   </style>
    		
  </head>
  <body style="overflow-x: hidden;" class="preload tile-1-bg">
  	


    <!-- Preloader 
    ============================================ -->
    <div class="page-preloader">
      <div class="vcenter"> <div class="vcenter-this"><img class="anim" src="<?php echo base_url();?>assets_site/assets_cms/images/loader.gif" alt="loading..." /></div></div>
    </div>
    <!-- /Preloader 
    ============================================ --> 
    
    <!-- Page Wrapper
    ++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page-wrapper boxed-wrapper shadow">

      <!-- Header Block
      <!-- Header Block
      ============================================== -->
      <header class="header-block line-top">
      
        <!-- Main Header
        ............................................ -->
        <div class="main-header container">
        
          <!-- Header Cols -->
          <div class="header-cols"> 
          
            <!-- Brand Col -->
            <div class="brand-col hidden-xs">
            
              <!-- vcenter -->
              <div class="vcenter">
                <!-- v-centered -->               
                <div class="vcenter-this">
                  <a href="<?php  echo site_url('main');?>">
                    <img src="<?php echo base_url();?>assets_site/assets_cms/images/logo.png" >
                  </a>
                </div>
                <!-- v-centered -->
              </div>
              <!-- vcenter -->

            </div>
            <!-- /Brand Col -->

            <!-- Right Col -->
            <div class="right-col">
            
              <!-- vcenter -->
              <div class="vcenter">
              
                <!-- v-centered -->
                <div class="vcenter-this">

                  <!-- Nav Side -->
                  <nav class="nav-side navbar hnav hnav-sm hnav-borderless" role="navigation">
                  
                    <!-- Dont Collapse -->
                    <div class="navbar-dont-collapse no-toggle">
                    
                      <!-- Nav Right -->
                      <ul class="nav navbar-nav navbar-right case-u active-bcolor navbar-center-xs">
                      	<?php  if (!$this->ion_auth->logged_in()){ ?>
                        <li class="dropdown has-panel">
                          <a aria-expanded="false" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-left ti ti-user"></i><span class="hidden-sm"><?php  echo lang('sign_in');?></span><i class="fa fa-angle-down toggler hidden-xs"></i></a>
                          
                          <!-- Dropdown Panel -->
                           
                          <div class="dropdown-menu dropdown-panel arrow-top dropdown-left-xs" data-keep-open="true">
                            <fieldset>
                                <?php if($this->session->flashdata('login_error')): 
                                      echo  $this->session->flashdata('login_error');
                                 endif; ?>
                            </fieldset>  
                            <fieldset>
                              <form id="sign_in" action="<?php echo site_url('main/login');?>" method="post">
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input class="form-control" placeholder="<?php echo lang('email') ;?>"  name="identity" type="email">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                    <input class="form-control" placeholder="<?php echo lang('password') ;?>" name="password" type="password">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <!-- <label ><input style="margin: 10px;" value="" type="checkbox"><?php echo lang('remember_me');?> </label> -->
                                </div>
                                <button class="btn btn-primary btn-block"><?php echo lang('sign_in') ;?></button>
                              </form>
                            </fieldset>
                            
                            <center>
                            	<a class="form-control"   href="<?php  echo site_url('main/forgot_password');?>"><?php echo lang('login_forgot_password_');?></a>
                            </center>
                           
                          </div>
                          <!-- /Dropdown Panel -->
                          
                        </li>
                        
                          <li class="dropdown has-panel">
                          <a aria-expanded="false" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-left ti ti-pencil-alt"></i><span class="hidden-sm"><?php echo lang('sign_up'); ?></span><i class="fa fa-angle-down toggler hidden-xs"></i></a>
                          
                          <!-- Dropdown Panel -->
                          <div class="dropdown-menu dropdown-panel arrow-top" data-keep-open="true">
                           
                              <form id="sign_up" action="<?php  echo site_url('main/create_user');?>" method="post" >
                                

							<fieldset>
                                 <div class="form-group">
                                  <div class="input-group">
                                    <?php if($this->session->flashdata('register_error')): 
                                      echo  $this->session->flashdata('register_error');
                                   endif; ?>
                                </div>
                              </div>

							</fieldset>
	
							<fieldset>
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input class="form-control" placeholder="<?php echo lang('first_name'); ?>" type="text" name="first_name">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input class="form-control" placeholder="<?php echo lang('last_name'); ?>" type="text" name="last_name">
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input class="form-control" placeholder="<?php echo lang('email'); ?>" type="email" name="email">
                                  </div>
                                </div>
                                 <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-mobile"></i></div>
                                    <input class="form-control" placeholder="<?php echo lang('phone'); ?>" type="text" name="phone">
                                  </div>
                                </div>
                                
                                <!-- town  -->

                      			<div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa "></i></div>
	                               		 <select  size="1" class="form-control"  name="town_id"  id="town_id" >
					                      		<option value="0"><?php  echo lang('select_town');?></option>
					                            <?php  foreach ($towns as $key => $town) { ?>
					                                <option value="<?php echo $town->id ?>"><?php  echo $town->{"name_{$lang}"}?></option> 
					                         	<?php   } ?> 
                                      <option value="0"><?php  echo  lang('other'); ?></option> 
					                      </select >
                                  </div>
                                </div>
                                
                                 <div class="form-group" id="town_name">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa "></i></div>
                                    <input class="form-control" placeholder="<?php echo lang('town_name'); ?>" type="text" name="town_name" >
                                  </div>
                                </div>
                                
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa "></i></div>
                                       <select  size="1" class="form-control"  name="age"  i >
                                        <option value="0"><?php  echo lang('select_age');?></option>
                                          <?php for ($i=10; $i < 100; $i++) { ?> 
                                             <option value="<?php  echo $i; ?>"><?php  echo $i; ?></option> 
                                         <?php } ?>
                                      <!--   <option value="0"><?php  echo  lang('other'); ?></option>  -->
                                    </select >

                                  </div>
                                </div>


                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa "></i></div>
                                     <select  size="1" class="form-control"  name="education_id"  id="education_id" >
                                    <option value="0"><?php  echo lang('select_education');?></option>
                                      <?php  foreach ($educations as $key => $education) { ?>
                                          <option value="<?php echo $education->ed_id ?>"><?php  echo $education->{"ed_name_{$lang}"}?></option> 
                                    <?php   } ?> 
                                      <option value="0"><?php  echo  lang('other'); ?></option> 
                                </select >
                                  </div>
                                </div>

                                <!-- !! town  -->
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                    <input class="form-control" placeholder="<?php echo lang('password'); ?>" type="password" name="password">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                    <input class="form-control" placeholder="<?php echo lang('repet_pass'); ?>" type="password" name="password_confirm">
                                  </div>
                                </div>
                                <!-- <div class="form-group">
                                  <label><input style="margin: 10px;" value="" type="checkbox"><?php echo lang('accept') ;?></label>
                                </div>
                                 -->
                               
                                 
                                  <div class="form-group">
                                  <div class="input-group">
                                     <?php echo $recaptcha_html; ?> 
                                  </div>
                                </div>
                                
                                <button class="btn btn-primary btn-block" type="submit"><?php echo lang('sign_up') ;?></button>
                              </form>
                            </fieldset>
                          </div>
                          <!-- /Dropdown Panel -->
                          
                        </li>
                        
                        <?php  }else{?>
                        <li>
                        	<a aria-expanded="false" href="<?php echo site_url('main/logout');?>" ><i class="icon-left ti ti-user"></i><span class="hidden-sm"><?php  echo lang('logout');?></span>
                        	</a>
                        </li>	                        
                        
                        <?php  } ?>

                      </ul>
                      <!-- /Nav Right-->

                    </div>
                    <!-- /Dont Collapse -->
                    
                  </nav>
                  <!-- /Nav Side -->
                
                </div>
                <!-- /v-centered -->
              </div>
              <!-- /vcenter -->
              
            </div>
            <!-- /Right Col -->
            
            <!-- Left Col -->
            <div class="left-col">
            
              <!-- vcenter -->
              <div class="vcenter">
                
                <!-- v-centered -->               
                <div class="vcenter-this">
                  
                  <form class="header-search" action="<?php  echo site_url('main/shop/3');?>">
                    <div class="form-group">
                      <input class="form-control" placeholder="<?php //echo lang('search');?>"  name="search" type="text">
                      <button class="btn btn-empty"><i class="fa fa-search"></i></button>
                    </div>
                  </form>

                </div>
                <!-- v-centered -->
                
              </div>
              <!-- vcenter -->
            
            </div>
            <!-- /Left Col -->
          </div>
          <!-- Header Cols -->
        
        </div>
        <!-- /Main Header
        .............................................. -->
        
        <!-- Nav Bottom
        .............................................. -->
        <nav class="nav-bottom hnav hnav-ruled white-bg boxed-section">
        
          <!-- Container -->
          <div class="container">
          
            <!-- Header-->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle no-border" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-navicon"></i>
              </button>
              <a class="navbar-brand visible-xs" href="#"><img src="<?php echo base_url();?>assets_site/assets_cms/images/logo.png" ></a>
            </div>
            <!-- /Header-->
          
            <!-- Collapse -->
            <div class="collapse navbar-collapse navbar-absolute" id="my_main_navbar">
            
              <!-- Navbar Center -->
              <ul class="nav navbar-nav navbar-center line-top line-pcolor case-c">
                <li class="active"><a href="<?php echo base_url();?>"><?php echo lang('home');?></a></li>
                
               <?php 
                   foreach ($main_articles as $key => $article) { ?>
               			<?php if ($article->sub_articles): ?>
            							<li>
            								<?php echo anchor("main/article/{$article->id}", $article->{"title_{$lang}"}.'<b class="caret"></b>', 'class="dropdown-toggle" data-toggle="dropdown"'); ?>
            								<ul class="dropdown-menu">
            									<?php foreach ($article->sub_articles as $sub_article): ?>
            										<li><?php echo anchor("main/article/{$sub_article->id}", $sub_article->{"title_{$lang}"}); ?></li> 
            									<?php endforeach ?>
            								</ul>
            			 				</li>
						         <?php else: ?>
								
                           <li><a href="<?php  echo site_url('main/article/'.$article->id);?>"><?php  echo $article->{"title_{$lang}"}?></a></li>
               <?php  endif; } ?>
                

                <?php if ($is_active_menu_video_albume) { ?>

                <li class="dropdown has-panel">
                  <a aria-expanded="false" href="#" class="dropdown-toggle" data-toggle="dropdown">
                  	<span class="hidden-sm"><?php echo lang('video_albums'); ?></span>

                  	<i class="fa fa-angle-down toggler hidden-xs"></i>
                  </a>

                  <!-- Dropdown Panel -->
                  <ul class="dropdown-menu dropdown-panel arrow-top dropdown-left-xs" data-keep-open="true">
                    <!-- <fieldset>
                      <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" size="1" class="form-control" >
                      		<option value=""><a href="">---</a></option>
                            <?php  foreach ($video_albums as $key => $album) { ?>
                                <option value="<?php echo base_url();?>index.php/main/view_album/<?php echo $album->id ?>"><?php  echo $album->{"alb_title_{$lang}"}?></option> 
                         	<?php   } ?>
                           
                      </select >
                    </fieldset> -->
                    <?php  foreach ($video_albums as $key => $album) { ?>                                 	
                    	<li> <a href="<?php echo base_url();?>index.php/main/view_album/<?php echo $album->id ?>"> <?php  echo $album->{"alb_title_{$lang}"}?> </a></li>
                  	<?php   } ?>
                  </ul>
                  <!-- /Dropdown Panel -->

                </li>

                <?php } ?>

                <?php if ($is_active_menu_images_albume) { ?>

                <li class="dropdown has-panel">
                  <a aria-expanded="false" href="#" class="dropdown-toggle" data-toggle="dropdown">
                  	<span class="hidden-sm"><?php echo lang('image_albums'); ?></span>
                  	<i class="fa fa-angle-down toggler hidden-xs"></i>
                  </a>
                  
                  <!-- Dropdown Panel -->
                  <ul class="dropdown-menu dropdown-panel arrow-top dropdown-left-xs" data-keep-open="true">
                  	<?php  foreach ($image_albums as $key => $album) { ?>                                 	
                    	<li> <a href="<?php echo base_url();?>index.php/main/view_album/<?php echo $album->id ?>"> <?php  echo $album->{"alb_title_{$lang}"}?> </a></li>
                  	<?php   } ?>
                  	
                    <!-- <fieldset>
                      <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);" size="1" class="form-control" >
                      		<option value=""><a href="">---</a></option>
                            <?php  foreach ($image_albums as $key => $album) { ?>
                                <option value="<?php echo base_url();?>index.php/main/view_album/<?php echo $album->id ?>"><?php  echo $album->{"alb_title_{$lang}"}?></option> 
                         	<?php   } ?>
                           
                      </select >
                    </fieldset> -->
                  </ul>
                  <!-- /Dropdown Panel -->
                  
                </li>

                 <?php } ?>


                 <?php if ($is_active_menu_products) { ?>

                <!-- <li><a href="headers.html">Culture</a></li> -->
                <li class="dropdown dropdown-mega"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('product');?><i class="fa fa-angle-down toggler"></i></a>
                  <!-- Mega Menu -->
                  <div class="mega-menu dropdown-menu">
                    <!-- Row -->
                    <div class="row">
                    	<div class="col-md-3">
	                       <img src="<?php echo base_url();?>assets_site/assets_cms/images/r.png" />
		                </div>
                     <?php for ($i=0; $i < sizeof($main_categories) ; $i++) { 
                     	  $sub_categories = $main_categories[$i]->sub_categories;
                     	  ?>
                          <div class="col-md-3">
	                        <h5><?php echo $main_categories[$i]->{"Cat_Name_{$lang}"}; ?> </h5>
	                        <?php for ($j=0; $j <sizeof($sub_categories) ; $j++) { ?>
								 <ul class="links">
		                          <li><a href="<?php echo base_url();?>index.php/main/products/<?php echo $sub_categories[$j]->Cat_ID ;?>"><?php echo $sub_categories[$j]->{"Cat_Name_{$lang}"};;?></a></li>
		                        </ul>
							<?php 
							}
	                        ?>
	                      </div>
           				  
				<?php } // enf for ?>
                     <div class="col-md-3">
                        <h5><?php echo lang('news'); ?> </h5>
					 	<ul class="links">
                          <li><a href="<?php echo site_url('main/shop/1');?>"><?php echo lang('latest_products');?></a></li>
                          <li><a href="<?php echo site_url('main/shop/2');?>"><?php echo lang('offer_products');?></a></li>
                        </ul>
	                </div>
                    </div>
                    <!-- /Row -->
                  </div>
                  <!-- /Mega Menu -->
                </li>

                <?php } ?>


                <?php if ($is_active_menu_contact_us){ ?>
                <li><a href="<?php echo base_url();?>index.php/main/contact_us"><?php echo lang('contact');?></a></li>
                <?php } ?>
              </ul>
              <!-- /Navbar Center -->
              
            </div>
            <!-- /Collapse -->
            
            <!-- Dont Collapse -->
            <div class="navbar-dont-collapse">

              <!-- Navbar btn-group -->
              <div class="navbar-btn-group btn-group navbar-right no-margin-r-xs">
              
                            
                <!-- user profile   -->
                
                <?php  if($this->ion_auth->logged_in()){?>
                <!-- Btn Wrapper -->
                <div class="btn-wrapper dropdown">
                
                  <a class="btn btn-outline" data-toggle="dropdown"><i class="ti-user"></i></a>
                  
                  <!-- Dropdown Menu -->
                  <ul class="dropdown-menu">
                    <li><a href="<?php echo site_url('main/edit_user');?>"><?php  echo lang('edit_profile');?></a></li> 
                    <li><a href="<?php echo site_url('main/orders/1');?>"><?php  echo lang('orders_history');?></a></li>
                    <li><a href="<?php echo site_url('main/orders/2');?>"><?php  echo lang('orders_pending');?></a></li>
                  </ul>
                  <!-- /Dropdown Menu -->
                  
                </div>
                <!-- /Btn Wrapper -->
                
                
                <?php } ?>
              <!-------------   filter  ------------>
              
                <!-- Btn Wrapper -->
                <div class="btn-wrapper dropdown">
                
                  <a class="btn btn-outline" data-toggle="dropdown"><i class="ti-search"></i></a>
                  
                  <!-- Dropdown Menu -->
                  <!-- <ul class="dropdown-menu">
                    <li><a href="#">account home</a></li>
                    <li><a href="#">order history</a></li>
                     <li><a href="#">profile</a></li>
                  </ul> -->
                  <div class="dropdown-menu dropdown-panel arrow-top dropdown-left-xs" data-keep-open="true">
                  	<!-- <div class="row"> 
                  		<div class="col-lg-6"> </div>
                  		<div class="col-lg-6"></div>		
                  	</div> -->
                  	
                  	<?php  $this->load->view('template/products_fillter_homepage');?>
                    
                  </div>
                  
                  <!-- /Dropdown Menu -->
                  
                </div>
                <!-- /Btn Wrapper -->
                
                <!-------------   filter  ------------>

		   		<!-------------   cart ------------>

 <?php  if($this->ion_auth->logged_in()){?>		   	
<!-- Btn Wrapper -->
<div class="btn-wrapper dropdown">

  <a aria-expanded="false" class="btn btn-outline" data-toggle="dropdown"><b class="count count-scolor count-round"><?php echo sizeof($this->cart->contents()); ?></b><i class="ti ti-bag"></i></a>
  
    <!-- Dropdown Panel -->
    <div class="dropdown-menu dropdown-panel dropdown-right" data-keep-open="true">
      <section>
        <!-- Mini Cart -->
        <ul class="mini-cart">
        <?php foreach ($this->cart->contents() as $items): ?>
          <!-- Item -->
          <li class="clearfix" id ="in_cat_9">
            <img src="<?php  echo $items['image']?>" alt="">
            <div class="text">
              <a class="title" href="#"><?php echo $items['name']?></a>
              <div class="details"><?php echo $items['qty'];?> x <?php echo $this->cart->format_number($items['price']); ?>
                <div class="btn-group">
                  <!-- <a class="btn btn-primary" href="#"><i class="fa fa-pencil"></i></a> -->
                  <!-- <a class="btn btn-default" href="#"><i class="fa fa-trash"></i></a> -->
                </div>
              </div>
            </div>
          </li>
          <?php  endforeach;?>
          <!-- /Item -->
          
        </ul>
        <!-- /Mini Cart -->
      </section>
      
      <section>
        <div class="row grid-10">
          <div class="col-md-6">
            <a class="btn btn-base btn-block margin-y-5" href="<?php  echo site_url('main/view_cart');?>"><?php echo  lang('view_cart'); ?></a>
          </div>
          <div class="col-md-6">
            <a class="btn btn-primary btn-block margin-y-5" href="<?php  echo site_url('main/checkout_cart');?>"><?php echo  lang('checkout_cart'); ?></a>
          </div>
        </div>
      </section>
    </div>
    <!-- /Dropdown Panel -->
  
</div>
<!-- /Btn Wrapper -->

<?php }else{ ?>
	
	<div aria-expanded="false" class="btn btn-outline"  onclick="show_login_message();"><i class="ti ti-bag"></i></div>
<?php } ?>

              </div>
              <!-- /Navbar btn-group -->
              
              <!-- Navbar Left -->
              <ul class="nav navbar-nav navbar-left navbar-right-xs">
                <li class="dropdown has-panel">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  	<!-- <img class="img-left" alt="" src="<?php echo base_url();?>assets_site/assets_cms/images/flags/us.gif"> -->
                  	<span>  <?php  echo lang($this->session->userdata('lang'));?> </span>
                  	<i class="fa fa-angle-down toggler"></i>
                  </a>
                  <!-- Dropdown Panel -->
                  <ul id="settings" class="dropdown-menu dropdown-panel" data-keep-open="true">
                  	<li><a href="<?php echo site_url('main/lang/en?url='.current_url());?> "> <?php  echo lang ('english');?></a></li>
                  	<li><a href="<?php echo site_url('main/lang/ar?url='.current_url());?> "> <?php  echo lang ('arabic');?></a></li>
                  <!-- 
                    <fieldset>
                     
                        <div class="form-group">
                          <label> <?php echo  lang('language');?></label>
                          <select  class="form-control" id="chang_lang_opt">
                            <option value="en" ><?php  echo lang ('english');?></option>
                            <option value="ar" ><?php  echo lang ('arabic');?></option>
                            <option value="rd" ><?php  echo lang ('rd');?></option>
                          </select >
                        </div>
                        

                        <button class="btn btn-primary btn-block" id="chang_lang_btn" onclick="chang_lang();">change</button>
                     
                    </fieldset> -->
                  </ul>
                  <!-- /Dropdown Panel -->
                  
                </li>
              </ul>
              <!-- /Navbar Left -->
            </div>
            <!-- /Dont Collapse -->

          </div>
          <!-- /Container -->
          
        </nav>
        <!-- /Nav Bottom
        .............................................. -->
        
      </header>
      <!-- /Header Block
      ============================================== -->
    
<?php echo $yield ; ?>
 <!-- Footer
      =================================================== -->
     
     <?php $this->load->view('template/subscripe');?>
            
      <footer class="footer-block">
      
        <!-- Container -->
        <div class="container cont-top clearfix">
        
          <!-- Row -->
          <div class="row">
          
            <!-- Brand -->
            <div class="col-md-3 brand-col brand-center">
              <div class="vcenter">
               <img class="img-responsive" width="350px" height="350px" src="<?php echo base_url();?>/upload/<?php echo $left_advertisement;?>" />
              </div>
            </div>
            <!-- /Brand -->
            
            <!-- Links -->
            <div class="col-md-6 links-col">
            
              <!-- Row -->
              <div class="row-fluid">
              
                <!-- Col -->
                <div id="about_us" class="col-xs-12 col-sm-9 col-md-9">
                  <h5><?php  echo $about_us_title;?></h5>
                  <?php 
                  $words='';
                  $str= explode(' ', $about_us_text);
				  $lenght=20;
				  if (count($str) < 20 )
				  	$lenght=count($str);
				  	
				  for ($i=0; $i < $lenght; $i++) { 
					  $words .=' '.$str[$i];
				  }
				  //sizeof($var, $mode)
                  ?>
                  <p><?php echo $words; if (count($str) > 20 ) { ?>... <a href="<?php echo $about_us_url ; ?>"> <?php echo lang('read_more'); ?></a> <?php } ?></p>
                    <!-- hlinks -->
                    <ul class="hlinks hlinks-icons color-icons-borders color-icons-bg color-icons-hovered">
                      <li><a href="<?php  echo $sm_facebook;?>" target="_blank"><i class="icon fa fa-facebook"></i></a></li>
                      <li><a href="<?php  echo $sm_twitter;?>" target="_blank"><i class="icon fa fa-twitter"></i></a></li>
                      <li><a href="<?php  echo $sm_google_plus;?>" target="_blank"><i class="icon fa fa-google-plus"></i></a></li>
                      <li><a href="<?php  echo $sm_instagram;?>" target="_blank" ><i class="icon fa fa-instagram"></i></a></li>
                      <li><a href="<?php  echo $sm_youtube;?>" target="_blank" ><i class="icon fa fa-youtube"></i></a></li>
                    </ul>
                    <!-- /hlinks -->              
                </div>
                <!-- /Col -->
                
                
                
                <!-- Col -->
                <!-- <div class="col-xs-6 col-sm-3 col-md-3 newsletter">
                 	<img class="img-responsive" width="350px" height="350px" src="<?php echo base_url();?>/upload/<?php echo $right_advertisement;?>" />
                </div> -->
                <!-- /Col -->
                
             </div>
             <!-- /Row -->
             
            </div>
            <!-- /Links -->
            
            <div class="col-md-3 brand-col brand-center">
              <div class="vcenter">
               <img class="img-responsive" width="350px" height="350px" src="<?php echo base_url();?>/upload/<?php echo $right_advertisement;?>" />
              </div>
            </div>
            
          </div>
          <!-- /Row -->
          
        </div>
        <!-- /Container -->
        
        <!-- Bottom -->
        <div class="footer-bottom invert-colors bcolor-bg">
        
          <!-- Container -->
          <div class="container">
          
            <!-- <span class="copy-text">&copy; ALL RIGHTS RESERVED © 2015 RICH TIME</span> -->
             <span class="copy-text"> <?php  echo lang('copyrights');?></span>
            <!-- hlinks -->
            <ul class="hlinks pull-right">
              <li><a href="<?php echo site_url('main/article/'.$about_us->id);?>"><?php  echo $about_us->{"title_{$lang}"};?> </a></li>
              <!-- <li><a href="#">BRODUCTS</a></li> -->
              <li><a href="<?php  echo site_url('main/article/'.$contact_us_id); ?>"><?php echo lang('contact'); ?></a></li>
            </ul>
            <!-- /hlinks -->
            
          </div>
          <!-- /Container -->
          
        </div>
        <!-- /Bottom -->
        
      </footer>
      <!-- /Footer
      =================================================== -->
      <?php
      	if(is_new_visitor()):?>
      		<!-- Promo Modal
		      ============================================ -->      
		      <div id="subscribe_modal" class="modal fade modal-promo" data-call="bs-modal" data-options="{show:true}">
		        <!-- Dialog -->
		        <div class="modal-dialog">
		        
		          <!-- Promo Image (use background image to allow scaling) -->
		          <div class="promo-img bg-cover" style="background:url(<?php echo base_url();?>assets_site/assets_cms/images/modal-promo.jpg)"></div>
		          
		          <!-- Text Col -->
		          <div class="text-col">
		            <div class="text">
		              <h5><?php  echo lang('daily_deals');?></h5>
		              
		              <img src="<?php echo base_url();?>assets_site/assets_cms/images/modal-promo-xs.jpg" alt="" class="visible-xs mgb-20" />
		              
		              <!-- Modal Form -->
		              <!-- <form class="form-inline modal-form"> -->
		              <?php //echo form_open("main/subscribe",array('class'=>'sub-form form-inline modal-form'));?>
		                <div class="form-group">
		                  <input id="myemail_modal" class="form-control" type="text" name="email" placeholder="<?php  echo lang('enter_your_email');?>" />
		                </div>	
		                <a onclick="subscribe_me()"  id="subscribeme" class="btn btn-base"><?php echo lang('subscribe'); ?></a>
		              <!-- </form> -->
		              <!-- /Modal Form -->
		              <p class="hidden-xs" ><?php  echo lang('follow_us');?></p>
		              
		              <!-- hlinks -->
		              <ul class="hlinks hlinks-icons hlinks-icons-round color-icons-bg color-icons-hovered">
		                      <li><a href="<?php  echo $sm_facebook;?>" target="_blank"><i class="icon fa fa-facebook"></i></a></li>
		                      <li><a href="<?php  echo $sm_twitter;?>" target="_blank"><i class="icon fa fa-twitter"></i></a></li>
		                      <li><a href="<?php  echo $sm_google_plus;?>" target="_blank"><i class="icon fa fa-google-plus"></i></a></li>
		                      <li><a href="<?php  echo $sm_instagram;?>" target="_blank"><i class="icon fa fa-instagram"></i></a></li>
		                      <li><a href="<?php  echo $sm_youtube;?>" target="_blank"><i class="icon fa fa-youtube"></i></a></li>
		              </ul>
		              <!-- /hlinks --> 
		            </div>
		          </div>
		          <!-- /Text Col -->
		
		          <button type="button" class="btn-close btn btn-base" data-dismiss="modal"><i class="fa fa-close"></i></button>
		        </div>
		        <!-- /Dialog -->
		      </div>
		      <!-- /Promo Modal
		      ============================================ --> 
      	<?php
		endif;
      ?>
      
      
    </div>
    <!-- /Page Wrapper
    ++++++++++++++++++++++++++++++++++++++++++++++ -->

    <!-- Javascript
    ================================================== -->
    <script src="<?php echo base_url();?>assets_site/assets_cms/uikit/js/jquery-latest.min.js"></script>
    <script src="<?php echo base_url();?>assets_site/assets_cms/uikit/js/uikit.js"></script>
    <!-- /JavaScript
    ================================================== -->
	<script src="<?php echo base_url();?>assets_site/assets_cms/uikit/js/gallery/jquery.prettyPhoto.js"></script>
	<script src="<?php echo base_url();?>assets_site/assets_cms/uikit/js/gallery/main.js"></script>
	 <script>
	    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
	    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
	    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	    s.parentNode.insertBefore(g,s)}(document,'script'));
	
	    // Colorbox Call
	
	    $(document).ready(function(){
	        $("[rel^='lightbox']").prettyPhoto();
	    });
	</script>
  </body>
</html>


<style>
	
	.notification
	{
	    background-color:#006699;
	    min-height:40px;
	    width:30%;
	    margin:0 auto;
	    text-align:center;
	    line-height:50px;
	    color:#fff;
	    font-size:18px;
	    box-shadow: 10px 10px 5px #888888;
	}

	.bs-example{
		margin: 20px;
		display: none;
	}
	
</style>


 <script src="<?php echo base_url('assets') ?>/js/bootstrap-growl.min.js"></script>
 
 <script type="text/javascript">

    $(document).ready(function(){
	
      <?php if($this->session->flashdata('messages_info')): ?>
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong><?php  echo lang('info');?></strong>: ",
          message: "<?php echo $this->session->flashdata('messages_info'); ?>"
        }, {type: "<?php  echo lang('info');?>",mouse_over: "pause"});
      <?php endif ?>

      <?php if($this->session->flashdata('messages_success')): ?>
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong><?php  echo lang('success');?></strong>: ",
          mouse_over: "pause",
          message: "<?php echo $this->session->flashdata('messages_success') ?>"
        }, {type:"<?php  echo lang('success');?>",mouse_over: "pause"});
      <?php endif ?>

      <?php if($this->session->flashdata('messages_warning')): ?>      
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong><?php  echo lang('warning');?></strong>: ",
          message: "<?php echo $this->session->flashdata('messages_warning'); ?>",
        }, {type: "<?php  echo lang('warning');?>",mouse_over: "pause"});
      <?php endif ?>

      <?php if($this->session->flashdata('messages_danger')): ?>
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong><?php  echo lang('danger');?></strong>: ",
          message: "<?php echo $this->session->flashdata('messages_danger') ?>"
        }, {type: "<?php  echo lang('danger');?>" ,mouse_over: "pause"});
      <?php endif ?>
      
    });
    </script>
    
    
<script>
function chang_lang () {
   lang=$('#chang_lang_opt').val();
   location.replace("<?php echo site_url('main/lang'); ?>" + "/"+lang);

}	


	
    $('#town_id').change(function(){
      town_id=$('#town_id').find(":selected").val();
      //alert(town_id);
      if( town_id ==0)
        $('#town_name').show();
  });

	var b_url = '<?php echo base_url();?>';
	var s_url = '<?php echo site_url();?>';
	function subscribe_me() {

	 	
		   	var url = s_url+'/main/subscribe';
		   	data = {};
			var obj = $('#myemail_modal'),
					name = obj.attr('name'),
					value = obj.val();
		
		   	data[name] = value;
		  	//console.log(data);
		  	$.ajax({
		   // see the (*)
			   url: url,
			   type: 'POST',
			   data: data,
			   success: function(response) {
		    		
		    		$('#subscribe_modal .modal-dialog .btn-close').click();
            alert(response);
		    		//$('#subscribe_modal').hide();
		    		//$('#myemail_modal').val("");
		   		}
		  	});
		  	//return false; //disable refresh
		
	}

function show_login_message () {
   $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong><?php  echo lang('info');?></strong>: ",
          message: "<?php echo lang('login_befor_add_to_cart'); ?>"
        }, {type: "<?php  echo lang('info');?>",mouse_over: "pause"});
}
</script>

<?php 
set_visitor();
set_date_statistic();
?>
