<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GWA</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets') ?>/css/bootstrap.min.css" rel="stylesheet">
    <?php if ($lang=='ar'): ?>
    <link href="<?php echo base_url('assets') ?>/css/bootstrap-rtl.min.css" rel="stylesheet">
    <?php endif ?>
    <link href="<?php echo base_url('assets') ?>/css/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url('assets') ?>/css/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets') ?>/css/prettyCheckable.css" rel="stylesheet">
    <link href="<?php echo base_url('assets') ?>/css/animate.min.css" rel="stylesheet">

    <link href="<?php echo base_url('assets') ?>/css/style.dashboard.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url('assets') ?>/js/jquery-2.1.1.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/jquery-ui.min.js"></script>

			<link href="<?php echo base_url('assets_site') ?>/css/font-awesome.min.css" rel="stylesheet">	
		
<style>
	
	.my_header_{
		/*position: relative;*/
		/*position: absolute !important;
		bottom: 100px;*/
		/*position: absolute;*/
	}
	.my_container_{
		/*position: absolute;
		top:100px;*/
		margin-top: 100px;
	}
</style>	
  </head>
	<body>
		<div class="my_header_">
    <?php if ($page != 'login'): ?>
      <nav class="navbar navbar-default navbar-fixed-top" role="navigation">

          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <?=anchor("", "Rich Time", 'class="navbar-brand"'); ?>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-ex1-collapse hidden-sm">
              <ul class="nav navbar-nav">
                  <li <?=($page === 'home')?'class="active"':''?> >
                      <?=anchor("dashboard/home", lang('home')); ?>
                  </li>
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('articles') ?> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <?php foreach ($articles as $item): ?>
                            <!-- <li><?=anchor("dashboard/articles/{$item->id}", $item->{"title_$lang"}); ?></li> -->
                            <li><?=anchor("dashboard/articles/{$item->id}", $item->{"title_{$lang}"}); ?></li>
                        <?php endforeach ?>
                          <li class="divider"></li>
                          <li><?=anchor("dashboard/articles", lang('edit_articles')); ?></li>
                      </ul>
                  </li>
                  <li <?=($page === 'constants')?'class="active"':''?> >
                      <?=anchor("dashboard/constants", lang('constants')); ?>
                  </li>
					
                  <li <?=($this->uri->segment(2) == 'advertisement')?'class="active"':''?> >
                      <?=anchor("dashboard/advertisement", lang('advertisement')); ?>
                  </li>
                  
                  <li <?php if(($this->uri->segment(2) == 'show_all_category') || ($this->uri->segment(2) == 'edit_category') || ($this->uri->segment(2) == 'add_category')  )
                  			{ echo  'class="active"' ; } ?> >
                      <?=anchor("dashboard/show_all_category", lang('categories')); ?>
                  </li>
                   <li <?=($this->uri->segment(2) == 'show_all_products_category') || ($this->uri->segment(2) == 'edit_product') || ($this->uri->segment(2) == 'add_product') || ($this->uri->segment(2) == 'delete_product')?'class="active"':''?> >
                      <?=anchor("dashboard/show_all_products_category", lang('products')); ?>
                  </li>
                   <li <?=($this->uri->segment(2) == 'product_offer')?'class="active"':''?> >
                      <?=anchor("dashboard/product_offers", lang('private_offers')); ?>
                  </li>
                  <li <?=($this->uri->segment(2) == 'show_all_styles') || ($this->uri->segment(2) == 'edit_style') || ($this->uri->segment(2) == 'add_style') || ($this->uri->segment(2) == 'delete_style')?'class="active"':''?> >
                      <?=anchor("dashboard/show_all_styles", lang('styles')); ?>
                  </li>

                   <li <?=($this->uri->segment(2) == 'albums') || ($this->uri->segment(2) == 'show_album') || ($this->uri->segment(2) == 'add_album') ||  ($this->uri->segment(2) == 'edit_album') ||  ($this->uri->segment(2) == 'delete_album')?'class="active"':''?> >
                      <?=anchor("dashboard/albums", lang('albums')); ?>
                  </li>
                   

                  
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('reports') ?> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                          <li><?=anchor("dashboard/reports/1", lang('report_order_count')); ?></li>
                          <li><?=anchor("dashboard/statistics", lang('statistics')); ?></li>

                      </ul>
                  </li>
                  
                  <li <?=($page === 'brands')?'class="active"':''?> >
                      <?=anchor("dashboard/brands/1", lang('brands')); ?>
                  </li>
                  
                  <!-- <li <?=($this->uri->segment(2) == 'orders')?'class="active"':''?> >
                      <?=anchor("dashboard/orders",  lang('orders')); ?>
                  </li>
                   -->

                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('orders') ?> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                          <li><?=anchor("dashboard/orders/1", lang('orders_history')); ?></li>
                          <li><?=anchor("dashboard/orders/2", lang('orders_pending')); ?></li>
                      </ul>
                  </li>
                  
                  
                  <li <?=(($this->uri->segment(2) == 'manage_users')||($this->uri->segment(2) == 'assig_priv')||($this->uri->segment(2) == 'delete_role'))?'class="active"':''?> class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo lang('privileges_usermanagement') ?> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                      	<li>
	                      <?=anchor("dashboard/manage_users", lang('manage_users')); ?>
	                  	</li>
	                  	<li class="divider"></li>
                        <?php 
                        	$priv = get_privileges_functions();
							if(!empty($priv)){
					          	foreach ($priv as $priv_value) {
				          			?><li><?=anchor("dashboard/$priv_value->func/",$priv_value->{"privilage_{$lang}"}); ?></li><li class="divider"></li><?php
								}
							}                  
                         ?>
                      </ul>
                  </li>
              </ul>
              <ul class="nav navbar-nav pull-right <?php echo ($lang=='ar')?'flip':''; ?>">
                <!-- <?php if (isset($article_id) && $article_id): ?>
                  <li>
                      <?=anchor("site/article/$article_id", lang('preview'), 'target="_blank"') ?>
                  </li>
                <?php endif ?> -->
                
                <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 	<span>  <?php  echo lang($this->session->userdata('lang'));?> </span> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                      	<li><a href="<?php echo site_url('dashboard/lang/en');?> "> <?php  echo lang ('english');?></a></li>
                  		<li><a href="<?php echo  site_url('dashboard/lang/ar');?> "> <?php  echo lang ('arabic');?></a></li>
                  		<!-- <li><a href="<?php site_url('main/lang/rd');?> "> <?php  echo lang ('rd');?></a></li> -->
                      </ul>
                  </li>
                  
                  <li>
                      <?=anchor('dashboard/logout', lang('logout')) ?>
                  </li>
              </ul>
          </div><!-- /.navbar-collapse -->
      </nav>
      
      </div>
    <?php endif ?>

	<div class="my_container_">
    	<?php echo $yield ?>
	</div>
    <div align="center">GWA © <?=date('Y');?> <br>

    <!-- <font face="tahoma"> <?php echo lang('language') ?> : </font><a href=<?=site_url('dashboard/lang/en')?> class="link">English</a> | <a href=<?=site_url('dashboard/lang/ar')?> class="link">عربي</a></div> -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets') ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/jasny-bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/prettyCheckable.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/script.dashboard.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/bootstrap-growl.min.js"></script>


 <script type="text/javascript">

    $(document).ready(function(){
	
      <?php if($this->session->flashdata('messages_info')): ?>
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong><?php  echo lang('info');?></strong>: ",
          message: "<?php echo $this->session->flashdata('messages_info'); ?>"
        }, {type: "<?php  echo lang('info');?>",mouse_over: "pause"});
      <?php endif ?>

      <?php if($this->session->flashdata('messages_success')): ?>
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong><?php  echo lang('success');?></strong>: ",
          mouse_over: "pause",
          message: "<?php echo $this->session->flashdata('messages_success') ?>"
        }, {type:"<?php  echo lang('success');?>",mouse_over: "pause"});
      <?php endif ?>

      <?php if($this->session->flashdata('messages_warning')): ?>      
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong><?php  echo lang('warning');?></strong>: ",
          message: "<?php echo $this->session->flashdata('messages_warning'); ?>",
        }, {type: "<?php  echo lang('warning');?>",mouse_over: "pause"});
      <?php endif ?>

      <?php if($this->session->flashdata('messages_danger')): ?>
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong><?php  echo lang('danger');?></strong>: ",
          message: "<?php echo $this->session->flashdata('messages_danger') ?>"
        }, {type: "<?php  echo lang('danger');?>" ,mouse_over: "pause"});
      <?php endif ?>
      
    });
    </script>
    
    
    
    <!-- <script type="text/javascript">
    $(document).ready(function(){

      <?php if($this->session->flashdata('messages_info')): ?>
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong>Info</strong>: ",
          message: "<?php echo $this->session->flashdata('messages_info') ?>"
        }, {type: 'info',mouse_over: "pause"});
      <?php endif ?>

      <?php if($this->session->flashdata('messages_success')): ?>
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong>Success</strong>: ",
          mouse_over: "pause",
          message: "<?php echo $this->session->flashdata('messages_success') ?>"
        }, {type: 'success',mouse_over: "pause"});
      <?php endif ?>

      <?php if($this->session->flashdata('messages_warning')): ?>
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong>Warning</strong>: ",
          message: "<?php echo $this->session->flashdata('messages_warning') ?>"
        }, {type: 'warning',mouse_over: "pause"});
      <?php endif ?>

      <?php if($this->session->flashdata('messages_danger')): ?>
        $.growl({
          icon: "glyphicon glyphicon-star",
          title: " <strong>Danger</strong>: ",
          message: "<?php echo $this->session->flashdata('messages_danger') ?>"
        }, {type: 'danger',mouse_over: "pause"});
      <?php endif ?>
      
    });
    </script> -->
	</body>
</html>

<script>
	// $(function() {
    // $(window).bind('scroll', function() { 
    	// dddd();
     // });
	// });
	
	$(function() {
    $(window).bind('resize', function() { 
    	dddd();
        // if (($('.my_header_').height()  <= 100  ) {                
            // $left.css('marginTop', 100);
        // } else {
            // $left.css('marginTop', 200);
        // }
             });
     });
	


$(document).ready(function(){		
	if ($('.navbar').height()  > 50 )
	   dddd();
});

function dddd () {
	      var left = $('.my_container_');
	      //$left.css('marginTop',  $('.navbar').height()  + 100);  
	      left.css('marginTop',  $('.navbar').height() +50);  
	}
	
</script>