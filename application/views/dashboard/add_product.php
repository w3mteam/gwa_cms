<style>
	.prod_category{
		display: inline;
	}	
	
</style>

<!-- Breadcrumbs -->
<?php if ($breadcrumbs): ?>
<ol class="breadcrumb">
	<li><?php echo anchor('dashboard/show_all_products_category', lang('categories')); ?></li>
	<?php foreach ($breadcrumbs as $item): ?>
		<li><?php echo anchor('dashboard/show_all_products_category/'.$item->Cat_ID, $item->{"Cat_Name_{$lang}"}); ?></li>
		<!-- <li><?php echo anchor('dashboard/articles/'.$item->id, $item->{"title_$lang"}); ?></li> -->
	<?php endforeach ?>
</ol>
<?php endif ?>



<!-- Update Content -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo lang('add_product'); ?></h3>
		</div>
		<div class="panel-body">

<?php
	if (validation_errors()){
			echo validation_errors();
		}

?>
<?php $this->load->helper('ckeditor'); ?>		
<?php echo form_open_multipart('dashboard/add_product/'.$this->uri->segment(3));?>

      <p>
 		<!-- en   -->
 			<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
	         	<?php  echo lang('prod_name_en');?>
	            <?php echo form_input($prod_name_en,'','class="form-control font_input"  ');?>
	            
	      		<?php  echo lang('prod_des_en');?>
	            <?php echo form_textarea($prod_description_en,'','class="form-control font_input" ');?>
	            <?php echo display_ckeditor(array('id' => 'prod_description_en', 'path' => 'assets/ckeditor', 'config' => cke_config(), ))?>
	      		
	      		<?php  echo lang('prod_price_en');?>
	            <?php echo form_input($prod_price_en,'','class="form-control font_input" ');?>
	            
	            <?php  echo lang('prod_price_discount_en');?>
	            <?php echo form_input($prod_price_discount_en,'','class="form-control font_input"  ');?>
            </div>
            <br />
         <!-- ar   -->
         <div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
        	<?php  echo lang('prod_name_ar');?>
            <?php echo form_input($prod_name_ar,'','class="form-control font_input" ');?>
            
            <?php  echo lang('prod_des_ar');?>
            <?php echo form_textarea($prod_description_ar,'','class="form-control font_input" ');?>
            <?php echo display_ckeditor(array('id' => 'prod_description_ar', 'path' => 'assets/ckeditor', 'config' => cke_config(), ))?>
            
            <?php  echo lang('prod_price_ar');?>
            <?php echo form_input($prod_price_ar,'','class="form-control font_input"  ');?>
            
  			<?php  echo lang('prod_price_discount_ar');?>
            <?php echo form_input($prod_price_discount_ar,'','class="form-control font_input"  ');?> 	
         	
         </div>
         <br />

            
        <!-- rd   -->
        <!--  
        <div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
        	<?php  echo lang('prod_name_rd');?>
            <?php echo form_input($prod_name_rd,'','class="form-control font_input" ');?>
            
      	    <?php  echo lang('prod_des_rd');?>
            <?php echo form_input($prod_description_rd,'','class="form-control font_input" ');?>

            <?php  echo lang('prod_price_rd');?>
            <?php echo form_input($prod_price_rd,'','class="form-control font_input"  ');?>
      		
      		<?php  echo lang('prod_price_discount_rd');?>
            <?php echo form_input($prod_price_discount_rd,'','class="form-control font_input"  ');?>
        </div>
		<br />
		-->
      		 <!-- other price  -->
      		<?php  echo lang('prod_price');?>
            <?php echo form_input($prod_price,'','class="form-control font_input"  ');?>
      		<?php  echo lang('prod_price_discount');?>
            <?php echo form_input($prod_price_discount,'','class="form-control font_input"  ');?>
            
      		
      		<?php  echo lang('prod_number');?>
            <?php echo form_input($prod_number,'','class="form-control font_input" ');?>
      		
      		<?php  echo lang('prod_quantity');?>
            <?php echo form_input($prod_quantity,'','class="form-control font_input" ');?>

      		<p class="col-md-offset-2"><?php echo lang('best_size') ?>: 458*458</p>
      		<p><?php echo lang('outer_image')?>:</p>
      		<?php bs3_image('image', false, false, 'image'); ?>
      		
      		<!-- 
      		<div class="row">
      			<div class="col-lg-2">
      				 <label for="input_gender" class="col-sm-2 control-label"><?php echo lang('gender');?>:</label>
      			</div>
      			<div class="col-lg-10">
      				<label for="input_gender" class="col-sm-1 control-label"><?php echo lang('man');?></label> 
	            	<input type="radio" name="gender" data-label="" value="man"  >
	            	<br />
	            		    <label for="input_gender" class="col-sm-1 control-label"><?php echo lang('women');?></label> 
	            	<input type="radio" name="gender" data-label="" value="women"  >
      			</div>
      			
      		</div> -->


			<?php  echo lang('style');?>
      		<select name="style_id" id="style_id" size="1" class="form-control">
      			<option value="0" ><?php  echo lang('select_style'); ?></option>
	 			<?php 
	 			foreach ($styles as $key => $style) { ?>
					 <option value="<?php echo $style->ps_id; ?>"><?php  echo $style->{"ps_title_{$lang}"}; ?></option>
				<?php  }
	 			?>
	 		</select>
	 		

			<!-- <?php  echo lang('category');?>
      		<select name="prod_main_cat_id" id="prod_main_cat_id" size="1" class="form-control">
      			<option value="0" ><?php  echo lang('select_gender'); ?></option>
	 			<?php 
	 			foreach ($main_categories as $key => $cat) { ?>
					 <option value="<?php echo $cat->Cat_ID; ?>"><?php  echo $cat->{"Cat_Name_{$lang}"}; ?></option>
				<?php  }
	 			?>
	 		</select>
	 		
	 		<?php  echo lang('category');?>
	 		<select name="prod_cat_id" id="prod_cat_id" size="1" class="form-control prod_category">

	 		</select> -->
	 		
      </p>

      <p><?php echo form_submit('submit', lang('add'),'class="btn btn-lg btn-default btn-block font_input"');?></p>

<?php echo form_close();?>
              
										
		</div>
	</div>
</div>

<script>
	$('#prod_main_cat_id').change(function(){
		get_sub_categories();
	});

	function get_sub_categories () {
		cat_id=$('#prod_main_cat_id').find(":selected").val();
		 //cat_id=$('#prod_main_cat_id:selected').val();
	      $.ajax({ 
		      type: "POST",
		      url: '<?php echo site_url('ajax_controller/get_sub_categories');?>'+'/'+cat_id,
		      data: {}, 
		      success: function (response) {
		      	cats=JSON.parse(response);
		      	$('#prod_cat_id').html('');
		      	$('#prod_cat_id').append(' <option value="0">'+'<?php echo lang('select_category'); ?>'+'</option>');
		      	for (var i=0; i < cats.length; i++) {
					$('#prod_cat_id').append(' <option value="'+cats[i].Cat_ID+'">'+cats[i].cat_name+'</option>');
				  };
				  $('#prod_cat_id').show();
		      	console.log(response);
		      } ,
		      error:function(e){
		      	console.log('error');
		      }         
		 });
	}

 
</script>

