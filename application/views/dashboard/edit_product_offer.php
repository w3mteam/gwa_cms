
<!-- Breadcrumbs -->
<?php if ($breadcrumbs): ?>
<ol class="breadcrumb">
	<li><?php echo anchor('dashboard/show_all_category', lang('categories')); ?></li>
	<?php foreach ($breadcrumbs as $item): ?>
		<li><?php echo anchor('dashboard/show_all_category/'.$item->Cat_ID, $item->{"Cat_Name_{$lang}"}); ?></li>
	<?php endforeach ?>
	<li><?php echo anchor('dashboard/edit_product/'.$product->prod_id, $product->{"prod_name_{$lang}"}); ?></li>
</ol>
<?php endif ?>


<!-- Update Content -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize($page) ?></h3>
		</div>
		<div class="panel-body">
		<?php
			if (validation_errors()){
					echo validation_errors();
				}

		?>
		
<?php echo form_open_multipart(current_url());?>


        <?php  bs3_input ($price['name'],$price['value']);?>

         <?php  bs3_input ($price_discount['name'],$price_discount['value']);?>

         <?php  bs3_input ($url['name'],$url['value']);?>
      	
      	<?php bs3_date($start_date['name'],$start_date['value']); ?>
      	<?php bs3_date($end_date['name'],$end_date['value']); ?>

      	<?php bs3_image('image',$pof_image?'product_offers/'.$pof_image:false, base_url().'assets/img/no_image.png', 'image'); ?>



      <p><?php echo form_submit('submit', lang('edit'),'class="btn btn-lg btn-default btn-block font_input"');?></p>

<?php echo form_close();?>
										
		</div>
	</div>
</div>


