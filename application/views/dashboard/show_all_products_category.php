    <!-- <script src="<?php echo base_url('assets') ?>/js/bootstrap.min.js"></script> -->
	<script src="<?php echo base_url('assets') ?>/js/bootstrap-dialog.js"></script>



<!-- Breadcrumbs -->
<?php if ($breadcrumbs): ?>
<ol class="breadcrumb">
	<li><?php echo anchor('dashboard/show_all_products_category', lang('categories')); ?></li>
	<?php foreach ($breadcrumbs as $item): ?>
		<li><?php echo anchor('dashboard/show_all_products_category/'.$item->Cat_ID, $item->{"Cat_Name_{$lang}"}); ?></li>
	<?php endforeach ?>
</ol>
<?php endif ?>


<!-- Update Content -->

<div class="container">
	
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo lang('update_products_price') ?></h3>
		</div>
		<div class="panel-body">
		<?php echo form_open_multipart('dashboard/update_products_price'); ?>
		
		<div class="row">
			<div class="col-lg-3">
				<input type="file" name="excel"  />
			</div>
			<div class="col-lg-4">
				<p><?php echo form_submit('submit', lang('update'),'class="btn btn-md btn-default font_input"');?></p>
			</div>
			
		</div>

		<?php  if ( sizeof($breadcrumbs) == $category_levels ){?>
			<a  class="btn btn-success" href="<?php echo site_url('dashboard/add_product/'.$this->uri->segment(3)); ?>"> <?php echo lang('add_product'); ?></a>
    	<?php }?>  
		 
		<?php echo form_close();?>
										
		</div>
	</div>
	
	
	<!--  -->

	<?php  if ( sizeof($breadcrumbs) <= $category_levels - 1 ){?>
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo lang('categories') ?></h3>
		</div>
		<div class="panel-body">
            	  <?php if(!$categories){?>
            		<?php echo '<p>'.lang('no_categories').'</p>';?>
            <?php }else{?>	
				<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
                            <tr>
								<th><?php echo lang('cat_name');?></th>

								<th><?php echo lang('show_sub_categories');?></th>
							</tr>
                        </thead>
                        <tbody>
				    	    <?php  foreach($categories as $cat){?>
				    	  	<tr class="gradeX">
				    	  		
							<td> <?php 	echo $cat->{"Cat_Name_{$lang}"} ; ?></td>
					    	<td><?php echo anchor("dashboard/show_all_products_category/$cat->Cat_ID",'<img src="'.base_url().'assets/img/items.png"  />','');?></td>
					    	
					    	</tr>
					    	<?php  }}?>
				 			  
			 			</tbody>
 				</table>

										
		</div>
	</div>
	<?php } ?>

					    	
	
	<?php  if ( sizeof($breadcrumbs) == $category_levels ){?>
	<!-- product panel -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo $page ?></h3>
			</div>
			<div class="panel-body">	
		       	<?php if(!$products){?>
		        		<?php echo lang('no_products') ;?>
		        <?php }else{?>	
					<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
		                        <tr>
										<th><?php echo lang('product_name');?></th>
										<th><?php echo lang('created_at');?></th>
										<th><?php echo lang('updated_at');?></th>
										<th><?php echo lang('private_offers');?></th>
										<th><?php echo lang('edit');?></th>
										<th><?php echo lang('delete');?></th>
								</tr>
		                    </thead>
		                    <tbody>
					    	    <?php  foreach($products as $prod){?>
					    	  	<tr class="gradeX">
					    	  		
					    	    <td> <?php 	echo $prod->{"prod_name_{$lang}"} ; ?></td>
					    	    <td> <?php 	echo $prod->created_at ; ?></td>
					    	    <td> <?php 	echo $prod->updated_at ; ?></td>
					    	 	<td><?php echo anchor("dashboard/add_product_offer/".$this->uri->segment(3).'/'.$prod->prod_id,lang('add_private_offer'));?></td>
								<td><?php echo anchor("dashboard/edit_product/$prod->prod_id?category=".$this->uri->segment(3),'<img src="'.base_url().'assets/img/c_edit.png"  />','');?> </td>
						    	<td onclick="delete_prod(<?php  echo $prod->prod_id;?>);" ><img src="<?php echo base_url();?>assets/img/c_drop.png"  /></td>				    	
						    	<?php  }}?>
					 			  
				 			</tbody>
					</table>

											
			</div>
		</div>

		<?php } ?>
	
</div>

<script>
	function delete_prod (id) {
	  BootstrapDialog.show({
            title: "<?php echo lang('delete_confirmation') ?>",
            message: "<?php echo lang('delete_confirmation_text') ?>",
            buttons: [{
                label:" <?php echo lang('delete') ?>",
                cssClass: 'btn-danger',
                action: function(dialog) {
                   location.replace("<?php  echo site_url('dashboard/delete_product');?>" +'/'+id+'/'+<?php  echo $this->uri->segment(3);?>)
                }
            }, {
                label:"  <?php echo lang('close') ?>",
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });
	}
	
</script>

