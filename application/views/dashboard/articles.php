<!-- Breadcrumbs -->
<?php if ($article): ?>
<ol class="breadcrumb">
	<li><?php echo anchor('dashboard/articles', lang('articles')); ?></li>
	<?php foreach ($breadcrumbs as $item): ?>
		<li><?php echo anchor('dashboard/articles/'.$item->id, $item->{"title_{$lang}"}); ?></li>
		<!-- <li><?php echo anchor('dashboard/articles/'.$item->id, $item->{"title_$lang"}); ?></li> -->
	<?php endforeach ?>
</ol>
<?php endif ?>

<!-- Sub Items -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo lang('sub_articles') ?></h3>
		</div>
		<div class="panel-body">

			<?php  if ( sizeof($breadcrumbs) <= $article_levels - 1){?>
			<div class="form-group col-md-2">
				<button type="button" class="btn btn-success" data-type="toggle" data-for="#create_form" data-default="hide"><?php echo lang('add_new_item') ?></button>
			</div>
			<?php } ?>

			<div class="form-group col-md-8"></div>
			<div class="form-group col-md-2"></div>

			<div class="clearfix"></div>

			<?php echo form_open('', 'class="form-horizontal" role="form" id="create_form"'); ?>

			<?php bs3_hidden('create', true) ?>

			<?php  bs3_input('title_en', false, 'required="true"') ?>
			<?php bs3_input('title_ar', false, 'required="true"') ?>
			<!-- <?php bs3_input('title_rd', false, 'required="true"') ?> -->

			<?php bs3_submit() ?>

			<?php echo form_close() ?>

			<!-- Read -->
			
			<?php if ($sub_articles): ?>
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th><?php echo lang('sort') ?></th>
								<th><?php echo lang('title_en') ?></th>
								<th><?php echo lang('title_ar') ?></th>
								<!-- <th><?php echo lang('title_rd') ?></th> -->
								<th><?php echo lang('updated_at') ?></th>
								<th><?php echo lang('details') ?></th>
								<th><?php echo lang('delete') ?></th>
							</tr>
						</thead>
						<tbody class="sortable" data-url="<?php echo current_url() ?>">
							<?php foreach ($sub_articles as $item): ?>
								<tr data-id="<?php echo $item->id ?>"  id="item-<?php echo $item->id ?>">
									<td class="handle" alt="move"></td>
									<td data-key="title_en"><?php echo $item->title_en ?></td>
									<td data-key="title_ar"><?php echo $item->title_ar ?></td>
									<!-- <td data-key="title_rd"><?php echo $item->title_rd ?></td> -->
									<td data-key="updated_at"><?php echo $item->updated_at ?></td>
									<td data-href="<?php echo site_url('dashboard/articles/'.$item->id) ?>" class="details"></td>
									<td data-toggle="modal" data-target="#delete-modal"></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>

				<!-- Delete -->

				<div class="modal fade" id="delete-modal">
					<div class="modal-dialog">
						<div class="modal-content">
							<?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title"><?php echo lang('delete_confirmation') ?></h4>
							</div>
							<div class="modal-body">
								<?php bs3_hidden('delete', true) ?>

								<?php bs3_hidden('item_id') ?>

								<?php echo lang('delete_confirmation_text') ?>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('close') ?></button>
								<button type="submit" class="btn btn-danger"><?php echo lang('delete') ?>!</button>
							</div>
							<?php echo form_close() ?>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			<?php endif ?>

		</div>
	</div>
</div>

<!-- Update Content -->
<?php if ($article): ?>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo ucfirst($item->{"title_{$lang}"}) ?></h3>
		</div>
		<div class="panel-body">

			<?php echo form_open_multipart('', 'class="form-horizontal" role="form" id="update_form"'); ?>

			<?php bs3_hidden('update', true) ?>

			<?php bs3_hidden('item_id', $article->id) ?>

			<?php  bs3_input('title_en', $article->title_en, 'required="true"') ?>
			<?php bs3_input('title_ar', $article->title_ar, 'required="true"') ?>
			<!-- <?php  bs3_input('title_rd', $article->title_rd) ?> -->

			<?php bs3_input('description_en', $article->description_en) ?>
			<?php bs3_input('description_ar', $article->description_ar) ?>
			<!-- <?php  bs3_input('description_rd', $article->description_rd) ?> -->

			<?php bs3_image('thumbnail', $article->thumbnail?'thumbnails/'.$article->thumbnail:false, base_url().'assets/img/no_image.png', 'update_icon'); ?>

			<?php bs3_image('image', $article->image?'images/'.$article->image:false, base_url().'assets/img/no_image.png', 'image'); ?>

			<?php bs3_input('youtube', $article->youtube) ?>

			<?php $this->load->helper('ckeditor'); ?>

			<?php  bs3_textarea('text_en', $article->text_en, 'rows="10"') ?>
            <?php echo display_ckeditor(array('id' => 'inputText_en', 'path' => 'assets/ckeditor', 'config' => cke_config(), ))?>
            <?php bs3_textarea('text_ar', $article->text_ar, 'rows="10"') ?>
            <?php echo display_ckeditor(array('id' => 'inputText_ar', 'path' => 'assets/ckeditor', 'config' => cke_config(), ))?>
            

			<?php bs3_submit() ?>

			<?php echo form_close(); ?>

		</div>
	</div>
</div>

<!-- Galleries -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo ucfirst($item->{"title_{$lang}"}) ?> <?php echo lang('gallery') ?></h3>
		</div>
		<div class="panel-body">
		
	    <div>

		    <?php if(isset($multimedia_crud['css_files'])):
				foreach($multimedia_crud['css_files'] as $file): ?>
					<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
				<?php endforeach; ?>
			<?php endif; ?>
			<?php if(isset($multimedia_crud['js_files'])): ?>
				<?php foreach($multimedia_crud['js_files'] as $file): ?>
					<script src="<?php echo $file; ?>"></script>
				<?php endforeach; ?>
			<?php endif; ?>

			<?php echo $multimedia_crud['output']; 
			?>
	    </div>

		</div>
	</div>
</div>

<?php endif ?>
