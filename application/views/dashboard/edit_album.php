
<!-- Update Content -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize(lang('edit_album')) ?></h3>
		</div>
		<div class="panel-body">
		
		<?php echo form_open('dashboard/edit_album/'.$album->id);?>

      <p>
         	<?php  echo lang('alb_title_en');?>
            <?php echo form_input($alb_title_en,'','class="form-control font_input" ');?>
            <?php  echo lang('alb_title_ar');?>
            <?php echo form_input($alb_title_ar,'','class="form-control font_input"  ');?>
            <!-- <?php  echo lang('alb_title_rd');?>
            <?php echo form_input($alb_title_rd,'','class="form-control font_input"  ');?> -->
            
            <!-- <?php echo lang('type');?>
      		<select name="type" size="1" class="form-control">
	 			<option value="image" <?php if($album->alb_type=="image"){ echo  'selected="selected"';}?>><?php echo  lang('image'); ?></option>
	 			<option value="video" <?php if($album->alb_type=="video"){ echo  'selected="selected"';}?>><?php echo  lang('video'); ?></option>
	 		</select> -->
      </p>


      <p><?php echo form_submit('submit', lang('edit'),'class="btn btn-lg btn-default btn-block font_input"');?></p>

<?php echo form_close();?>
    
										
		</div>
	</div>
</div>

<!-- galleries -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo lang('gallery') ?></h3>
		</div>
		<div class="panel-body">
		
	    <div>

		    <?php if(isset($multimedia_crud['css_files'])):
				foreach($multimedia_crud['css_files'] as $file): ?>
					<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
				<?php endforeach; ?>
			<?php endif; ?>
			<?php if(isset($multimedia_crud['js_files'])): ?>
				<?php foreach($multimedia_crud['js_files'] as $file): ?>
					<script src="<?php echo $file; ?>"></script>
				<?php endforeach; ?>
			<?php endif; ?>

			<?php echo $multimedia_crud['output']; 
			?>
	    </div>

		</div>
	</div>
</div>

<?php  if ($album->alb_type=="image"){ ?>
	<script>
		$('#file_url').attr('disabled','disabled');
		$('#save_file_url').attr('disabled','disabled');
	</script>
<?php }else{ ?>
	<script>
		$('#fine-uploader').attr('disabled','disabled');
	</script>
<?php }?>

