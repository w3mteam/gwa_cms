<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>assets/img/edit_user.png"  /><?php echo lang('edit_user_profile_header') ?></p>
            </div>
		<div class="panel-body">
			<p><?php echo lang('edit_user_subheading');?></p>
			
			<div id="infoMessage"><?php echo $message;?></div>
			
			<?php echo form_open(uri_string());?>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <p>
						            <?php echo lang('edit_user_fname_label', 'first_name');?> <br />
						            <?php echo form_input($first_name);?>
						      </p>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <p>
						            <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
						            <?php echo form_input($last_name);?>
						      </p>
						</div>
					</div>
					<div class="row">
						<!-- <div class="col-md-6 col-sm-6 col-xs-12">
							<p>
						            <?php echo lang('edit_user_company_label', 'company');?> <br />
						            <?php echo form_input($company);?>
						      </p>
						</div> -->
						<div class="col-md-6 col-sm-6 col-xs-12">
							<p>
					            <?php echo lang('edit_user_phone_label', 'phone');?> <br />
					            <?php echo form_input($phone);?>
					      </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<p>
						            <?php echo lang('edit_user_password_label', 'password');?> <br />
						            <?php echo form_input($password);?>
						      </p>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<p>
					            <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
					            <?php echo form_input($password_confirm);?>
					      </p>
						</div>
					</div>
			      <?php if ($this->ion_auth->is_admin()): ?>
			
			          <h3><?php echo lang('edit_user_groups_heading');?></h3>
			          <?php foreach ($groups as $group):?>
			              <?php
			                  $gID=$group['id'];
			                  $checked = '';
			                  $item = '';
			                  foreach($currentGroups as $grp) {
			                      if ($gID == $grp->id) {
			                          $checked= ' checked="checked"';
			                      break;
			                      }
			                  }
			              ?>
			              
			              <div class="form-group">
				            <label for="input" class="col-sm-2 control-label"><?php echo htmlspecialchars($group["name_{$lang}"],ENT_QUOTES,'UTF-8');?>:</label>
				            <div >
				                 <input type="radio" name="groups[]" value="<?php echo $group['id'];?>" <?php echo $checked;?>  data-label="" >
				            </div>
				        </div>
				        <br>

			          <?php endforeach?>
			
			      <?php endif ?>
			
			      <?php echo form_hidden('id', $user->id);?>
			      <?php echo form_hidden($csrf); ?>
			
			      <p><?php echo form_submit('submit', lang('edit_user_submit_btn'),'class="btn btn-primary btn-leg btn-block"');?></p>
			
			<?php echo form_close();?>
	
        </div>
	</div>
</div>

