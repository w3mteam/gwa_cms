

<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p><img src="<?php echo base_url(); ?>assets/img/c_add.png"  /><?=lang('create_user_heading');?></p>
        </div>
		<div class="panel-body">
        	<h1><?php echo lang('create_user_heading');?></h1>
			<p><?php echo lang('create_user_subheading');?></p>
			
			<div id="infoMessage"><?php echo $message;?></div>
			
			<?php echo form_open("dashboard/create_user");?>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<p>
						            <?php echo lang('create_user_fname_label', 'first_name');?> <br />
						            <?php echo form_input($first_name);?>
						      </p>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<p>
						            <?php echo lang('create_user_lname_label', 'last_name');?> <br />
						            <?php echo form_input($last_name);?>
						      </p>
						</div>
						<!-- <div class="col-md-4 col-sm-4 col-xs-12">
							 <p>
						            <?php echo lang('create_user_company_label', 'company');?> <br />
						            <?php echo form_input($company);?>
						      </p>
						</div> -->
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
				      		<p>
						            <?php echo lang('create_user_email_label', 'email');?> <br />
						            <?php echo form_input($email);?>
						      </p>
			      		</div>
			      		<div class="col-md-6 col-sm-6 col-xs-12">
				      		<p>
					            <?php echo lang('create_user_phone_label', 'phone');?> <br />
					            <?php echo form_input($phone);?>
					      </p>
			      		</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-xs-6 col-xs-12">
							<p>
					            <?php echo lang('create_user_password_label', 'password');?> <br />
					            <?php echo form_input($password);?>
					      </p>
						</div>
						<div class="col-md-6 col-xs-6 col-xs-12">
							 <p>
						            <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
						            <?php echo form_input($password_confirm);?>
						      </p>
						</div>
					</div>
			      	<div class="row">
			      		<div class="col-md-12 col-xs-12 col-xs-12">
			      		 	<p><?php echo form_submit('submit', lang('create_user_submit_btn'),'class="btn btn-primary btn-block"');?></p>
			      		 </div>
			      	</div>
			     
			
			<?php echo form_close();?>

       </div>
	</div>
</div>


