<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>assets/img/show_user.png"  />عرض المستخدمين</p>
            </div>
		<div class="panel-body">
            <h1><?php echo lang('deactivate_heading');?></h1>
			<p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>
			
			<?php echo form_open("dashboard/deactivate/".$user->id);?>
			
			  <p>
			  	<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
			    <input type="radio" name="confirm" value="yes" checked="checked"  data-label="" />
			    <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
			    <input type="radio" name="confirm" value="no"  data-label="" />
			  </p>
			
			  <?php echo form_hidden($csrf); ?>
			  <?php echo form_hidden(array('id'=>$user->id)); ?>
			
			  <p><?php echo form_submit('submit', lang('deactivate_submit_btn'),'class="btn btn-primary btn-block"');?></p>
			
			<?php echo form_close();?>
	
        </div>
	</div>
</div>


