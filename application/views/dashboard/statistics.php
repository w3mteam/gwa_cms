<script src="<?php  echo base_url();?>assets/js/highcharts.js"></script>
<script src="<?php  echo base_url();?>assets/js/exporting.js"></script>

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize(lang('statistics')) ?></h3>
		</div>
		<div class="panel-body">
				<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
                            <tr>
								<th><?php echo lang('stat_content');?></th>
								<th><?php echo lang('stat_number');?></th>
							</tr>
                        </thead>
                        <tbody>
                        	
				    	   
				    	  	<tr class="gradeX">
					    	    <td> <?php 	echo lang('articles');?></td>
					    	    <td> <?php 	echo $articles;?></td>
				 			</tr>
				 			
				 			<tr>
					 			<td> <?php 	echo lang('categories');?></td>
					    	    <td> <?php 	echo $categories;?></td>
				 			</tr>
				 			
				 			<tr>
								<td> <?php 	echo lang('products');?></td>
					    	    <td> <?php 	echo $products;?></td>
				 			</tr>
				 			
				 			<tr>
								<td> <?php 	echo lang('users');?></td>
					    	    <td> <?php 	echo $users;?></td>
				 			</tr>
			 			</tbody>
 				</table>

										
		</div>
	</div>
</div>

<!-- visite_rate -->
 
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize(lang('visite_rate')) ?></h3>
		</div>
		<div class="panel-body">
			<div id="users_rate" style="min-width: 310px; height: 400px; margin: 0 auto"></div
		</div>
	</div>
</div>
<script>
	var visiteRate_temp=<?php echo json_encode($visiting_rate); ?>;

	var visiteRate=[];
	for (var i=0; i < visiteRate_temp.length; i++) {
		months=[];
		
		var array = $.map(visiteRate_temp[i][1], function(value, index) {
		    return [value];
		});
		
	  for (var j=0; j <array.length; j++) {
		 months.push(array[j]);
	  };
	  visiteRate.push({"name":visiteRate_temp[i][0],"data":months})
	};
	
	console.log(visiteRate)
	// var visiteRate=[{
            // name: 'Tokyo',
            // data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        // }, {
            // name: 'New York',
            // data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
        // }, {
            // name: 'Berlin',
            // data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
        // }, {
            // name: 'London',
            // data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        // }];
	$(function () {
    $('#users_rate').highcharts({
        title: {
            text: '<?php echo humanize(lang('visite_rate')) ?>',
            x: -20 //center
        },
        // subtitle: {
            // text: 'Source: WorldClimate.com',
            // x: -20
        // },
        xAxis: {
            categories: ['01', '02', '03', '04', '05', '06',
                '07', '08', '09', '10', '11', '12']
        },
        yAxis: {
            title: {
                text: '<?php echo lang('stat_count');?>'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        // tooltip: {
            // valueSuffix: '°C'
        // },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: visiteRate
    });
});
	
</script>

	<!-- /visite_rate -->
	

 	<!-- browsers -->
 
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize(lang('browsers')) ?></h3>
		</div>
		<div class="panel-body">
			<div id="browsers" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
		</div>
	</div>
</div>
<script>
	var browsers_temp=<?php echo json_encode($browser_statistics); ?>;
	var browsers= [];
	for (var i=0; i < browsers_temp.length; i++) {
	 browsers.push([browsers_temp[i].browser,parseInt(browsers_temp[i].count)]);  
	};
	$(function () {
    $('#browsers').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '<?php echo humanize(lang('browsers')) ?>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '<?php echo lang('stat_count');?>'
            }
        },
        legend: {
            enabled: false
        },
        // tooltip: {
            // pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>'
        // },
        series: [{
            name: '<?php echo humanize(lang('stat_count')) ?>',
            data:browsers,
            dataLabels: {
                enabled: true,
                //rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
	
</script>

	<!-- /browsers -->
	
 	<!-- countries -->
 
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize(lang('countries')) ?></h3>
		</div>
		<div class="panel-body">
			<div id="countries" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
		</div>
	</div>
</div>

<script>
	var countries_temp=<?php echo json_encode($country_statistics); ?>;
	var countries= [];
	for (var i=0; i < countries_temp.length; i++) {
	 countries.push([countries_temp[i].Name,parseInt(countries_temp[i].Count)]);  
	};
	
	console.log(countries);
	$(function () {
    $('#countries').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '<?php echo humanize(lang('countries')) ?>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '<?php echo lang('stat_count');?>'
            }
        },
        legend: {
            enabled: false
        },
        // tooltip: {
            // pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>'
        // },
        series: [{
            name: '<?php echo humanize(lang('stat_count')) ?>',
            data:countries,
            dataLabels: {
                enabled: true,
                //rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
	
</script>

	<!-- /countries -->
	
	<!-- products -->
 
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize(lang('products')) ?></h3>
		</div>
		<div class="panel-body">
			<div id="products" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
		</div>
	</div>
</div>

<?php  
$products=array();
 foreach ($most_product_views as $key => $product) {
     array_push($products,array($product->{"prod_name_{$lang}"} ,$product->prod_views));
 }
?>
<script>
	var products_temp=<?php echo json_encode($products); ?>;
	var products= [];
	for (var i=0; i < products_temp.length; i++) {
	 products.push([products_temp[i][0],parseInt(products_temp[i][1])]);  
	};
	
	console.log(products);
	$(function () {
    $('#products').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '<?php echo humanize(lang('products')) ?>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '<?php echo lang('stat_count');?>'
            }
        },
        legend: {
            enabled: false
        },
        // tooltip: {
            // pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>'
        // },
        series: [{
            name: '<?php echo humanize(lang('stat_count')) ?>',
            data:products,
            dataLabels: {
                enabled: true,
                //rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
	
</script>

	<!-- /products -->
	

	<!-- articlees -->
 
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize(lang('articles')) ?></h3>
		</div>
		<div class="panel-body">
			<div id="articles" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
		</div>
	</div>
</div>

<?php  
$articles=array();
 foreach ($most_article_views as $key => $article) {
     array_push($articles,array($article->{"title_{$lang}"} ,$article->views));
 }
?>
<script>
	var articles_temp=<?php echo json_encode($articles); ?>;
	var articles= [];
	for (var i=0; i < articles_temp.length; i++) {
	 articles.push([articles_temp[i][0],parseInt(articles_temp[i][1])]);  
	};
	
	console.log(articles);
	$(function () {
    $('#articles').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '<?php echo humanize(lang('articles')) ?>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '<?php echo lang('stat_count');?>'
            }
        },
        legend: {
            enabled: false
        },
        // tooltip: {
            // pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>'
        // },
        series: [{
            name: '<?php echo humanize(lang('stat_count')) ?>',
            data:articles,
            dataLabels: {
                enabled: true,
                //rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
	
</script>

	<!-- /articlees -->
	


	<!-- categories -->
 
 <script>

function draw_category_chart (categories_temp,id) {
	var categories= [];
	for (var i=0; i < categories_temp.length; i++) {
	 categories.push([categories_temp[i][0],parseInt(categories_temp[i][1])]);  
	};
	
	console.log(categories);
	$(function () {
    $('#categories_'+id).highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '<?php echo humanize(lang('categories')) ?>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '<?php echo lang('stat_count');?>'
            }
        },
        legend: {
            enabled: false
        },
        // tooltip: {
            // pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>'
        // },
        series: [{
            name: '<?php echo humanize(lang('stat_count')) ?>',
            data:categories,
            dataLabels: {
                enabled: true,
                //rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
});
	}
</script>

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize(lang('categories')) ?></h3>
		</div>

		<div class="panel-body">
				<?php  
					$categories=array();
					 foreach ($most_category_views as $key => $category) {?>
					 	<h2><?php  echo $category->{"Cat_Name_{$lang}"};?></h2>
					 <?php   
					 	$categories=array();
					 	foreach ($category->sub_categories as $key => $sub_cat) {
							  array_push($categories,array($sub_cat->{"Cat_Name_{$lang}"} ,$sub_cat->views));
						 }
						?>
						<div id="categories_<?php echo $category->Cat_ID; ?>" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
						<hr />
						<script> draw_category_chart(<?php echo json_encode($categories)?>,<?php echo $category->Cat_ID; ?>);</script>
						<?php
					 }
				?>
		
			
		</div>
	</div>
</div>


	<!-- /categories -->
	
