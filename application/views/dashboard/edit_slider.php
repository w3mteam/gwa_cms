<style>

</style>
<!-- Update Content -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize(lang('edit_slider')) ?></h3>
		</div>
		<div class="panel-body">

<?php
	if (validation_errors()){
			echo validation_errors();
		}
	if(isset($error)){
		echo $error;
	}

?>
	
<?php echo form_open_multipart('dashboard/edit_slider/'.$this->uri->segment(3));?>

         <!-- en   -->
 		<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
	         	<?php  echo lang('title_en');?>
	            <?php echo form_input($title_en,'','class="form-control font_input"  ');?>
	            
	      		<?php  echo lang('text_en');?>
	            <?php echo form_input($text_en,'','class="form-control font_input" ');?>
		</div>
		<br />
         <!-- ar   -->
         <div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
        	<?php  echo lang('title_ar');?>
            <?php echo form_input($title_ar,'','class="form-control font_input" ');?>
            
            <?php  echo lang('text_ar');?>
            <?php echo form_input($text_ar,'','class="form-control font_input" ');?>	
        	
         </div>
         <br />

          <?php  echo lang('url');?>
          <?php echo form_input($url,'','class="form-control font_input" ');?>

        <!-- rd   -->
        <!--  
        <div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
               <div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
        	<?php  echo lang('title_rd');?>
            <?php echo form_input($title_rd,'','class="form-control font_input" ');?>
            
            <?php  echo lang('text_rd');?>
            <?php echo form_input($text_rd,'','class="form-control font_input" ');?>	
        	
         </div>
         <br />
		-->
      	
      	<?php bs3_image('image',$slider->image?'slideshow/'.$slider->image:false, base_url().'assets/img/no_image.png', 'image'); ?>


      <p><?php echo form_submit('submit', lang('edit'),'class="btn btn-lg btn-default btn-block font_input"');?></p>

<?php echo form_close();?>
              
										
		</div>
	</div>
</div>