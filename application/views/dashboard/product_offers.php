
<!-- Slideshow Items -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo lang('private_offers') ?></h3>
		</div>
		<div class="panel-body">
			<div class="form-group col-md-8"></div>
			<div class="form-group col-md-2"></div>

			<div class="clearfix"></div>

			<!-- Read -->
			
			<?php if ($offers): ?>
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th><?php echo lang('sort') ?></th>
								<th><?php echo lang('image') ?></th>
								<th><?php echo lang('product') ?></th>
								<th><?php echo lang('price') ?></th>
								<th><?php echo lang('price_discount') ?></th>
								<th><?php echo lang('url') ?></th>
								<th><?php echo lang('start_date') ?></th>
								<th><?php echo lang('end_date') ?></th>
								<th><?php echo lang('created_at') ?></th>
								<th><?php echo lang('updated_at') ?></th>
								<th><?php echo lang('edit') ?></th>
								<th><?php echo lang('delete') ?></th>
							</tr>
						</thead>
						<tbody class="sortable" data-url="product_offers">
							<?php foreach ($offers as $item): ?>
								<tr data-id="<?php echo $item->pof_id ?>"  id="item-<?php echo $item->pof_id ?>">
									<td class="handle" alt="move"></td>
									<td data-key="image">
										<a class="thumbnail" style="display: inline-block;margin-bottom: 0px;">
											<img src="<?php echo base_url("assets/uploads/product_offers/{$item->pof_image}") ?>" class="img-responsive" alt="">
										</a>
									</td>
									<td data-key="image"><?php echo anchor("dashboard/edit_product/$item->prod_id?category=".$item->prod_cat_id,'<img class="img-responsive"  src="'.base_url().'assets/uploads/products/'.$item->prod_image.' "/>','style="display: inline-block;margin-bottom: 0px;" class="thumbnail"');?> </td>
									<td data-key="price"><?php echo $item->pof_price ?></td>
									<td data-key="price_discount"><?php echo $item->pof_price_discount ?></td>
									<td data-key="pof_url"><?php echo $item->pof_url ?></td>
									<td data-key="pof_start_date"><?php echo $item->pof_start_date ?></td>
									<td data-key="pof_end_date"><?php echo $item->pof_end_date ?></td>

									<td data-key="pof_end_date"><?php echo $item->created_at ?></td>
									<td data-key="pof_end_date"><?php echo $item->updated_at ?></td>
									<td data-key="updated_at"><a class="btn btn-default" href="<?php echo site_url('dashboard/edit_product_offer/'.$item->pof_id); ?>"> <?php echo lang('edit'); ?></a> </td>
									<td data-toggle="modal" data-target="#delete-modal"></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>

				<!-- Delete -->

				<div class="modal fade" id="delete-modal">
					<div class="modal-dialog">
						<div class="modal-content">
							<?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title"><?php echo lang('delete_confirmation') ?></h4>
							</div>
							<div class="modal-body">
								<?php bs3_hidden('delete', true) ?>

								<?php bs3_hidden('item_id') ?>

								<?php echo lang('delete_confirmation_text') ?>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('close') ?></button>
								<button type="submit" class="btn btn-danger"><?php echo lang('delete') ?>!</button>
							</div>
							<?php echo form_close() ?>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			<?php endif ?>

		</div>
	</div>
</div>


<!-- Update Content -->
