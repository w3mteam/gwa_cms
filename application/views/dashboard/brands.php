
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"> <?php echo lang('brands') ?></h3>
		</div>
		<div class="panel-body">
			
		<p ><?php echo lang('best_size') ?>: 250*154</p>

	    <div>
			
		    <?php if(isset($multimedia_crud['css_files'])):
				foreach($multimedia_crud['css_files'] as $file): ?>
					<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
				<?php endforeach; ?>
			<?php endif; ?>
			<?php if(isset($multimedia_crud['js_files'])): ?>
				<?php foreach($multimedia_crud['js_files'] as $file): ?>
					<script src="<?php echo $file; ?>"></script>
				<?php endforeach; ?>
			<?php endif; ?>

			<?php echo $multimedia_crud['output']; 
			?>
	    </div>

		</div>
	</div>
</div>


	<!-- <script>
		$('#file_url').attr('disabled','disabled');
		$('#save_file_url').attr('disabled','disabled');
	</script> -->
