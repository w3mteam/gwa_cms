
<!-- Update Content -->
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize($page) ?></h3>
		</div>
		<div class="panel-body">

			<?php echo form_open_multipart('', 'class="form-horizontal" role="form" id="update_form"'); ?>

			<?php bs3_hidden('update', true) ?>
			<!-- <div style="padding: 15px;">
				<?php bs3_input('title', $title) ?>
				<?php bs3_input('description', $description) ?>
				<?php bs3_textarea('keywords', $keywords) ?>
				<?php bs3_input('author', $author) ?>
			</div>
			<hr /> -->
			<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
				<?php bs3_input('contact_email_from', $contact_email_from) ?>
				<?php bs3_input('contact_email_to', $contact_email_to) ?>
				<?php bs3_input('contact_us_mail', $contact_us_mail) ?>
				<?php bs3_input('contact_us_phone', $contact_us_phone) ?>
			</div>
			<br />
			
		<!-- 	<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
				<?php bs3_input('shop_now', $shop_now) ?>
			</div> -->
			<br />
			
			<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
				<?php bs3_input('brands_header_en', $brands_header_en) ?>
				<?php bs3_input('brands_header_ar', $brands_header_ar) ?>
				<!-- <?php bs3_input('brands_header_rd', $brands_header_rd) ?> -->
			</div>
			<br />
			<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
				<?php bs3_input('unit_', $unit_) ?>
				<?php bs3_input('unit_en', $unit_en) ?>
				<?php bs3_input('unit_ar', $unit_ar) ?>
				<!-- <?php bs3_input('unit_rd', $unit_rd) ?> -->
			</div>
			<br />
			
			<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
				<?php bs3_input('min_price', $min_price) ?>
				<?php bs3_input('min_price_en', $min_price_en) ?>
				<?php bs3_input('min_price_ar', $min_price_ar) ?>
				<!-- <?php bs3_input('min_price_rd', $min_price_rd) ?> -->
				<?php bs3_input('max_price', $max_price) ?>
				<?php bs3_input('max_price_en', $max_price_en) ?>
				<?php bs3_input('max_price_ar', $max_price_ar) ?>
				<!-- <?php bs3_input('max_price_rd', $max_price_rd) ?> -->
			</div>
			<br />
			
			
			<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
				<?php bs3_input('order_email_from', $order_email_from) ?>
				<?php bs3_input('order_email_to', $order_email_to) ?>
			</div>
			<br />



			<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
				<?php bs3_input('category_levels', $category_levels) ?>
				<?php bs3_input('article_levels', $article_levels) ?>
			</div>
			<br />
			
			
			<div style="padding: 15px;">
				<?php bs3_input('sm_facebook', $sm_facebook) ?>
				<?php bs3_input('sm_twitter', $sm_twitter) ?>
				<?php bs3_input('sm_youtube', $sm_youtube) ?>
				<?php bs3_input('sm_google_plus', $sm_google_plus) ?>
				<?php bs3_input('sm_instagram', $sm_instagram) ?>
			</div>

			<?php bs3_submit() ?>

			<?php echo form_close(); ?>

		</div>
	</div>
</div>