<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>assets/img/show_user.png"  /><?php echo lang('index_heading');?></p>
            </div>
		<div class="panel-body">
            	<h1><?php echo lang('index_heading');?></h1>
				<p><?php echo lang('index_subheading');?></p>
				<a class="btn btn-primary" href="<?php  echo site_url('dashboard/export_users_to_excel');?>"><?php echo lang('export_users_to_excel');?></a>
				<div id="infoMessage"> <p><?php echo $message;?></p></div>
				
              <div class="table-responsive">
                    <table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
                            <tr>
								<th><?php echo lang('index_fname_th');?></th>
								<th><?php echo lang('index_lname_th');?></th>
								<th><?php echo lang('index_email_th');?></th>
								<th><?php echo lang('index_groups_th');?></th>
								<th><?php echo lang('index_status_th');?></th>
								<th><?php echo lang('index_action_th');?></th>
							</tr>
                        </thead>
						
						<?php foreach ($users as $user):?>
						<tbody>
							<tr class="gradeX">
								<td><?php echo $user->first_name;?></td>
								<td><?php echo $user->last_name;?></td>
								<td><?php echo $user->email;?></td>
								<td>
									<?php foreach ($user->groups as $group):?>
										<?php echo anchor("dashboard/edit_group/".$group->id, htmlspecialchars($group->{"name_{$lang}"},ENT_QUOTES,'UTF-8')) ;?><br />
					                <?php endforeach?>
								</td>
								<td><?php echo ($user->active) ? anchor("dashboard/deactivate/".$user->id, lang('index_active_link')) : anchor("dashboard/activate/". $user->id, lang('index_inactive_link'));?></td>
								<td><?php echo anchor("dashboard/edit_user/".$user->id, lang('edit')) ;?></td>
							</tr>
						</tbody>
						<?php endforeach;?>
					</table>
				<p><div class="col-md-6 col-sm-6 col-xs-12"><?php echo anchor('dashboard/create_user', lang('index_create_user_link'),'class="btn btn-primary btn-block"')?></div>
					<div class="col-md-6 col-sm-6 col-xs-12"><?php echo anchor('dashboard/create_group', lang('index_create_group_link'),'class="btn btn-primary btn-block"')?></div>
					  
					</p>
                <!-- On rows -->
                </div>
            </div>
	</div>
</div>