
	
<!-- Breadcrumbs -->
<?php if ($breadcrumbs): ?>
<ol class="breadcrumb">
	<li><?php echo anchor('dashboard/show_all_category', lang('categories')); ?></li>
	<?php foreach ($breadcrumbs as $item): ?>
		<li><?php echo anchor('dashboard/show_all_category/'.$item->Cat_ID, $item->{"Cat_Name_{$lang}"}); ?></li>
		<!-- <li><?php echo anchor('dashboard/articles/'.$item->id, $item->{"title_$lang"}); ?></li> -->
	<?php endforeach ?>
</ol>
<?php endif ?>

	
<!-- Update Content -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo $page ?></h3>
		</div>
		<div class="panel-body">
				<!--  remove this condtion to enable unlimited  sub category  -->
				
				<a  class="btn btn-success" href="<?php echo site_url('dashboard/add_category/'.$this->uri->segment(3)); ?>"> <?php echo lang('add_category'); ?></a>
				
				
            	 <?php if(!$categories){?>
            		<?php echo '<p>'.lang('no_categories').'</p>';?>
            	<?php }else{?>	
				<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
                            <tr>
								<th><?php echo lang('cat_name');?></th>
								<th><?php echo lang('created_at');?></th>
								<th><?php echo lang('updated_at');?></th>
								<th><?php echo lang('edit');?></th>
								<th><?php echo lang('delete');?></th>
								<?php  if ( sizeof($breadcrumbs) <= $category_levels - 2){?>
								<th><?php echo lang('show_sub_categories');?></th>
								<?php } ?>
							</tr>
                        </thead>
                        <tbody>
				    	    <?php  foreach($categories as $cat){?>
				    	  	<tr class="gradeX">
				    	  		
				    	    <td> <?php 	echo $cat->{"Cat_Name_{$lang}"} ; ?></td>
				    	    <td> <?php 	echo $cat->created_at ; ?></td>
				    	    <td> <?php 	echo $cat->updated_at ; ?></td>
							<td><?php echo anchor("dashboard/edit_category/$cat->Cat_ID",'<img src="'.base_url().'assets/img/c_edit.png"  />','');?> </td>
							<!-- <td><?php echo anchor("dashboard/delete_category/$cat->Cat_ID",'<img src="'.base_url().'assets/img/c_drop.png"  />','confirm("are you sure to delete");');?></td> -->
					    	<td onclick="delete_cat(<?php  echo $cat->Cat_ID;?>);" ><img src="<?php echo base_url();?>assets/img/c_drop.png"  /></td>
					    	<?php  if ( sizeof($breadcrumbs) <= $category_levels - 2){?>
					    	<td><?php echo anchor("dashboard/show_all_category/$cat->Cat_ID",'<img src="'.base_url().'assets/img/items.png"  />','');?></td>
					    	<?php } ?>
					    	<?php  }}?>
				 			</tr>
			 			</tbody>
 				</table>

										
		</div>
	</div>
</div>


<!-- <script src="<?php echo base_url('assets') ?>/js/bootstrap.min.js"></script> -->
<script src="<?php echo base_url('assets') ?>/js/bootstrap-dialog.js"></script>
<script>
	function delete_cat (id) {
	  BootstrapDialog.show({
            title: "<?php echo lang('delete_confirmation') ?>",
            message: "<?php echo lang('delete_confirmation_text') ?>",
            buttons: [{
                label:" <?php echo lang('delete') ?>",
                cssClass: 'btn-danger',
                action: function(dialog) {
                   location.replace("<?php  echo site_url('dashboard/delete_category');?>" +'/'+id)
                }
            }, {
                label:"  <?php echo lang('close') ?>",
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });
	}
	
</script>

