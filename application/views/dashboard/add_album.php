
<!-- Update Content -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize(lang('add_album')) ?></h3>
		</div>
		<div class="panel-body">
		
		<?php echo form_open('dashboard/add_album/'.$this->uri->segment(3));?>

      <p>
         	<?php  echo lang('alb_title_en');?>
            <?php echo form_input($alb_title_en,'','class="form-control font_input" ');?>
            <?php  echo lang('alb_title_ar');?>
            <?php echo form_input($alb_title_ar,'','class="form-control font_input"  ');?>
            <!-- <?php  echo lang('alb_title_rd');?>
            <?php echo form_input($alb_title_rd,'','class="form-control font_input"  ');?> -->
            <!-- <?php echo lang('type');?>
      		<select name="type" size="1" class="form-control">
	 			<option value="image"><?php echo  lang('image'); ?></option>
	 			<option value="video"><?php echo  lang('video'); ?></option>
	 		</select> -->
      </p>


      <p><?php echo form_submit('submit', lang('add'),'class="btn btn-lg btn-default btn-block font_input"');?></p>

<?php echo form_close();?>
    
										
		</div>
	</div>
</div>


