<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>assets/img/delete_role.png" /><?php  echo lang('delete_role');?></p>
		</div>
		<div class="panel-body">
			<div class="row">
				
				<div class="col-md-12 col-sm-12 col-xs-12">
						<?php if($mode == "no_priv"){
							echo "<p>$message</p>";
						}else {
							
							?>
						<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
			                    <tr>
									<th><p> <?php  echo lang('current_role');?></p></th>
								</tr>
			                </thead>
			                <tbody id="items_c_r">
			                	<?php
									foreach ($roles as $value) {
										echo'<tr><td><a style="float:left;padding-right:25px;" href="'.base_url().'index.php/dashboard/remove_role/'.$value->id.'"><img src="'.base_url().'assets/img/c_drop.png" /></a>'.$value->{"name_{$lang}"}.'</td></tr>';
									}
								?>
								
			                </tbody>
						</table>
						<?php }?>
				</div>
			</div>
			
		</div>
	</div>
</div>




