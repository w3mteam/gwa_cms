
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">
				<?php if ($this->uri->segment(3) == 1) 
						echo lang('report_order_count');
				?>
			</h3>
		</div>
		<div class="panel-body">
			<!-- Accordion -->
		  <div class="panel-group" id="accordion">
		  <?php  if($products) {?>
		  <?php foreach ($products as $key => $product) : ?>
			  

		  	
		    <!-- Panel -->
		    <div class="panel panel-default">
		      <!-- Heading -->
		      <div class="panel-heading">
		        <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#<?php  echo $product->prod_id?>">
		          <?php echo lang('product_name').': '.$product->{"prod_name_{$lang}"} ; ?>
		        </a>
		        </h4>
		      </div>
		      <!-- /Heading -->
		      
		      <!-- Collapse -->
		      <div id="<?php  echo $product->prod_id?>" class="panel-collapse collapse">
		        <div class="panel-body">
		        	
		        <table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
                    <tr>
	                  <th class=" hidden-xs"><?php echo  lang('image');?></th>
	                  <th><?php echo lang('description');?></th>
	                  <th class="hidden-xs"><?php echo lang('order_count');?></th>
					</tr>
                </thead>
                <tbody>
		    	  	<tr class="gradeX">
		    	  		
		    	    <td><img  class="image hidden-xs" width="150"  height="150" src="<?php  echo base_url().'assets/uploads/products/'.$product->prod_image;?>" /></td>
					<td><div class="title"><?php echo $product->{"prod_name_{$lang}"};?></div></td>
			    	<td><?php  echo $product->prod_order_count; ?></td>

		 			</tr>
	 			</tbody>
		</table>
		        </div>
		      </div>
		      <!-- /Collapse -->
		      
		    </div>
		    <!-- /Panel -->
		    <?php 	  endforeach;
					}else  echo lang('no_roders');
					?>
		  </div>
		  <!-- /Accordion -->
	  
		</div>
	</div>
</div>
  
  
  
  
  
  
