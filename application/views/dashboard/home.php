<style type="text/css">
	.menu_is_active{
		padding: 0px 15px;
	}
</style>

<!-- Slideshow Items -->
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><?php echo lang('slideshow') ?></h3>
		</div>
		<div class="panel-body">

			<div class="form-group col-md-2">
				<button type="button" class="btn btn-success" data-type="toggle" data-for="#create_form" data-default="hide"><?php echo lang('add_new_item') ?></button>
			</div>
			<div class="form-group col-md-8"></div>
			<div class="form-group col-md-2"></div>

			<div class="clearfix"></div>

			<?php echo form_open_multipart('', 'class="form-horizontal" role="form" id="create_form"'); ?>

			<?php bs3_hidden('create', true) ?>

			<?php  bs3_input('title_en', false, 'required="true"') ?>
			<?php  bs3_input('text_en') ?>

			<?php bs3_input('title_ar', false, 'required="true"') ?>
			<?php bs3_input('text_ar') ?>

			<?php bs3_input('url') ?>

			<!-- <?php bs3_input('title_rd') ?> -->
			<!-- <?php bs3_input('text_rd') ?>  -->

			<?php bs3_image('image', false, base_url().'assets/img/no_image.png', 'image'); ?>
			<p class="col-md-offset-2"><?php echo lang('best_size') ?>: 1140*500</p>
			<?php bs3_submit() ?>

			<?php echo form_close() ?>

			<!-- Read -->
			
			<?php if ($slideshow): ?>
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th><?php echo lang('sort') ?></th>
								<th><?php echo lang('image') ?></th>
								<th><?php echo lang('title_en') ?></th>
								<th><?php echo lang('title_ar') ?></th>
								<!-- <th><?php echo lang('title_rd') ?></th> -->
								<th><?php echo lang('text_en') ?></th>
								<th><?php echo lang('text_ar') ?></th>
								<!-- <th><?php echo lang('text_rd') ?></th> -->
								<th><?php echo lang('updated_at') ?></th>
								<th><?php echo lang('delete') ?></th>
							</tr>
						</thead>
						<tbody class="sortable" data-url="articles">
							<?php foreach ($slideshow as $item): ?>
								<tr data-id="<?php echo $item->id ?>"  id="item-<?php echo $item->id ?>">
									<td class="handle" alt="move"></td>
									<td data-key="image">
										<a class="thumbnail" style="display: inline-block;margin-bottom: 0px;">
											<img src="<?php echo base_url("assets/uploads/slideshow/{$item->image}") ?>" class="img-responsive" alt="">
										</a>
									</td>
									<td data-key="title_en"><?php echo $item->title_en ?></td>
									<td data-key="title_ar"><?php echo $item->title_ar ?></td>
									<!-- <td data-key="title_rd"><?php echo $item->title_rd ?></td> -->
									<td data-key="text_en"><?php echo $item->text_en ?></td>
									<td data-key="text_ar"><?php echo $item->text_ar ?></td>
									<!-- <td data-key="text_rd"><?php echo $item->text_rd ?></td> -->
									<td data-key="updated_at"><a class="btn btn-default" href="<?php echo site_url('dashboard/edit_slider/'.$item->id); ?>"> <?php echo lang('edit'); ?></a> </td>
									<td data-toggle="modal" data-target="#delete-modal"></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>

				<!-- Delete -->

				<div class="modal fade" id="delete-modal">
					<div class="modal-dialog">
						<div class="modal-content">
							<?php echo form_open('', 'class="form-horizontal" role="form"'); ?>
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title"><?php echo lang('delete_confirmation') ?></h4>
							</div>
							<div class="modal-body">
								<?php bs3_hidden('delete', true) ?>

								<?php bs3_hidden('item_id') ?>

								<?php echo lang('delete_confirmation_text') ?>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('close') ?></button>
								<button type="submit" class="btn btn-danger"><?php echo lang('delete') ?>!</button>
							</div>
							<?php echo form_close() ?>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			<?php endif ?>

		</div>
	</div>
</div>


<!-- Update Content -->

 
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo humanize($page) ?></h3>
		</div>
		<div class="panel-body">

			<?php echo form_open_multipart('', 'class="form-horizontal" role="form" id="update_form"'); ?>

			<?php bs3_hidden('update', true) ?>
			<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
				<?php bs3_input('shop_now', $shop_now) ?>
				<?php bs3_input('shop_now_slider', $shop_now) ?>
			</div>
			<hr />
			<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
				<?php bs3_input('about_us_url', $about_us_url, 'required="true"') ?>
				<?php bs3_input('about_us_title', $about_us_title, 'required="true"') ?>
				<?php bs3_textarea('about_us_text_en', $about_us_text_en) ?>
				<?php bs3_textarea('about_us_text_ar', $about_us_text_ar) ?>
				<!-- 
					<?php bs3_textarea('about_us_text_rd', $about_us_text_rd) ?>
				 -->
				

			</div>
			<hr>

			<div style="background-color: #f9f9f9;padding: 15px;border-top: 1px solid #ddd;">
				<?php  echo lang('image_albums');?>
				<div class="menu_is_active">
					<?php bs3_radio('is_active_menu_images_albume','active', 1,$is_active_menu_images_albume?1:0) ?>
					<?php bs3_radio('is_active_menu_images_albume','not_active', 0,!$is_active_menu_images_albume?1:0) ?>
				</div>

				<hr>
				<?php  echo lang('video_albums');?>
				<div class="menu_is_active">
					<?php bs3_radio('is_active_menu_video_albume','active', 1,$is_active_menu_video_albume?1:0) ?>
					<?php bs3_radio('is_active_menu_video_albume','not_active', 0,!$is_active_menu_video_albume?1:0) ?>
				</div>
				<hr>


				<?php  echo lang('contact_us');?>
				<div class="menu_is_active">
					<?php bs3_radio('is_active_menu_contact_us','active', 1,$is_active_menu_contact_us?1:0) ?>
					<?php bs3_radio('is_active_menu_contact_us','not_active', 0,!$is_active_menu_contact_us?1:0) ?>
				</div>
				<hr>

				<?php  echo lang('products');?>
				<div class="menu_is_active">
					<?php bs3_radio('is_active_menu_products','active', 1,$is_active_menu_products?1:0) ?>
					<?php bs3_radio('is_active_menu_products','not_active', 0,!$is_active_menu_products?1:0) ?>
				</div>
				<hr>
			</div>


			<?php bs3_submit() ?>

			<?php echo form_close(); ?>

		</div>
	</div>
</div>
