  
<!-- Slideshow Items -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">
				<?php if ($this->uri->segment(3) == 2) 
						echo lang('orders_pending');
					 else 
					 	echo lang('orders_history');
				?>
			</h3>
		</div>
		<div class="panel-body">
			<!-- Accordion -->
		  <div class="panel-group" id="accordion">
		  <?php  if($orders) {?>
		  <?php foreach ($orders as $key => $order) : ?>
			  
		    <!-- Panel -->
		    <div class="panel panel-default">
		      <!-- Heading -->
		      <div class="panel-heading">
		        <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#<?php  echo $order->order_id?>">
		          <?php echo lang('order_from').' '.$order->first_name.' '.$order->last_name ; ?>
		        </a>
		        </h4>
		      </div>
		      <!-- /Heading -->
		      
		      <!-- Collapse -->
		      <div id="<?php  echo $order->order_id?>" class="panel-collapse collapse">
		        <div class="panel-body">
		        	
		<!-- user data -->
	         <div class="table-responsive">
                <table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
                        <tr>
							<th><?php echo lang('index_fname_th');?></th>
							<th><?php echo lang('index_lname_th');?></th>
							<th><?php echo lang('index_email_th');?></th>
							<th><?php echo lang('index_phone_th');?></th>
						</tr>
                    </thead>
				
					<tbody>
						<tr class="gradeX">
							<td><?php echo $order->first_name;?></td>
							<td><?php echo $order->last_name;?></td>
							<td><?php echo $order->email;?></td>
							<td><?php echo $order->phone;?></td>
						</tr>
					</tbody>
				</table>

		<!-- !user data -->
		        <table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
				<thead>
                    <tr>
	                  <th class=" hidden-xs"><?php echo  lang('image');?></th>
	                  <th><?php echo lang('description');?></th>
	                  <th><?php echo lang('qty');?> </th>
	                   <?php 
 							if ($this->uri->segment(3) == 2){ ?>
 								<th><?php echo lang('prod_qty_avaliabel');?> </th>
 						<?php 	}
	                   ?>
	                  <th class="hidden-xs"><?php echo lang('price');?></th>
	                  <th class="hidden-xs"><?php echo lang('price_discount');?></th>

	                  <th class="hidden-xs"><?php echo lang('created_at');?></th>
	                   <th class="hidden-xs"><?php echo lang('updated_at');?></th>
					</tr>
                </thead>
                <tbody>
		    	    <?php  foreach($order->products as $product){  ?>
		    	  	<tr class="gradeX">
		    	  		
		    	    <td><img  class="image hidden-xs" width="150"  height="150" src="<?php  echo base_url().'assets/uploads/products/'.$product->prod_image;?>" /></td>
					<td><a target="_blank" href="<?php echo site_url('main/product/'.$product->prod_id);?>"> <div class="title"><?php echo $product->{"prod_name_{$lang}"};?></div> </a> </td>
			    	<td >  <?php echo $product->qty;?></td>
			    	 <?php 
 							if ($this->uri->segment(3) == 2){ ?>
 								<th><?php echo $product->prod_quantity;?> </th>
 						<?php 	}
	                   ?>
			    	<td><?php echo $product->price;  ?></td>
			    	<td><?php echo $product->price_discount;  ?></td>
			    	<td> <?php echo $order->created_at ; ?></td>
			    	<td> <?php echo $order->updated_at ; ?></td>
		 			</tr>
		 			<?php  }?>
	 			</tbody>
		</table>


		<!-- Cart Summary -->
            <table class="cart-summary">
              <tr>
			
                <td class="totals"> 
                  <table class="cart-totals">
                    <tr>
                      <td class="cart-total"><?php echo lang('total');?></td>
                      <td class="cart-total price"><?php echo $order->total; ?></td>
                    </tr>
                  </table>
                </td>

              </tr>
            </table>  
            <!-- /Cart Summary -->

		        	
		        <?php if ($order->status == 'pending'){?>
		        	<br />
		        	<a  class="btn btn-success" href="<?php echo site_url('dashboard/accept_order/'.$order->order_id); ?>"> <?php echo lang('accept'); ?></a>
		        	<a  class="btn btn-danger" href="<?php echo site_url('dashboard/reject_order/'.$order->order_id); ?>"> <?php echo lang('reject'); ?></a>
		        <?php }else{ ?>
		        	<div  class="btn btn-info" > <?php echo lang($order->status); ?></div>
		        <?php }?>
		        </div>
		      </div>
		      <!-- /Collapse -->
		      
		    </div>
		    <!-- /Panel -->
		    <?php 	  endforeach;
					}else  echo lang('no_roders');
					?>
		  </div>
		  <!-- /Accordion -->
	  
		</div>
	</div>
</div>
  
  
  
  
  
  
