
	
<!-- Update Content -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo $page ?></h3>
		</div>
		<div class="panel-body">
				<a  class="btn btn-success" href="<?php echo site_url('dashboard/add_style'); ?>"> <?php echo lang('add_style'); ?></a>
            	 <?php if(!$styles){?>
            		<?php echo '<p>'.lang('no_styles').'</p>';?>
            <?php }else{?>	
				<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
                            <tr>
								<th><?php echo lang('style_title');?></th>
								<th><?php echo lang('edit');?></th>
								<th><?php echo lang('delete');?></th>
							</tr>
                        </thead>
                        <tbody>
				    	    <?php  foreach($styles as $style){?>
				    	  	<tr class="gradeX">
				    	  		
				    	    <td> <?php 	echo $style->{"ps_title_{$lang}"} ; ?></td>
							<td><?php echo anchor("dashboard/edit_style/$style->ps_id",'<img src="'.base_url().'assets/img/c_edit.png"  />','');?> </td>
					    	<td onclick="delete_cat(<?php  echo $style->ps_id;?>);" ><img src="<?php echo base_url();?>assets/img/c_drop.png"  /></td>
					    	<?php  }}?>
				 			</tr>
			 			</tbody>
 				</table>

										
		</div>
	</div>
</div>

<!-- <script src="<?php echo base_url('assets') ?>/js/bootstrap.min.js"></script> -->
<script src="<?php echo base_url('assets') ?>/js/bootstrap-dialog.js"></script>
<script>
	function delete_cat (id) {
	  BootstrapDialog.show({
            title: "<?php echo lang('delete_confirmation') ?>",
            message: "<?php echo lang('delete_confirmation_text') ?>",
            buttons: [{
                label:" <?php echo lang('delete') ?>",
                cssClass: 'btn-danger',
                action: function(dialog) {
                   location.replace("<?php  echo site_url('dashboard/delete_style');?>" +'/'+id)
                }
            }, {
                label:"  <?php echo lang('close') ?>",
                action: function(dialog) {
                    dialog.close();
                }
            }]
        });
	}
	
</script>

