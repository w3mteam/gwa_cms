
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-8">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>assets/img/assign_role.png" />Assigning Roles</p>
		</div>
		<div class="panel-body">
			<select name="user" id="user" size="1" class="form-control">
				<option>--</option>
				<?php
					foreach ($users as $value) {
						echo'<option value="'.$value->id.'">'.$value->username.'</option>';
					}
				?>
			</select>
			<br />
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
		                    <tr>
								<th><p>Current Roles</p></th>
							</tr>
		                </thead>
		                <tbody id="items_c_r"></tbody>
					</table>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
		                    <tr>
								<th><p>Add Roles</p></th>
							</tr>
		                </thead>
		                <tbody id="items_a_r"></tbody>
					</table>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
		                    <tr>
								<th><p>Delete Roles</p></th>
							</tr>
		                </thead>
		                <tbody id="items_d_r"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#user').on('change', function (e) {
		    var optionSelected = $("option:selected", this);
		    var valueSelected = this.value;
		    var form_data = {'user_id' : valueSelected };
		    $.ajax({
		        url: "<?php echo base_url();?>index.php/dashboard/get_roles",
		        type: 'POST',
		        data: form_data,
		        success: function(data){
		            	var jsonData = JSON.parse(data);
		        		$('#items_c_r').html('');
		        		$('#items_d_r').html('');
		            	for (var i = 0; i < jsonData.length; i++) {
		            		$('#items_d_r').append('<tr><td>' + jsonData[i].name + '<a style="float:left;padding-right:25px;" href="<?php echo base_url(); ?>index.php/dashboard/delete_users_roles/'+jsonData[i].id+'"><img src="<?php echo base_url();?>assets/img/c_drop.png"  /></a></td></tr>');
		            		$('#items_c_r').append('<tr><td>' + jsonData[i].name + '</td></tr>');
		            	}
		        },
		        error: function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			    }
		    });
		    $.ajax({
		        url: "<?php echo base_url();?>index.php/dashboard/get_non_roles",
		        type: 'POST',
		        data: form_data,
		        success: function(data){
		        		$('#items_a_r').html('');
		            	var jsonData = JSON.parse(data);
		            	for (var i = 0; i < jsonData.length; i++) {
		            		$('#items_a_r').append('<tr><td>' + jsonData[i].name + '<a style="float:left;padding-right:25px;" href="<?php echo base_url(); ?>index.php/dashboard/add_users_role/'+jsonData[i].id+'/'+form_data['user_id']+'"><img src="<?php echo base_url();?>assets/img/add.png"  /></a></td></tr>');
		            	}
		        },
		        error: function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			    }
		    });
		    return false;
		});
	});
	
</script>



