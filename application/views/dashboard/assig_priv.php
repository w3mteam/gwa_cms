<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>assets/img/assign_privilage.png" /><?=lang('assigning_priv');?></p>
		</div>
		<div class="panel-body">
			<?php if($mode != "no_priv"){
				if($message != ''){
					echo $message;
				} 
				?>
				<select name="role" id="role" size="1" class="form-control">
					<option value=""></option>
					<?php
						foreach ($roles as $value) {
							echo'<option value="'.$value->id.'">'.$value->{"name_{$lang}"}.'</option>';
						}
					?>
				</select>
				<br />
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
			                    <tr>
									<th><p><?=lang('current_priv');?></p></th>
								</tr>
			                </thead>
			                <tbody id="items_d_p"></tbody>
						</table>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
			                    <tr>
									<th><p><?=lang('add_priv');?></p></th>
									<th><p><?=lang('priv_name');?></p></th>
								</tr>
			                </thead>
			                <tbody id="items_a_p"></tbody>
						</table>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12" >
						<a id="update_info" href="javascript:void(0);" class="btn btn-info center-block"><?=lang('update_info');?></a>
					</div>
				</div>
			<?php
			}else{
				echo $message;
			} ?>
			
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var lang = '<?php echo  $lang;?>';
		var var_name = 'privilage_'+lang;
		$('body').on('click','#delete_priv',function(){
		
			var role_id = $(this).attr('roleid');
			var priv_id = $(this).attr('privid');
			var itemid  = $(this).attr('itemid');
			var text = $(this).parent().text();
			var data  = {role_id :role_id,priv_id:priv_id};
			var elem = $(this);
			$.ajax({
		        url: "<?php echo base_url();?>index.php/dashboard/delete_role_privilage",
		        type: 'POST',
		        data: data,
		        success: function(data){
		            	$('#items_a_p').append('<tr><td><input class="add_priv" roleid="'+role_id+'" type="checkbox" value="'+itemid+'" /></td><td>' + text+ '</td></tr>');
						console.log(elem.parent().parent());
		            	elem.parent().parent().remove();
		        },
		        error: function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			    }
		    });
		});
		$('#update_info').on('click',function (){
			var privs = [];
			var role_id;
			$('input:checkbox.add_priv').each(function () {
				if(this.checked)
			   		privs.push ($(this).val());
			   	role_id = $(this).attr('roleid');
			});
			var data  = {role_id :role_id,privs:privs};
			
		
			$.ajax({
		        url: "<?php echo base_url();?>index.php/dashboard/add_role_priv",
		        type: 'POST',
		        data: data,
		        success: function(data){
		        		
		            	$('input:checkbox.add_priv').each(function () {
							if(this.checked){
								var tr = $(this).parent().parent();
								var prive_id = tr.find(':first-child').find(':first-child').val();
								var role_id = tr.find(':first-child').find(':first-child').attr('roleid');
								var itemid = tr.find(':first-child').find(':first-child').attr('itemid');
								var prive_name =  tr.find(':last-child');
								$('#items_d_p').append('<tr><td>' + prive_name.text()+ '<a id="delete_priv" itemid="'+itemid+'" privid="'+prive_id+'" roleid="'+role_id+'" style="float:left;padding-right:25px;" href="javascript:void(0);"><img src="<?php echo base_url();?>assets/img/c_drop.png"  /></a></td></tr>');
								$(this).parent().parent().remove();
							}
						});
		        },
		        error: function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			    }
		    });
		});
    	$('#role').on('change', function (e) {
    		$('select option')
			    .filter(function() {
			        return !this.value || $.trim(this.value).length == 0;
			    })
			   .remove();
   
		    var optionSelected = $("option:selected", this);
		    var valueSelected = this.value;
		    var form_data = {'role_id' : valueSelected };
		    $.ajax({
		        url: "<?php echo base_url();?>index.php/dashboard/get_privilages",
		        type: 'POST',
		        data: form_data,
		        success: function(data){
		        		
		            	var jsonData = JSON.parse(data);
		        		$('#items_c_p').html('');
		        		$('#items_d_p').html('');
		            	for (var i = 0; i < jsonData.length; i++) {
		            		
		            		$('#items_d_p').append('<tr><td>' + jsonData[i][var_name]+ '<a id="delete_priv" privid="'+jsonData[i].id_privilage+'" itemid="'+jsonData[i].id+'" roleid="'+jsonData[i].id_role+'" style="float:left;padding-right:25px;" href="javascript:void(0);"><img src="<?php echo base_url();?>assets/img/c_drop.png"  /></a></td></tr>');
		            		$('#items_c_p').append('<tr><td>' + jsonData[i][var_name] + '</td></tr>');
		            	}
		        },
		        error: function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			    }
		    });
		    $.ajax({
		        url: "<?php echo base_url();?>index.php/dashboard/get_non_privilages",
		        type: 'POST',
		        data: form_data,
		        success: function(data){
		        		$('#items_a_p').html('');
		            	var jsonData = JSON.parse(data);
		            	for (var i = 0; i < jsonData.length; i++) {
		            		$('#items_a_p').append('<tr><td><input class="add_priv" roleid="'+form_data['role_id']+'" itemid="'+jsonData[i].id+'" type="checkbox" value="'+jsonData[i].id+'" /></td><td>' + jsonData[i][var_name] + '</td></tr>');
		            	}
		        },
		        error: function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			    }
		    });
		    return false;
		});
	});
	
</script>



