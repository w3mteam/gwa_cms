<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>assets/img/Create_new_Group.png"  /><?php echo lang('create_group_heading');?></p>
        </div>
		<div class="panel-body">
        	<h1><?php echo lang('create_group_heading');?></h1>
			<p><?php echo lang('create_group_subheading');?></p>
			
			<div id="infoMessage"><?php echo $message;?></div>
			
			<?php echo form_open("dashboard/create_group");?>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<p>
					            <?php echo lang('create_group_name_label_en', 'group_name_en');?> <br />
					            <?php echo form_input($group_name_en,'','class="form-control"');?>
					      	</p>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<p>
					            <?php echo lang('create_group_name_label_ar', 'group_name_ar');?> <br />
					            <?php echo form_input($group_name_ar,'','class="form-control"');?>
					      	</p>
						</div>
					</div>
					<!-- <div class="row"> -->
						
						<!-- <div class="col-md-6 col-sm-6 col-xs-12">
							 <p>
						            <?php echo lang('create_group_name_label_rd', 'group_name_rd');?> <br />
						            <?php echo form_input($group_name_rd,'','class="form-control"');?>
						      </p>
						</div> -->
						
					<!-- </div> -->
			      	<div class="row">
			      		<div class="col-md-12 col-sm-12 col-xs-12">
							 <p>
						            <?php echo lang('create_group_desc_label', 'description');?> <br />
						            <?php echo form_textarea($description,'','class="form-control"');?>
						      </p>
						</div>
			      	</div>
			
			     
			
			      <p><?php echo form_submit('submit', lang('create_group_submit_btn'),'class="btn btn-primary btn-block"');?></p>
			
			<?php echo form_close();?>
       </div>
	</div>
</div>

