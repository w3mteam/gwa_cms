<!-- Breadcrumbs -->
<?php if ($breadcrumbs): ?>
<ol class="breadcrumb">
	<li><?php echo anchor('dashboard/show_all_category', lang('categories')); ?></li>
	<?php foreach ($breadcrumbs as $item): ?>
		<li><?php echo anchor('dashboard/show_all_category/'.$item->Cat_ID, $item->{"Cat_Name_{$lang}"}); ?></li>
		<!-- <li><?php echo anchor('dashboard/articles/'.$item->id, $item->{"title_$lang"}); ?></li> -->
	<?php endforeach ?>
</ol>
<?php endif ?>

<!-- Update Content -->

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
		<h3 class="panel-title"><?php echo lang('edit_category'); ?></h3>
		</div>
		<div class="panel-body">
            	 <?php if($mode=='successful'){?>
            		<?php echo $message;?>
            <?php }else{?>	
		
<?php echo form_open('dashboard/edit_category/'.$this->uri->segment(3));?>

      <p>
      	    <?php  echo lang('cat_name_en');?>
            <?php echo form_input($cat_name_en,'','class="form-control font_input" ');?>
            <?php  echo lang('cat_name_ar');?>
            <?php echo form_input($cat_name_ar,'','class="form-control font_input"  ');?>
            <!-- <?php  echo lang('cat_name_rd');?>
            <?php echo form_input($cat_name_rd,'','class="form-control font_input"  ');?> -->
            <?php echo lang('parent_category');?>
            
			<select name="parent_id" size="1" class="form-control">
		 			<option value="0"><?php echo  lang('no_parent'); ?></option>
		 			<?php 
		 			foreach ($main_categories as $key => $cat) { ?>
		 				<?php  if ($parent_id == $cat->Cat_ID && $parent_id != 0){?>
							 <option value="<?php echo $cat->Cat_ID; ?>" selected="selected"><?php  echo $cat->{"Cat_Name_{$lang}"}; ?></option>
						 <?php }else { ?>
						 	<option value="<?php echo $cat->Cat_ID; ?>"><?php  echo $cat->{"Cat_Name_{$lang}"}; ?></option>
						<?php  }
					  }
		 			?>
	 		</select>
	 		
	 		<?php //  bs3_image('image', $category->image?'category/'.$category->image:false, base_url().'assets/img/no_image.png', 'image'); ?>
	 		
      </p>

       <p><?php echo form_submit('submit', lang('edit'),'class="btn btn-lg btn-default btn-block"');?></p>

<?php echo form_close();?>
              <?php }?> 
										
		</div>
	</div>
</div>



