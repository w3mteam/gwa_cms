 <!-- Intro Block
      ============================================ -->
      <div class="intro-block mgb-20">
      
        <!-- Container -->
        <div class="container">

          <!-- Slider Wrapper -->
          <div class="intro-slider">
          
            <!-- BxSlider -->
           <div class="bxslider" data-call="bxslider" data-options="{pager:false, mode:'fade',auto: true}">
             <?php for ($i=0; $i < sizeof($slideshow) ; $i++) { ?>
              <!-- Slide -->
              <div class="slide">
                <img class="img-main" src="<?php echo base_url();?>assets/uploads/slideshow/<?php echo $slideshow[$i]->image; ?>" alt=""/><!-- slider image + background -->
                <!-- Text -->
                <div class="text">
                  <div class="vcenter">
                    <div class="vcenter-this text-block">
                    	<!-- <form action="<?php echo $shop_now; ?>"  method="post">
                      		<button class="btn btn-base bx-layer" type="submit"  data-anim="bounceInUp" data-dur="1000" data-delay="1000"><?php echo lang('shop_now');?></button>                    	
                    	</form> -->
                    	<p> <?php  echo $slideshow[$i]->{"title_{$lang}"}?> </p> <br />
                    	<p> <?php  echo $slideshow[$i]->{"text_{$lang}"}?></p>
                    	<a class="btn btn-primary btn-borderless"  href="<?php echo $shop_now; ?>" ><?php echo lang('shop_now');?></a>
                    </div>
                  </div>
                </div>
                <!-- /Text -->
              </div>
              <?php } ?>
              <!-- /Slide -->
            
            </div>
            <!-- /BxSlider -->
            
          </div>
          <!-- Slider Wrapper -->

        </div>
        <!-- /Container -->
      
      </div>
      <!-- /Intro Block
      ============================================ -->