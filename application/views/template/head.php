<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home</title>
	<script>
		var base_url = '<?php echo base_url();?>';
	</script>
    <!-- Bootstrap core CSS
    ================================================== -->
    <link href="<?php echo base_url();?>assets_site/assets_cms/uikit/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template
    ================================================== -->
    <link href="<?php echo base_url();?>assets_site/assets_cms/uikit/css/uikit.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>assets_site/assets_cms/uikit/js/html5shiv.js"></script>
    <script src="<?php echo base_url();?>assets_site/assets_cms/uikit/js/respond.min.js"></script>
    <![endif]-->
  </head>