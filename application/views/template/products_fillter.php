            <form action="<?php echo site_url('main/shop'); ?>"  method="post">
                    <fieldset>
                      <select  size="1" class="form-control"  name="gender_id"  id="gender_id_p" >
                      		<option value="0"><a href=""><?php  echo lang('select_gender');?></a></option>
                            <?php  foreach ($main_categories as $key => $category) { ?>
                                <option value="<?php echo  $category->Cat_ID;  ?>"><?php  echo $category->{"Cat_Name_{$lang}"}?></option> 
                         	<?php   } ?>
                           
                      </select >
                    </fieldset>
                    
                     <fieldset>
                      <select  size="1" class="form-control"  name="category_id" id="category_id_p"  >
                      		<option value="0"><a href=""><?php  echo lang('select_category');?></a></option>
                                                       
                      </select >
                    </fieldset>
                    
                    <fieldset>
                      <select  size="1" class="form-control"  name="style_id" >
                      		<option value="0"><a href=""><?php  echo lang('select_style');?></a></option>
                            <?php  foreach ($styles as $key => $style) { ?>
                                <option value="<?php echo $style->ps_id ?>"><?php  echo $style->{"ps_title_{$lang}"}?></option> 
                         	<?php   } ?>
                           
                      </select >
                    </fieldset>
                   
	                <fieldset>
	                <!-- Price Range Form -->
	                
	                  <label><?php echo lang('min_price');?></label>
	                  <div class="form-group slider-group">
	                    <div class="slider-ctrl" data-call="jui-slider" data-target="#min2" data-options="{min:<?php  echo $min_price?>, max:<?php  echo $max_price?>, value:<?php  echo $avarge_price;?>}"></div>
	                    <input id="min2"  name="min_price" class="form-control" value="500" type="text">
	                  </div>              
	                  <label><?php echo lang('max_price');?></label>
	                  <div class="form-group slider-group">
	                    <div class="slider-ctrl" data-call="jui-slider" data-target="#max2" data-options="{min:<?php  echo $min_price?>, max:<?php  echo $max_price?>, value:<?php  echo $avarge_price;?>}"></div>
	                    <input id="max2" name="max_price" class="form-control" value="500" type="text">
	                  </div>  
	                
	                <!-- /Price Range Form -->
	              
				</fieldset>
                  
                    <fieldset>
                    	<center>
                    		<button class="btn btn-base checkout"  type="submit"><i class="icon-left fa fa-shopping-cart"></i><?php echo  lang('shop'); ?></button>
                     	</center>
                     </fieldset>
                    </form>



  <script type="text/javascript">


        $('#gender_id_p').change(function(){
    get_sub_categories();
  });

  function get_sub_categories () {
    cat_id=$('#gender_id_p').find(":selected").val();
     //cat_id=$('#prod_main_cat_id:selected').val();
        $.ajax({ 
          type: "POST",
          url: '<?php echo site_url('ajax_controller/get_sub_categories');?>'+'/'+cat_id,
          data: {}, 
          success: function (response) {
            cats=JSON.parse(response);
            $('#category_id_p').html('');
            $('#category_id_p').append(' <option value="0">'+"<?php echo lang('select_category');?>"+'</option>');
            for (var i=0; i < cats.length; i++) {
          $('#category_id_p').append(' <option value="'+cats[i].Cat_ID+'">'+cats[i].cat_name+'</option>');
          };
          //$('#category_id').show();
            console.log(response);
          } ,
          error:function(e){
            console.log('error');
          }         
     });
  }

</script>
