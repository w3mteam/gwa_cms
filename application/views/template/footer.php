
 <!-- Footer
      =================================================== -->
      <footer class="footer-block">
      
        <!-- Container -->
        <div class="container cont-top clearfix">
        
          <!-- Row -->
          <div class="row">
          
            <!-- Brand -->
            <div class="col-md-3 brand-col brand-center">
              <div class="vcenter">
               <img class="img-responsive" width="350px" height="350px" src="<?php echo base_url();?>/upload/<?php echo $left_advertisement;?>" />
              </div>
            </div>
            <!-- /Brand -->
            
            <!-- Links -->
            <div class="col-md-9 links-col">
            
              <!-- Row -->
              <div class="row-fluid">
              
                <!-- Col -->
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <h5>About us</h5>
                  <p>Helena is a freelance fashion designer who specialises in print designs and combining fabrics. My designs have been sold all over the world.</p>
                    <!-- hlinks -->
                    <ul class="hlinks hlinks-icons color-icons-borders color-icons-bg color-icons-hovered">
                      <li><a href="#"><i class="icon fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="icon fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="icon fa fa-rss"></i></a></li>
                      <li><a href="#"><i class="icon fa fa-google-plus"></i></a></li>
                      <li><a href="#"><i class="icon fa fa-instagram"></i></a></li>
                      <li><a href="#"><i class="icon fa fa-youtube"></i></a></li>
                    </ul>
                    <!-- /hlinks -->              
                </div>
                <!-- /Col -->
                
                <!-- Col -->
                <div class="col-xs-6 col-sm-3 col-md-3">
                  <h5>member</h5>
                  <ul class="vlinks">
                    <li><a href="#">Account</a></li>
                    <li><a href="#">Wishlist and Favourites</a></li>
                    <li><a href="#">Purchase History</a></li>
                    <li><a href="#">View Cart</a></li>
                  </ul>
                </div>
                <!-- /Col -->
                
                <!-- Col -->
                <div class="col-xs-6 col-sm-3 col-md-3 newsletter">
                 	<img class="img-responsive" width="350px" height="350px" src="<?php echo base_url();?>/upload/<?php echo $right_advertisement;?>" />
                </div>
                <!-- /Col -->
                
             </div>
             <!-- /Row -->
             
            </div>
            <!-- /Links -->
            
          </div>
          <!-- /Row -->
          
        </div>
        <!-- /Container -->
        
        <!-- Bottom -->
        <div class="footer-bottom invert-colors bcolor-bg">
        
          <!-- Container -->
          <div class="container">
          
            <span class="copy-text">&copy; ALL RIGHTS RESERVED © 2015 RICH TIME</span>
            <!-- hlinks -->
            <ul class="hlinks pull-right">
              <li><a href="#">ABOUT </a></li>
              <li><a href="#">BRODUCTS</a></li>
              <li><a href="#">CONTACT US</a></li>
            </ul>
            <!-- /hlinks -->
            
          </div>
          <!-- /Container -->
          
        </div>
        <!-- /Bottom -->
        
      </footer>
      <!-- /Footer
      =================================================== -->
      
      <!-- Promo Modal
      ============================================ -->      
      <div class="modal fade modal-promo" data-call="bs-modal" data-options="{show:true}">
        <!-- Dialog -->
        <div class="modal-dialog">
        
          <!-- Promo Image (use background image to allow scaling) -->
          <div class="promo-img bg-cover" style="background:url(<?php echo base_url();?>assets_site/assets_cms/images/modal-promo.jpg)"></div>
          
          <!-- Text Col -->
          <div class="text-col">
            <div class="text">
              <h5>daily exclusive deals</h5>
              
              <img src="<?php echo base_url();?>assets_site/assets_cms/images/modal-promo-xs.jpg" alt="" class="visible-xs mgb-20" />
              <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet.</p>
              
              <!-- Modal Form -->
              <form class="form-inline modal-form">
                <div class="form-group">
                  <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address"/>
                </div>	
                <button type="submit" class="btn btn-base">Subscribe</button>
              </form>
              <!-- /Modal Form -->
              <p class="hidden-xs" >Follow us on social media for exclusive deals.</p>
              
              <!-- hlinks -->
              <ul class="hlinks hlinks-icons hlinks-icons-round color-icons-bg color-icons-hovered">
                <li><a href="#"><i class="icon fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="icon fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="icon fa fa-rss"></i></a></li>
                <li><a href="#"><i class="icon fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="icon fa fa-youtube"></i></a></li>
              </ul>
              <!-- /hlinks --> 
            </div>
          </div>
          <!-- /Text Col -->

          <button type="button" class="btn-close btn btn-base" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <!-- /Dialog -->
      </div>
      <!-- /Promo Modal
      ============================================ --> 
      
    </div>
    <!-- /Page Wrapper
    ++++++++++++++++++++++++++++++++++++++++++++++ -->

    <!-- Javascript
    ================================================== -->
    <script src="<?php echo base_url();?>assets_site/assets_cms/uikit/js/jquery-latest.min.js"></script>
    <script src="<?php echo base_url();?>assets_site/assets_cms/uikit/js/uikit.js"></script>
    <!-- /JavaScript
    ================================================== -->

  </body>
</html>
