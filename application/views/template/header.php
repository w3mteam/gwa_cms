
  <body class="preload tile-1-bg">
  
    <!-- Preloader 
    ============================================ -->
    <div class="page-preloader">
      <div class="vcenter"> <div class="vcenter-this"><img class="anim" src="<?php echo base_url();?>assets_site/assets_cms/images/loader.gif" alt="loading..." /></div></div>
    </div>
    <!-- /Preloader 
    ============================================ --> 
    
    <!-- Page Wrapper
    ++++++++++++++++++++++++++++++++++++++++++++ -->
    <div class="page-wrapper boxed-wrapper shadow">

      <!-- Header Block
      ============================================== -->
      <header class="header-block line-top">
      
        <!-- Main Header
        ............................................ -->
        <div class="main-header container">
        
          <!-- Header Cols -->
          <div class="header-cols"> 
          
            <!-- Brand Col -->
            <div class="brand-col hidden-xs">
            
              <!-- vcenter -->
              <div class="vcenter">
                <!-- v-centered -->               
                <div class="vcenter-this">
                  <a href="index.html">
                    <img src="<?php echo base_url();?>assets_site/assets_cms/images/logo.png" alt="HELENA">
                  </a>
                </div>
                <!-- v-centered -->
              </div>
              <!-- vcenter -->

            </div>
            <!-- /Brand Col -->

            <!-- Right Col -->
            <div class="right-col">
            
              <!-- vcenter -->
              <div class="vcenter">
              
                <!-- v-centered -->
                <div class="vcenter-this">

                  <!-- Nav Side -->
                  <nav class="nav-side navbar hnav hnav-sm hnav-borderless" role="navigation">
                  
                    <!-- Dont Collapse -->
                    <div class="navbar-dont-collapse no-toggle">
                    
                      <!-- Nav Right -->
                      <ul class="nav navbar-nav navbar-right case-u active-bcolor navbar-center-xs">
                        <li class="dropdown has-panel">
                          <a aria-expanded="false" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-left ti ti-user"></i><span class="hidden-sm">sign in</span><i class="fa fa-angle-down toggler hidden-xs"></i></a>
                          
                          <!-- Dropdown Panel -->
                          <div class="dropdown-menu dropdown-panel arrow-top dropdown-left-xs" data-keep-open="true">
                            <fieldset>
                              <form>
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input class="form-control" placeholder="Email" type="email">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                    <input class="form-control" placeholder="Password" type="password">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="checkbox-inline"><input value="" type="checkbox">Remember me </label>
                                </div>
                                <button class="btn btn-primary btn-block">sign in</button>
                              </form>
                            </fieldset>
                          </div>
                          <!-- /Dropdown Panel -->
                          
                        </li>
                        
                        <li class="dropdown has-panel">
                          <a aria-expanded="false" href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-left ti ti-pencil-alt"></i><span class="hidden-sm">sign up</span><i class="fa fa-angle-down toggler hidden-xs"></i></a>
                          
                          <!-- Dropdown Panel -->
                          <div class="dropdown-menu dropdown-panel arrow-top" data-keep-open="true">
                            <fieldset>
                              <form>
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input class="form-control" placeholder="Email" type="email">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                    <input class="form-control" placeholder="Password" type="password">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                    <input class="form-control" placeholder="Repeat Password" type="password">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="checkbox-inline"><input value="" type="checkbox">I accept the terms and conditions.</label>
                                </div>
                                
                                <button class="btn btn-primary btn-block">sign up</button>
                              </form>
                            </fieldset>
                          </div>
                          <!-- /Dropdown Panel -->
                          
                        </li>
                      </ul>
                      <!-- /Nav Right-->

                    </div>
                    <!-- /Dont Collapse -->
                    
                  </nav>
                  <!-- /Nav Side -->
                
                </div>
                <!-- /v-centered -->
              </div>
              <!-- /vcenter -->
              
            </div>
            <!-- /Right Col -->
            
            <!-- Left Col -->
            <div class="left-col">
            
              <!-- vcenter -->
              <div class="vcenter">
                
                <!-- v-centered -->               
                <div class="vcenter-this">
                  
                  <form class="header-search">
                    <div class="form-group">
                      <input class="form-control" placeholder="SEARCH" type="text">
                      <button class="btn btn-empty"><i class="fa fa-search"></i></button>
                    </div>
                  </form>

                </div>
                <!-- v-centered -->
                
              </div>
              <!-- vcenter -->
            
            </div>
            <!-- /Left Col -->
          </div>
          <!-- Header Cols -->
        
        </div>
        <!-- /Main Header
        .............................................. -->
        
        <!-- Nav Bottom
        .............................................. -->
        <nav class="nav-bottom hnav hnav-ruled white-bg boxed-section">
        
          <!-- Container -->
          <div class="container">
          
            <!-- Header-->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle no-border" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-navicon"></i>
              </button>
              <a class="navbar-brand visible-xs" href="#"><img src="<?php echo base_url();?>assets_site/assets_cms/images/logo-xs.png" alt="H"></a>
            </div>
            <!-- /Header-->
          
            <!-- Collapse -->
            <div class="collapse navbar-collapse navbar-absolute">
            
              <!-- Navbar Center -->
              <ul class="nav navbar-nav navbar-center line-top line-pcolor case-c">
                <li class="active"><a href="index.html">Home</a></li>
                <?php die('tette');
                   foreach ($main_articles as $key => $article) { ?>
                 		<li><a href="<?php  echo site_url('main/article/'.$article->id);?>"><?php  echo $article->{"title_{$lang}"}?></a></li>
               <?php  } ?>
                
                <li class="dropdown dropdown-mega"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Products<i class="fa fa-angle-down toggler"></i></a>
                  <!-- Mega Menu -->
                  <div class="mega-menu dropdown-menu">
                    <!-- Row -->
                    <div class="row">
                     <?php for ($i=0; $i < sizeof($main_categories) ; $i++) { 
                     	  $sub_categories = $main_categories[$i]->sub_categories;
						  //var_dump($sub_categories);
                     	  if ($i < 3) {
                     	  ?>
                          <div class="col-md-3">
	                        <h5><?php echo $main_categories[$i]->Cat_Name_en; ?> </h5>
	                        <?php for ($j=0; $j <sizeof($sub_categories) ; $j++) { ?>
								 <ul class="links">
		                          <li><a href="<?php echo base_url();?>index.php/main/view_products_by_category/<?php echo $sub_categories[$j]->Cat_ID ;?>"><?php echo $sub_categories[$j]->Cat_Name_en;?></a></li>
		                        </ul>
							<?php 
							}
	                        ?>
	                      </div>
                   <?php  } //end if ($i < 3) ?>
                  		<!--------------------------------------->
                  		<?php if ($i == 4) { ?>
                  			 <div class="col-md-3">
		                        <h5>other pages</h5>
		                        <ul class="links">
		                        <?php } //end if ($i == 4) ?>
		                        <?php if($i > 4) { ?>
			                          <li><a href="#"><?php echo $main_categories[$i]->Cat_Name_en; ?></a></li>
								<?php } if( $i == sizeof($main_categories)) { ?>
								</ul> 
							 </div>
								<?php } ?>
                  		<!--------------------------------------->	
           				  
				<?php } // enf for ?>
                      <!-- col -->
                     
                      <!-- /col -->
                      
                      <!-- col -->
                     
                      <!-- /col -->
                    </div>
                    <!-- /Row -->
                  </div>
                  <!-- /Mega Menu -->
                </li>
                <li><a href="footers.html">Contact Us</a></li>
                
              </ul>
              <!-- /Navbar Center -->
              
            </div>
            <!-- /Collapse -->
            
            <!-- Dont Collapse -->
            <div class="navbar-dont-collapse">

              <!-- Navbar btn-group -->
              <div class="navbar-btn-group btn-group navbar-right no-margin-r-xs">
              
                <!-- Btn Wrapper -->
                <div class="btn-wrapper dropdown">
                
                  <a class="btn btn-outline" data-toggle="dropdown"><i class="ti-plus"></i></a>
                  
                  <!-- Dropdown Menu -->
                  <ul class="dropdown-menu">
                    <li><a href="#">account home</a></li>
                    <li><a href="#">order history</a></li>
                     <li><a href="#">profile</a></li>
                  </ul>
                  <!-- /Dropdown Menu -->
                  
                </div>
                <!-- /Btn Wrapper -->

                <!-- Btn Wrapper -->
                <div class="btn-wrapper dropdown">
                
                  <a aria-expanded="false" class="btn btn-outline" data-toggle="dropdown"><b class="count count-scolor count-round">2</b><i class="ti ti-bag"></i></a>
                  
                    <!-- Dropdown Panel -->
                    <div class="dropdown-menu dropdown-panel dropdown-right" data-keep-open="true">
                      <section>
                        <!-- Mini Cart -->
                        <ul class="mini-cart">
                          <!-- Item -->
                          <li class="clearfix">
                            <img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt="">
                            <div class="text">
                              <a class="title" href="#">Modern black hat</a>
                              <div class="details">2 x $40.50
                                <div class="btn-group">
                                  <a class="btn btn-primary" href="#"><i class="fa fa-pencil"></i></a>
                                  <a class="btn btn-default" href="#"><i class="fa fa-trash"></i></a>
                                </div>
                              </div>
                            </div>
                          </li>
                          <!-- /Item -->
                          
                          <!-- Item -->
                          <li class="clearfix">
                            <img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt="">
                            <div class="text">
                              <a class="title" href="#">Sexy lace blouse</a>
                              <div class="details">1 x $95.00
                                <div class="btn-group">
                                  <a class="btn btn-primary" href="#"><i class="fa fa-pencil"></i></a>
                                  <a class="btn btn-default" href="#"><i class="fa fa-trash"></i></a>
                                </div>
                              </div>
                            </div>
                          </li>
                          <!-- /Item -->
                        </ul>
                        <!-- /Mini Cart -->
                      </section>
                      
                      <section>
                        <div class="row grid-10">
                          <div class="col-md-6">
                            <a class="btn btn-base btn-block margin-y-5" href="cart.html">view cart</a>
                          </div>
                          <div class="col-md-6">
                            <a class="btn btn-primary btn-block margin-y-5" href="checkout.html">checkout</a>
                          </div>
                        </div>
                      </section>
                    </div>
                    <!-- /Dropdown Panel -->
                  
                </div>
                <!-- /Btn Wrapper -->

              </div>
              <!-- /Navbar btn-group -->
              
              <!-- Navbar Left -->
              <ul class="nav navbar-nav navbar-left navbar-right-xs">
                <li class="dropdown has-panel">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="img-left" alt="" src="<?php echo base_url();?>assets_site/assets_cms/images/flags/us.gif"><span>USD</span><i class="fa fa-angle-down toggler"></i></a>
                  <!-- Dropdown Panel -->
                  <div id="settings" class="dropdown-menu dropdown-panel" data-keep-open="true">
                    <fieldset>
                      <form>
                        <div class="form-group">
                          <label>Language</label>
                          <select class="form-control">
                            <option>English</option>
                            <option>French</option>
                            <option>German</option>
                            <option>Spanish</option>
                          </select>
                        </div>
                        
                        <div class="form-group">
                          <label>Currency</label>
                          <select class="form-control">
                            <option>US Dollar</option>
                            <option>Kenyan Shillings</option>
                            <option>TZ Shillings</option>
                            <option>British Pound</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label class="checkbox-inline"><input value="" type="checkbox">Remember settings </label>

                        </div>
                        <button class="btn btn-primary btn-block">change</button>
                      </form>
                    </fieldset>
                  </div>
                  <!-- /Dropdown Panel -->
                  
                </li>
              </ul>
              <!-- /Navbar Left -->
            </div>
            <!-- /Dont Collapse -->

          </div>
          <!-- /Container -->
          
        </nav>
        <!-- /Nav Bottom
        .............................................. -->
        
      </header>
      <!-- /Header Block
      ============================================== -->
    