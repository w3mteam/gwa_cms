      <?php $this->load->view('template/slider');
	  
      ?>
      
      <!-- Content Block
      ============================================ -->
      <div class="content-block">
      
        <!-- Container -->
        <div class="container no-pad-t">

          <!-- Product Tabs -->
          <div class="product-tabs">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-tabs-line-bottom line-pcolor nav-tabs-center case-u" role="tablist">
              <li class="active"><a href="#tab-men" data-toggle="tab">Men</a></li>
              <li><a href="#tab-women" data-toggle="tab">Women</a></li>
              <li><a href="#tab-news" data-toggle="tab">News</a></li>
            </ul>
            <!-- /Nav Tabs -->

            <!-- Tab panes -->
            <div class="tab-content tab-no-borders">
            
              <!-- Tab Latest -->
              <div class="tab-pane active" id="tab-men">
              
                <!-- Row -->
                <div class="row">
                
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <span class="label label-sale">sale</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Grey winter jacket</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price price-old">$2350</span><span class="price">$1500</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Black lase blouse</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$2600</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <span class="label label-hot">popular</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Chinese style coat</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$840</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html" >add t</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Long striped dress</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$1820</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Srapless night dress</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$675</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <span class="label label-sale">sale</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Gold detailed dress</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price price-old">$1550</span><span class="price">$1220</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      
                      <span class="label label-sale">sale</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">One pice business suit</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price price-old">$3350</span><span class="price">$2050</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Premium fur jacket</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$8400</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="product.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                
                </div>
                <!-- /Row -->
              
              </div>
              <!-- /Tab Latest -->
              
              <!-- Tab Featured -->
              <div class="tab-pane" id="tab-women">
              
                <!-- Row -->
                <div class="row">
                
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Srapless night dress</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$675</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <span class="label label-sale">sale</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Gold detailed dress</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price price-old">$1550</span><span class="price">$1220</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      
                      <span class="label label-sale">sale</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">One pice business suit</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price price-old">$3350</span><span class="price">$2050</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Premium fur jacket</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$8400</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="product.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix" id="">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <span class="label label-sale">sale</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Grey winter jacket</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price price-old">$2350</span><span class="price">$1500</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" >add to cartttttttt</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Black lase blouse</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$2600</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <span class="label label-hot">popular</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Chinese style coat</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$840</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Long striped dress</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$1820</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                
                </div>
                <!-- /Row -->
              
              </div>
              <!-- /Tab Featured -->
            
              <!-- Tab Trending -->
              <div class="tab-pane" id="tab-news">
              
                <!-- Row -->
                <div class="row">
                
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" data-gal="prettyPhoto[gallery 2]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product2.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Black lase blouse</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$2600</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" data-gal="prettyPhoto[gallery 3]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product3.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <span class="label label-hot">popular</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Chinese style coat</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$840</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" data-gal="prettyPhoto[gallery 4]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product4.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Long striped dress</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$1820</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" data-gal="prettyPhoto[gallery 7]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product7.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      
                      <span class="label label-sale">sale</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">One pice business suit</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price price-old">$3350</span><span class="price">$2050</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" data-gal="prettyPhoto[gallery 8]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product8.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Premium fur jacket</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$8400</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="product.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" data-gal="prettyPhoto[gallery 1]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product1.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <span class="label label-sale">sale</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Grey winter jacket</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price price-old">$2350</span><span class="price">$1500</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" data-gal="prettyPhoto[gallery 5]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product5.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Srapless night dress</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price">$675</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  
                  <!-- Col -->
                  <div class="col-sm-6 col-md-3">
                  
                    <!-- product -->
                    <div class="product clearfix">
                    
                      <!-- Image -->
                      <div class="image"> 
                        <a href="product.html" class="main"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a>
                        <ul class="additional">
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" data-gal="prettyPhoto[gallery 6]" title="Product Name"><img src="<?php echo base_url();?>assets_site/assets_cms/images/products/product6.jpg" alt=""></a></li>
                        </ul>
                      </div>
                      <!-- Image -->
                      
                      <span class="label label-sale">sale</span>
                      
                      <!-- Details -->
                      <div class="details">
                      
                        <a class="title" href="product.html">Gold detailed dress</a>
                        
                        <!-- rating -->
                        <ul class="hlinks hlinks-rating">
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li class="active"><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                          <li><a href="#"><i class="icon fa fa-star"></i></a></li>
                        </ul>
                        <!-- /rating -->
                        
                        <p class="desc">Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates.</p>
                        
                        <!-- Price Box -->
                        <div class="price-box">
                          <span class="price price-old">$1550</span><span class="price">$1220</span>
                        </div>
                        <!-- /Price Box -->
                        
                        <!-- buttons -->
                        <div class="btn-group">
                          <a class="btn btn-outline btn-base-hover" href="cart.html">add to cart</a>	
                          <a class="btn btn-outline btn-default-hover" href="product.html"><i class="icon fa fa-heart"></i></a>
                        </div> 
                        <!-- /buttons -->
                        
                      </div>
                      <!-- /Details -->
                      
                    </div>
                    <!-- /product -->
                  
                  </div>
                  <!-- /Col -->
                  

                
                </div>
                <!-- /Row -->
              
              </div>
              <!-- /Tab Trending -->
            </div>
            <!-- /Tab Panes -->
            
          </div>
          <!-- /Product Tabs -->
          
        </div>
        <!-- /Container -->
        
      </div>
      <!-- /Content Block
      ============================================ -->
      <?php $this->load->view('template/subscripe');?>