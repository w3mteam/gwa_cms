
      	<!-- Content Block
      ============================================ -->
      <div class="content-block" >
      
        <!-- Container -->
        <div class="container cont-md">
        
          <!-- Section Title -->
          <div class="section-title line-pcolor">
            <h2><?php echo  $brands_header;?></h2>
          </div>
          <!-- /Section Title -->
          
          <!-- Slider Wrapper -->
          <div class="brand-slider">
          
            <!-- BxSlider -->
            <div class="bxslider" data-call="bxslider" data-options="{pager:false, slideMargin:20}" data-breaks="[{screen:0, slides:2}, {screen:460, slides:3}, {screen:768, slides:5}]">
             
             <?php  foreach ($brands as $key => $brand) { ?>
              <!-- Slide -->
              <div class="slide">
              <a href="<?php  echo $brand->title;?>"> <img class="img-main" src="<?php echo base_url();?>assets/uploads/brands/<?php   echo $brand->url;?>" alt=""/> </a> 
              </div>
              <!-- /Slide -->

             <?php  } ?>
             
            </div>
            <!-- /BxSlider -->
            
          </div>
          <!-- Slider Wrapper -->
        </div>
        <!-- /Container -->
      
      </div>
      
 <!-- Content Block
      ============================================ -->

      <!-- Newsletter Block
      ============================================ -->
      <div class="newsletter-block boxed-section overlay-dark-m cover-2-bg">
      
        <!-- Container -->
        <div class="container">
        <!-- <?php echo form_open("main/subscribe",array('class'=>'sub-form'));?> -->
            <!-- Row -->
            <div class="row grid-10">
              <!-- Col -->
              <div class="col-sm-3 col-md-2">
                <span class="case-c"><?php  echo lang('newsletter_mail_list');?></span>
              </div>
              <!-- Col -->
              
              <!-- Col -->
              <div class="col-sm-6 col-md-8">
                  <input id="myemail" class="form-control" type="text" name="email" placeholder="<?php  echo lang('enter_your_email');?>" />
              </div>
              <!-- Col -->
              
              <!-- Col -->
              <div class="col-sm-3 col-md-2">
                <a id="subscribeme" class="btn btn-block btn-color yellow-bg"><i class="icon-left fa fa-envelope"></i><?php echo lang('subscribe'); ?></a>
              </div>
              <!-- /Col -->
              
            </div>
            <!-- /Row -->
         <!-- </form> -->
        </div>
        <!-- /Container -->
      
      </div>
      <!-- /Newsletter Block
      =================================================== -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
<script>
	var b_url = '<?php echo base_url();?>';
	var s_url = '<?php echo site_url();?>';
$(document).ready(function () {

 	$('#subscribeme').on('click', function() {
	   	var url = s_url+'/main/subscribe';
	   	data = {};
		var obj = $('#myemail'),
				name = obj.attr('name'),
				value = obj.val();
	
	   	data[name] = value;
	  
	  	$.ajax({
	   // see the (*)
		   url: url,
		   type: 'POST',
		   data: data,
		   success: function(response) {
	    		alert(response);
	    		$('#myemail').val("");
	   		}
	  	});
	  	return false; //disable refresh
	});
}); 
</script>
