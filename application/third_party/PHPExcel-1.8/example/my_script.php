<?php

//include PHPExcel library
require_once APPPATH."third_party/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php";
 
 function read_excel_file($path)
 {
    //load Excel template file
	$objTpl = PHPExcel_IOFactory::load($path);
	$objTpl->setActiveSheetIndex(0);  //set first sheet as active
	 
	$objWorksheet=$objTpl->getActiveSheet();
	$highestRow = $objWorksheet->getHighestRow(); 
	$highestColumn = $objWorksheet->getHighestColumn(); 
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 
	
	 for ($row = 1; $row <= $highestRow; ++$row) {
	  for ($col = 0; $col <= $highestColumnIndex; ++$col) {
	    $rows[$row][$col] = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
	  }
	 }
	return $rows;
 }
 
 
function export_file_excel($data,$lang)
 {
 	$path=APPPATH."third_party/PHPExcel-1.8/example/users_template.xls";
 	/** Error reporting */
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);

    //load Excel template file
	$objTpl = PHPExcel_IOFactory::load($path);
	$objTpl->setActiveSheetIndex(0);  //set first sheet as active
	 
	$objWorksheet=$objTpl->getActiveSheet();
	$highestRow = $objWorksheet->getHighestRow(); 
	$highestColumn = $objWorksheet->getHighestColumn(); 
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 
	
	
	 // for ($row = 2; $row <= sizeof($data)+2; ++$row) {
	 //  // for ($col = 0; $col <= $highestColumnIndex; ++$col) {
	 //  //  $objWorksheet->getCellByColumnAndRow($col, $row)->setValue($data[$row -2][$col]);
	 //  // }
	 // 	$objWorksheet->getCellByColumnAndRow(0, $row)->setValue($data[$row -2][$col]);
	 // }

	 $row=2;
	 foreach ($data as $key => $item) {
	 	$objWorksheet->getCellByColumnAndRow(0, $row)->setValue($item->first_name);
	 	$objWorksheet->getCellByColumnAndRow(1, $row)->setValue($item->last_name);
	 	$objWorksheet->getCellByColumnAndRow(2, $row)->setValue($item->email);
	 	$objWorksheet->getCellByColumnAndRow(3, $row)->setValue($item->phone);
	 	$objWorksheet->getCellByColumnAndRow(4, $row)->setValue($item->town_name?$item->town_name:$item->{"name_{$lang}"});
	 	$objWorksheet->getCellByColumnAndRow(5, $row)->setValue($item->{"ed_name_{$lang}"});
	 	$objWorksheet->getCellByColumnAndRow(6, $row)->setValue($item->age);
	 	$row++;
	 }
	 
	 //prepare download
	$filename=mt_rand(1,100000).'.xls'; //just some random filename
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$filename.'"');
	header('Cache-Control: max-age=0');
	 
	$objWriter = PHPExcel_IOFactory::createWriter($objTpl, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)

 	//send it to user, of course you can save it to disk also
	//$objWriter->save(base_url().'assets/uploads/excel_files/'.$filename); 
	// $path=APPPATH.'third_party/PHPExcel-1.8/example/'.$filename;
	// $objWriter->save($path);  //send it to user, of course you can save it to disk also

	//return $path;

	$objWriter->save('php://output');
	//exit; //done.. exiting!
 }

 ?>