<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['upload_button'] = 'Upload files here';
$lang['upload-drop-area'] = 'Drop files here to upload';
$lang['upload-cancel'] = 'Cancel';
$lang['upload-failed'] = 'Failed';

$lang['loading'] = 'Loading, please wait...';
$lang['deleting'] = 'Deleting, please wait...';
$lang['saving_title'] = 'Saving title...';
$lang['saving_video'] = 'Saving video...';
$lang['video_saved'] = 'Video Saved...';
$lang['saving_file'] = 'Saving file...';
$lang['file_saved'] = 'File Saved...';

$lang['list_delete'] = 'Delete';
$lang['list_thumb_upload'] = 'upload thumb';
$lang['alert_delete'] = 'Are you sure that you want to delete this image?';

/* End of file english.php */
/* Location: ./assets/image_crud/languages/english.php */