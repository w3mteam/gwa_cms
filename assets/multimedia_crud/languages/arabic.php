<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['upload_button'] = 'ارفع الملفت هنا';
$lang['upload-drop-area'] = 'القي الملفات هنا من أجل رفعها';
$lang['upload-cancel'] = 'إلغاء الأمر';
$lang['upload-failed'] = 'فشل';

$lang['loading'] = 'جاري التحميل, الرجاء الانتظار';
$lang['deleting'] = 'جاري الحذف, الرجاء الانتظار';
$lang['saving_title'] = 'جاري حفظ العنوان';
$lang['saving_file'] = 'يتم حفظ الملف';
$lang['file_saved'] = 'تم حفظ الملف...';


$lang['list_delete'] = 'حذف';
$lang['alert_delete'] = 'هل أنت متأكد من أنك تريد حذف الملف؟';

/* End of file arabic.php */
/* Location: ./assets/image_crud/languages/arabic.php */