<?php
	//here to set all the required js and css file to include in <head> tag.
	$this->set_js('assets/multimedia_crud/js/jquery-1.8.2.min.js');
	$this->set_js('assets/multimedia_crud/js/fineuploader-3.2.min.js');
	$this->set_js('assets/multimedia_crud/yoxview/yoxview-init.js');
	$this->set_js('assets/multimedia_crud/js/jquery-migrate-1.2.1.min.js');
	$this->set_js('assets/multimedia_crud/js/jquery-ui-1.9.0.custom.min.js');
	
	$this->set_css('assets/multimedia_crud/css/fineuploader.css');
	$this->set_css('assets/multimedia_crud/css/photogallery.css');
?>

<?php // this code to start yoxview ?>
<script type="text/javascript">
    $(document).ready(function(){

        $(".yoxview").yoxview(
        	{ 
            	allowInternalLinks: true,
            	//onOpen: yoxviewOnOpen,
            	onClose: yoxviewOnClose,
            	onSelect: yoxviewOnSelect,
            	<?php if ($IsNeedButton == true):?>
            	skin: "top_menu",
            	<?php endif; ?>
        	}
        );

        function yoxviewOnOpen()
		{
		    // alert('YoxView is now open!');

		    link = $($.yoxview.currentImage.thumbnailImg.context);

		    if(link.data("type") == "audio"){
		    	container = $(link.attr('href'));
			    var div = '<audio controls><source id="audio_control" src="'+container.data("src")+'" type="audio/mp3" />Your browser does not support the audio element.</audio>';
			    container.html(div);
			}else if(link.data("type") == "video"){
		    	container = $(link.attr('href'));
				console.log(container.data("src"));		    	
			    var div = '<video class="video-js vjs-default-skin" controls preload="auto" width="100%" height="100%" data-setup="{}"><source src="'+container.data("src")+'" type="video/mp4"></video>';
			    container.html(div);
			}

		    // console.log($.yoxview.currentImage.thumbnailImg.context);
		}

        function yoxviewOnSelect()
		{
		    $('audio').remove();
		    $('video').remove();

		    yoxviewOnOpen();

		}

        function yoxviewOnClose()
		{
		    // alert('YoxView is now yoxviewOnClose!');

		    link = $($.yoxview.currentImage.thumbnailImg.context);

		    if(link.data("type") == "audio"){
		    	container = $(link.attr('href'));
		    	container.html('');
			}else if(link.data("type") == "video"){
		    	container = $(link.attr('href'));
		    	container.html('');
			}

		    // console.log($.yoxview.currentImage.thumbnailImg.context);
		}
    });
</script>

<script>
$(function(){
	<?php if ( ! $unset_upload) {?>
		createUploader();
	<?php }?>
});
function loadPhotoGallery(){
	$.ajax({
		url: '<?php echo $ajax_list_url?>',
		cache: false,
		dataType: 'text',
		beforeSend: function()
		{
			$('.file-upload-messages-container:first').show();
			$('.file-upload-message').html("<?php echo $this->l('loading');?>");
		},
		complete: function()
		{
			$('.qq-upload-list').hide();
			$('.qq-upload-list').html('');
			$('.file-upload-messages-container').hide();
			$('.file-upload-message').html('');
		},
		success: function(data){
			$('#ajax-list').html(data);
			//loadColorbox();
		}
	});
}

function createUploader() {
	var uploader = new qq.FineUploader({
		element: document.getElementById('fine-uploader'),
		request: {
			 endpoint: '<?php echo $upload_url?>'
		},
		validation: {
			 allowedExtensions: [<?php echo $this->getValidExtForJavaScript(); ?>]
		},		
		callbacks: {
			 onComplete: function(id, fileName, responseJSON) {
				 loadPhotoGallery();
			 }
		},
		debug: true,
		/*template: '<div class="qq-uploader">' +
			'<div class="qq-upload-drop-area"><span><?php echo $this->l("upload-drop-area");?></span></div>' +
			'<div class="qq-upload-button"><?php echo $this->l("upload_button");?></div>' +
			'<ul class="qq-upload-list"></ul>' +
			'</div>',
		fileTemplate: '<li>' +
			'<span class="qq-upload-file"></span>' +
			'<span class="qq-upload-spinner"></span>' +
			'<span class="qq-upload-size"></span>' +
			'<a class="qq-upload-cancel" href="#"><?php echo $this->l("upload-cancel");?></a>' +
			'<span class="qq-upload-failed-text"><?php echo $this->l("upload-failed");?></span>' +
			'</li>',
*/
	});
}

function createThumbUploader(url, id) {
	var uploader = new qq.FineUploader({
		element: document.getElementById('fine-uploader_'+id),
		request: {
			 endpoint: url
		},
		validation: {
			 allowedExtensions: ['jpeg', 'jpg', 'png', 'gif']
		},		
		callbacks: {
			 onComplete: function(id, fileName, responseJSON) {
				 loadPhotoGallery();
			 }
		},
		debug: true,
		/*template: '<div class="qq-uploader">' +
			'<div class="qq-upload-drop-area"><span><?php echo $this->l("upload-drop-area");?></span></div>' +
			'<div class="qq-upload-button"><?php echo $this->l("upload_button");?></div>' +
			'<ul class="qq-upload-list"></ul>' +
			'</div>',
		fileTemplate: '<li>' +
			'<span class="qq-upload-file"></span>' +
			'<span class="qq-upload-spinner"></span>' +
			'<span class="qq-upload-size"></span>' +
			'<a class="qq-upload-cancel" href="#"><?php echo $this->l("upload-cancel");?></a>' +
			'<span class="qq-upload-failed-text"><?php echo $this->l("upload-failed");?></span>' +
			'</li>',
*/
	});
}

<?php if($this-> is_url_form_needed()): //to put needed javascript code for video uploading..?>

function ytVidId(url) {
  var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  return (url.match(p)) ? RegExp.$1 : false;
}

function validFlash(url) {
    var regexp = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
	return (url.match(regexp)) ? true: false ;
}

function saveVideo(data_video_url)
{
    if (<?=$this -> get_url_cond(); ?>) {
        $.ajax({
            url: '<?php echo $upload_file_url; ?>',
            type: 'post',
            data: {file_url: data_video_url},
            beforeSend: function()
            {
                $('#video_alert').removeClass();
                $('#video_alert').addClass('alert alert-info');
                $('#video_alert').fadeIn();
                $('#video_alert').html("<?php echo $this->l('saving_file');?>");
            },
            complete: function()
            {
                $('#video_alert').addClass('alert-success');
                $('#video_alert').html("<?php echo $this->l('file_saved');?>");
                $('#video_alert').delay(2000).fadeOut();
                $('#file_url').val('');
                loadPhotoGallery();
            }
            });
    } else {
        $('#video_alert').removeClass();
        $('#video_alert').addClass('alert alert-danger');
        $('#video_alert').fadeIn();
        $('#video_alert').html("Invalid URL...");
        $('#video_alert').delay(2000).fadeOut();
    }

        
}
<?php endif;?>

function saveTitle(data_id, data_title)
{
	  	$.ajax({
			url: '<?php echo $insert_title_url; ?>',
			type: 'post',
			data: {primary_key: data_id, value: data_title},
			beforeSend: function()
			{
				$('.file-upload-messages-container:first').show();
				$('.file-upload-message').html("<?php echo $this->l('saving_title');?>");
			},
			complete: function()
			{
				$('.file-upload-messages-container').hide();
				$('.file-upload-message').html('');
			}
			});
}

window.onload = createUploader;

</script>

<?php if(!$unset_upload): ?>
<!-- <div id="file-uploader-demo1" class="floatL upload-button-container"></div>
<div class="file-upload-messages-container hidden">
	<div class="message-loading"></div>
	<div class="file-upload-message"></div>
	<div class="clear"></div>
</div>-->

<div id="fine-uploader"></div>

<?php if($this-> is_url_form_needed() && !$IsAjax): //form needed for video url..?>

<div class="" id="video_alert"></div>
<form class="form-inline" role="form" style="margin-bottom: 15px;">
  <div class="form-group">
    <label class="sr-only" for="file_url">Enter URL</label>
    <input type="text" class="form-control" id="file_url" placeholder="Enter URL">
  </div>
  <button type="button" class="btn btn-default" id="save_file_url">Save URL</button>
</form>

<script type='text/javascript'>
    $(function(){
        $('#save_file_url').click(function(){
            saveVideo($("#file_url").val());
        });
    });
</script>
<?php endif;?>
<?php endif;?>

<div class="clear"> </div>
<div id='ajax-list'>
	<?php if(!empty($photos)): ?>
	<script type='text/javascript'>
		$(function(){
			$('.delete-anchor').click(function(){
				if(confirm('<?php echo $this->l("alert_delete");?>'))
				{
					$.ajax({
						url:$(this).attr('href'),
						beforeSend: function()
						{
							$('.file-upload-messages-container:first').show();
							$('.file-upload-message').html("<?php echo $this->l('deleting');?>");
						},
						success: function(){
							loadPhotoGallery();
						}
					});
				}
				return false;
			});
    		$(".photos-crud").sortable({
        		handle: '.move-box',
        		opacity: 0.6,
        		cursor: 'move',
        		revert: true,
        		update: function() {
    				var order = $(this).sortable("serialize");
	    				$.post("<?php echo $ordering_url?>", order, function(theResponse){});
    			}
    		});
    		$('.ic-title-field').keyup(function(e) {
    			if(e.keyCode == 13) {
					var data_id = $(this).attr('data-id');
					var data_title = $(this).val();

					saveTitle(data_id, data_title);
    			}
    		});

    		$('.ic-title-field').bind({
    			  click: function() {
    				$(this).css('resize','both');
    			    $(this).css('overflow','auto');
    			    $(this).animate({height:80},600);
    			  },
    			  blur: function() {
      			    $(this).css('resize','none');
      			  	$(this).css('overflow','hidden');
      			  	$(this).animate({height:20},600);

					var data_id = $(this).attr('data-id');
					var data_title = $(this).val();

					saveTitle(data_id, data_title);
    			  }
    		});
		});
	</script>
	
	<div class="thumbnails yoxview">
	<ul class='photos-crud'>
	<?php foreach($photos as $photo_num => $photo):?>
			<li id="photos_<?php echo $photo->$primary_key; ?>">
				<div class='photo-box'>
					<?php if ($photo->file_type == "audio"):?>

					<a data-type="audio" href="#audio<?=$photo->$primary_key?>" id="audio_<?=$photo->$primary_key?>"><img src='<?php echo $photo->thumbnail_url?>' <?php if (isset($photo->title)) {echo 'title="'.str_replace('"',"&quot;",$photo->title).'" ';}?>  style="margin-bottom: 5px;"/></a>
                	<div style="clear:both"></div>	
					<div id="audio<?=$photo->$primary_key?>" style="display: none; width: 300px; height: 35px; padding-top: 5px; background: inherit; overflow: hidden;" data-src="<?php echo $photo->image_url?>">
					</div>

					<?php elseif($photo->file_type == "video") : ?>

					<a data-type="video" href="#video<?=$photo->$primary_key?>"><img src='<?php echo $photo->thumbnail_url?>' <?php if (isset($photo->title)) {echo 'title="'.str_replace('"',"&quot;",$photo->title).'" ';}?> width='90' height='60' class='basic-image'  style="margin-bottom: 5px;" /></a>
					<div id="video<?=$photo->$primary_key?>" style="width: 640px; height: 400px; background: inherit; overflow: hidden;" data-src="<?php echo $photo->image_url?>">
					</div>
					
					<?php elseif($photo->file_type == "flash-url") : ?>
					
					<a href='<?php echo $photo->image_url?>' target="_blank"  ><img src='<?php echo $photo->thumbnail_url?>' alt="First" <?php if (isset($photo->title)) {echo 'title="'.str_replace('"',"&quot;",$photo->title).'" ';}?> width='90' height='60' class='basic-image' style="margin-bottom: 5px;" /></a>
					
					<?php else: ?>

					<a href='<?php echo $photo->image_url?>'><img src='<?php echo $photo->thumbnail_url?>' alt="First" <?php if (isset($photo->title)) {echo 'title="'.str_replace('"',"&quot;",$photo->title).'" ';}?> width='90' height='60' class='basic-image' style="margin-bottom: 5px;" /></a>
					
					<?php endif; ?>

					<?php if($thumb_field != null):?>
					<div id='fine-uploader_<?=$photo->id?>' class="fine-uploader-thumb"></div>
					<script>
						$(function(){
							createThumbUploader('<?=$photo->thumb_upload_url?>','<?=$photo->id?>');
						});
					</script>
					<?php endif; ?>
					
					<?php if($title_field !== null): ?>
					<textarea class="ic-title-field" data-id="<?php echo $photo->$primary_key; ?>" autocomplete="off" ><?php echo $photo->$title_field; ?></textarea>
					<div class="clear"></div>
					<?php endif; ?>
					
					<?php if($has_priority_field):?> <div class="move-box"></div> <?php endif;?>
					<?php if(!$unset_delete):?>
					<div class='delete-box'>
						<a href='<?php echo $photo->delete_url?>' class='delete-anchor' tabindex="-1"><?php echo $this->l('list_delete');?></a>
					</div>
					<?php endif; ?>
					<div class="clear"></div>
				</div>
			</li>


			<script>
				$(function(){
					$('.fine-uploader-thumb .qq-upload-button div').each(function( index ) {
						console.log($(this).first());
						$(this).first().html("Change Thumbnail");
					});
				});
			</script>
	<?php endforeach; ?>
		</ul>
		</div>	
		<div class='clear'></div>
	<?php endif; ?>
</div>