<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| Files extensions
| -------------------------------------------------------------------
| this is all extensions allowed to upload in multimedia crud.
|
*/
$allowed_extensions = array(
	// allowed extensions for images
	'ext_image' => array('jpeg', 'jpg', 'png', 'gif'),
	
	// allowed extensions for images
	'ext_video' => array('mp4'),
	
	// allowed extensions for images
	'ext_audio' => array('mp3', 'wav'),
	
	// allowed extensions for flash
	'ext_flash' => array('swf'),
	
	// allowed extensions for docs
	'ext_docs' => array('pdf', 'docx', 'zip', 'rar')
);

/* End of file allowed_extentions.php */
/* Location: ./application/config/allowed_extensions.php */