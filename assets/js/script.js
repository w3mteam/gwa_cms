//Print ele4 with custom options
$(".print-btn").on('click', function() {
    $('.printable').print({
        globalStyles : true, // Use Global styles
        mediaPrint : false, // Add link with attrbute media=print
        iframe : false, //Print in a hidden iframe
        noPrintSelector : ".avoid-print", // Don't print this
        append : '',
        prepend : $('.print-logo').html() + $('.print-title').html()
    });
});
