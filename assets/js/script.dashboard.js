$(document).ready(function(){

	$("#add_form_toggle").on('click', function(event) {
		$("#add_form").stop().toggle('slow');
	});

	$("[data-type='toggle']").on('click', function(event) {
		var el = $($(this).data('for'));
		el.stop().toggle('slow');
		el.prop("disabled",!el.prop("disabled"));
	});

	$("[data-type='toggle']").each(function(index, el) {
		if($(el).data('default') == 'hide'){
			$($(el).data('for')).hide();
			$($(el).data('for')).prop("disabled",!$($(el).data('for')).prop("disabled"));
		}
	});

	var inputList = $('input[type="checkbox"][nostyle!="true"], input[type="radio"][nostyle!="true"]');
	for (var i = inputList.length - 1; i >= 0; i--) {
		$(inputList[i]).prettyCheckable();
	}

	$('[data-target="#update-modal"],[data-target="#delete-modal"]').on('click', function(event) {
		event.preventDefault();
		var item_id = $(this).closest('tr').data('id');
		var modal = $($(this).data('target'));

		modal.find('input[name="item_id"]').val(item_id);

		update(item_id);
	});

	$('[data-href]').on('click', function(event) {
		event.preventDefault();
		window.location = $(this).data('href');
	});

	var sort = $('.sortable');
	if(sort.length){
		sort.sortable({
	        handle : '.handle', 
	        update : function () { 
	            var order = 'sort=true&' + sort.sortable('serialize'); 
	            $.post(sort.data('url'), order, function(theResponse){});
	        } 
	    }); 
	}
    

});